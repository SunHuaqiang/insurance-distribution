import Vue from 'vue'
import App from 'src/App'
//----------Vuex------------------------
import Vuex from 'vuex';

import {
  LoadingPlugin,
  XHeader,
  AlertPlugin,
  ConfirmPlugin,
  ToastPlugin
} from 'vux'
Vue.use(Vuex);
Vue.use(XHeader);
Vue.use(LoadingPlugin);
Vue.use(AlertPlugin);
Vue.use(ConfirmPlugin);
Vue.use(ToastPlugin);
//-----------Vuex-----------------------------
import store from 'src/store';
import { RouteUrl, Mutations } from 'src/common/const';
//----------vue-router-----------------------
import routes from 'src/router/routes';
import VueRouter from 'vue-router';
Vue.use(VueRouter)

const router = new VueRouter({
  // mode: 'history',
  routes
})
//-------------router Loding -------------
router.beforeEach(function (to, from, next) {
  store.commit(Mutations.UPDATE_LOADING_STATUS, true)
  //保单查询
  if (to.path == RouteUrl.POLICY_INQUIRY && store.state.user.isLogin) {
    next(RouteUrl.ORDER);
  }

  //订单
  if (to.path == RouteUrl.ORDER) {
    //不是查找状态 也不是登录状态 访问订单页面
    if (!store.state.user.isLogin && !store.state.order.isFind) {
      next(RouteUrl.LOGIN);
    }
  }
  // if (to.path == RouteUrl.ID_CAR_AUTHENTICATION) {
  //   // if(){

  //   // }
  // }

  // 百度统计代码(需配合index.html的script使用,分析数据时链接带#的表示路由后的)
  if (from.name) {
    _hmt.push(['_trackPageview', '/#' + to.fullPath, window.location.origin]);
  } else {
    _hmt.push(['_trackPageview', '/#' + to.fullPath]);
  }
  next()
})

router.afterEach(function (to) {
  store.commit(Mutations.UPDATE_LOADING_STATUS, false)
})
// "vue-layer": "^0.8.4",
//--------------AMap -------------------------
// import AMap from 'vue-amap';     // "vue-amap": "^0.3.1",
// AMap.initAMapApiLoader({
//   key: 'ca3ea8d989236bfdde5ce4bf028fe713',
//   plugin: ['AMap.PlaceSearch', 'AMap.Geolocation']
// });

//-------------FastClick----------------------
import FastClick from 'fastclick'
FastClick.attach(document.body)

Vue.config.productionTip = false;

//--------------axios------------------------
// import share from 'src/common/share';
import axios from 'src/common/http';
//------------prototype-----------------------
import Common from 'src/common/common';
import encode from 'src/common/util/encryption';
import Init from 'src/js/init';
Vue.prototype.$common = Common;
Vue.prototype.$init = Init;
Vue.prototype.$http = axios;
Vue.prototype.$encode = encode;
// Vue.prototype.$share = share;
//------------路由拦截------------------------
router.beforeEach((to, from, next) => {
  next()


  //NProgress.start();
  // if (to.path == '/login') {
  //   sessionStorage.removeItem('user');
  // }
  // let user = JSON.parse(sessionStorage.getItem('user'));
  // if (!user && to.path != '/login') {
  //   next({ path: '/login' })
  // } else {
  //   next()
  // }
})

/* eslint-disable no-new */
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app-box')
