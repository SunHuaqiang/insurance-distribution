import {
    OWNER_TYPE, RouteUrl, Mutations, CAR_INSURED_RELATION, PerIdentifyType, CAR_EntIdentifyType
} from 'src/common/const';
export default {
    getOwnerType() {
        const OWNER_TYPE_LIST = [{
            key: OWNER_TYPE.PERSONAL,
            value: "个人"
        }, {
            key: OWNER_TYPE.GROUP,
            value: "单位"
        }
        ];
        return OWNER_TYPE_LIST;
    },
    getIdTypeEnt() {
        const ID_TYPE_ENT_LIST = [
            {
                key: CAR_EntIdentifyType.ORGANIZATION,
                value: "组织机构代码"
            }, {
                key: CAR_EntIdentifyType.THREE_IN_ONE,
                value: "营业执照"
            }, {
                key: CAR_EntIdentifyType.OTHER,
                value: "其他"
            }
        ];
        return ID_TYPE_ENT_LIST;
    },
    getIdType() {
        const ID_TYPE_LIST = [
            {
                key: PerIdentifyType.ID_CAR,
                value: "身份证"
            }, {
                key: PerIdentifyType.OTHER_ID_CAR,
                value: "永久居留身份证"
            }, {
                key: PerIdentifyType.PASSPORT,
                value: "护照"
            }, {
                key: PerIdentifyType.CERTIFICATE_OFFICERS,
                value: "军官证"
            }, {
                key: PerIdentifyType.OTHER,
                value: "其他"
            }
        ];
        return ID_TYPE_LIST;
    },
    getCarInsuredRelation() {
        const CAR_INSURED_RELATION_List = [{
            key: CAR_INSURED_RELATION.NATURAL_PERSON,
            value: "自然人"
        }, {
            key: CAR_INSURED_RELATION.NO_NATURAL_PERSON,
            value: "非自然人"
        }
        ];
        return CAR_INSURED_RELATION_List;
    },
    getPreminumCommitResCode(_this, val) {
        // debugger
        switch (val) {
            case '0':
                _this.$common.showErrorMsg(_this, "投保中.");
                return false;
                break;
            case '1':
                _this.$common.showErrorMsg(_this, "完成.");
                return true;
                break;
            case '2':
                _this.$common.showErrorMsg(_this, "核保未通过..");
                _this.$common.goUrl(_this, RouteUrl.CAR_OFFER);
                return false;
                break;
            case '5':
                _this.$common.showErrorMsg(_this, "待验证..");
                return false;
                break;
            case '7':
                _this.$common.showErrorMsg(_this, "等待验车..");
                return false;
                break;
            case '8':   //等待支付
                return true;
                break;
            case '81':
                _this.$common.showErrorMsg(_this, "支付成功..");
                return false;
                break;
            case '9':
                let msgComData = {
                    title: "订单提交成功",
                    content: "你的订单正在进行审核<br>工作人员会在一个工作日内与你联系<br>请保持电话通畅",
                    alertBtn: "查看保单",
                    alertBtnUrl: RouteUrl.ORDER,
                    backBtn: "回到首页",
                    backBtnUrl: RouteUrl.INDEX,
                };
                _this.$common.storeCommit(_this, Mutations.IS_SHOW_MSG_COM_DATA, msgComData);
                // _this.$common.showErrorMsg(_this, "订单提交成功,你的订单正在进行人工审核，工作人员会在一个工作日内与您联系"); //等待核保..
                return false;
                break;
            case '10':

                _this.$common.showErrorMsg(_this, "核保中..");
                return false;
                break;
            default:
                return "";
                break;
        }
    },
    //企业类型
    getGroupType() {
        const GROUP_TYPE = [{
            key: "0",
            value: "一般企业",
        }, {
            key: "1",
            value: "军队、武警等机关性质 ",
        },
        ];
        return GROUP_TYPE;
    },
    getAreaSort(value) {
        if (value != "" && value != null) {
            let carType = value.substring(0, 1);
            switch (carType) {
                case '京':
                    return "01";
                    break;
                case '甘':
                    return "03";
                    break;
                case '陕':
                    return "04";
                    break;
                case '宁':
                    return "05";
                    break;
                case '青':
                    return "06";
                    break;
                case '贵':
                    return "07";
                    break;
                case '云':
                    return "08";
                    break;
                case '新':
                    return "09";
                    break;
                case '川':
                    return "10";
                    break;
                case '豫':
                    return "11";
                    break;
                case '湘':
                    return "12";
                    break;
                case '鄂':
                    return "13";
                    break;
                // case '甘'://台湾
                //     return "01";
                //     break;
                case '琼':
                    return "15";
                    break;
                case '桂':
                    return "16";
                    break;
                case '粤':
                    return "17";
                    break;

                case '赣':
                    return "18";
                    break;
                case '苏':
                    return "19";
                    break;
                case '闽':
                    return "20";
                    break;
                case '浙':
                    return "21";
                    break;
                case '皖':
                    return "22";
                    break;
                case '鲁':
                    return "23";
                    break;
                case '黑':
                    return "25";
                    break;

                case '吉':
                    return "26";
                    break;
                case '辽':
                    return "27";
                    break;
                case '蒙':
                    return "28";
                    break;
                case '冀':
                    return "29";
                    break;
                case '晋':
                    return "31";
                    break;
                case '津':
                    return "33";
                    break;
                case '沪':
                    return "34";
                    break;
                case '藏':
                    return "35";
                    break;
                case '渝':
                    return "32";
                    break;
                default:
                    return "";
                    break;
            }
        } else {
            return "";
        }
    }
}




