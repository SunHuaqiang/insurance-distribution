import { PopState, FooterShow, Mutations, SaveName, StrLenth, SelectorTag, RouteUrl } from 'src/common/const';
import { RequestUrl } from 'src/common/url';

export default {
    shareInit(data) {
        let _this = data._this;// _this 中必须包含user saveInsure  shareUuid
        let imgUrl = data.shareData.imgUrl;
        let title = data.shareData.title;
        let desc = data.shareData.desc;
        let url = data.shareData.url;
        let type = 3;//默认归属于商店
        //分类
        if (_this.$common.isNotEmpty(data.shareData.type)) {
            type = data.shareData.type;
        }

        $('#shop-share').share({ sites: ['qzone', 'qq', 'weibo'], url: url, source: "恒华出行宝", title: title, description: desc, image: imgUrl });

        $(document).on('click', '.icon-qzone', function () {
            _this.$common.saveShare("空间", _this, type);
        });
        $(document).on('click', '.icon-qq', function () {
            _this.$common.saveShare("QQ", _this, type);
        });
        $(document).on('click', '.icon-weibo', function () {
            _this.$common.saveShare("微博", _this, type);
        });
    }
}