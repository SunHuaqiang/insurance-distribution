import Weixin from 'src/js/weChat/share/weixin'; //引入微信分享js
import { RequestUrl } from 'src/common/url';
export default {
    /**
     * 初始微信分享用户信息
     * @param {*} data 
     */
    weChatShare(data) {
        //get请求微信域名绑定的服务地址
        let _this = data._this;
        let ua = window.navigator.userAgent.toLowerCase();
        if (ua.match(/MicroMessenger/i) != 'micromessenger' && _this.isWeAuth) {
            return;
        }

        // alert("-----------weChatShare------------");
        // alert(data.shareData.type);
        if (typeof (sessionStorage["WECHAT_SHARE"]) == "undefined" || sessionStorage["WECHAT_SHARE"] == null || sessionStorage["WECHAT_SHARE"] == "false") {
            _this.$http.get(RequestUrl.WECHAT_SHARE).then(function (result) {
                if (!_this.$common.isEmpty(result)) {
                    //分享初始化
                    // alert("-----------http------------");
                    // alert(data.shareData.type);
                    // alert(result);
                    sessionStorage["WECHAT_SHARE"] = result;
                    // alert("query:" + sessionStorage["WECHAT_SHARE"]);
                    // alert("title:" + data.shareData.title, "  desc:" + data.shareData.desc, "  imgUrl:" + data.shareData.imgUrl + "  url:" + data.shareData.url);
                    Weixin.share(result, data.shareData);
                }
            })
        } else {
            // alert(sessionStorage["WECHAT_SHARE"]);
            // alert("title:" + data.shareData.title, "  desc:" + data.shareData.desc, "  imgUrl:" + data.shareData.imgUrl + "  url:" + data.shareData.url);
            Weixin.share(sessionStorage["WECHAT_SHARE"], data.shareData);
        }
    },

    //百度分享插件
    // baiduShare(data) {
    //     // if (IsWechat.verify()) {
    //     //     return;
    //     // }
    //     let _this = data._this;
    //     let shareData = data.shareData;
    //     let title = shareData.title; //标题
    //     let desc = shareData.desc; //描述
    //     let imgUrl = shareData.imgUrl; //图片链接
    //     let url = shareData.url;//分享链接,可带参数
    //     // console.log('share_url  === baidu= ' + url);
    //     // console.log('share_title === baidu= ' + title);
    //     // console.log('share_desc === baidu= ' + desc);
    //     // console.log('share_imgUrl === baidu= ' + imgUrl);
    //     window._bd_share_config = {
    //         "common": {
    //             "bdSnsKey": {},
    //             "bdText": title,//分享内容
    //             "bdMini": "2",//下拉浮层中分享按钮的列数
    //             "bdMiniList": false,
    //             "bdPic": imgUrl,//分享的图片
    //             "bdStyle": "0",
    //             "bdSize": "32",
    //             "bdUrl": url,//分享地址
    //             "bdDesc": desc,//分享摘要
    //         },
    //         "share": {},
    //         //悬浮窗分享设置
    //         // "slide": {},
    //         //图片分享设置
    //         // "image": {
    //         //     "viewList": ["qzone", "tsina", "tqq", "renren", "weixin"],
    //         //     "viewText": "分享到：",
    //         //     "viewSize": "24"
    //         // },
    //         //划词分享设置
    //         // "selectShare": {
    //         //     "bdContainerClass": null,
    //         //     "bdSelectMiniList": ["qzone", "tsina", "tqq", "renren", "weixin"]
    //         // }
    //     };
    //     let srcUrl = '/sticcxb/static/api/js/share.js?v=89860593.js?cdnversion=' + ~(-new Date() / 36e5);
    //     //移除重复异步加载的JavaScript分享文件
    //     $("script[src='" + srcUrl + "']").remove();

    //     const s = document.createElement('script');
    //     s.type = 'text/javascript';
    //     s.src = srcUrl;
    //     document.body.appendChild(s);
    //     //一定要加init初始化，否则始终只有第一次有效
    //     if (window._bd_share_main) {
    //         window._bd_share_main.init();
    //     }
    //     $(document).on('click', '.bds_tsina', function () {
    //         _this.$common.saveShare("百度-新浪", _this, "");
    //     });
    //     $(document).on('click', '.bds_qzone', function () {
    //         _this.$common.saveShare("百度-空间", _this, "");
    //     });
    //     $(document).on('click', '.bds_tqq', function () {
    //         _this.$common.saveShare("百度-腾讯微博", _this, "");
    //     });
    //     $(document).on('click', '.bds_weixin', function () {
    //         _this.$common.saveShare("百度-微信", _this, "");
    //     });
    //     // setTimeout(function () {



    //     //     $(".bds_tsina").on('click', function () {
    //     //         sessionStorage["SHARE_TYPE"] = "bds_qzone";
    //     //         _this.saveShare();
    //     //     });

    //     //     $(".bds_qzone").on('click', function () {
    //     //         console.log("bds_qzone");
    //     //         sessionStorage["SHARE_TYPE"] = "bds_qzone";
    //     //         _this.saveShare();
    //     //     });

    //     //     $(".bds_tqq").on('click', function () {
    //     //         sessionStorage["SHARE_TYPE"] = "bds_qzone";
    //     //         _this.saveShare();
    //     //     });

    //     //     $(".bds_weixin").on('click', function () {
    //     //         sessionStorage["SHARE_TYPE"] = "bds_qzone";
    //     //         _this.saveShare();
    //     //     });

    //     // }, 100);

    // }
}