import { Mutations, SaveName, SelectorTag, RouteUrl } from 'src/common/const'
import { RequestUrl } from "src/common/url";
import DateUtil from 'src/common/util/dateUtil';
import Config from "src/common/config/shoConfig";
import { debug } from 'util';
export default {
    state: {
        shopHeaderState: {
            isMana: false,
            isShowDetl: '0',
            isShowHot: '0',
            listType: '0',
            acDetl: 0,
            acHot: 0,
            shopName: "",
            shopIntroduct: "",
            header: "",
            baseType: ""

        },
        //-----------performanceStatistics------------------
        detailsIsEnd: false,
        performanceData: {},
        displayDetail: [],
        detalisIsQuery: false,

        monthIsEnd: false,
        monthData: {},
        displayMonthDetail: [],
        monthIsQuery: false,

        displayYear: [],
        number: ['', '一', "二", "三", "四", "五", "六", "七", "八", "九", "十", "十一", "十二"],

        //-----------------performanceDetails--------------

        curShoPerDetail: [],
        isDetailQuery: false, //是否在进行请求，一直刷新操作  从而重复请求
        isPerDeEnd: false,
        perDetailData: {},
        perDetailType: "all",

        //----------shopManage------------------------------
        shopPro: {},
        // isQuery: false,
        saveShowManageData: {},

        //----------软文------------------------------
        curShowSoftPaperList: [],//当前选中类型的数据列表
        softPaperIsEnd: false,
        softPaperIsQuery: false, //是否在进行请求，一直刷新操作  从而重复请求
        dtoData: {},//参数和存储的全部数据
        selectPaperType: Config.getSoftPaperType(0).key,//当前选中的类型标识,默认为all=全部
        //----------海报------------------------------
        curShowPosterList: [],//当前选中类型的数据列表
        posterIsEnd: false,
        posterIsQuery: false, //是否在进行请求，一直刷新操作  从而重复请求
        dtoDataPoster: {},//参数和存储的全部数据
        selectPosterType: Config.getPosterType(0).key,//当前选中的类型标识,默认为all=全部
        //----------常驻地区列表------------------------------
        hotAreaList: [],

        saveShopInfoMsg: "",

        isCurPerDetail: false,
        curPerDetail: [],

        curShowMsg: "",

        addPro: [],
        npPro: [],

        isGetMonth: false,
        MonthCountIsQuery: {},
        MonthCount: {},

        promotionFee: [],
        curPromotionFee: {},


        qrcodeList: [],
        curQrcode: "",

        channelPro: false,
    },
    mutations: {
        [Mutations.GET_QRCODE](state, data) {
            let _this = data._this;
            if (typeof (state.qrcodeList[data.query.productCName + data.query.isGetHeader]) == "undefined") {
                _this.$common.storeCommit(_this, Mutations.UPDATE_LOADING_STATUS, true);//loading
                _this.$http.post(RequestUrl.SHARE_ZXING_QR_CODE, data.query).then(function (res) {
                    _this.$common.storeCommit(_this, Mutations.UPDATE_LOADING_STATUS, false);//loading
                    if (res.success) {
                        state.qrcodeList[data.query.productCName + data.query.isGetHeader] = res.result;
                        state.curQrcode = res.result;
                    }
                });
            } else {
                state.curQrcode = state.qrcodeList[data.query.productCName + data.query.isGetHeader];
            }

        },
        [Mutations.GET_PROMOTION_FEE](state, data) {
            let _this = data._this;
            let userCode = data.userCode;
            if (typeof (state.promotionFee[userCode]) == "undefined") {
                let branchCode = "";

                if (_this.$common.isNotEmpty(_this.user.userDto.agrementNo) && _this.$common.isNotEmpty(_this.user.userDto.branchCode)) {
                    branchCode = _this.user.userDto.branchCode;
                } else if (_this.$common.isNotEmpty(_this.user.userDto.areaCode)) {
                    branchCode = _this.user.userDto.areaCode;
                }

                if (_this.$common.isEmpty(branchCode)) {
                    branchCode = _this.hotArea.areaCode;
                }

                let query = {
                    userCode: userCode,
                    branchCode: branchCode
                }
                //本地token
                let localToken = localStorage[SaveName.JWT_TOKEN_NAME];
                $.ajax({
                    type: "POST",
                    url: RequestUrl.SHOP_RATE,
                    data: query,
                    async: false,
                    headers: {
                        "authorization": localToken,
                        // "Accept": "application/json; charset=utf-8",
                        // "Content-Type": "application/x-www-form-urlencoded",
                    },
                    success: function (res, status, xhr) {
                        _this.$common.reJwtVal(res);
                        state.promotionFee[userCode] = res.result;
                        state.curPromotionFee = res.result;;
                    }
                });
                // _this.$http.post(RequestUrl.SHOP_RATE, query).then(function (res) {
                //     state.promotionFee[userCode] = res.result;
                //     state.curPromotionFee = res.result;;
                // });
            } else {
                state.curPromotionFee = state.promotionFee[userCode]
            }

        },
        [Mutations.PER_MONTH_COUNT](state, data) {
            let _this = data._this;
            let userCode = data.userCode;
            let userCodeBs = "henghua";
            if (_this.$common.isNotEmpty(data.userCodeBs)) {
                userCodeBs = data.userCodeBs;
            }
            let query = {
                userCode: userCode,
                userCodeBs: userCodeBs,
                TYPE: "FORM"
            }
            if (typeof (state.MonthCountIsQuery[userCode]) == "undefined") {
                state.MonthCountIsQuery[userCode] = true;
                _this.$http.post(RequestUrl.PERFORMANCE_MONTH, query).then(function (res) {
                    for (let i = 0; i < res.result.length; i++) {
                        state.MonthCount[userCode + res.result[i].days] = res.result[i].amount;
                    }
                });
            }
        },
        [Mutations.SHOP_INFO_SAVE](state, data) {
            let type = data.type;
            let _this = data._this;
            if (type == 'save') {
                let shopName = data.shopName;
                let shopIntroduct = data.introduct;
                let userCode = data.userCode;
                let queryData = {
                    userCode: userCode,
                    shopName: shopName,
                    shopIntroduct: shopIntroduct,
                    TYPE: "FORM"
                }
                if (shopName == "") {
                    state.saveShopInfoMsg = "店铺名字不能为空";
                    return;
                }
                if (shopName.length < 3) {
                    state.saveShopInfoMsg = "店铺名字过短";
                    return;
                }
                if (state.shopHeaderState.shopName == shopName && state.shopHeaderState.shopIntroduct == shopIntroduct) {
                    state.saveShopInfoMsg = "内容没有更改!";
                    return;
                }
                _this.$http.post(RequestUrl.SAVE_SHOP_NAME, queryData).then(function (res) {
                    if (res.success) {
                        state.shopHeaderState.shopName = shopName;
                        state.shopHeaderState.shopIntroduct = shopIntroduct;
                        state.saveShopInfoMsg = "修改成功!";
                        _this.shopEdit = false;
                    } else {
                        state.saveShopInfoMsg = res.result;
                    }
                });
            } else {
                state.saveShopInfoMsg = "";
            }
        },
        [Mutations.LIST_TYPE](state, data) {
            state.shopHeaderState.listType = data;
        },
        [Mutations.SET_IS_SHOW_DETL](state, data) {
            state.shopHeaderState.isShowDetl = data;
        },
        [Mutations.SET_IS_SHOW_HOT](state, data) {
            state.shopHeaderState.isShowHot = data;
        },
        [Mutations.SET_SHOP_HEADER_STATE](state, data) {
            let _this = data._this;
            let userCode = data.userCode;
            let type = data.type;
            let isMana = data.isMana;
            let query = {
                userCode: userCode,
                TYPE: "FORM",
                type: type
            }
            _this.$http.post(RequestUrl.FIND_SHOP_INFO, query).then(function (res) {
                state.shopHeaderState.isMana = isMana;

                let shopType = res.result.shopState + "";
                // if (isMana) {
                //     // state.shopHeaderState.isShowDetl = '1';
                //     // state.shopHeaderState.isShowHot = '1';
                // } 
                // if (isMana) {
                // let shopType = res.result.shopState + "";
                state.shopHeaderState.baseType = shopType;
                if (_this.$common.isNotEmpty(shopType[0])) {
                    state.shopHeaderState.isShowDetl = shopType[0];
                } else {
                    state.shopHeaderState.isShowDetl = '0';
                }
                if (_this.$common.isNotEmpty(shopType[1])) {
                    state.shopHeaderState.isShowHot = shopType[1];
                } else {
                    state.shopHeaderState.isShowHot = '0';
                }
                let listType = shopType[2];
                if (_this.$common.isNotEmpty(listType)) {
                    state.shopHeaderState.listType = listType;
                } else {
                    state.shopHeaderState.listType = '0';
                }
                // listType
                // let test = shopType[3];
                // debugger
                // }

                state.shopHeaderState.acDetl = res.result.volume;
                state.shopHeaderState.acHot = res.result.popularity;
                state.shopHeaderState.shopName = res.result.shopName;
                state.shopHeaderState.header = res.result.header;
                if (_this.$common.isEmpty(state.shopHeaderState.header)) {
                    state.shopHeaderState.header = RequestUrl.WEB_PATH + "sticcxb/upload/assets/x3/logo.jpg";
                }
                if (!_this.$common.isEmpty(res.result.shopIntroduct)) {
                    state.shopHeaderState.shopIntroduct = res.result.shopIntroduct;
                } else {
                    if (isMana) {
                        state.shopHeaderState.shopIntroduct = "我的微门店";
                    } else {
                        state.shopHeaderState.shopIntroduct = "";
                    }
                }
                _this.$common.storeCommit(_this, Mutations.IS_QUERY_HEADER, true);
                // if (data.isShopIndex) {
                //     _this.initShare();
                // }

            });
        },
        //获取常驻地区列表
        [Mutations.GET_ALL_HOT_AREA](state, _this) {
            state.hotAreaList = [];//置空
            //在数组开头添加一个或多个元素，防止IOS系统在选择时第一项选择无效的情况,key在vux的selector组件中表示选项值,value表示显示的值
            state.hotAreaList.unshift({ key: SelectorTag.keyNull, value: SelectorTag.valNull });
            //查询所有常驻地区信息
            _this.$http.post(RequestUrl.FIND_ALL_HOT_AREA).then(function (res) {
                if (res.state == 0 && !_this.$common.isEmpty(res.result)) {
                    for (let i = 0; i < res.result.length; i++) {
                        //添加选项到selector中
                        state.hotAreaList.push({ key: res.result[i], value: res.result[i].areaName });
                        //当前页面的热门地区列表
                        _this.hotareas = state.hotAreaList;
                    }
                }
            });
        },
        //-----------performanceStatistics------------------
        [Mutations.GET_DETAILS_DAY](state, data) {
            let _this = data._this;
            let seleState = data.seleState;
            let operation = data.operation;
            let userDto = data.userDto;
            seleState = userDto.userCode;
            let registeDate = DateUtil.getDateByStrYmdOrYmdhms(userDto.registeDate).getTime();
            let baseQueryData = {
                year: DateUtil.getNumNowYMD("Y"),
                month: parseInt(DateUtil.getNumNowYMD("M")) + 1,
            }
            if (typeof (state.performanceData[seleState]) == "undefined") {
                let baseDataInfo = {
                    isisEnd: false,
                    queryData: baseQueryData,
                    perList: [],
                }
                state.detailsIsEnd = false;
                state.performanceData[seleState] = baseDataInfo;
            }
            if (operation == "refresh") {
                state.detailsIsEnd = false;
                state.displayDetail = [];
                state.performanceData[seleState].queryData = baseQueryData;
                state.performanceData[seleState].isisEnd = false;
                state.performanceData[seleState].perList = [];
                state.performanceData[seleState].queryData.month--;
            }
            if (!state.detalisIsQuery && !state.performanceData[seleState].isisEnd) {
                // state.detailsIsEnd = true;
                state.detalisIsQuery = true;
                if (operation == "infinite") {
                    if (state.performanceData[seleState].queryData.month == "01") {
                        state.performanceData[seleState].queryData.month = 12;
                        state.performanceData[seleState].queryData.year--;
                    } else {
                        state.performanceData[seleState].queryData.month--;
                    }
                }
                if (state.performanceData[seleState].queryData.month < 10) {
                    state.performanceData[seleState].queryData.month = "0" + state.performanceData[seleState].queryData.month;
                }
                let userCodeBs = "henghua";
                if (_this.$common.isNotEmpty(userDto.userCodeBs)) {
                    userCodeBs = userDto.userCodeBs;
                }
                let query = {
                    TYPE: "FORM",
                    userCode: userDto.userCode,
                    queryType: "D",
                    queryParam: state.performanceData[seleState].queryData.year + "" + state.performanceData[seleState].queryData.month,
                    userCodeBs: userCodeBs
                }
                // if(){

                // }
                if (typeof (state.MonthCount[userDto.userCode + query.queryParam]) != 'undefined') {
                    _this.$http.post(RequestUrl.PERFORMANCE, query)
                        .then(function (res) {
                            state.detalisIsQuery = false;
                            // state.detailsIsEnd = false;
                            let saveDate = {
                                year: state.performanceData[seleState].queryData.year,
                                month: state.performanceData[seleState].queryData.month,
                                day: []
                            }
                            if (res.result.length > 0) {
                                let staList = {};
                                for (let i = 0; i < res.result.length; i++) {
                                    staList[res.result[i].days] = "¥" + res.result[i].amount;
                                }

                                let week = DateUtil.getWeekDayForShop(saveDate.year + "/" + saveDate.month + "/1");
                                //填补空白部分
                                for (let i = 0; i < week; i++) {
                                    let dayDate = {
                                        day: "",
                                        sumPrice: ""
                                    }
                                    saveDate.day.push(dayDate);
                                }
                                let monthDay = DateUtil.getMonthDay(saveDate.year, saveDate.month);
                                for (let i = 0; i < monthDay; i++) {
                                    let price = "";
                                    let index = i + 1;
                                    if (index < 10) {
                                        index = "0" + index;
                                    }
                                    if (typeof (staList[index]) != "undefined") {
                                        price = staList[index];
                                    }
                                    let dataDate = {
                                        day: i + 1,
                                        sumPrice: price
                                    }
                                    saveDate.day.push(dataDate);
                                }
                                state.performanceData[seleState].perList.push(saveDate);
                                state.displayDetail = state.performanceData[seleState].perList.slice();
                            }



                            let timestamp = new Date(saveDate.year + "/" + saveDate.month + "/1").getTime();

                            if (registeDate > timestamp) {
                                state.detailsIsEnd = true;
                                state.performanceData[seleState].isisEnd = state.detailsIsEnd
                            }
                            // state.detailsIsEnd = state.performanceData[seleState].isisEnd;

                            // if (saveDate.month == 1) {
                            //     state.detailsIsEnd = true;
                            // }
                        })
                } else {
                    state.detalisIsQuery = false;
                    let saveDate = {
                        year: state.performanceData[seleState].queryData.year,
                        month: state.performanceData[seleState].queryData.month,
                        day: []
                    }
                    let timestamp = new Date(saveDate.year + "/" + saveDate.month + "/1").getTime();
                    if (registeDate > timestamp) {
                        state.detailsIsEnd = true;
                        state.performanceData[seleState].isisEnd = state.detailsIsEnd
                    }
                }


                // let saveDate = {
                //     year: state.performanceData[seleState].queryData.year,
                //     month: state.performanceData[seleState].queryData.month,
                //     day: []
                // }

                // let week = DateUtil.getWeekDayForShop(saveDate.year + "-" + saveDate.month + "-1");
                // //填补空白部分
                // for (let i = 0; i < week; i++) {
                //     let dayDate = {
                //         day: "",
                //         sumPrice: ""
                //     }
                //     saveDate.day.push(dayDate);
                // }
                // let monthDay = DateUtil.getMonthDay(saveDate.year, saveDate.month);
                // for (let i = 0; i < monthDay; i++) {
                //     let dataDate = {
                //         day: i + 1,
                //         sumPrice: i + "00"
                //     }
                //     saveDate.day.push(dataDate);
                // }
                // state.performanceData[seleState].perList.push(saveDate);

                // state.detailsIsEnd = state.performanceData[seleState].isisEnd;
                // state.displayDetail = state.performanceData[seleState].perList.slice();
                // if (saveDate.month == 1) {
                //     state.detailsIsEnd = true;
                // }


            } else {
                state.detailsIsEnd = state.performanceData[seleState].isisEnd;
                state.displayDetail = state.performanceData[seleState].perList.slice();
            }

        },
        [Mutations.GET_NEW_MONTH](state, data) {
            let userDto = data.userDto;
            let reTime = userDto.registeDate;
            let yearList = DateUtil.getReqYear(reTime);
            let _this = data._this;
            let userCodeBs = "henghua";
            if (_this.$common.isNotEmpty(userDto.userCodeBs)) {
                userCodeBs = userDto.userCodeBs;
            }
            // let userCode = data.userCode;
            let query = {
                TYPE: "FORM",
                userCode: userDto.userCode,
                queryType: "M",
                userCodeBs: userCodeBs,
                queryParam: yearList
            }
            _this.$http.post(RequestUrl.PERFORMANCE, query)
                .then(function (res) {
                    let showData = [];
                    for (var key in res.result) {
                        if (res.result[key].length > 0) {
                            let staList = {};
                            for (let i = 0; i < res.result[key].length; i++) {
                                staList[res.result[key][i].days] = "¥" + res.result[key][i].amount;
                            }
                            let saveDate = {
                                year: key,
                                // month: state.monthData[seleState].queryData.month,
                                day: []
                            }
                            for (let i = 1; i < 13; i++) {
                                let index = i;
                                let price = "";
                                if (i < 10) {
                                    index = "0" + i;
                                }
                                if (typeof (staList[index]) != "undefined") {
                                    price = staList[index];
                                }
                                let setNumber = i + "";
                                if (i < 10) {
                                    setNumber = "0" + i;
                                }
                                let dataDate = {
                                    day: state.number[i],
                                    sumPrice: price,
                                    dayNumber: setNumber
                                }
                                saveDate.day.push(dataDate);
                            }
                            showData.push(saveDate);
                        }
                    }
                    state.displayMonthDetail = showData;
                    state.isGetMonth = true;
                })

        },
        // [Mutations.GET_DETAILS_MONTH](state, data) {
        //     let seleState = data.seleState;
        //     let operation = data.operation;
        //     let userDto = data.userDto;
        //     seleState = userDto.userCode;
        //     let _this = data._this;
        //     let registeDate = userDto.registeDate;
        //     let baseQueryData = {
        //         year: DateUtil.getNumNowYMD("Y") + 1,
        //         // month: DateUtil.getNumNowYMD("M") + 1,
        //     }
        //     if (typeof (state.monthData[seleState]) == "undefined") {
        //         let baseDataInfo = {
        //             isisEnd: false,
        //             queryData: baseQueryData,
        //             perList: [],
        //         }
        //         state.displayMonthDetail = [];
        //         state.monthData[seleState] = baseDataInfo;
        //     }
        //     if (operation == "refresh") {
        //         state.monthIsEnd = false;
        //         state.monthData[seleState].queryData = baseQueryData;
        //         state.monthData[seleState].isisEnd = false;
        //         state.monthData[seleState].perList = [];
        //         state.monthData[seleState].queryData.year--;
        //     }
        //     if (!state.monthIsQuery && !state.monthData[seleState].isisEnd) {
        //         state.monthIsQuery = true;
        //         if (operation == "infinite") {
        //             state.monthData[seleState].queryData.year--;
        //             // if (state.monthData[seleState].queryData.month == "1") {
        //             //     // state.monthData[seleState].queryData.month = 12;
        //             //     state.monthData[seleState].queryData.year--;
        //             // } else {
        //             //     // state.monthData[seleState].queryData.month--;
        //             // }
        //         }

        //         let query = {
        //             TYPE: "FORM",
        //             userCode: userDto.userCode,
        //             queryType: "M",
        //             queryParam: state.monthData[seleState].queryData.year
        //         }

        //         _this.$http.post(RequestUrl.PERFORMANCE, query)
        //             .then(function (res) {
        //                 let saveDate = {
        //                     year: state.monthData[seleState].queryData.year,
        //                     day: []
        //                 }
        //                 state.monthIsQuery = false;
        //                 if (res.result.length > 0) {
        //                     let staList = {};
        //                     for (let i = 0; i < res.result.length; i++) {
        //                         staList[res.result[i].days] = "¥" + res.result[i].amount;
        //                     }
        //                     let saveDate = {
        //                         year: state.monthData[seleState].queryData.year,
        //                         // month: state.monthData[seleState].queryData.month,
        //                         day: []
        //                     }
        //                     for (let i = 1; i < 13; i++) {
        //                         let index = i;
        //                         let price = "";
        //                         if (i < 10) {
        //                             index = "0" + i;
        //                         }
        //                         if (typeof (staList[index]) != "undefined") {
        //                             price = staList[index];
        //                         }
        //                         let setNumber = i + "";
        //                         if (i < 10) {
        //                             setNumber = "0" + i;
        //                         }
        //                         let dataDate = {
        //                             day: state.number[i],
        //                             sumPrice: price,
        //                             dayNumber: setNumber
        //                         }
        //                         saveDate.day.push(dataDate);
        //                     }
        //                     state.monthData[seleState].perList.push(saveDate);
        //                     state.displayMonthDetail = state.monthData[seleState].perList.slice();

        //                 }

        //                 let timestamp = new Date(saveDate.year + "/1/1").getTime();
        //                 if (registeDate > timestamp) {
        //                     state.monthIsEnd = true;
        //                     state.monthData[seleState].isisEnd = state.monthIsEnd;
        //                 }


        //             })


        //         // let saveDate = {
        //         //     year: state.monthData[seleState].queryData.year,
        //         //     // month: state.monthData[seleState].queryData.month,
        //         //     day: []
        //         // }
        //         // for (let i = 0; i < 12; i++) {
        //         //     let dataDate = {
        //         //         day: state.number[i],
        //         //         sumPrice: i + "00"
        //         //     }
        //         //     saveDate.day.push(dataDate);
        //         // }
        //         // state.monthData[seleState].perList.push(saveDate);

        //         // state.monthIsEnd = state.monthData[seleState].isisEnd;
        //         // state.displayMonthDetail = state.monthData[seleState].perList.slice();
        //         // if (saveDate.month == 1) {
        //         //     state.monthIsEnd = true;
        //         // }


        //     } else {
        //         state.monthIsEnd = state.monthData[seleState].isisEnd;
        //         state.displayMonthDetail = state.monthData[seleState].perList.slice();
        //     }

        // },
        [Mutations.GET_DETAILS_YEAR](state, data) {
            let userDto = data.userDto;
            let _this = data._this;
            let userCodeBs = "henghua";
            if (_this.$common.isNotEmpty(userDto.userCodeBs)) {
                userCodeBs = userDto.userCodeBs;
            }
            let query = {
                TYPE: "FORM",
                userCode: userDto.userCode,
                userCodeBs: userCodeBs,
                queryType: "Y",
                queryParam: ""
            }

            _this.$http.post(RequestUrl.PERFORMANCE, query)
                .then(function (res) {
                    state.displayYear = res.result;
                    // _this.$common.goUrl(_this, RouteUrl.PERFORMANCE_DETAILS);
                })
        },
        [Mutations.YMD_PERFORMANCE_DETAILS](state, perDetailData) {
            if (perDetailData.type == "goUrl") {
                state.isCurPerDetail = false;
            } else {
                let _this = perDetailData._this;
                state.curShowMsg = perDetailData.curShowMsg;
                state.isCurPerDetail = true;
                _this.$http.post(RequestUrl.PERFORMANCE_DETAIL, perDetailData.queryData)
                    .then(function (res) {
                        let showCurData = {};
                        if (perDetailData.queryData.queryType == "d") {
                            let showOrder = {
                                text: perDetailData.curShowMsg,
                                orderList: res.result
                            }
                            showCurData[perDetailData.curShowMsg] = showOrder;
                            // state.curPerDetail = res.result;
                            // showCurData.push(showOrder);
                        } else if (perDetailData.queryData.queryType == "m") {
                            for (let i = 0; i < res.result.length; i++) {
                                let text = perDetailData.curShowMsg + DateUtil.getDataType(res.result[i].createTime, 'd') + "日";
                                if (typeof (showCurData[text]) == 'undefined') {
                                    let showOrder = {
                                        text: text,
                                        orderList: []
                                    }
                                    showCurData[text] = showOrder;
                                }
                                showCurData[text].orderList.push(res.result[i]);
                            }
                        } else if (perDetailData.queryData.queryType == "y") {
                            // for (let i = 1; i < 13; i++) {
                            //     let month = i;
                            //     if(month){

                            //     }
                            //     let text = perDetailData.curShowMsg + "" + i + "月";
                            //     let showOrder = {
                            //         text: text,
                            //         orderList: []
                            //     }
                            //     showCurData[text] = showOrder;
                            // }
                            // for (let i = 0; i < res.result.length; i++) {
                            //     let month = DateUtil.getDataType(res.result[i].createTime, 'm');
                            //     let textAl = perDetailData.curShowMsg + month + "月";
                            //     showCurData[textAl].orderList.push(res.result[i]);
                            // }

                            for (let i = 0; i < res.result.length; i++) {
                                let text = perDetailData.curShowMsg + DateUtil.getDataType(res.result[i].createTime, 'm') + "月";
                                if (typeof (showCurData[text]) == 'undefined') {
                                    let showOrder = {
                                        text: text,
                                        orderList: []
                                    }
                                    showCurData[text] = showOrder;
                                }
                                showCurData[text].orderList.push(res.result[i]);
                            }

                        }
                        state.curPerDetail = showCurData;
                        // state.curPerDetail = res.result;
                        _this.$common.goUrl(_this, RouteUrl.PERFORMANCE_DETAILS);

                    })
            }




        },
        [Mutations.PERFORMANCE_DETAILS_PER_DE_END](state, isPerDeEnd) {
            state.isPerDeEnd = isPerDeEnd;
        },
        //-----------------performanceDetails--------------
        [Mutations.PERFORMANCE_DETAILS](state, perDetailData) {
            // debugger
            // let isShop = perDetailData.isShop;
            let _this = perDetailData._this;
            let operation = perDetailData.operation;
            let riskCode = perDetailData.riskCode;
            let type = perDetailData.type;
            let registeDate = perDetailData.registeDate;
            // if (riskCode == "2726") {
            // }
            state.perDetailType = riskCode;
            let baseQueryData = {
                // TYPE: 'FORM',
                // type: riskCode,
                year: DateUtil.getNumNowYMD("Y"),
                month: parseInt(DateUtil.getNumNowYMD("M")) + 1,
                // userCode: perDetailData.userCode,
            }
            // let month = parseInt(baseQueryData.month);

            //如果没有定义这个险种信息 就进行创建
            //   alert(typeof (state.perDetailData[type+riskCode]));
            //   let test = (typeof (state.perDetailData[type+riskCode]) == 'undefine');
            if (typeof (state.perDetailData[type + riskCode]) == 'undefined') {
                let baseDataInfo = {
                    isisPerDeEnd: false,
                    queryData: baseQueryData,
                    insurance: []
                }
                state.curShoPerDetail = [];
                state.isPerDeEnd = false;
                state.perDetailData[type + riskCode] = baseDataInfo;
            }

            if (operation == "refresh") {
                //上拉刷新操作  赋初始值
                state.isPerDeEnd = false;
                state.perDetailData[type + riskCode].queryData = baseQueryData;
                state.perDetailData[type + riskCode].isisPerDeEnd = false;
                state.perDetailData[type + riskCode].insurance = [];
                state.perDetailData[type + riskCode].queryData.month--;
                state.curShoPerDetail = [];
            }
            if (!state.isDetailQuery && !state.perDetailData[type + riskCode].isisPerDeEnd) {
                state.isDetailQuery = true;
                if (operation == "infinite") {
                    //下拉刷新操作 页码加一
                    if (state.perDetailData[type + riskCode].queryData.month == "01") {
                        state.perDetailData[type + riskCode].queryData.month = 12;
                        state.perDetailData[type + riskCode].queryData.year--;
                    } else {
                        state.perDetailData[type + riskCode].queryData.month--;
                    }

                }
                // let url = "";
                // if (type) {
                //     url = RequestUrl.SHOP_PRODUCT_LIST;
                // } else {
                //     url = RequestUrl.PRODUCT_LIST;
                // }
                //开始进行请求操作
                if (state.perDetailData[type + riskCode].queryData.month < 10) {
                    state.perDetailData[type + riskCode].queryData.month = "0" + state.perDetailData[type + riskCode].queryData.month;
                }
                let query = {
                    TYPE: "FORM",
                    userCode: type,
                    userCodeBs: perDetailData.userCodeBs,
                    riskCode: riskCode,
                    queryParam: state.perDetailData[type + riskCode].queryData.year + "" + state.perDetailData[type + riskCode].queryData.month
                }
                state.isDetailQuery = true;
                if (typeof (state.MonthCount[type + query.queryParam]) != 'undefined') {
                    _this.$http.post(RequestUrl.PERFORMANCE_LIST, query)
                        .then(function (res) {
                            state.isDetailQuery = false;
                            let saveDate = {
                                year: state.perDetailData[type + riskCode].queryData.year,
                                month: state.perDetailData[type + riskCode].queryData.month,
                                showData: "",
                                data: []
                            }
                            if (res.result.length > 0) {
                                saveDate.data = res.result;
                                if (saveDate.year == baseQueryData.year && saveDate.month == baseQueryData.month) {
                                    saveDate.showData = "本月";
                                } else {
                                    saveDate.showData = saveDate.year + "年" + saveDate.month + "月";
                                }
                                let insure = res.result;
                                // for (let i = 0; i < insure.length; i++) {
                                //     saveDate.data.push(insure[i]);
                                //     // state.perDetailData[type + riskCode].insurance.data.push(insure[i]);
                                // }
                                state.perDetailData[type + riskCode].insurance.push(saveDate);
                                // state.perDetailData[type + riskCode].insurance.year = saveDate.year;
                                // state.perDetailData[type + riskCode].insurance.month = saveDate.month;
                            }
                            state.curShoPerDetail = state.perDetailData[type + riskCode].insurance;
                            let timestamp = new Date(saveDate.year + "/" + saveDate.month + "/1").getTime();
                            state.isPerDeEnd = false;
                            if (registeDate > timestamp) {
                                state.isPerDeEnd = true;
                                state.perDetailData[type + riskCode].isisPerDeEnd = true;
                            }


                            // let insure
                            // if (type) {
                            //     insure = res.result;
                            // } else {
                            //     insure = res.result.content;
                            // }
                            // for (let i = 0; i < insure.length; i++) {
                            //     let parm = {
                            //         type: 'add',
                            //         id: insure[i].id,
                            //         product: insure[i]
                            //     }
                            //     _this.$common.storeCommit(_this, Mutations.SET_PRODUCT_LIST, parm);
                            //     state.perDetailData[type + riskCode].insurance.push(insure[i]);
                            // }
                            // if (insure.length < state.perDetailData[type + riskCode].queryData.pageSize) {
                            //     state.perDetailData[type + riskCode].isisPerDeEnd = true;
                            //     state.isPerDeEnd = true;
                            // }
                            // state.curShoPerDetail = state.perDetailData[type + riskCode].insurance.slice();

                            // state.isDetailQuery = false;

                            // perDetailData.done(state.isPerDeEnd);
                        })
                } else {
                    state.isDetailQuery = false;
                    let saveDate = {
                        year: state.perDetailData[type + riskCode].queryData.year,
                        month: state.perDetailData[type + riskCode].queryData.month,
                        showData: "",
                        data: []
                    }
                    let timestamp = new Date(saveDate.year + "/" + saveDate.month + "/1").getTime();
                    state.isPerDeEnd = false;
                    if (registeDate > timestamp) {
                        state.isPerDeEnd = true;
                        state.perDetailData[type + riskCode].isisPerDeEnd = true;
                    }
                    state.curShoPerDetail = state.perDetailData[type + riskCode].insurance;
                    // state.curShoPerDetail.push({ amount: "20", createTime: 1501556814000, name: "", planName: "", productName: "", riskCode: "3105", riskName: "" })
                }
            } else {
                state.isPerDeEnd = state.perDetailData[type + riskCode].isisPerDeEnd;
                state.curShoPerDetail = state.perDetailData[type + riskCode].insurance.slice();
                // perDetailData.done(state.isPerDeEnd);
            }

        },
        [Mutations.SET_ISPERDE_END](state, isPerDeEnd) {
            state.isPerDeEnd = isPerDeEnd;
            state.curShoPerDetail = [];
        },
        //软文
        [Mutations.SHOP_SOFT_PAPER_LIST](state, dtoData) {
            let _this = dtoData._this;
            let branchCode = dtoData.branchCode;
            let serchKey = dtoData.serchKey;
            let operation = dtoData.operation;//Scroller滑动事件
            let selectPaperType = dtoData.selectPaperType;//选择的软文类型
            let type = selectPaperType;//
            //更新当前选择的类型
            state.selectPaperType = selectPaperType;
            let key = type + serchKey;
            let baseQueryData = {
                TYPE: 'FORM',
                type: type,
                // sorttype: 'ASC',
                pageNumber: 0,
                pageSize: 10,
                branchCode: "",
                serchKey: "",
                userCode: dtoData.userCode
            }
            //如果没有定义这个险种信息 就进行创建
            // alert(JSON.stringify(state.dtoData));
            //当前选中类型数据为空时，封装查询参数
            if (typeof (state.dtoData[key]) == 'undefined') {
                let baseDataInfo = {
                    isisEnd: false,
                    queryData: baseQueryData,
                    curTypeList: []
                }
                state.curShowSoftPaperList = [];
                state.softPaperIsEnd = false;
                state.dtoData[key] = baseDataInfo;
            }

            if (operation == "refresh") {
                //下拉刷新操作,赋初始值
                state.softPaperIsEnd = false;
                state.dtoData[key].queryData = baseQueryData;
                state.dtoData[key].isisEnd = false;
                state.dtoData[key].curTypeList = [];
                state.dtoData[key].queryData.pageNumber++;
            }
            //初始查询且当前选中的类型数据为空
            if (!state.softPaperIsQuery && !state.dtoData[key].isisEnd) {
                if (operation == "infinite") {
                    //上拉加载操作 页码加1
                    state.dtoData[key].queryData.pageNumber++;
                }
                //产品类型编码数组，用于对应显示类型标签循环
                let riskCodeList = _this.$store.state.colorRiskTyle;
                //开始请求接口操作
                state.softPaperIsQuery = true;
                state.dtoData[key].queryData.branchCode = "0000";
                state.dtoData[key].queryData.serchKey = serchKey;

                let queryShopRate = {
                    _this: _this,
                    userCode: dtoData.userCode
                };
                // _this.$common.storeCommit(_this, Mutations.GET_PROMOTION_FEE, queryShopRate);
                _this.$http.post(RequestUrl.ARTICLE_LIST, state.dtoData[key].queryData).then(function (res) {
                    if (res.state == 0) {
                        let lists = res.result;
                        for (let i = 0; i < lists.length; i++) {
                            // debugger
                            // let test1 = lists[i].productId;
                            // let rate = state.curPromotionFee[lists[i].productId];
                            // if (typeof (state.curPromotionFee[lists[i].productId]) != "undefined") {
                            //     lists[i].rate = state.curPromotionFee[lists[i].productId] * (1 - state.curPromotionFee["feeRate"]);
                            //     // curPromotionFee=
                            // } else {
                            //     lists[i].rate = lists[i].rate * (1 - state.curPromotionFee["feeRate"]);
                            // }

                            // 新增软文对应的产品类型名称属性，便于显示类型标签
                            if (!_this.$common.isEmpty(riskCodeList)) {
                                lists[i].riskName = riskCodeList[lists[i].riskCode].riskName;

                                // for (let j = 0; j < riskCodeList.length; j++) {
                                //     if (lists[i].riskCode == riskCodeList[j].key) {//编码
                                //         lists[i].riskName = riskCodeList[j].value;//类型名
                                //         break;
                                //     }
                                // }
                            }
                            //将当前选中类型的软文列表添加至对应的数组中
                            state.dtoData[key].curTypeList.push(lists[i]);
                        }
                        //当查询数据记录数小于当前选中类型数据的单页记录条数，则设置可继续查询
                        if (state.dtoData[key].queryData.pageSize > lists.length) {
                            state.dtoData[key].isisEnd = true;
                            state.softPaperIsEnd = true;
                        }
                        //当前选中类型数据
                        state.curShowSoftPaperList = state.dtoData[key].curTypeList.slice();
                        state.softPaperIsQuery = false;
                    }
                });
            } else {
                //非初始查询或者当前选中类型的数据已存在
                state.softPaperIsEnd = state.dtoData[key].isisEnd;
                //从已有的数组中返回选定的元素
                state.curShowSoftPaperList = state.dtoData[key].curTypeList.slice();
            }
        },
        //海报posterList
        [Mutations.SHOP_POSTER_LIST](state, dtoData) {
            let _this = dtoData._this;
            let branchCode = dtoData.branchCode;
            let serchKey = dtoData.serchKey;
            let operation = dtoData.operation;//Scroller滑动事件
            let selectPosterType = dtoData.selectPosterType;//选择的软文类型
            let type = selectPosterType;//当前选中类型
            //更新当前选择的类型
            state.selectPosterType = selectPosterType;
            let key = type + serchKey;
            let baseQueryData = {
                TYPE: 'FORM',
                type: type,
                pageNumber: 0,
                pageSize: 10,
                branchCode: "",
                serchKey: "",
                userCode: dtoData.userCode
            }
            //如果没有定义这个险种信息 就进行创建
            // alert(JSON.stringify(state.dtoData));
            //当前选中类型数据为空时，封装查询参数
            if (typeof (state.dtoDataPoster[key]) == 'undefined') {
                let baseDataInfo = {
                    isisEnd: false,
                    queryData: baseQueryData,
                    curTypeList: []
                }
                state.curShowPosterList = [];
                state.posterIsEnd = false;
                state.dtoDataPoster[key] = baseDataInfo;
            }

            if (operation == "refresh") {
                //下拉刷新操作,赋初始值
                state.posterIsEnd = false;
                state.dtoDataPoster[key].queryData = baseQueryData;
                state.dtoDataPoster[key].isisEnd = false;
                state.dtoDataPoster[key].curTypeList = [];
                state.dtoDataPoster[key].queryData.pageNumber++;
            }
            //初始查询且当前选中的类型数据为空
            if (!state.posterIsQuery && !state.dtoDataPoster[key].isisEnd) {
                if (operation == "infinite") {
                    //上拉加载操作 页码加1
                    state.dtoDataPoster[key].queryData.pageNumber++;
                }
                //产品类型编码数组，用于对应显示类型标签循环
                let riskCodeList = _this.$store.state.riskCodeList;
                //开始请求接口操作
                state.posterIsQuery = true;
                state.dtoDataPoster[key].queryData.branchCode = "0000";
                state.dtoDataPoster[key].queryData.serchKey = serchKey;
                let queryShopRate = {
                    _this: _this,
                    userCode: dtoData.userCode
                };
                // _this.$common.storeCommit(_this, Mutations.GET_PROMOTION_FEE, queryShopRate);
                _this.$http.post(RequestUrl.POSTER_LIST, state.dtoDataPoster[key].queryData).then(function (res) {
                    if (res.state == 0) {
                        let lists = res.result;
                        for (let i = 0; i < lists.length; i++) {
                            // if (lists[i].type == "2") {
                            //     if (typeof (state.curPromotionFee[lists[i].productId]) != "undefined") {
                            //         lists[i].rate = state.curPromotionFee[lists[i].productId] * (1 - state.curPromotionFee["feeRate"]);
                            //         // curPromotionFee=
                            //     } else {
                            //         lists[i].rate = lists[i].rate * (1 - state.curPromotionFee["feeRate"]);
                            //     }
                            // }


                            // debugger
                            // if (typeof (state.curPromotionFee[lists[i].productId]) != "undefined") {
                            //     lists[i].rate = state.curPromotionFee[lists[i].productId] - state.curPromotionFee["feeRate"];
                            //     // curPromotionFee=
                            // } else {
                            //     lists[i].rate = lists[i].rate - state.curPromotionFee["feeRate"];
                            // }
                            // 新增软文对应的产品类型名称属性，便于显示类型标签
                            if (!_this.$common.isEmpty(riskCodeList)) {
                                for (let j = 0; j < riskCodeList.length; j++) {
                                    if (lists[i].riskCode == riskCodeList[j].key) {//编码
                                        lists[i].riskName = riskCodeList[j].value;//类型名
                                        break;
                                    }
                                }
                            }
                            //将当前选中类型的软文列表添加至对应的数组中
                            state.dtoDataPoster[key].curTypeList.push(lists[i]);
                        }
                        //当查询数据记录数小于当前选中类型数据的单页记录条数，则设置可继续查询
                        if (lists.length < state.dtoDataPoster[key].queryData.pageSize) {
                            state.dtoDataPoster[key].isisEnd = true;
                            state.posterIsEnd = true;
                        }
                        //当前选中类型数据
                        state.curShowPosterList = state.dtoDataPoster[key].curTypeList.slice();
                        state.posterIsQuery = false;
                    }
                });
            } else {
                //非初始查询或者当前选中类型的数据已存在
                state.posterIsEnd = state.dtoDataPoster[key].isisEnd;
                //从已有的数组中返回选定的元素
                state.curShowPosterList = state.dtoDataPoster[key].curTypeList.slice();
            }
        },
        [Mutations.PRODUCT_MANAGE](state, data) {
            let userCode = data.userCode;
            let branchCode = data.branchCode;
            let _this = data._this;
            let operation = data.operation;
            let type = data.type;
            let isManage = false;
            let page = data.page;
            if (page == 'manager') {
                isManage = true;
            }
            if (operation == "get" && !state.isQuery) {
                let query = {
                    userCode: userCode,
                    branchCode: "0000",
                    riskCode: type,
                    TYPE: "FORM"
                };
                let queryRate = {
                    _this: _this,
                    userCode: userCode
                };
                // alert("branchCode:  " + branchCode);
                // alert(JSON.stringify(query));
                // _this.$common.storeCommit(_this, Mutations.GET_PROMOTION_FEE, queryRate);
                _this.$http.post(RequestUrl.PRODUCT_MANAGE, query)
                    .then(function (res) {
                        // alert(JSON.stringify(res));
                        let shopPro = {};
                        let resShopList = res.result.sp;
                        let resPcList = res.result.pc;
                        let channelPro = res.result.ch;
                        if (channelPro != false) {
                            let saveCh = {};
                            for (let i = 0; i < channelPro.length; i++) {
                                saveCh[channelPro[i].productId] = "";
                            }
                            state.channelPro = saveCh;
                        } else {
                            state.channelPro = false;
                        }
                        let addListAll = [];
                        let noListAll = [];
                        let rateList = [];
                        for (let i = 0; i < resPcList.length; i++) {
                            rateList[resPcList[i].productId] = resPcList[i].rate;
                        }
                        for (let i = 0; i < resShopList.length; i++) {
                            let rate = resShopList[i].rate;
                            if (rate != 'NaN' && !isNaN(rate) && rate != 0.0 && rate != 0) {
                                resShopList[i].productNameClass = 'product-name-rate';
                                resShopList[i].productTypeClass = 'product-type-rate';
                                resShopList[i].productInfoSetClass = 'product-info-set-rate';
                                if (isManage) {
                                    resShopList[i].productCname = _this.productCname(resShopList[i].productCname, 1);
                                }
                            } else {
                                resShopList[i].productNameClass = 'product-name';
                                resShopList[i].productTypeClass = 'product-type';
                                resShopList[i].productInfoSetClass = 'product-info-set';
                                if (isManage) {
                                    resShopList[i].productCname = _this.productCname(resShopList[i].productCname, 0);
                                }
                            }

                            resShopList[i].rate = rateList[resShopList[i].productId];

                            // if (typeof (state.curPromotionFee[resShopList[i].productId]) != "undefined") {
                            //     resShopList[i].rate = state.curPromotionFee[resShopList[i].productId] * (1 - state.curPromotionFee["feeRate"]);

                            // } else {
                            //     let numResShop = 1 - state.curPromotionFee["feeRate"];
                            //     let rate = resPcList[i].rate;
                            //     resShopList[i].rate = rate * numResShop;
                            //     // alert(numResShop + "  " + rate + "" + resShopList[i].rate);
                            //     // if (isNaN(resShopList[i].rate)) {
                            //     //     alert(numResShop + "  " + rate + "" + resShopList[i].rate);
                            //     // }
                            // }
                            shopPro[resShopList[i].productId] = resShopList[i]
                        }

                        state.shopPro = shopPro;
                        for (let i = 0; i < resPcList.length; i++) {
                            let rate = resPcList[i].rate;
                            if (rate != 'NaN' && !isNaN(rate) && rate != 0.0 && rate != 0) {
                                resPcList[i].productNameClass = 'product-name-rate';
                                resPcList[i].productTypeClass = 'product-type-rate';
                                resPcList[i].productInfoSetClass = 'product-info-set-rate';
                                if (isManage) {
                                    resPcList[i].productCname = _this.productCname(resPcList[i].productCname, 1);
                                }
                            } else {
                                resPcList[i].productNameClass = 'product-name';
                                resPcList[i].productTypeClass = 'product-type';
                                resPcList[i].productInfoSetClass = 'product-info-set';
                                if (isManage) {
                                    resPcList[i].productCname = _this.productCname(resPcList[i].productCname, 0);
                                }
                            }
                            // if (typeof (state.curPromotionFee[resPcList[i].productId]) != "undefined") {
                            //     resPcList[i].rate = state.curPromotionFee[resPcList[i].productId] * (1 - state.curPromotionFee["feeRate"]);
                            //     // curPromotionFee=
                            // } else {
                            //     let numResPc = 1 - state.curPromotionFee["feeRate"];
                            //     resPcList[i].rate = resPcList[i].rate * numResPc;
                            //     // alert(numResPc + "  " + resPcList[i].rate);
                            //     if (isNaN(resPcList[i].rate)) {
                            //         alert(numResPc);
                            //     }
                            // }

                            resPcList[i].order = i

                            //添加到其他的险种选择 未上架和已上架
                            // if (typeof (state.saveShowManageData[resPcList[i].riskCode]) == "undefined") {
                            //     let date = {
                            //         addPro: [],
                            //         npPro: []
                            //     }
                            //     state.saveShowManageData[resPcList[i].riskCode] = date;
                            // }
                            //判断产品是否在产品列表中
                            if (typeof (shopPro[resPcList[i].productId]) == "undefined") {
                                // state.saveShowManageData[resPcList[i].riskCode].npPro.push(resPcList[i]);
                                resPcList[i].id = "";
                                noListAll.push(resPcList[i]);  //全部
                            } else {
                                // state.saveShowManageData[resPcList[i].riskCode].addPro.push(resPcList[i]);
                                resPcList[i].id = shopPro[resPcList[i].productId].id;
                                if (type == "all") {
                                    resPcList[i].order = shopPro[resPcList[i].productId].serialNo;
                                    addListAll[shopPro[resPcList[i].productId].serialNo] = resPcList[i]; //全部
                                } else {
                                    resPcList[i].order = shopPro[resPcList[i].productId].riskSerialNo;
                                    addListAll[shopPro[resPcList[i].productId].riskSerialNo] = resPcList[i]; //全部
                                }
                                resPcList[i].serialNo = shopPro[resPcList[i].productId].serialNo;
                                resPcList[i].riskSerialNo = shopPro[resPcList[i].productId].riskSerialNo;
                                // addListAll[shopPro[resPcList[i].productId].serialNo] = resPcList[i]; //全部
                                // addListAll.push(resPcList[i])
                            }
                        }
                        let newArray = [];
                        for (let x in addListAll) {
                            newArray.push(addListAll[x]);
                        }
                        newArray = newArray.reverse();
                        let all = {
                            addPro: newArray,
                            npPro: noListAll
                        }
                        state.saveShowManageData[type] = all;
                        _this.addPro = state.saveShowManageData[type].addPro;
                        _this.saveAddPro[type] = state.saveShowManageData[type].addPro;
                        _this.npPro = state.saveShowManageData[type].npPro;
                        state.addPro = state.saveShowManageData[type].addPro;
                        state.npPro = state.saveShowManageData[type].npPro;
                    })
            } else {
                _this.addPro = state.saveShowManageData[type].addPro;
                _this.npPro = state.saveShowManageData[type].npPro;
            }
        },
        [Mutations.PRODUCT_MANAGE_SAVE](state, data) {
            let type = data.type;
            let _this = data._this;
            let addPro = _this.addPro;
            let npPro = _this.npPro;
            if (state.addPro == addPro && state.npPro == npPro) {
                return;
            }
            let saveData = [];
            if (type == "all") {
                for (let i = 0; i < addPro.length; i++) {
                    let baseNum;
                    if (addPro[i].id == "") {
                        baseNum = 99 + i;
                    } else {
                        baseNum = addPro[i].riskSerialNo;
                    }
                    let data = {
                        id: addPro[i].id,
                        userCode: _this.user.userDto.userCode,
                        productId: addPro[i].productId,
                        serialNo: i,
                        riskSerialNo: baseNum,
                        isShow: 1,
                    }
                    saveData.push(data);
                }
            } else {
                for (let i = 0; i < addPro.length; i++) {
                    let baseNum;
                    if (addPro[i].id == "") {
                        baseNum = 99 + i;
                    } else {
                        baseNum = addPro[i].serialNo;
                    }
                    let data = {
                        id: addPro[i].id,
                        userCode: _this.user.userDto.userCode,
                        productId: addPro[i].productId,
                        serialNo: baseNum,
                        riskSerialNo: i,
                        isShow: 1,
                    }
                    saveData.push(data);
                }
            }
            for (let i = 0; i < npPro.length; i++) {
                if (npPro[i].id != "") {
                    let data = {
                        id: npPro[i].id,
                        userCode: _this.user.userDto.userCode,
                        productId: npPro[i].productId,
                        serialNo: "",
                        riskSerialNo: "",
                        isShow: 0,
                    }
                    saveData.push(data);
                }
            }
            let query = {
                storeList: {
                    saveData
                }
            }
            // _this.$http.post(RequestUrl.SAVE_STORE_PRO, saveData)
            //     .then(function (res) {

            //     })
            //本地token
            let localToken = localStorage[SaveName.JWT_TOKEN_NAME];
            $.ajax({
                type: "POST",
                url: RequestUrl.SAVE_STORE_PRO,
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify(saveData),
                async: false,
                headers: {
                    "authorization": localToken,
                    // "Accept": "application/json; charset=utf-8",
                    // "Content-Type": "application/x-www-form-urlencoded",
                },
                success: function (res, status, xhr) {
                    _this.$common.reJwtVal(res);
                }
            });

        },
    },
    actions: {

    },
    getters: {

    }
}
