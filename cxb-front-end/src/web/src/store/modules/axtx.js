import {
    Mutations, RouteUrl
} from 'src/common/const';
import OrderConfig from 'src/common/config/orderConfig';
import {
    RequestUrl
} from 'src/common/url';
export default {
    state: {
        appliDto: {
            //投保人信息
            name: "",
            idNum: "",
            phoneNum: "",
            mailAddress: "",
            carLicenseNum: "",
            carFrameNum: "",
            stringDate: "",
            noLicenseNum: false,
            planCode: "",
            sumPremium: "",
            saleCode: "",
            salerName: "",
            agreementCode: "",
            riskCode: "",
            isNewCar: false,
            channelName: "",
            sex: "",
            birth: "",
        },
        vinList: {},

        maskVin: {},
    },
    mutations: {
        [Mutations.AXTX_SAVE_DATA](state, saveData) {
            let data = saveData;
            state.appliDto = data;
        },
        [Mutations.AXTX_GET_DATA](state, _this) {
            _this.appliDto = state.appliDto;
            _this.isNewCar = state.appliDto.isNewCar;
        },
        [Mutations.AXTX_GET_VIN](state, _this) {
            if (!_this.isNewCar) {
                let carNo = _this.appliDto.carLicenseNum;
                let reg = /^[\u4e00-\u9fa5][a-zA-Z0-9]{6}$/;
                if (reg.test(carNo)) {
                    if (typeof (state.vinList[carNo]) == "undefined") {
                        let query = {
                            documentNo: carNo
                        }
                        _this.$http.post(RequestUrl.QUERY_VIN, query)
                            .then(function (res) {
                                if (res.success) {
                                    if (_this.appliDto.carFrameNum == "" && _this.$common.isNotEmpty(res.result) && res.result.length > 13) {
                                        state.vinList[carNo] = res.result;
                                        let vinNum = state.vinList[carNo];
                                        let resVim = _this.$common.mask(vinNum, 4, 14, '*');
                                        state.maskVin[resVim] = "";
                                        _this.appliDto.carFrameNum = resVim;

                                    }
                                }
                            })
                    } else {
                        if (_this.appliDto.carFrameNum == "") {
                            let vinNum = state.vinList[carNo];

                            _this.appliDto.carFrameNum = _this.$common.mask(vinNum, 4, 14, '*');
                        }
                    }
                }
            }
        },

    },
    actions: {

    },
    getters: {

    }
}