import Vue from 'vue'
import Vuex from 'vuex'
import mutations from './mutations'
import actions from './action'
import {
  FooterIndex,
  PopState,
  FooterShow
} from 'src/common/const'
// ----------vuex-i18n -------------------
import vuexI18n from 'vuex-i18n';
import {
  TRANSLATIONS_CH
} from './modules/i18n/ch';
import {
  TRANSLATIONS_EN
} from './modules/i18n/en';

// -----------store list------------------
//自营平台
import car from './modules/car';
import insurance from './modules/insurance';
import order from './modules/order';
import pay from './modules/pay';
import saveInsure from './modules/saveInsure';
// import saveDetails from './modules/saveDetails';
import detailsStore from './modules/detailsStore';
import shop from './modules/shop';
import integral from './modules/integral';
import clauseStore from './modules/clauseStore';
import seleValue from './modules/seleValue';
import activity from './modules/activity';
import axtx from './modules/axtx';
import channel from './modules/channel';
import Common from 'src/common/common';
Vue.use(Vuex)

// -----------user---------------------
import user from './modules/user';
// ---------base state--------------
const state = {
  uuid: Common.getUuid(),
  shareUuid: Common.getUuid(),
  isOldVersion: false,//默认不是旧版本号
  isShop: false,

  loading: false,
  payStyle: "background:#636365;",
  state: "正在投保中..",

  loadingAxtx: false,
  payStyleAxtx: "background:#636365;",
  stateAxtx: "正在投保中..",
  insureResDataAxtx: {
    res: '',
    curprice: '0.00',
  },

  seleCountry: [],
  insureResData: {
    res: '',
    curprice: '0.00',
  },
  test: 0, // 测试
  footerState: FooterIndex.INDEX,
  footerShow: FooterShow.TRUE,
  popState: PopState.VUX_POP_NULL,
  isLoading: false,
  // //地区
  // region: [],
  showSeleCountry: false, // 显示国家的录入信息
  //url 
  initUrl: {},
  // 热门保险
  hotInsuList: [],
  // 首页菜单
  indexMenuList: [],
  // 首页广告
  adList: [],
  // 保险列表
  riskCode: [],
  // 服务页面菜单
  serverMenu: [],
  // 服务菜单大类
  serverMenuType: [],
  // 保险类别
  riskCodeList: [],

  iframe: {
    isIframe: false,
    iframeUrl: "",
    iframeName: "",
  },
  activity: {
    activityDiv: "",
    activityName: "",
  },


  // 类型 对应 保险信息
  insureBaseInfo: {
    seleType: 'all',
    all: []
  },

  colorRiskTyle: [],


  abroadTravel: '',


  isWeChar: false,

  configBtn: {
    isShopIndex: true,
    isShopMore: true,
    isShopService: true,
    isShowMy: true,
    isShowBtn: true,
  },


  msgData: {
    headerTitle: "",
    gaBackUrl: "1",
    buttonUrl: "1",
    title: "1",
    description: "1",
    buttonsType: "primary",
    buttonsText: "1",
  },

  carAddr: "浙",

  insuranceListTop: "position: fixed;padding-top: 92px;padding-bottom: 75px;z-index:0;",
  vueScrollerBtn: "background: #F0F0F0;height: 0px;",
  vueScrollerNoMoreData: "",

  //软文
  isQuerySoftPaper: false,//是否查询到点击的软文
  //软文内容
  softPaper: {
    name: "",
    riskCode: "",
    productId: "",
    productName: "",
    summaryInfo: "",
    content: "",
    imgUrl: "",
    views: "",
  },
  //海报
  isQueryPoster: false,//是否查询到点击的海报
  //海报内容
  poster: {
    name: "",
    riskCode: "",
    productId: "",
    productName: "",
    summaryInfo: "",
    imgUrl: "",
    views: ""
  },

  isQuery: false,
  isInitData: false,
  initIndexData: {
    TYPE: "FORM",
    type: "hot",
    sorttype: "ASC",
    pageNumber: 1,
    pageSize: 10,
    region: "",
    pointX: "",
    pointY: "",
    userId: "",
    uuid: "",
    userCode: "",
    isShop: false,
    markUuid: "",
  },


  shareData: {
    callBackUrl: "",
    singleMember: "",
    successfulUrl: "",
  },
  isWeAuth: true,

  isQueryHeader: false,

  isShowVueScrollerRe: true,
  dealUid: "",
  accessUrl: "",
  isShowRate: false,


  popAd: [],


  redTrigger: true,
  trigger: false,

  isJwtQuery: false,


  shopMenu: {
    '店铺管理': [],
    '销售工具': [],
    '我的团队': [],
  },
  shopMenuType: [],

  newCarSele: "浙",
  queryStyle: "background: #C8161D !important; color: #fff !important;",
  baseIsQuery: true,

  msgComData: {
    title: "",
    content: "",
    alertBtn: "",
    alertBtnUrl: "",
    backBtn: "",
    backBtnUrl: "",
  },
  isShowMsgComData: false,

  isQueryAd: false,


  serviceRows: 4,
  shopRows: 4
}

const store = new Vuex.Store({
  state,
  actions,
  mutations,
  modules: {
    //自营平台
    i18n: vuexI18n.store,
    insurance,
    user,
    order,
    pay,
    saveInsure,
    clauseStore,
    activity,
    // saveDetails,
    detailsStore,
    seleValue,
    axtx,
    shop,
    car,
    integral,
    channel
  }
})

Vue.use(vuexI18n.plugin, store);
Vue.i18n.add('en', TRANSLATIONS_EN);
Vue.i18n.add('ch', TRANSLATIONS_CH);
Vue.i18n.set('ch');

export default store;
