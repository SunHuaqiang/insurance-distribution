package cn.com.libertymutual.sys.action;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import cn.com.libertymutual.core.web.ServiceResult;
import cn.com.libertymutual.sys.bean.SysRole;
import cn.com.libertymutual.sys.bean.SysRoleMenu;
import cn.com.libertymutual.sys.service.api.RoleUserService;

import com.alibaba.fastjson.JSONArray;

@RefreshScope
@RestController
@RequestMapping("/systemCfg/")
/**
 * @author:dylan.qiu
 * @Description: Role User Controller
 * @date:   2017年8月11日 下午1:35:36
 */
public class RoleUserController {
	@Autowired
	private RoleUserService roleUserService;
	
	@RequestMapping(value="roleFindAll")
	public ServiceResult roleFindAll(boolean isLookup,String lookupDataOne,String lookupDataTwo,int pageNumber, int pageSize, String sortfield,String sorttype){
		return roleUserService.roleFindAll(isLookup, lookupDataOne, lookupDataTwo, pageNumber, pageSize, sortfield, sorttype);
	}
	
	@RequestMapping(value="roleMenuFind")
	public JSONArray roleManuFind(String roleId){
		return roleUserService.roleManuFind(roleId);
	}
	@ResponseBody
	@RequestMapping(value="saveRoleMenu")
	public ServiceResult saveRoleMenu(@RequestBody SysRoleMenu sysRoleMenu){
		return roleUserService.saveRoleMenu(sysRoleMenu);
	}	
	
	@RequestMapping(value="deleteRoleMenu")
	public ServiceResult deleteRoleMenu(SysRoleMenu sysRoleMenu){
		return roleUserService.deleteRoleMenu(sysRoleMenu);
	}		
	
	@RequestMapping(value="saveRole")
	public ServiceResult saveRole(SysRole sysRole){
		return roleUserService.saveRole(sysRole);
	}
	
	@RequestMapping(value="deleteRole")
	public ServiceResult deleteRole(SysRole sysRole){
		return roleUserService.deleteRole(sysRole);
	}
	
	
	@RequestMapping(value="getNotExistMenu")
	public JSONArray getNotExistMenu(String roleId){
		return roleUserService.getNotExistMenu(roleId);
	}	
	
	
}
