package cn.com.libertymutual.sys.bean;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * Created by Ryan on 2016-12-21.
 */
public class SysUserMenuResPK implements Serializable {
    private String userid;
    private Integer menuid;
    private String resid;

    @Column(name = "USERID", nullable = false, length = 16)
    @Id
    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    @Column(name = "MENUID", nullable = false, precision = 0)
    @Id
    public Integer getMenuid() {
        return menuid;
    }

    public void setMenuid(Integer menuid) {
        this.menuid = menuid;
    }

    @Column(name = "RESID", nullable = false, length = 32)
    @Id
    public String getResid() {
        return resid;
    }

    public void setResid(String resid) {
        this.resid = resid;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SysUserMenuResPK that = (SysUserMenuResPK) o;

        if (userid != null ? !userid.equals(that.userid) : that.userid != null) return false;
        if (menuid != null ? !menuid.equals(that.menuid) : that.menuid != null) return false;
        if (resid != null ? !resid.equals(that.resid) : that.resid != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = userid != null ? userid.hashCode() : 0;
        result = 31 * result + (menuid != null ? menuid.hashCode() : 0);
        result = 31 * result + (resid != null ? resid.hashCode() : 0);
        return result;
    }
}
