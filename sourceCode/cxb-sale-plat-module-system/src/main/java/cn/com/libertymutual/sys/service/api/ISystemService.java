package cn.com.libertymutual.sys.service.api;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import cn.com.libertymutual.core.exception.CustomJDBCException;
import cn.com.libertymutual.core.exception.CustomLangException;
import cn.com.libertymutual.core.web.ServiceResult;
import cn.com.libertymutual.sys.vo.MenuInfoVO;

import com.alibaba.fastjson.JSONArray;

public interface ISystemService {
	/**
	 * 
	* @Title login 
	* @Description 登录获取用户信息
	* @param request
	* @param appFlag 是否为应用服务端     //如果appFlag 未传,则说明是业务端登录获取菜单,否则是在后端登录的
			
	* @param fmenuId 父菜单
	* @param result 返回的数据结果集
	* @throws CustomLangException  自定义异常
	* @throws CustomJDBCException  数据库异常
	* @return void
	*/
	public void login( HttpServletRequest request,  Integer fmenuId, ServiceResult result ) throws CustomLangException ; 
	/**
	 * 
	* @Title getAccessMenuInfo 
	* @Description 根据父级菜单生成下级树状结构的菜单
	* @param fmenuId 父级菜单
	* @param result 返回的数据集
	* @throws CustomLangException  自定义异常
	* @throws CustomJDBCException  数据库异常 
	* @return void
	 */
	public void getAccessMenuInfo( Integer fmenuId, ServiceResult result ) throws CustomLangException ;
	
	/**
	 * 
	* @Title getJsonMenuInfo 
	* @Description 
	* @param uiType
	* @param menuId
	* @param execType
	* @param roleId
	* @throws CustomLangException    
	* @return JSONArray
	 */
	public JSONArray getJsonMenuInfo( String uiType, Integer menuId, String execType, String roleId ) throws CustomLangException;
	
	public List<MenuInfoVO> getAccessMenuInfo(Integer fmenuId ,List<String> roleIds ,String userId) throws CustomLangException;
	
}
