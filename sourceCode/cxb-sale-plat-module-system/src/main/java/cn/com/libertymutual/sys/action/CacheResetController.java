package cn.com.libertymutual.sys.action;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import cn.com.libertymutual.core.annotation.SystemLog;
import cn.com.libertymutual.core.exception.BusinessExceptionEnums;
import cn.com.libertymutual.core.exception.CustomLangException;
import cn.com.libertymutual.core.web.ServiceResult;
import cn.com.libertymutual.sys.service.api.IInitSharedMemory;
@RefreshScope
@RequestMapping("/systemCfg")
@RestController
public class CacheResetController {
	private Logger log = LoggerFactory.getLogger(getClass());
	
	private static final String ALL="all";
	private static final String MENU = "menu";
	private static final String CODE = "code";
	private static final String AREA = "area";
	private static final String RISK = "risk";
	private static final String KIND = "kind";
	private static final String PKG = "pkg";
	private static final String DEPRECIATION = "depreciation";
	private static final String SERVERINF = "svrinf";
	private static final String BASE_PREMIUM = "basePremium";
	private static final String BASE_TAX = "baseTax";
	private static final String PREDEFINE="predefine";
	private static final String ACCOUNT="account";
	private static final String AGREEMENTCONF="agreementconf";
	private static final String SYSPAYTYPE="syspaytype";
	private static final String SYSFILETYPE="sysfiletype";

	@Autowired
	private IInitSharedMemory  initSharedMemory;
	
	@ResponseBody
	@RequestMapping(value="reload",method=RequestMethod.POST)
	@SystemLog(description="刷新缓存")
    public ServiceResult CacheReset(String type) { 
		ServiceResult sr = new ServiceResult();
		
		try {
			if(type==null){
				throw new CustomLangException(BusinessExceptionEnums.PARAMETER_NOT_NULL,"请输入缓存类型type=?");
			}
			switch ( type ) {
				case ALL:
					initSharedMemory.initData();
					break;
				case MENU:
					initSharedMemory.initSysMenu();
					break;
				case CODE:
					initSharedMemory.initCodeInto();
					break;
				case AREA:
					//initSharedMemory.areaSortIntoRedis();
					break;
				case RISK:
					//initSharedMemory.initRiskInfo();
					break;
				case KIND:
					//initSharedMemory.kindIntoReids();
					break;
				case PKG:
					//pubParamsService.packageIntoReids();
					//initSharedMemory.initAllMealKind();
					break;
				case SERVERINF:
					initSharedMemory.initServiceInfo();
					break;
				case DEPRECIATION:
					//初始化折旧系数
					//initSharedMemory.initDepreciationConfig();
					break;
				case BASE_PREMIUM:
					//初始化折旧系数
					//initSharedMemory.basePremiumIntoReids();
					break;
				case BASE_TAX:
					//车船税配置
					//initSharedMemory.baseTaxIntoReids();
					break;
				case PREDEFINE:
					//自定义配置
					//initSharedMemory.initPredefineReids();
					break;
				case ACCOUNT:
					//账户信息
					//initSharedMemory.initAccountReids();
					break;
				case AGREEMENTCONF:
					//业务关系代码配置
					//initSharedMemory.initAgreementConfReids();
					break;
				case SYSPAYTYPE:
					//支付方式刷新
					//initSharedMemory.initSysPayTypeReids();
					break;
				case SYSFILETYPE:
					//文件类型刷新
					//initSharedMemory.initSysFileTypeReids();
					break;
				default:
					log.info(type); 
					break;
			}
		} catch (Exception e) {
			
			CustomLangException ce = new CustomLangException(e);
			log.error(e.getMessage(),e);
			//返回失败信息给前端
			sr.setAppFail(ce);
			return sr;
		}
		sr.setResult("刷新缓存成功!"); 
		return sr;
        
    }
}
