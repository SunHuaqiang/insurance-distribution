package cn.com.libertymutual.sys.bean;

import java.io.Serializable;

import javax.persistence.*;

/**
 * Created by Ryan on 2016-09-09.
 */
@Entity
@Table(name = "tb_sys_code_risk",  catalog = "")
public class SysCodeRisk   implements Serializable{
    /**
	 * 
	 */
	private static final long serialVersionUID = -4790145796199481494L;
	private int id;
    private String codeType;
    private String code;
    private String riskCode;

    @Id    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "ID", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "CODE_TYPE", nullable = false, length = 50)
    public String getCodeType() {
        return codeType;
    }

    public void setCodeType(String codeType) {
        this.codeType = codeType;
    }

    @Basic
    @Column(name = "CODE", nullable = false, length = 50)
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Basic
    @Column(name = "RISK_CODE", nullable = false, length = 4)
    public String getRiskCode() {
        return riskCode;
    }

    public void setRiskCode(String riskCode) {
        this.riskCode = riskCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SysCodeRisk that = (SysCodeRisk) o;

        if (id != that.id) return false;
        if (codeType != null ? !codeType.equals(that.codeType) : that.codeType != null) return false;
        if (code != null ? !code.equals(that.code) : that.code != null) return false;
        if (riskCode != null ? !riskCode.equals(that.riskCode) : that.riskCode != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (codeType != null ? codeType.hashCode() : 0);
        result = 31 * result + (code != null ? code.hashCode() : 0);
        result = 31 * result + (riskCode != null ? riskCode.hashCode() : 0);
        return result;
    }

	@Override
	public String toString() {
		return "SysCodeRisk [id=" + id + ", codeType=" + codeType + ", code="
				+ code + ", riskCode=" + riskCode + "]";
	}
}
