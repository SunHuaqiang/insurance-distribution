package cn.com.libertymutual.sys.service.api;

import cn.com.libertymutual.core.base.dto.UserInfo;
import cn.com.libertymutual.core.web.ServiceResult;

public interface ISysUserService {
	ServiceResult  findAll(boolean isLookup,String lookupDataOne,String lookupDataTwo,int pageNumber, int pageSize, String sortfield,String sorttype);
	UserInfo initUser(String userId  );
}
