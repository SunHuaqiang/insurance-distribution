package cn.com.libertymutual.sys.service.api;

import java.util.Map;

import cn.com.libertymutual.sys.bean.SysMenu;

public interface IInitSharedMemory {

	
	
	public void initData();
	
	public Map<String, SysMenu> initSysMenu();
	public void initServiceInfo();

	public void initCodeInto();
 

 

}
