package cn.com.libertymutual.sys.bean;

import javax.persistence.*;

/**
 * Created by Ryan on 2016-12-21.
 */
@Entity
@Table(name = "tb_sys_user_menu_res", catalog = "")
@IdClass(SysUserMenuResPK.class)
public class SysUserMenuRes {
    private String userid;
    private Integer menuid;
    private String resid;

    @Id
    @Column(name = "USERID", nullable = false, length = 16)
    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    @Id
    @Column(name = "MENUID", nullable = false, precision = 0)
    public Integer getMenuid() {
        return menuid;
    }

    public void setMenuid(Integer menuid) {
        this.menuid = menuid;
    }

    @Id
    @Column(name = "RESID", nullable = false, length = 32)
    public String getResid() {
        return resid;
    }

    public void setResid(String resid) {
        this.resid = resid;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SysUserMenuRes that = (SysUserMenuRes) o;

        if (userid != null ? !userid.equals(that.userid) : that.userid != null) return false;
        if (menuid != null ? !menuid.equals(that.menuid) : that.menuid != null) return false;
        if (resid != null ? !resid.equals(that.resid) : that.resid != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = userid != null ? userid.hashCode() : 0;
        result = 31 * result + (menuid != null ? menuid.hashCode() : 0);
        result = 31 * result + (resid != null ? resid.hashCode() : 0);
        return result;
    }
}
