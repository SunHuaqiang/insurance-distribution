package cn.com.libertymutual.sys.service.api;

import java.util.List;

import com.alibaba.fastjson.JSONArray;

import cn.com.libertymutual.core.web.ServiceResult;
import cn.com.libertymutual.sys.bean.SysMenu;
import cn.com.libertymutual.sys.bean.SysRole;
import cn.com.libertymutual.sys.bean.SysRoleMenu;

/**
 * @author:dylan.qiu
 * @Description: RoleUser Service接口 
 * @date:   2017年8月11日 下午1:36:27
 */
public interface RoleUserService {
	/**
	 * @Description: 查找角色
	 * @param isLookup 是否查找
	 * @param lookupDataOne  查找字段一
	 * @param lookupDataTwo  查找字段二
	 * @param pageNumber 
	 * @param pageSize
	 * @param sortfield 排序字段 
	 * @param sorttype 排序类型ASC DESC
	 * @return 带分页的角色信息
	 */
	ServiceResult roleFindAll(boolean isLookup, String lookupDataOne,
			String lookupDataTwo, int pageNumber, int pageSize,
			String sortfield, String sorttype);
	
	/**
	 * @Description: 根据角色roleId查找所对应的菜单信息
	 * @param roleId 
	 * @return
	 */
	JSONArray roleManuFind(String roleId);
	
	ServiceResult saveRoleMenu(SysRoleMenu sysRoleMenu);
	
	ServiceResult deleteRoleMenu(SysRoleMenu sysRoleMenu);
	
	ServiceResult saveRole(SysRole sysRole);
	
	ServiceResult deleteRole(SysRole sysRole);
	
	JSONArray getNotExistMenu(String roleId);
	
}
