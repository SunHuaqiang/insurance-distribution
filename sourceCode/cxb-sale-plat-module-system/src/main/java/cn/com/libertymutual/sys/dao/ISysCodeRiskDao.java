package cn.com.libertymutual.sys.dao;

import org.springframework.data.repository.CrudRepository;

import cn.com.libertymutual.sys.bean.SysCodeRisk;
public interface ISysCodeRiskDao  extends CrudRepository<SysCodeRisk, Integer>{
	
}