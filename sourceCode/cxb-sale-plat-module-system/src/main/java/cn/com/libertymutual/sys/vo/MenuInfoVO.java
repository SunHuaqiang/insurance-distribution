package cn.com.libertymutual.sys.vo;

import java.io.Serializable;
import java.util.List;

import cn.com.libertymutual.sys.bean.SysMenu;
	
public class MenuInfoVO extends SysMenu implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2701754409951178620L;
	/**
	 * 样式信息
	 */
	private String clazz;
	
	/**
	 * 屏蔽的权限Id(按钮权限)
	 */
	private List<String> resIds;
	/**
	 * 子节点信息列表
	 */
	private List<MenuInfoVO> child;
	
	public String getClazz() {
		return clazz;
	}
	public void setClazz(String clazz) {
		this.clazz = clazz;
	}
	public List<MenuInfoVO> getChild() {
		return child;
	}
	public void setChild(List<MenuInfoVO> child) {
		this.child = child;
	}
	public List<String> getResIds() {
		return resIds;
	}
	public void setResIds(List<String> resIds) {
		this.resIds = resIds;
	}
	
	
}
