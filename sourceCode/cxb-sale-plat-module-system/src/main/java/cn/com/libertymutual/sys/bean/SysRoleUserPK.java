package cn.com.libertymutual.sys.bean;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * Created by Ryan on 2016-12-21.
 */
public class SysRoleUserPK implements Serializable {
    private Integer roleid;
    private String userid;

    @Column(name = "ROLEID", nullable = false)
    @Id
    public Integer getRoleid() {
        return roleid;
    }

    public void setRoleid(Integer roleid) {
        this.roleid = roleid;
    }

    @Column(name = "USERID", nullable = false, length = 16)
    @Id
    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SysRoleUserPK that = (SysRoleUserPK) o;

        if (roleid != null ? !roleid.equals(that.roleid) : that.roleid != null) return false;
        if (userid != null ? !userid.equals(that.userid) : that.userid != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = roleid != null ? roleid.hashCode() : 0;
        result = 31 * result + (userid != null ? userid.hashCode() : 0);
        return result;
    }
}
