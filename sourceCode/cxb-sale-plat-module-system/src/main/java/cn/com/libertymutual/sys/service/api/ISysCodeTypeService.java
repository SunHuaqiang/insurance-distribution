package cn.com.libertymutual.sys.service.api;

import java.util.List;

import cn.com.libertymutual.sys.bean.SysCodeType;

public interface ISysCodeTypeService {

	public	List<SysCodeType> listAll(); 

}
