package cn.com.libertymutual.sp.dto;

public class UserDto {
	private String wxOpenId;// 用户的标识，对当前公众号唯一
	private String registerType = "0";// 注册类型：0=wx自主/分享注册(个人用户)，1=后台注册(渠道用户)，2=分享注册(渠道用户)
	private String employeeCode;// 员工编码
	private String userCode;
	private String userName;// 姓名
	private String nickName;// 昵称
	private String shopName;
	private String isMergeUser;
	private String type; // 0内部用户，1外部用户
	private Integer roleType; // 0渠道经理，1渠道用户
	private String idType; // 证件类型
	private String userType;
	private String idNumber;// 证件号
	private String password;// 密码
	private String oldPassword;// 原始密码
	private String imgCode; // 图形码
	private String mobileCode;// 手机验证码
	private String mobile; // 手机号
	private String refereeId;// 分享ID
	private String refereePro;// 分享产品ID
	private String storeFlag;// 是否开店
	private String comCode; // 所属部门--上级
	private String branchCode;// 机构编码
	private String agrementNo;// 业务关系代码
	private String agrementName;// 代理名称
	private String channelCode;// 渠道编码
	private String channelName;// 渠道名称
	private String saleCode;// 销售人员代码
	private String saleName;// 销售人员姓名
	private String areaCode;// 常驻地区编码
	private String areaName;// 常驻地区名
	private String orgCode;// 组织机构代码
	private String licenceNo;// 营业执照号
	private String customerType;// 关系代码客户类型
	private String customerName;// 关系代码客户名称
	private Double baseRate; // 下级佣金比例
	private String userRealName;
	private String canModify;// 是否允许前端修改渠道信息,0:否,1:是
	private String canScore;
	private String defaultAuth;// 是否默认订单查询权限,0:否,1:是
	private String upUserCode;// 关系上属
	private String rankChangeType;// 职称级别

	public String getWxOpenId() {
		return wxOpenId;
	}

	public void setWxOpenId(String wxOpenId) {
		this.wxOpenId = wxOpenId;
	}

	public String getRegisterType() {
		return registerType;
	}

	public void setRegisterType(String registerType) {
		this.registerType = registerType;
	}

	public String getEmployeeCode() {
		return employeeCode;
	}

	public void setEmployeeCode(String employeeCode) {
		this.employeeCode = employeeCode;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public String getIsMergeUser() {
		return isMergeUser;
	}

	public void setIsMergeUser(String isMergeUser) {
		this.isMergeUser = isMergeUser;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Integer getRoleType() {
		return roleType;
	}

	public void setRoleType(Integer roleType) {
		this.roleType = roleType;
	}

	public String getIdType() {
		return idType;
	}

	public void setIdType(String idType) {
		this.idType = idType;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public String getIdNumber() {
		return idNumber;
	}

	public void setIdNumber(String idNumber) {
		this.idNumber = idNumber;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getOldPassword() {
		return oldPassword;
	}

	public void setOldPassword(String oldPassword) {
		this.oldPassword = oldPassword;
	}

	public String getImgCode() {
		return imgCode;
	}

	public void setImgCode(String imgCode) {
		this.imgCode = imgCode;
	}

	public String getMobileCode() {
		return mobileCode;
	}

	public void setMobileCode(String mobileCode) {
		this.mobileCode = mobileCode;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getRefereeId() {
		return refereeId;
	}

	public void setRefereeId(String refereeId) {
		this.refereeId = refereeId;
	}

	public String getRefereePro() {
		return refereePro;
	}

	public void setRefereePro(String refereePro) {
		this.refereePro = refereePro;
	}

	public String getStoreFlag() {
		return storeFlag;
	}

	public void setStoreFlag(String storeFlag) {
		this.storeFlag = storeFlag;
	}

	public String getComCode() {
		return comCode;
	}

	public void setComCode(String comCode) {
		this.comCode = comCode;
	}

	public String getBranchCode() {
		return branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	public String getAgrementNo() {
		return agrementNo;
	}

	public void setAgrementNo(String agrementNo) {
		this.agrementNo = agrementNo;
	}

	public String getAgrementName() {
		return agrementName;
	}

	public void setAgrementName(String agrementName) {
		this.agrementName = agrementName;
	}

	public String getChannelCode() {
		return channelCode;
	}

	public void setChannelCode(String channelCode) {
		this.channelCode = channelCode;
	}

	public String getChannelName() {
		return channelName;
	}

	public void setChannelName(String channelName) {
		this.channelName = channelName;
	}

	public String getSaleCode() {
		return saleCode;
	}

	public void setSaleCode(String saleCode) {
		this.saleCode = saleCode;
	}

	public String getSaleName() {
		return saleName;
	}

	public void setSaleName(String saleName) {
		this.saleName = saleName;
	}

	public String getAreaCode() {
		return areaCode;
	}

	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}

	public String getAreaName() {
		return areaName;
	}

	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}

	public String getOrgCode() {
		return orgCode;
	}

	public void setOrgCode(String orgCode) {
		this.orgCode = orgCode;
	}

	public String getLicenceNo() {
		return licenceNo;
	}

	public void setLicenceNo(String licenceNo) {
		this.licenceNo = licenceNo;
	}

	public String getCustomerType() {
		return customerType;
	}

	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public Double getBaseRate() {
		return baseRate;
	}

	public void setBaseRate(Double baseRate) {
		this.baseRate = baseRate;
	}

	public String getUserRealName() {
		return userRealName;
	}

	public void setUserRealName(String userRealName) {
		this.userRealName = userRealName;
	}

	public String getCanModify() {
		return canModify;
	}

	public void setCanModify(String canModify) {
		this.canModify = canModify;
	}

	public String getCanScore() {
		return canScore;
	}

	public void setCanScore(String canScore) {
		this.canScore = canScore;
	}

	public String getDefaultAuth() {
		return defaultAuth;
	}

	public void setDefaultAuth(String defaultAuth) {
		this.defaultAuth = defaultAuth;
	}

	public String getUpUserCode() {
		return upUserCode;
	}

	public void setUpUserCode(String upUserCode) {
		this.upUserCode = upUserCode;
	}

	public String getRankChangeType() {
		return rankChangeType;
	}

	public void setRankChangeType(String rankChangeType) {
		this.rankChangeType = rankChangeType;
	}

	@Override
	public String toString() {
		return "UserDto [wxOpenId=" + wxOpenId + ", registerType=" + registerType + ", userCode=" + userCode + ", employeeCode=" + employeeCode
				+ ", userName=" + userName + ", nickName=" + nickName + ", shopName=" + shopName + ", isMergeUser=" + isMergeUser + ", type=" + type
				+ ", roleType=" + roleType + ", idType=" + idType + ", userType=" + userType + ", idNumber=" + idNumber + ", password=" + password
				+ ", oldPassword=" + oldPassword + ", imgCode=" + imgCode + ", mobileCode=" + mobileCode + ", mobile=" + mobile + ", refereeId="
				+ refereeId + ", refereePro=" + refereePro + ", storeFlag=" + storeFlag + ", comCode=" + comCode + ", branchCode=" + branchCode
				+ ", agrementNo=" + agrementNo + ", agrementName=" + agrementName + ", channelCode=" + channelCode + ", channelName=" + channelName
				+ ", saleCode=" + saleCode + ", saleName=" + saleName + ", areaCode=" + areaCode + ", areaName=" + areaName + ", orgCode=" + orgCode
				+ ", licenceNo=" + licenceNo + ", customerType=" + customerType + ", customerName=" + customerName + ", baseRate=" + baseRate
				+ ", userRealName=" + userRealName + ", canModify=" + canModify + ", canScore=" + canScore + ", defaultAuth=" + defaultAuth
				+ ", upUserCode=" + upUserCode + ", rankChangeType=" + rankChangeType + "]";
	}
}
