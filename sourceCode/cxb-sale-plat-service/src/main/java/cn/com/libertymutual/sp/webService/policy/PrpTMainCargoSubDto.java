
package cn.com.libertymutual.sp.webService.policy;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for prpTMainCargoSubDto complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="prpTMainCargoSubDto">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="brandName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="conveyanceSub" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="flightNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="licenseNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="shipAge" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="shipName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="siteName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="startDateSub" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="tonCount" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="trainName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="trainNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="voyageNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "prpTMainCargoSubDto", propOrder = {
    "brandName",
    "conveyanceSub",
    "flightNo",
    "licenseNo",
    "shipAge",
    "shipName",
    "siteName",
    "startDateSub",
    "tonCount",
    "trainName",
    "trainNo",
    "voyageNo"
})
public class PrpTMainCargoSubDto implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = -5110268373391743959L;
	protected String brandName;
    protected String conveyanceSub;
    protected String flightNo;
    protected String licenseNo;
    protected double shipAge;
    protected String shipName;
    protected String siteName;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar startDateSub;
    protected double tonCount;
    protected String trainName;
    protected String trainNo;
    protected String voyageNo;

    /**
     * Gets the value of the brandName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBrandName() {
        return brandName;
    }

    /**
     * Sets the value of the brandName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBrandName(String value) {
        this.brandName = value;
    }

    /**
     * Gets the value of the conveyanceSub property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConveyanceSub() {
        return conveyanceSub;
    }

    /**
     * Sets the value of the conveyanceSub property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConveyanceSub(String value) {
        this.conveyanceSub = value;
    }

    /**
     * Gets the value of the flightNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFlightNo() {
        return flightNo;
    }

    /**
     * Sets the value of the flightNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFlightNo(String value) {
        this.flightNo = value;
    }

    /**
     * Gets the value of the licenseNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLicenseNo() {
        return licenseNo;
    }

    /**
     * Sets the value of the licenseNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLicenseNo(String value) {
        this.licenseNo = value;
    }

    /**
     * Gets the value of the shipAge property.
     * 
     */
    public double getShipAge() {
        return shipAge;
    }

    /**
     * Sets the value of the shipAge property.
     * 
     */
    public void setShipAge(double value) {
        this.shipAge = value;
    }

    /**
     * Gets the value of the shipName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShipName() {
        return shipName;
    }

    /**
     * Sets the value of the shipName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShipName(String value) {
        this.shipName = value;
    }

    /**
     * Gets the value of the siteName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSiteName() {
        return siteName;
    }

    /**
     * Sets the value of the siteName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSiteName(String value) {
        this.siteName = value;
    }

    /**
     * Gets the value of the startDateSub property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getStartDateSub() {
        return startDateSub;
    }

    /**
     * Sets the value of the startDateSub property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setStartDateSub(XMLGregorianCalendar value) {
        this.startDateSub = value;
    }

    /**
     * Gets the value of the tonCount property.
     * 
     */
    public double getTonCount() {
        return tonCount;
    }

    /**
     * Sets the value of the tonCount property.
     * 
     */
    public void setTonCount(double value) {
        this.tonCount = value;
    }

    /**
     * Gets the value of the trainName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTrainName() {
        return trainName;
    }

    /**
     * Sets the value of the trainName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTrainName(String value) {
        this.trainName = value;
    }

    /**
     * Gets the value of the trainNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTrainNo() {
        return trainNo;
    }

    /**
     * Sets the value of the trainNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTrainNo(String value) {
        this.trainNo = value;
    }

    /**
     * Gets the value of the voyageNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVoyageNo() {
        return voyageNo;
    }

    /**
     * Sets the value of the voyageNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVoyageNo(String value) {
        this.voyageNo = value;
    }

}
