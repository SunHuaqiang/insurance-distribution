package cn.com.libertymutual.production.dao.nomorcldatasource;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import cn.com.libertymutual.production.model.nomorcldatasource.Prpdkind;
import cn.com.libertymutual.production.model.nomorcldatasource.PrpdkindExample;
import cn.com.libertymutual.production.model.nomorcldatasource.PrpdkindKey;
import cn.com.libertymutual.production.model.nomorcldatasource.Prpdkindlibrary;
@Mapper
public interface PrpdkindMapper {
    int countByExample(PrpdkindExample example);

    int deleteByExample(PrpdkindExample example);

    int deleteByPrimaryKey(PrpdkindKey key);

    int insert(Prpdkind record);

    int insertSelective(Prpdkind record);

    List<Prpdkind> selectByExample(PrpdkindExample example);

    Prpdkind selectByPrimaryKey(PrpdkindKey key);

    int updateByExampleSelective(@Param("record") Prpdkind record, @Param("example") PrpdkindExample example);

    int updateByExample(@Param("record") Prpdkind record, @Param("example") PrpdkindExample example);

    int updateByPrimaryKeySelective(Prpdkind record);

    int updateByPrimaryKey(Prpdkind record);
    
    @Select("SELECT 1 FROM prpdkind a WHERE a.kindcode = #{kindCode} and exists (SELECT 1 FROM prpditem b where b.riskcode = a.riskcode)")
    List<Object> checkItemLinked(String kindCode);
    
    @Select(
    		"SELECT * "
    				+ "  FROM PRPDKINDLIBRARY A "
    				+ " WHERE A.VALIDSTATUS = '1' "
    				+ "   AND EXISTS (SELECT 1 "
    				+ "          FROM PRPDKIND B "
    				+ "         WHERE B.KINDCODE = A.KINDCODE "
    				+ "           AND B.KINDVERSION = A.KINDVERSION "
    				+ "           AND B.RISKCODE = #{riskCode} "
    				+ "           AND B.RISKVERSION = #{riskVersion} "
    				+ "           AND B.VALIDSTATUS = '1' "
    				+ "			  AND (SYSDATE() BETWEEN B.STARTDATE AND B.ENDDATE)) "
    		)
	List<Prpdkindlibrary> findKindsByRisk(@Param("riskCode") String riskCode,
                                          @Param("riskVersion") String riskVersion);
}