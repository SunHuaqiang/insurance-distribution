package cn.com.libertymutual.wx.service;

import java.io.InputStream;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.alibaba.fastjson.JSONObject;

import cn.com.libertymutual.core.web.ServiceResult;
import cn.com.libertymutual.sp.bean.TbSpOrder;
import cn.com.libertymutual.sp.bean.TbSpUser;
import cn.com.libertymutual.sp.dto.wechat.RuleReplyMsg;
import cn.com.libertymutual.sp.dto.wechat.TQueryRuleMsgResponseDto;
import cn.com.libertymutual.wx.message.UserToken;
import cn.com.libertymutual.wx.message.WeChatUser;
import cn.com.libertymutual.wx.message.responsedto.ResponseBaseMessage;
import cn.com.libertymutual.wx.message.responsedto.TextMessage;

public interface WeChatService {

	/**
	 * Remarks: 微信域名校验——排序方法<br>
	 * Version：1.0<br>
	 * Author：AoYi<br>
	 * DateTime：2017年11月17日上午11:04:48<br>
	 * Project：liberty_sale_plat<br>
	 * @param timestamp
	 * @param nonce
	 * @return
	 */
	public String sort(String timestamp, String nonce);

	/**
	 * Remarks: 微信域名校验——sha1加密字符串str<br>
	 * Version：1.0<br>
	 * Author：AoYi<br>
	 * DateTime：2017年11月17日上午11:04:41<br>
	 * Project：liberty_sale_plat<br>
	 * @param str
	 * @return
	 */
	public String sha1(String str);

	/**Remarks: 获取当前全局access_token，有效期7200秒，需要进行缓存<br>
	 * Version：1.0<br>
	 * Author：AoYi<br>
	 * DateTime：2017年11月15日上午11:27:53<br>
	 * Project：liberty_sale_plat<br>
	 * @param title 请求标题
	 * @return
	 */
	public String getPublicAccessTokenCurrent(String title);

	/**Remarks: 获取JSSDK Ticket，有效期7200秒，需要进行缓存<br>
	 * Version：1.0<br>
	 * Author：AoYi<br>
	 * DateTime：2017年11月17日上午11:02:06<br>
	 * Project：liberty_sale_plat<br>
	 * @return
	 */
	public String getJSTicket();

	/**Remarks: 获取code,用于获取用户基本信息时获取AccessToken的code<br>
	 * Version：1.0<br>
	 * Author：AoYi<br>
	 * DateTime：2017年11月17日上午11:05:21<br>
	 * Project：liberty_sale_plat<br>
	 * @param REDIRECT_URI
	 * @param type
	 * @return
	 */
	public String getCode(String REDIRECT_URI, String type);

	/**Remarks: 获取用户授权access_token<br>
	 * Version：1.0<br>
	 * Author：AoYi<br>
	 * DateTime：2017年11月17日上午11:05:11<br>
	 * Project：liberty_sale_plat<br>
	 * @param code
	 * @return
	 */
	public UserToken getUserAccessToken(String code);

	/**Remarks: 获取微信用户基本信息<br>
	 * Version：1.0<br>
	 * Author：AoYi<br>
	 * DateTime：2017年11月17日上午10:22:43<br>
	 * Project：liberty_sale_plat<br>
	 * @param userAccessToken
	 * @param openId
	 * @param type
	 * @return
	 */
	public WeChatUser getUserInfo(String userAccessToken, String openId, String type);

	/**Remarks: 过滤表情字符等信息<br>
	 * Version：1.0<br>
	 * Author：AoYi<br>
	 * DateTime：2018年1月23日下午1:46:25<br>
	 * Project：liberty_sale_plat<br>
	 * @param str
	 * @return
	 */
	public String replaceEmoji(String str);

	/**Remarks: 把request请求流转换为字符串<br>
	 * Version：1.0<br>
	 * Author：AoYi<br>
	 * DateTime：2017年11月15日下午4:40:01<br>
	 * Project：liberty_sale_plat<br>
	 * @param in request.getInputStream()返回的值
	 * @return
	 * @throws Exception
	 */
	public String inputStream2String(InputStream in) throws Exception;

	/**Remarks: 解析微信发来的请求（XML），把xml格式内容转化为java map数据集合对象<br>
	 * Version：1.0<br>
	 * Author：AoYi<br>
	 * DateTime：2017年11月15日下午4:39:05<br>
	 * Project：liberty_sale_plat<br>
	 * @param inputXml
	 * @return
	 * @throws Exception
	 */
	public Map<String, String> parseXml(String inputXml) throws Exception;

	/**Remarks: 根据用户操作和发送的消息内容匹配回复规则<br>
	 * Version：1.0<br>
	 * Author：AoYi<br>
	 * DateTime：2017年11月16日下午7:13:25<br>
	 * Project：liberty_sale_plat<br>
	 * @param receiveMap
	 * @return
	 */
	public TQueryRuleMsgResponseDto getRule(Map<String, String> receiveMap);

	/** 根据客户发送的关键字内容进行相应消息发送<br>
	 * Version：1.0<br>
	 * Author：AoYi<br>
	 * DateTime：2018年3月20日下午7:05:01<br>
	 * @param request
	 * @param fromUserName 接收消息方
	 * @param content 接收的用户消息
	 * @param returnXML 返回的XML
	 * @param textMessage 返回信息的文本对象
	 * @return 返回returnXML
	 */
	public Map<String, String> keywordsReplyAndReturnXml(HttpServletRequest request, String fromUserName, String content, String returnXML,
			TextMessage textMessage);

	/**
	 * 微信发送消息
	 * @param json 发送JSON字符串
	 * @return
	 */
	public boolean sendMessage(String json);

	/**Remarks: 发送单个文本消息<br>
	 * Version：1.0<br>
	 * Author：AoYi<br>
	 * DateTime：2017年12月25日下午2:13:02<br>
	 * Project：liberty_sale_plat<br>
	 * @param toUserOpenId
	 * @param msgtype
	 * @param content
	 */
	public boolean sendTextMessageSingle(String toUserOpenId, String msgtype, String content);

	/**Remarks: 微信公众号给店主（分享人）推送消息<br>
	 * Version：1.0<br>
	 * Author：AoYi<br>
	 * DateTime：2018年2月8日上午11:00:34<br>
	 * Project：liberty_sale_plat<br>
	 * @param or
	 * @param remarks
	 * @return
	 */
	public boolean sendOderWeChatMsg(TbSpOrder or, String remarks);

	/**Remarks: 发送单个图文消息<br>
	 * Version：1.0<br>
	 * Author：AoYi<br>
	 * DateTime：2017年11月21日下午1:14:05<br>
	 * Project：liberty_sale_plat<br>
	 * @param fromUserName 接收消息人OpenId
	 * @param title 标题
	 * @param description 描述
	 * @param url 链接地址
	 * @param picurl 图片地址
	 */
	public void sendNewsMessageSingle(String fromUserName, String title, String description, String url, String picurl);

	/**
	 * 调用微信接口发送JSON数据
	 * @param url
	 * @param paramsMap
	 * @return
	 */
	public JSONObject postData(String url, String jsonStr);

	/**
	 * 获取用户所在分组
	 */
	public String getUserGroupIdByOpenId(String openId);

	/**Remarks: 微信公众平台服务号自动[回复]个人信息<br>
	 * Version：1.0<br>
	 * Author：AoYi<br>
	 * DateTime：2017年11月18日下午4:11:51<br>
	 * Project：liberty_sale_plat<br>
	 * @param returnXML
	 * @param receiveMap
	 * @param outputMessage
	 * @param textMessage
	 * @param ruleReplyMsg
	 * @return
	 */
	public String returnSingleMsg(String returnXML, Map<String, String> receiveMap, ResponseBaseMessage outputMessage, TextMessage textMessage,
			RuleReplyMsg ruleReplyMsg);

	/**
	 * Remarks: 将WeChatUser对象的属性值映射到TbSpUser中，如果有映射则返回true<br>
	 * Version：1.0<br>
	 * Author：AoYi<br>
	 * DateTime：2018年1月22日上午11:20:02<br>
	 * Project：liberty_sale_plat<br>
	 * @param weChatUser
	 * @param user
	 * @param isMust
	 */
	public boolean copyWeChatToBbean(WeChatUser weChatUser, TbSpUser user, boolean isMust);

	public ServiceResult reissue(String userCode, String type, String policyNo, String activityName, Integer beans, Integer times);

}
