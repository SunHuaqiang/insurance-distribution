package cn.com.libertymutual.production.service.api.business;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import cn.com.libertymutual.production.pojo.request.PrpdEFileRequest;
import cn.com.libertymutual.production.pojo.response.Response;
import cn.com.libertymutual.production.service.api.PrpdEFileService;
import cn.com.libertymutual.production.service.api.PrpdKindLibraryService;
import cn.com.libertymutual.production.service.impl.SystemLog;
import cn.com.libertymutual.production.utils.Constant;
import cn.com.libertymutual.production.utils.ftp.ProductionFtpSetting;

/** 
 * @Description: 后台业务逻辑入口
 * @author Steven.Li
 * @date 2017年7月28日
 *  
 */
public abstract class EFileBusinessService {

	protected static final Logger log = Constant.log;
	
	@Autowired
	protected SystemLog systemLog;
	@Autowired
	protected PrpdEFileService prpdEFileService;
	@Autowired
	@Qualifier("singleRestTemplate")
	protected RestTemplate uploadTemplate;
	@Autowired
	protected ProductionFtpSetting productionFtpSetting;
	@Autowired
	protected PrpdKindLibraryService prpdKindLibraryService;
	
	/**
	 * 获取备案号基本信息
	 * @param prpdEFileRequest
	 * @return
	 */
	public abstract Response findPrpdEFile(PrpdEFileRequest prpdEFileRequest);
	
	/**
	 * 新增备案号
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public abstract Response insertEfile(MultipartHttpServletRequest fileRequest, PrpdEFileRequest prpdEFileRequest) throws Exception;
	
	/**
	 * 修改备案号
	 * @param fileRequest
	 * @param prpdEFileRequest
	 * @return
	 * @throws Exception
	 */
	public abstract Response updateEfile(MultipartHttpServletRequest fileRequest, PrpdEFileRequest prpdEFileRequest) throws Exception;
	
	/**
	 * 下载上传的备案号图片
	 * @param req
	 * @param response
	 * @return
	 */
	public abstract Response download(String fileNm,HttpServletResponse response);
}