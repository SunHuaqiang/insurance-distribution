package cn.com.libertymutual.production.model.nomorcldatasource;

import java.util.Date;

public class TbSpUser {
    private Integer id;

    private String userCode;

    private String userCodeBs;

    private String wxOpenId;

    private String employeeCode;

    private String userName;

    private String nickName;

    private String password;

    private String mobile;

    private String idNumber;

    private String channelType;

    private String customName;

    private String email;

    private String userType;

    private String applyAgrnoType;

    private String agrementNo;

    private String agrementName;

    private String branchCode;

    private String comCode;

    private String relationship;

    private String saleCode;

    private String saleName;

    private String channelCode;

    private String channelName;

    private String areaCode;

    private String areaName;

    private String introduceId;

    private String headUrl;

    private String wxSubscribe;

    private String wxSex;

    private String wxCity;

    private String wxCountry;

    private String wxProvince;

    private String wxLanguage;

    private Date wxSubscribeTime;

    private String wxUnionid;

    private String wxRemark;

    private String wxGroupid;

    private String wxTagidList;

    private Integer points;

    private Date registeDate;

    private Date lastloginDate;

    private Integer loginTimes;

    private Integer loginTimesContinuity;

    private String shippingAddress;

    private String level;

    private Date storeCreateTime;

    private String storeFlag;

    private String loginState;

    private String state;

    private String refereeId;

    private String typeState;

    private Integer popularity;

    private String refereePro;

    private String shopIntroduc;

    private String shopName;

    private String shopBgImg;

    private Integer configSerialno;

    private String idType;

    private Double baseRate;

    private String registerType;

    private String canModify;

    private String canScore;

    private String upUsercode;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode == null ? null : userCode.trim();
    }

    public String getUserCodeBs() {
        return userCodeBs;
    }

    public void setUserCodeBs(String userCodeBs) {
        this.userCodeBs = userCodeBs == null ? null : userCodeBs.trim();
    }

    public String getWxOpenId() {
        return wxOpenId;
    }

    public void setWxOpenId(String wxOpenId) {
        this.wxOpenId = wxOpenId == null ? null : wxOpenId.trim();
    }

    public String getEmployeeCode() {
        return employeeCode;
    }

    public void setEmployeeCode(String employeeCode) {
        this.employeeCode = employeeCode == null ? null : employeeCode.trim();
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName == null ? null : userName.trim();
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName == null ? null : nickName.trim();
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password == null ? null : password.trim();
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile == null ? null : mobile.trim();
    }

    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber == null ? null : idNumber.trim();
    }

    public String getChannelType() {
        return channelType;
    }

    public void setChannelType(String channelType) {
        this.channelType = channelType == null ? null : channelType.trim();
    }

    public String getCustomName() {
        return customName;
    }

    public void setCustomName(String customName) {
        this.customName = customName == null ? null : customName.trim();
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email == null ? null : email.trim();
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType == null ? null : userType.trim();
    }

    public String getApplyAgrnoType() {
        return applyAgrnoType;
    }

    public void setApplyAgrnoType(String applyAgrnoType) {
        this.applyAgrnoType = applyAgrnoType == null ? null : applyAgrnoType.trim();
    }

    public String getAgrementNo() {
        return agrementNo;
    }

    public void setAgrementNo(String agrementNo) {
        this.agrementNo = agrementNo == null ? null : agrementNo.trim();
    }

    public String getAgrementName() {
        return agrementName;
    }

    public void setAgrementName(String agrementName) {
        this.agrementName = agrementName == null ? null : agrementName.trim();
    }

    public String getBranchCode() {
        return branchCode;
    }

    public void setBranchCode(String branchCode) {
        this.branchCode = branchCode == null ? null : branchCode.trim();
    }

    public String getComCode() {
        return comCode;
    }

    public void setComCode(String comCode) {
        this.comCode = comCode == null ? null : comCode.trim();
    }

    public String getRelationship() {
        return relationship;
    }

    public void setRelationship(String relationship) {
        this.relationship = relationship == null ? null : relationship.trim();
    }

    public String getSaleCode() {
        return saleCode;
    }

    public void setSaleCode(String saleCode) {
        this.saleCode = saleCode == null ? null : saleCode.trim();
    }

    public String getSaleName() {
        return saleName;
    }

    public void setSaleName(String saleName) {
        this.saleName = saleName == null ? null : saleName.trim();
    }

    public String getChannelCode() {
        return channelCode;
    }

    public void setChannelCode(String channelCode) {
        this.channelCode = channelCode == null ? null : channelCode.trim();
    }

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName == null ? null : channelName.trim();
    }

    public String getAreaCode() {
        return areaCode;
    }

    public void setAreaCode(String areaCode) {
        this.areaCode = areaCode == null ? null : areaCode.trim();
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName == null ? null : areaName.trim();
    }

    public String getIntroduceId() {
        return introduceId;
    }

    public void setIntroduceId(String introduceId) {
        this.introduceId = introduceId == null ? null : introduceId.trim();
    }

    public String getHeadUrl() {
        return headUrl;
    }

    public void setHeadUrl(String headUrl) {
        this.headUrl = headUrl == null ? null : headUrl.trim();
    }

    public String getWxSubscribe() {
        return wxSubscribe;
    }

    public void setWxSubscribe(String wxSubscribe) {
        this.wxSubscribe = wxSubscribe == null ? null : wxSubscribe.trim();
    }

    public String getWxSex() {
        return wxSex;
    }

    public void setWxSex(String wxSex) {
        this.wxSex = wxSex == null ? null : wxSex.trim();
    }

    public String getWxCity() {
        return wxCity;
    }

    public void setWxCity(String wxCity) {
        this.wxCity = wxCity == null ? null : wxCity.trim();
    }

    public String getWxCountry() {
        return wxCountry;
    }

    public void setWxCountry(String wxCountry) {
        this.wxCountry = wxCountry == null ? null : wxCountry.trim();
    }

    public String getWxProvince() {
        return wxProvince;
    }

    public void setWxProvince(String wxProvince) {
        this.wxProvince = wxProvince == null ? null : wxProvince.trim();
    }

    public String getWxLanguage() {
        return wxLanguage;
    }

    public void setWxLanguage(String wxLanguage) {
        this.wxLanguage = wxLanguage == null ? null : wxLanguage.trim();
    }

    public Date getWxSubscribeTime() {
        return wxSubscribeTime;
    }

    public void setWxSubscribeTime(Date wxSubscribeTime) {
        this.wxSubscribeTime = wxSubscribeTime;
    }

    public String getWxUnionid() {
        return wxUnionid;
    }

    public void setWxUnionid(String wxUnionid) {
        this.wxUnionid = wxUnionid == null ? null : wxUnionid.trim();
    }

    public String getWxRemark() {
        return wxRemark;
    }

    public void setWxRemark(String wxRemark) {
        this.wxRemark = wxRemark == null ? null : wxRemark.trim();
    }

    public String getWxGroupid() {
        return wxGroupid;
    }

    public void setWxGroupid(String wxGroupid) {
        this.wxGroupid = wxGroupid == null ? null : wxGroupid.trim();
    }

    public String getWxTagidList() {
        return wxTagidList;
    }

    public void setWxTagidList(String wxTagidList) {
        this.wxTagidList = wxTagidList == null ? null : wxTagidList.trim();
    }

    public Integer getPoints() {
        return points;
    }

    public void setPoints(Integer points) {
        this.points = points;
    }

    public Date getRegisteDate() {
        return registeDate;
    }

    public void setRegisteDate(Date registeDate) {
        this.registeDate = registeDate;
    }

    public Date getLastloginDate() {
        return lastloginDate;
    }

    public void setLastloginDate(Date lastloginDate) {
        this.lastloginDate = lastloginDate;
    }

    public Integer getLoginTimes() {
        return loginTimes;
    }

    public void setLoginTimes(Integer loginTimes) {
        this.loginTimes = loginTimes;
    }

    public Integer getLoginTimesContinuity() {
        return loginTimesContinuity;
    }

    public void setLoginTimesContinuity(Integer loginTimesContinuity) {
        this.loginTimesContinuity = loginTimesContinuity;
    }

    public String getShippingAddress() {
        return shippingAddress;
    }

    public void setShippingAddress(String shippingAddress) {
        this.shippingAddress = shippingAddress == null ? null : shippingAddress.trim();
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level == null ? null : level.trim();
    }

    public Date getStoreCreateTime() {
        return storeCreateTime;
    }

    public void setStoreCreateTime(Date storeCreateTime) {
        this.storeCreateTime = storeCreateTime;
    }

    public String getStoreFlag() {
        return storeFlag;
    }

    public void setStoreFlag(String storeFlag) {
        this.storeFlag = storeFlag == null ? null : storeFlag.trim();
    }

    public String getLoginState() {
        return loginState;
    }

    public void setLoginState(String loginState) {
        this.loginState = loginState == null ? null : loginState.trim();
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state == null ? null : state.trim();
    }

    public String getRefereeId() {
        return refereeId;
    }

    public void setRefereeId(String refereeId) {
        this.refereeId = refereeId == null ? null : refereeId.trim();
    }

    public String getTypeState() {
        return typeState;
    }

    public void setTypeState(String typeState) {
        this.typeState = typeState == null ? null : typeState.trim();
    }

    public Integer getPopularity() {
        return popularity;
    }

    public void setPopularity(Integer popularity) {
        this.popularity = popularity;
    }

    public String getRefereePro() {
        return refereePro;
    }

    public void setRefereePro(String refereePro) {
        this.refereePro = refereePro == null ? null : refereePro.trim();
    }

    public String getShopIntroduc() {
        return shopIntroduc;
    }

    public void setShopIntroduc(String shopIntroduc) {
        this.shopIntroduc = shopIntroduc == null ? null : shopIntroduc.trim();
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName == null ? null : shopName.trim();
    }

    public String getShopBgImg() {
        return shopBgImg;
    }

    public void setShopBgImg(String shopBgImg) {
        this.shopBgImg = shopBgImg == null ? null : shopBgImg.trim();
    }

    public Integer getConfigSerialno() {
        return configSerialno;
    }

    public void setConfigSerialno(Integer configSerialno) {
        this.configSerialno = configSerialno;
    }

    public String getIdType() {
        return idType;
    }

    public void setIdType(String idType) {
        this.idType = idType == null ? null : idType.trim();
    }

    public Double getBaseRate() {
        return baseRate;
    }

    public void setBaseRate(Double baseRate) {
        this.baseRate = baseRate;
    }

    public String getRegisterType() {
        return registerType;
    }

    public void setRegisterType(String registerType) {
        this.registerType = registerType == null ? null : registerType.trim();
    }

    public String getCanModify() {
        return canModify;
    }

    public void setCanModify(String canModify) {
        this.canModify = canModify == null ? null : canModify.trim();
    }

    public String getCanScore() {
        return canScore;
    }

    public void setCanScore(String canScore) {
        this.canScore = canScore == null ? null : canScore.trim();
    }

    public String getUpUsercode() {
        return upUsercode;
    }

    public void setUpUsercode(String upUsercode) {
        this.upUsercode = upUsercode == null ? null : upUsercode.trim();
    }
}