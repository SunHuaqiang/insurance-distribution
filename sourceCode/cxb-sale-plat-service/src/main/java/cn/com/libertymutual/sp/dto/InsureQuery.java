package cn.com.libertymutual.sp.dto;

import java.util.List;

import cn.com.libertymutual.sp.dto.car.dto.TPrptItemKindDto;

public class InsureQuery {
	private Double amount;
	private String areaCode;
	private String branchCode; // 分公司
	private String callBackUrl;
	private String channelName;
	private String city;
	private String clauseUrl; // 条款下载url
	private String deadLine;
	private String dealUid;
	private Boolean isHotArea;
	private String planId;
	private String planName;
	private String proRiskCode;
	private String productId;// 产品id
	private String productName;// 产品名称
	private String refereeId; // 推荐人id
	private String risk;// 险种
	private String riskName;// 险种名称
	private String shareUid;
	private String singleMember;
	private String successfulUrl;
	private String accessUrl;
	private String minAge;
	private Boolean isNeedUpload;
	private String otherInfo;
	private String premium; // 商业险中总保费
	private String mtplPremium;// 交强险总保费
	private String agreementCode;
	private String salerName;
	private String saleCode;

	public String getAgreementCode() {
		return agreementCode;
	}

	public void setAgreementCode(String agreementCode) {
		this.agreementCode = agreementCode;
	}

	public String getSalerName() {
		return salerName;
	}

	public void setSalerName(String salerName) {
		this.salerName = salerName;
	}

	public String getSaleCode() {
		return saleCode;
	}

	public void setSaleCode(String saleCode) {
		this.saleCode = saleCode;
	}

	private List<TPrptItemKindDto> tprpTitemKindListDto;

	public List<TPrptItemKindDto> getTprpTitemKindListDto() {
		return tprpTitemKindListDto;
	}

	public void setTprpTitemKindListDto(List<TPrptItemKindDto> tprpTitemKindListDto) {
		this.tprpTitemKindListDto = tprpTitemKindListDto;
	}

	public String getPremium() {
		return premium;
	}

	public void setPremium(String premium) {
		this.premium = premium;
	}

	public String getMtplPremium() {
		return mtplPremium;
	}

	public void setMtplPremium(String mtplPremium) {
		this.mtplPremium = mtplPremium;
	}

	public String getOtherInfo() {
		return otherInfo;
	}

	public void setOtherInfo(String otherInfo) {
		this.otherInfo = otherInfo;
	}

	public Boolean getIsNeedUpload() {
		return isNeedUpload;
	}

	public void setIsNeedUpload(Boolean isNeedUpload) {
		this.isNeedUpload = isNeedUpload;
	}

	public Double getAmount() {
		return amount;
	}

	public String getMinAge() {
		return minAge;
	}

	public void setMinAge(String minAge) {
		this.minAge = minAge;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public String getAreaCode() {
		return areaCode;
	}

	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}

	public String getBranchCode() {
		return branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	public String getCallBackUrl() {
		return callBackUrl;
	}

	public void setCallBackUrl(String callBackUrl) {
		this.callBackUrl = callBackUrl;
	}

	public String getChannelName() {
		return channelName;
	}

	public void setChannelName(String channelName) {
		this.channelName = channelName;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getClauseUrl() {
		return clauseUrl;
	}

	public void setClauseUrl(String clauseUrl) {
		this.clauseUrl = clauseUrl;
	}

	public String getDeadLine() {
		return deadLine;
	}

	public void setDeadLine(String deadLine) {
		this.deadLine = deadLine;
	}

	public String getDealUid() {
		return dealUid;
	}

	public void setDealUid(String dealUid) {
		this.dealUid = dealUid;
	}

	public Boolean getIsHotArea() {
		return isHotArea;
	}

	public void setIsHotArea(Boolean isHotArea) {
		this.isHotArea = isHotArea;
	}

	public String getPlanId() {
		return planId;
	}

	public void setPlanId(String planId) {
		this.planId = planId;
	}

	public String getPlanName() {
		return planName;
	}

	public void setPlanName(String planName) {
		this.planName = planName;
	}

	public String getProRiskCode() {
		return proRiskCode;
	}

	public void setProRiskCode(String proRiskCode) {
		this.proRiskCode = proRiskCode;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getRefereeId() {
		return refereeId;
	}

	public void setRefereeId(String refereeId) {
		this.refereeId = refereeId;
	}

	public String getRisk() {
		return risk;
	}

	public void setRisk(String risk) {
		this.risk = risk;
	}

	public String getRiskName() {
		return riskName;
	}

	public void setRiskName(String riskName) {
		this.riskName = riskName;
	}

	public String getShareUid() {
		return shareUid;
	}

	public void setShareUid(String shareUid) {
		this.shareUid = shareUid;
	}

	public String getSingleMember() {
		return singleMember;
	}

	public void setSingleMember(String singleMember) {
		this.singleMember = singleMember;
	}

	public String getSuccessfulUrl() {
		return successfulUrl;
	}

	public void setSuccessfulUrl(String successfulUrl) {
		this.successfulUrl = successfulUrl;
	}

	public String getAccessUrl() {
		return accessUrl;
	}

	public void setAccessUrl(String accessUrl) {
		this.accessUrl = accessUrl;
	}

}
