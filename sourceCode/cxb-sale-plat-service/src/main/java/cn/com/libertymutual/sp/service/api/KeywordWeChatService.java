package cn.com.libertymutual.sp.service.api;

import cn.com.libertymutual.core.web.ServiceResult;
import cn.com.libertymutual.sp.bean.TbSpKeywordWeChat;

public interface KeywordWeChatService {

	ServiceResult insertKeyWordwechat(TbSpKeywordWeChat keywordWeChat) throws Exception;

	ServiceResult updateKeyWordwechat(TbSpKeywordWeChat keywordWeChat) throws Exception;

	ServiceResult weChatKeywords(String keyName, String keyWord, String type, Integer pageNumber, Integer pageSize);

}