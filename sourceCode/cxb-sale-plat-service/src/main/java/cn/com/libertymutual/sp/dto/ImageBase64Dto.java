package cn.com.libertymutual.sp.dto;

import java.io.Serializable;
import java.util.List;

import io.swagger.annotations.ApiModel;

@ApiModel
public class ImageBase64Dto implements Serializable {

	/**
	 * base64多图对象
	 */
	private static final long serialVersionUID = 867647541169468179L;

	private List<String> images;

	public List<String> getImages() {
		return images;
	}

	public void setImages(List<String> images) {
		this.images = images;
	}
}
