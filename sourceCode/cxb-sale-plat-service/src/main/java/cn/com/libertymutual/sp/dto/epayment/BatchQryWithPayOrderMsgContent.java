package cn.com.libertymutual.sp.dto.epayment;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BatchQryWithPayOrderMsgContent", propOrder = {
    "txnTotal",
    "txnListCount",
    "withPayOrderListContent"
})
public class BatchQryWithPayOrderMsgContent implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 476079556291217497L;

	
	private String txnTotal;//总记录数
	 @XmlElement(name = "TxnListCount")
	private String txnListCount;//查询结果记录返回数
	 @XmlElement(name = "WithPayOrderListContent")
	private WithPayOrderListContent withPayOrderListContent;
	public String getTxnTotal() {
		return txnTotal;
	}
	public void setTxnTotal(String txnTotal) {
		this.txnTotal = txnTotal;
	}
	public String getTxnListCount() {
		return txnListCount;
	}
	public void setTxnListCount(String txnListCount) {
		this.txnListCount = txnListCount;
	}
	public WithPayOrderListContent getWithPayOrderListContent() {
		return withPayOrderListContent;
	}
	public void setWithPayOrderListContent(
			WithPayOrderListContent withPayOrderListContent) {
		this.withPayOrderListContent = withPayOrderListContent;
	}
	
	
}
