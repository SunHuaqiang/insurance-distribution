package cn.com.libertymutual.production.pojo.response;

import cn.com.libertymutual.production.model.nomorcldatasource.Prpditemlibrary;

public class ItemKind extends Prpditemlibrary {

	private String kindcode;
	private String kindversion;
	private String riskcode;
	private String riskversion;
	private String owneritemcode;
	private String key;

	public String getKindcode() {
		return kindcode;
	}

	public void setKindcode(String kindcode) {
		this.kindcode = kindcode;
	}

	public String getKindversion() {
		return kindversion;
	}

	public void setKindversion(String kindversion) {
		this.kindversion = kindversion;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getRiskcode() {
		return riskcode;
	}

	public void setRiskcode(String riskcode) {
		this.riskcode = riskcode;
	}

	public String getRiskversion() {
		return riskversion;
	}

	public void setRiskversion(String riskversion) {
		this.riskversion = riskversion;
	}

	public String getOwneritemcode() {
		return owneritemcode;
	}

	public void setOwneritemcode(String owneritemcode) {
		this.owneritemcode = owneritemcode;
	}

}
