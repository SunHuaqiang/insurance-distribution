package cn.com.libertymutual.sp.service.api;

import cn.com.libertymutual.core.web.ServiceResult;
import cn.com.libertymutual.sp.dto.CommonQueryRequestDTO;
import cn.com.libertymutual.sp.dto.savePlan.PropsalSaveRequestDto;

public interface AxtxService {
	ServiceResult insure(PropsalSaveRequestDto request);

	ServiceResult findVin(CommonQueryRequestDTO querydto);

}
