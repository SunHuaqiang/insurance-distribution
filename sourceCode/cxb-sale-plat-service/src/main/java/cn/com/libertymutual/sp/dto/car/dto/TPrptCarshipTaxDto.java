package cn.com.libertymutual.sp.dto.car.dto;

import java.io.Serializable;

@SuppressWarnings("serial")
public class TPrptCarshipTaxDto implements Serializable {
	private String taxpayerName;
	private String taxPayerIdentificationCode;

	public String getTaxpayerName() {
		return taxpayerName;
	}

	public void setTaxpayerName(String taxpayerName) {
		this.taxpayerName = taxpayerName;
	}

	public String getTaxPayerIdentificationCode() {
		return taxPayerIdentificationCode;
	}

	public void setTaxPayerIdentificationCode(String taxPayerIdentificationCode) {
		this.taxPayerIdentificationCode = taxPayerIdentificationCode;
	}

}
