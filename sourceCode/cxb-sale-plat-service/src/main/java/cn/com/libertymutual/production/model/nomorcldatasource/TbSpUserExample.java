package cn.com.libertymutual.production.model.nomorcldatasource;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class TbSpUserExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public TbSpUserExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("ID is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("ID is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("ID =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("ID <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("ID >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("ID >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("ID <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("ID <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("ID in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("ID not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("ID between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("ID not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andUserCodeIsNull() {
            addCriterion("USER_CODE is null");
            return (Criteria) this;
        }

        public Criteria andUserCodeIsNotNull() {
            addCriterion("USER_CODE is not null");
            return (Criteria) this;
        }

        public Criteria andUserCodeEqualTo(String value) {
            addCriterion("USER_CODE =", value, "userCode");
            return (Criteria) this;
        }

        public Criteria andUserCodeNotEqualTo(String value) {
            addCriterion("USER_CODE <>", value, "userCode");
            return (Criteria) this;
        }

        public Criteria andUserCodeGreaterThan(String value) {
            addCriterion("USER_CODE >", value, "userCode");
            return (Criteria) this;
        }

        public Criteria andUserCodeGreaterThanOrEqualTo(String value) {
            addCriterion("USER_CODE >=", value, "userCode");
            return (Criteria) this;
        }

        public Criteria andUserCodeLessThan(String value) {
            addCriterion("USER_CODE <", value, "userCode");
            return (Criteria) this;
        }

        public Criteria andUserCodeLessThanOrEqualTo(String value) {
            addCriterion("USER_CODE <=", value, "userCode");
            return (Criteria) this;
        }

        public Criteria andUserCodeLike(String value) {
            addCriterion("USER_CODE like", value, "userCode");
            return (Criteria) this;
        }

        public Criteria andUserCodeNotLike(String value) {
            addCriterion("USER_CODE not like", value, "userCode");
            return (Criteria) this;
        }

        public Criteria andUserCodeIn(List<String> values) {
            addCriterion("USER_CODE in", values, "userCode");
            return (Criteria) this;
        }

        public Criteria andUserCodeNotIn(List<String> values) {
            addCriterion("USER_CODE not in", values, "userCode");
            return (Criteria) this;
        }

        public Criteria andUserCodeBetween(String value1, String value2) {
            addCriterion("USER_CODE between", value1, value2, "userCode");
            return (Criteria) this;
        }

        public Criteria andUserCodeNotBetween(String value1, String value2) {
            addCriterion("USER_CODE not between", value1, value2, "userCode");
            return (Criteria) this;
        }

        public Criteria andUserCodeBsIsNull() {
            addCriterion("USER_CODE_BS is null");
            return (Criteria) this;
        }

        public Criteria andUserCodeBsIsNotNull() {
            addCriterion("USER_CODE_BS is not null");
            return (Criteria) this;
        }

        public Criteria andUserCodeBsEqualTo(String value) {
            addCriterion("USER_CODE_BS =", value, "userCodeBs");
            return (Criteria) this;
        }

        public Criteria andUserCodeBsNotEqualTo(String value) {
            addCriterion("USER_CODE_BS <>", value, "userCodeBs");
            return (Criteria) this;
        }

        public Criteria andUserCodeBsGreaterThan(String value) {
            addCriterion("USER_CODE_BS >", value, "userCodeBs");
            return (Criteria) this;
        }

        public Criteria andUserCodeBsGreaterThanOrEqualTo(String value) {
            addCriterion("USER_CODE_BS >=", value, "userCodeBs");
            return (Criteria) this;
        }

        public Criteria andUserCodeBsLessThan(String value) {
            addCriterion("USER_CODE_BS <", value, "userCodeBs");
            return (Criteria) this;
        }

        public Criteria andUserCodeBsLessThanOrEqualTo(String value) {
            addCriterion("USER_CODE_BS <=", value, "userCodeBs");
            return (Criteria) this;
        }

        public Criteria andUserCodeBsLike(String value) {
            addCriterion("USER_CODE_BS like", value, "userCodeBs");
            return (Criteria) this;
        }

        public Criteria andUserCodeBsNotLike(String value) {
            addCriterion("USER_CODE_BS not like", value, "userCodeBs");
            return (Criteria) this;
        }

        public Criteria andUserCodeBsIn(List<String> values) {
            addCriterion("USER_CODE_BS in", values, "userCodeBs");
            return (Criteria) this;
        }

        public Criteria andUserCodeBsNotIn(List<String> values) {
            addCriterion("USER_CODE_BS not in", values, "userCodeBs");
            return (Criteria) this;
        }

        public Criteria andUserCodeBsBetween(String value1, String value2) {
            addCriterion("USER_CODE_BS between", value1, value2, "userCodeBs");
            return (Criteria) this;
        }

        public Criteria andUserCodeBsNotBetween(String value1, String value2) {
            addCriterion("USER_CODE_BS not between", value1, value2, "userCodeBs");
            return (Criteria) this;
        }

        public Criteria andWxOpenIdIsNull() {
            addCriterion("WX_OPEN_ID is null");
            return (Criteria) this;
        }

        public Criteria andWxOpenIdIsNotNull() {
            addCriterion("WX_OPEN_ID is not null");
            return (Criteria) this;
        }

        public Criteria andWxOpenIdEqualTo(String value) {
            addCriterion("WX_OPEN_ID =", value, "wxOpenId");
            return (Criteria) this;
        }

        public Criteria andWxOpenIdNotEqualTo(String value) {
            addCriterion("WX_OPEN_ID <>", value, "wxOpenId");
            return (Criteria) this;
        }

        public Criteria andWxOpenIdGreaterThan(String value) {
            addCriterion("WX_OPEN_ID >", value, "wxOpenId");
            return (Criteria) this;
        }

        public Criteria andWxOpenIdGreaterThanOrEqualTo(String value) {
            addCriterion("WX_OPEN_ID >=", value, "wxOpenId");
            return (Criteria) this;
        }

        public Criteria andWxOpenIdLessThan(String value) {
            addCriterion("WX_OPEN_ID <", value, "wxOpenId");
            return (Criteria) this;
        }

        public Criteria andWxOpenIdLessThanOrEqualTo(String value) {
            addCriterion("WX_OPEN_ID <=", value, "wxOpenId");
            return (Criteria) this;
        }

        public Criteria andWxOpenIdLike(String value) {
            addCriterion("WX_OPEN_ID like", value, "wxOpenId");
            return (Criteria) this;
        }

        public Criteria andWxOpenIdNotLike(String value) {
            addCriterion("WX_OPEN_ID not like", value, "wxOpenId");
            return (Criteria) this;
        }

        public Criteria andWxOpenIdIn(List<String> values) {
            addCriterion("WX_OPEN_ID in", values, "wxOpenId");
            return (Criteria) this;
        }

        public Criteria andWxOpenIdNotIn(List<String> values) {
            addCriterion("WX_OPEN_ID not in", values, "wxOpenId");
            return (Criteria) this;
        }

        public Criteria andWxOpenIdBetween(String value1, String value2) {
            addCriterion("WX_OPEN_ID between", value1, value2, "wxOpenId");
            return (Criteria) this;
        }

        public Criteria andWxOpenIdNotBetween(String value1, String value2) {
            addCriterion("WX_OPEN_ID not between", value1, value2, "wxOpenId");
            return (Criteria) this;
        }

        public Criteria andEmployeeCodeIsNull() {
            addCriterion("EMPLOYEE_CODE is null");
            return (Criteria) this;
        }

        public Criteria andEmployeeCodeIsNotNull() {
            addCriterion("EMPLOYEE_CODE is not null");
            return (Criteria) this;
        }

        public Criteria andEmployeeCodeEqualTo(String value) {
            addCriterion("EMPLOYEE_CODE =", value, "employeeCode");
            return (Criteria) this;
        }

        public Criteria andEmployeeCodeNotEqualTo(String value) {
            addCriterion("EMPLOYEE_CODE <>", value, "employeeCode");
            return (Criteria) this;
        }

        public Criteria andEmployeeCodeGreaterThan(String value) {
            addCriterion("EMPLOYEE_CODE >", value, "employeeCode");
            return (Criteria) this;
        }

        public Criteria andEmployeeCodeGreaterThanOrEqualTo(String value) {
            addCriterion("EMPLOYEE_CODE >=", value, "employeeCode");
            return (Criteria) this;
        }

        public Criteria andEmployeeCodeLessThan(String value) {
            addCriterion("EMPLOYEE_CODE <", value, "employeeCode");
            return (Criteria) this;
        }

        public Criteria andEmployeeCodeLessThanOrEqualTo(String value) {
            addCriterion("EMPLOYEE_CODE <=", value, "employeeCode");
            return (Criteria) this;
        }

        public Criteria andEmployeeCodeLike(String value) {
            addCriterion("EMPLOYEE_CODE like", value, "employeeCode");
            return (Criteria) this;
        }

        public Criteria andEmployeeCodeNotLike(String value) {
            addCriterion("EMPLOYEE_CODE not like", value, "employeeCode");
            return (Criteria) this;
        }

        public Criteria andEmployeeCodeIn(List<String> values) {
            addCriterion("EMPLOYEE_CODE in", values, "employeeCode");
            return (Criteria) this;
        }

        public Criteria andEmployeeCodeNotIn(List<String> values) {
            addCriterion("EMPLOYEE_CODE not in", values, "employeeCode");
            return (Criteria) this;
        }

        public Criteria andEmployeeCodeBetween(String value1, String value2) {
            addCriterion("EMPLOYEE_CODE between", value1, value2, "employeeCode");
            return (Criteria) this;
        }

        public Criteria andEmployeeCodeNotBetween(String value1, String value2) {
            addCriterion("EMPLOYEE_CODE not between", value1, value2, "employeeCode");
            return (Criteria) this;
        }

        public Criteria andUserNameIsNull() {
            addCriterion("USER_NAME is null");
            return (Criteria) this;
        }

        public Criteria andUserNameIsNotNull() {
            addCriterion("USER_NAME is not null");
            return (Criteria) this;
        }

        public Criteria andUserNameEqualTo(String value) {
            addCriterion("USER_NAME =", value, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameNotEqualTo(String value) {
            addCriterion("USER_NAME <>", value, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameGreaterThan(String value) {
            addCriterion("USER_NAME >", value, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameGreaterThanOrEqualTo(String value) {
            addCriterion("USER_NAME >=", value, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameLessThan(String value) {
            addCriterion("USER_NAME <", value, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameLessThanOrEqualTo(String value) {
            addCriterion("USER_NAME <=", value, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameLike(String value) {
            addCriterion("USER_NAME like", value, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameNotLike(String value) {
            addCriterion("USER_NAME not like", value, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameIn(List<String> values) {
            addCriterion("USER_NAME in", values, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameNotIn(List<String> values) {
            addCriterion("USER_NAME not in", values, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameBetween(String value1, String value2) {
            addCriterion("USER_NAME between", value1, value2, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameNotBetween(String value1, String value2) {
            addCriterion("USER_NAME not between", value1, value2, "userName");
            return (Criteria) this;
        }

        public Criteria andNickNameIsNull() {
            addCriterion("NICK_NAME is null");
            return (Criteria) this;
        }

        public Criteria andNickNameIsNotNull() {
            addCriterion("NICK_NAME is not null");
            return (Criteria) this;
        }

        public Criteria andNickNameEqualTo(String value) {
            addCriterion("NICK_NAME =", value, "nickName");
            return (Criteria) this;
        }

        public Criteria andNickNameNotEqualTo(String value) {
            addCriterion("NICK_NAME <>", value, "nickName");
            return (Criteria) this;
        }

        public Criteria andNickNameGreaterThan(String value) {
            addCriterion("NICK_NAME >", value, "nickName");
            return (Criteria) this;
        }

        public Criteria andNickNameGreaterThanOrEqualTo(String value) {
            addCriterion("NICK_NAME >=", value, "nickName");
            return (Criteria) this;
        }

        public Criteria andNickNameLessThan(String value) {
            addCriterion("NICK_NAME <", value, "nickName");
            return (Criteria) this;
        }

        public Criteria andNickNameLessThanOrEqualTo(String value) {
            addCriterion("NICK_NAME <=", value, "nickName");
            return (Criteria) this;
        }

        public Criteria andNickNameLike(String value) {
            addCriterion("NICK_NAME like", value, "nickName");
            return (Criteria) this;
        }

        public Criteria andNickNameNotLike(String value) {
            addCriterion("NICK_NAME not like", value, "nickName");
            return (Criteria) this;
        }

        public Criteria andNickNameIn(List<String> values) {
            addCriterion("NICK_NAME in", values, "nickName");
            return (Criteria) this;
        }

        public Criteria andNickNameNotIn(List<String> values) {
            addCriterion("NICK_NAME not in", values, "nickName");
            return (Criteria) this;
        }

        public Criteria andNickNameBetween(String value1, String value2) {
            addCriterion("NICK_NAME between", value1, value2, "nickName");
            return (Criteria) this;
        }

        public Criteria andNickNameNotBetween(String value1, String value2) {
            addCriterion("NICK_NAME not between", value1, value2, "nickName");
            return (Criteria) this;
        }

        public Criteria andPasswordIsNull() {
            addCriterion("PASSWORD is null");
            return (Criteria) this;
        }

        public Criteria andPasswordIsNotNull() {
            addCriterion("PASSWORD is not null");
            return (Criteria) this;
        }

        public Criteria andPasswordEqualTo(String value) {
            addCriterion("PASSWORD =", value, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordNotEqualTo(String value) {
            addCriterion("PASSWORD <>", value, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordGreaterThan(String value) {
            addCriterion("PASSWORD >", value, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordGreaterThanOrEqualTo(String value) {
            addCriterion("PASSWORD >=", value, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordLessThan(String value) {
            addCriterion("PASSWORD <", value, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordLessThanOrEqualTo(String value) {
            addCriterion("PASSWORD <=", value, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordLike(String value) {
            addCriterion("PASSWORD like", value, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordNotLike(String value) {
            addCriterion("PASSWORD not like", value, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordIn(List<String> values) {
            addCriterion("PASSWORD in", values, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordNotIn(List<String> values) {
            addCriterion("PASSWORD not in", values, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordBetween(String value1, String value2) {
            addCriterion("PASSWORD between", value1, value2, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordNotBetween(String value1, String value2) {
            addCriterion("PASSWORD not between", value1, value2, "password");
            return (Criteria) this;
        }

        public Criteria andMobileIsNull() {
            addCriterion("MOBILE is null");
            return (Criteria) this;
        }

        public Criteria andMobileIsNotNull() {
            addCriterion("MOBILE is not null");
            return (Criteria) this;
        }

        public Criteria andMobileEqualTo(String value) {
            addCriterion("MOBILE =", value, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileNotEqualTo(String value) {
            addCriterion("MOBILE <>", value, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileGreaterThan(String value) {
            addCriterion("MOBILE >", value, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileGreaterThanOrEqualTo(String value) {
            addCriterion("MOBILE >=", value, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileLessThan(String value) {
            addCriterion("MOBILE <", value, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileLessThanOrEqualTo(String value) {
            addCriterion("MOBILE <=", value, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileLike(String value) {
            addCriterion("MOBILE like", value, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileNotLike(String value) {
            addCriterion("MOBILE not like", value, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileIn(List<String> values) {
            addCriterion("MOBILE in", values, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileNotIn(List<String> values) {
            addCriterion("MOBILE not in", values, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileBetween(String value1, String value2) {
            addCriterion("MOBILE between", value1, value2, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileNotBetween(String value1, String value2) {
            addCriterion("MOBILE not between", value1, value2, "mobile");
            return (Criteria) this;
        }

        public Criteria andIdNumberIsNull() {
            addCriterion("ID_NUMBER is null");
            return (Criteria) this;
        }

        public Criteria andIdNumberIsNotNull() {
            addCriterion("ID_NUMBER is not null");
            return (Criteria) this;
        }

        public Criteria andIdNumberEqualTo(String value) {
            addCriterion("ID_NUMBER =", value, "idNumber");
            return (Criteria) this;
        }

        public Criteria andIdNumberNotEqualTo(String value) {
            addCriterion("ID_NUMBER <>", value, "idNumber");
            return (Criteria) this;
        }

        public Criteria andIdNumberGreaterThan(String value) {
            addCriterion("ID_NUMBER >", value, "idNumber");
            return (Criteria) this;
        }

        public Criteria andIdNumberGreaterThanOrEqualTo(String value) {
            addCriterion("ID_NUMBER >=", value, "idNumber");
            return (Criteria) this;
        }

        public Criteria andIdNumberLessThan(String value) {
            addCriterion("ID_NUMBER <", value, "idNumber");
            return (Criteria) this;
        }

        public Criteria andIdNumberLessThanOrEqualTo(String value) {
            addCriterion("ID_NUMBER <=", value, "idNumber");
            return (Criteria) this;
        }

        public Criteria andIdNumberLike(String value) {
            addCriterion("ID_NUMBER like", value, "idNumber");
            return (Criteria) this;
        }

        public Criteria andIdNumberNotLike(String value) {
            addCriterion("ID_NUMBER not like", value, "idNumber");
            return (Criteria) this;
        }

        public Criteria andIdNumberIn(List<String> values) {
            addCriterion("ID_NUMBER in", values, "idNumber");
            return (Criteria) this;
        }

        public Criteria andIdNumberNotIn(List<String> values) {
            addCriterion("ID_NUMBER not in", values, "idNumber");
            return (Criteria) this;
        }

        public Criteria andIdNumberBetween(String value1, String value2) {
            addCriterion("ID_NUMBER between", value1, value2, "idNumber");
            return (Criteria) this;
        }

        public Criteria andIdNumberNotBetween(String value1, String value2) {
            addCriterion("ID_NUMBER not between", value1, value2, "idNumber");
            return (Criteria) this;
        }

        public Criteria andChannelTypeIsNull() {
            addCriterion("CHANNEL_TYPE is null");
            return (Criteria) this;
        }

        public Criteria andChannelTypeIsNotNull() {
            addCriterion("CHANNEL_TYPE is not null");
            return (Criteria) this;
        }

        public Criteria andChannelTypeEqualTo(String value) {
            addCriterion("CHANNEL_TYPE =", value, "channelType");
            return (Criteria) this;
        }

        public Criteria andChannelTypeNotEqualTo(String value) {
            addCriterion("CHANNEL_TYPE <>", value, "channelType");
            return (Criteria) this;
        }

        public Criteria andChannelTypeGreaterThan(String value) {
            addCriterion("CHANNEL_TYPE >", value, "channelType");
            return (Criteria) this;
        }

        public Criteria andChannelTypeGreaterThanOrEqualTo(String value) {
            addCriterion("CHANNEL_TYPE >=", value, "channelType");
            return (Criteria) this;
        }

        public Criteria andChannelTypeLessThan(String value) {
            addCriterion("CHANNEL_TYPE <", value, "channelType");
            return (Criteria) this;
        }

        public Criteria andChannelTypeLessThanOrEqualTo(String value) {
            addCriterion("CHANNEL_TYPE <=", value, "channelType");
            return (Criteria) this;
        }

        public Criteria andChannelTypeLike(String value) {
            addCriterion("CHANNEL_TYPE like", value, "channelType");
            return (Criteria) this;
        }

        public Criteria andChannelTypeNotLike(String value) {
            addCriterion("CHANNEL_TYPE not like", value, "channelType");
            return (Criteria) this;
        }

        public Criteria andChannelTypeIn(List<String> values) {
            addCriterion("CHANNEL_TYPE in", values, "channelType");
            return (Criteria) this;
        }

        public Criteria andChannelTypeNotIn(List<String> values) {
            addCriterion("CHANNEL_TYPE not in", values, "channelType");
            return (Criteria) this;
        }

        public Criteria andChannelTypeBetween(String value1, String value2) {
            addCriterion("CHANNEL_TYPE between", value1, value2, "channelType");
            return (Criteria) this;
        }

        public Criteria andChannelTypeNotBetween(String value1, String value2) {
            addCriterion("CHANNEL_TYPE not between", value1, value2, "channelType");
            return (Criteria) this;
        }

        public Criteria andCustomNameIsNull() {
            addCriterion("CUSTOM_NAME is null");
            return (Criteria) this;
        }

        public Criteria andCustomNameIsNotNull() {
            addCriterion("CUSTOM_NAME is not null");
            return (Criteria) this;
        }

        public Criteria andCustomNameEqualTo(String value) {
            addCriterion("CUSTOM_NAME =", value, "customName");
            return (Criteria) this;
        }

        public Criteria andCustomNameNotEqualTo(String value) {
            addCriterion("CUSTOM_NAME <>", value, "customName");
            return (Criteria) this;
        }

        public Criteria andCustomNameGreaterThan(String value) {
            addCriterion("CUSTOM_NAME >", value, "customName");
            return (Criteria) this;
        }

        public Criteria andCustomNameGreaterThanOrEqualTo(String value) {
            addCriterion("CUSTOM_NAME >=", value, "customName");
            return (Criteria) this;
        }

        public Criteria andCustomNameLessThan(String value) {
            addCriterion("CUSTOM_NAME <", value, "customName");
            return (Criteria) this;
        }

        public Criteria andCustomNameLessThanOrEqualTo(String value) {
            addCriterion("CUSTOM_NAME <=", value, "customName");
            return (Criteria) this;
        }

        public Criteria andCustomNameLike(String value) {
            addCriterion("CUSTOM_NAME like", value, "customName");
            return (Criteria) this;
        }

        public Criteria andCustomNameNotLike(String value) {
            addCriterion("CUSTOM_NAME not like", value, "customName");
            return (Criteria) this;
        }

        public Criteria andCustomNameIn(List<String> values) {
            addCriterion("CUSTOM_NAME in", values, "customName");
            return (Criteria) this;
        }

        public Criteria andCustomNameNotIn(List<String> values) {
            addCriterion("CUSTOM_NAME not in", values, "customName");
            return (Criteria) this;
        }

        public Criteria andCustomNameBetween(String value1, String value2) {
            addCriterion("CUSTOM_NAME between", value1, value2, "customName");
            return (Criteria) this;
        }

        public Criteria andCustomNameNotBetween(String value1, String value2) {
            addCriterion("CUSTOM_NAME not between", value1, value2, "customName");
            return (Criteria) this;
        }

        public Criteria andEmailIsNull() {
            addCriterion("EMAIL is null");
            return (Criteria) this;
        }

        public Criteria andEmailIsNotNull() {
            addCriterion("EMAIL is not null");
            return (Criteria) this;
        }

        public Criteria andEmailEqualTo(String value) {
            addCriterion("EMAIL =", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailNotEqualTo(String value) {
            addCriterion("EMAIL <>", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailGreaterThan(String value) {
            addCriterion("EMAIL >", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailGreaterThanOrEqualTo(String value) {
            addCriterion("EMAIL >=", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailLessThan(String value) {
            addCriterion("EMAIL <", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailLessThanOrEqualTo(String value) {
            addCriterion("EMAIL <=", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailLike(String value) {
            addCriterion("EMAIL like", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailNotLike(String value) {
            addCriterion("EMAIL not like", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailIn(List<String> values) {
            addCriterion("EMAIL in", values, "email");
            return (Criteria) this;
        }

        public Criteria andEmailNotIn(List<String> values) {
            addCriterion("EMAIL not in", values, "email");
            return (Criteria) this;
        }

        public Criteria andEmailBetween(String value1, String value2) {
            addCriterion("EMAIL between", value1, value2, "email");
            return (Criteria) this;
        }

        public Criteria andEmailNotBetween(String value1, String value2) {
            addCriterion("EMAIL not between", value1, value2, "email");
            return (Criteria) this;
        }

        public Criteria andUserTypeIsNull() {
            addCriterion("USER_TYPE is null");
            return (Criteria) this;
        }

        public Criteria andUserTypeIsNotNull() {
            addCriterion("USER_TYPE is not null");
            return (Criteria) this;
        }

        public Criteria andUserTypeEqualTo(String value) {
            addCriterion("USER_TYPE =", value, "userType");
            return (Criteria) this;
        }

        public Criteria andUserTypeNotEqualTo(String value) {
            addCriterion("USER_TYPE <>", value, "userType");
            return (Criteria) this;
        }

        public Criteria andUserTypeGreaterThan(String value) {
            addCriterion("USER_TYPE >", value, "userType");
            return (Criteria) this;
        }

        public Criteria andUserTypeGreaterThanOrEqualTo(String value) {
            addCriterion("USER_TYPE >=", value, "userType");
            return (Criteria) this;
        }

        public Criteria andUserTypeLessThan(String value) {
            addCriterion("USER_TYPE <", value, "userType");
            return (Criteria) this;
        }

        public Criteria andUserTypeLessThanOrEqualTo(String value) {
            addCriterion("USER_TYPE <=", value, "userType");
            return (Criteria) this;
        }

        public Criteria andUserTypeLike(String value) {
            addCriterion("USER_TYPE like", value, "userType");
            return (Criteria) this;
        }

        public Criteria andUserTypeNotLike(String value) {
            addCriterion("USER_TYPE not like", value, "userType");
            return (Criteria) this;
        }

        public Criteria andUserTypeIn(List<String> values) {
            addCriterion("USER_TYPE in", values, "userType");
            return (Criteria) this;
        }

        public Criteria andUserTypeNotIn(List<String> values) {
            addCriterion("USER_TYPE not in", values, "userType");
            return (Criteria) this;
        }

        public Criteria andUserTypeBetween(String value1, String value2) {
            addCriterion("USER_TYPE between", value1, value2, "userType");
            return (Criteria) this;
        }

        public Criteria andUserTypeNotBetween(String value1, String value2) {
            addCriterion("USER_TYPE not between", value1, value2, "userType");
            return (Criteria) this;
        }

        public Criteria andApplyAgrnoTypeIsNull() {
            addCriterion("APPLY_AGRNO_TYPE is null");
            return (Criteria) this;
        }

        public Criteria andApplyAgrnoTypeIsNotNull() {
            addCriterion("APPLY_AGRNO_TYPE is not null");
            return (Criteria) this;
        }

        public Criteria andApplyAgrnoTypeEqualTo(String value) {
            addCriterion("APPLY_AGRNO_TYPE =", value, "applyAgrnoType");
            return (Criteria) this;
        }

        public Criteria andApplyAgrnoTypeNotEqualTo(String value) {
            addCriterion("APPLY_AGRNO_TYPE <>", value, "applyAgrnoType");
            return (Criteria) this;
        }

        public Criteria andApplyAgrnoTypeGreaterThan(String value) {
            addCriterion("APPLY_AGRNO_TYPE >", value, "applyAgrnoType");
            return (Criteria) this;
        }

        public Criteria andApplyAgrnoTypeGreaterThanOrEqualTo(String value) {
            addCriterion("APPLY_AGRNO_TYPE >=", value, "applyAgrnoType");
            return (Criteria) this;
        }

        public Criteria andApplyAgrnoTypeLessThan(String value) {
            addCriterion("APPLY_AGRNO_TYPE <", value, "applyAgrnoType");
            return (Criteria) this;
        }

        public Criteria andApplyAgrnoTypeLessThanOrEqualTo(String value) {
            addCriterion("APPLY_AGRNO_TYPE <=", value, "applyAgrnoType");
            return (Criteria) this;
        }

        public Criteria andApplyAgrnoTypeLike(String value) {
            addCriterion("APPLY_AGRNO_TYPE like", value, "applyAgrnoType");
            return (Criteria) this;
        }

        public Criteria andApplyAgrnoTypeNotLike(String value) {
            addCriterion("APPLY_AGRNO_TYPE not like", value, "applyAgrnoType");
            return (Criteria) this;
        }

        public Criteria andApplyAgrnoTypeIn(List<String> values) {
            addCriterion("APPLY_AGRNO_TYPE in", values, "applyAgrnoType");
            return (Criteria) this;
        }

        public Criteria andApplyAgrnoTypeNotIn(List<String> values) {
            addCriterion("APPLY_AGRNO_TYPE not in", values, "applyAgrnoType");
            return (Criteria) this;
        }

        public Criteria andApplyAgrnoTypeBetween(String value1, String value2) {
            addCriterion("APPLY_AGRNO_TYPE between", value1, value2, "applyAgrnoType");
            return (Criteria) this;
        }

        public Criteria andApplyAgrnoTypeNotBetween(String value1, String value2) {
            addCriterion("APPLY_AGRNO_TYPE not between", value1, value2, "applyAgrnoType");
            return (Criteria) this;
        }

        public Criteria andAgrementNoIsNull() {
            addCriterion("AGREMENT_NO is null");
            return (Criteria) this;
        }

        public Criteria andAgrementNoIsNotNull() {
            addCriterion("AGREMENT_NO is not null");
            return (Criteria) this;
        }

        public Criteria andAgrementNoEqualTo(String value) {
            addCriterion("AGREMENT_NO =", value, "agrementNo");
            return (Criteria) this;
        }

        public Criteria andAgrementNoNotEqualTo(String value) {
            addCriterion("AGREMENT_NO <>", value, "agrementNo");
            return (Criteria) this;
        }

        public Criteria andAgrementNoGreaterThan(String value) {
            addCriterion("AGREMENT_NO >", value, "agrementNo");
            return (Criteria) this;
        }

        public Criteria andAgrementNoGreaterThanOrEqualTo(String value) {
            addCriterion("AGREMENT_NO >=", value, "agrementNo");
            return (Criteria) this;
        }

        public Criteria andAgrementNoLessThan(String value) {
            addCriterion("AGREMENT_NO <", value, "agrementNo");
            return (Criteria) this;
        }

        public Criteria andAgrementNoLessThanOrEqualTo(String value) {
            addCriterion("AGREMENT_NO <=", value, "agrementNo");
            return (Criteria) this;
        }

        public Criteria andAgrementNoLike(String value) {
            addCriterion("AGREMENT_NO like", value, "agrementNo");
            return (Criteria) this;
        }

        public Criteria andAgrementNoNotLike(String value) {
            addCriterion("AGREMENT_NO not like", value, "agrementNo");
            return (Criteria) this;
        }

        public Criteria andAgrementNoIn(List<String> values) {
            addCriterion("AGREMENT_NO in", values, "agrementNo");
            return (Criteria) this;
        }

        public Criteria andAgrementNoNotIn(List<String> values) {
            addCriterion("AGREMENT_NO not in", values, "agrementNo");
            return (Criteria) this;
        }

        public Criteria andAgrementNoBetween(String value1, String value2) {
            addCriterion("AGREMENT_NO between", value1, value2, "agrementNo");
            return (Criteria) this;
        }

        public Criteria andAgrementNoNotBetween(String value1, String value2) {
            addCriterion("AGREMENT_NO not between", value1, value2, "agrementNo");
            return (Criteria) this;
        }

        public Criteria andAgrementNameIsNull() {
            addCriterion("AGREMENT_NAME is null");
            return (Criteria) this;
        }

        public Criteria andAgrementNameIsNotNull() {
            addCriterion("AGREMENT_NAME is not null");
            return (Criteria) this;
        }

        public Criteria andAgrementNameEqualTo(String value) {
            addCriterion("AGREMENT_NAME =", value, "agrementName");
            return (Criteria) this;
        }

        public Criteria andAgrementNameNotEqualTo(String value) {
            addCriterion("AGREMENT_NAME <>", value, "agrementName");
            return (Criteria) this;
        }

        public Criteria andAgrementNameGreaterThan(String value) {
            addCriterion("AGREMENT_NAME >", value, "agrementName");
            return (Criteria) this;
        }

        public Criteria andAgrementNameGreaterThanOrEqualTo(String value) {
            addCriterion("AGREMENT_NAME >=", value, "agrementName");
            return (Criteria) this;
        }

        public Criteria andAgrementNameLessThan(String value) {
            addCriterion("AGREMENT_NAME <", value, "agrementName");
            return (Criteria) this;
        }

        public Criteria andAgrementNameLessThanOrEqualTo(String value) {
            addCriterion("AGREMENT_NAME <=", value, "agrementName");
            return (Criteria) this;
        }

        public Criteria andAgrementNameLike(String value) {
            addCriterion("AGREMENT_NAME like", value, "agrementName");
            return (Criteria) this;
        }

        public Criteria andAgrementNameNotLike(String value) {
            addCriterion("AGREMENT_NAME not like", value, "agrementName");
            return (Criteria) this;
        }

        public Criteria andAgrementNameIn(List<String> values) {
            addCriterion("AGREMENT_NAME in", values, "agrementName");
            return (Criteria) this;
        }

        public Criteria andAgrementNameNotIn(List<String> values) {
            addCriterion("AGREMENT_NAME not in", values, "agrementName");
            return (Criteria) this;
        }

        public Criteria andAgrementNameBetween(String value1, String value2) {
            addCriterion("AGREMENT_NAME between", value1, value2, "agrementName");
            return (Criteria) this;
        }

        public Criteria andAgrementNameNotBetween(String value1, String value2) {
            addCriterion("AGREMENT_NAME not between", value1, value2, "agrementName");
            return (Criteria) this;
        }

        public Criteria andBranchCodeIsNull() {
            addCriterion("BRANCH_CODE is null");
            return (Criteria) this;
        }

        public Criteria andBranchCodeIsNotNull() {
            addCriterion("BRANCH_CODE is not null");
            return (Criteria) this;
        }

        public Criteria andBranchCodeEqualTo(String value) {
            addCriterion("BRANCH_CODE =", value, "branchCode");
            return (Criteria) this;
        }

        public Criteria andBranchCodeNotEqualTo(String value) {
            addCriterion("BRANCH_CODE <>", value, "branchCode");
            return (Criteria) this;
        }

        public Criteria andBranchCodeGreaterThan(String value) {
            addCriterion("BRANCH_CODE >", value, "branchCode");
            return (Criteria) this;
        }

        public Criteria andBranchCodeGreaterThanOrEqualTo(String value) {
            addCriterion("BRANCH_CODE >=", value, "branchCode");
            return (Criteria) this;
        }

        public Criteria andBranchCodeLessThan(String value) {
            addCriterion("BRANCH_CODE <", value, "branchCode");
            return (Criteria) this;
        }

        public Criteria andBranchCodeLessThanOrEqualTo(String value) {
            addCriterion("BRANCH_CODE <=", value, "branchCode");
            return (Criteria) this;
        }

        public Criteria andBranchCodeLike(String value) {
            addCriterion("BRANCH_CODE like", value, "branchCode");
            return (Criteria) this;
        }

        public Criteria andBranchCodeNotLike(String value) {
            addCriterion("BRANCH_CODE not like", value, "branchCode");
            return (Criteria) this;
        }

        public Criteria andBranchCodeIn(List<String> values) {
            addCriterion("BRANCH_CODE in", values, "branchCode");
            return (Criteria) this;
        }

        public Criteria andBranchCodeNotIn(List<String> values) {
            addCriterion("BRANCH_CODE not in", values, "branchCode");
            return (Criteria) this;
        }

        public Criteria andBranchCodeBetween(String value1, String value2) {
            addCriterion("BRANCH_CODE between", value1, value2, "branchCode");
            return (Criteria) this;
        }

        public Criteria andBranchCodeNotBetween(String value1, String value2) {
            addCriterion("BRANCH_CODE not between", value1, value2, "branchCode");
            return (Criteria) this;
        }

        public Criteria andComCodeIsNull() {
            addCriterion("COM_CODE is null");
            return (Criteria) this;
        }

        public Criteria andComCodeIsNotNull() {
            addCriterion("COM_CODE is not null");
            return (Criteria) this;
        }

        public Criteria andComCodeEqualTo(String value) {
            addCriterion("COM_CODE =", value, "comCode");
            return (Criteria) this;
        }

        public Criteria andComCodeNotEqualTo(String value) {
            addCriterion("COM_CODE <>", value, "comCode");
            return (Criteria) this;
        }

        public Criteria andComCodeGreaterThan(String value) {
            addCriterion("COM_CODE >", value, "comCode");
            return (Criteria) this;
        }

        public Criteria andComCodeGreaterThanOrEqualTo(String value) {
            addCriterion("COM_CODE >=", value, "comCode");
            return (Criteria) this;
        }

        public Criteria andComCodeLessThan(String value) {
            addCriterion("COM_CODE <", value, "comCode");
            return (Criteria) this;
        }

        public Criteria andComCodeLessThanOrEqualTo(String value) {
            addCriterion("COM_CODE <=", value, "comCode");
            return (Criteria) this;
        }

        public Criteria andComCodeLike(String value) {
            addCriterion("COM_CODE like", value, "comCode");
            return (Criteria) this;
        }

        public Criteria andComCodeNotLike(String value) {
            addCriterion("COM_CODE not like", value, "comCode");
            return (Criteria) this;
        }

        public Criteria andComCodeIn(List<String> values) {
            addCriterion("COM_CODE in", values, "comCode");
            return (Criteria) this;
        }

        public Criteria andComCodeNotIn(List<String> values) {
            addCriterion("COM_CODE not in", values, "comCode");
            return (Criteria) this;
        }

        public Criteria andComCodeBetween(String value1, String value2) {
            addCriterion("COM_CODE between", value1, value2, "comCode");
            return (Criteria) this;
        }

        public Criteria andComCodeNotBetween(String value1, String value2) {
            addCriterion("COM_CODE not between", value1, value2, "comCode");
            return (Criteria) this;
        }

        public Criteria andRelationshipIsNull() {
            addCriterion("Relationship is null");
            return (Criteria) this;
        }

        public Criteria andRelationshipIsNotNull() {
            addCriterion("Relationship is not null");
            return (Criteria) this;
        }

        public Criteria andRelationshipEqualTo(String value) {
            addCriterion("Relationship =", value, "relationship");
            return (Criteria) this;
        }

        public Criteria andRelationshipNotEqualTo(String value) {
            addCriterion("Relationship <>", value, "relationship");
            return (Criteria) this;
        }

        public Criteria andRelationshipGreaterThan(String value) {
            addCriterion("Relationship >", value, "relationship");
            return (Criteria) this;
        }

        public Criteria andRelationshipGreaterThanOrEqualTo(String value) {
            addCriterion("Relationship >=", value, "relationship");
            return (Criteria) this;
        }

        public Criteria andRelationshipLessThan(String value) {
            addCriterion("Relationship <", value, "relationship");
            return (Criteria) this;
        }

        public Criteria andRelationshipLessThanOrEqualTo(String value) {
            addCriterion("Relationship <=", value, "relationship");
            return (Criteria) this;
        }

        public Criteria andRelationshipLike(String value) {
            addCriterion("Relationship like", value, "relationship");
            return (Criteria) this;
        }

        public Criteria andRelationshipNotLike(String value) {
            addCriterion("Relationship not like", value, "relationship");
            return (Criteria) this;
        }

        public Criteria andRelationshipIn(List<String> values) {
            addCriterion("Relationship in", values, "relationship");
            return (Criteria) this;
        }

        public Criteria andRelationshipNotIn(List<String> values) {
            addCriterion("Relationship not in", values, "relationship");
            return (Criteria) this;
        }

        public Criteria andRelationshipBetween(String value1, String value2) {
            addCriterion("Relationship between", value1, value2, "relationship");
            return (Criteria) this;
        }

        public Criteria andRelationshipNotBetween(String value1, String value2) {
            addCriterion("Relationship not between", value1, value2, "relationship");
            return (Criteria) this;
        }

        public Criteria andSaleCodeIsNull() {
            addCriterion("SALE_CODE is null");
            return (Criteria) this;
        }

        public Criteria andSaleCodeIsNotNull() {
            addCriterion("SALE_CODE is not null");
            return (Criteria) this;
        }

        public Criteria andSaleCodeEqualTo(String value) {
            addCriterion("SALE_CODE =", value, "saleCode");
            return (Criteria) this;
        }

        public Criteria andSaleCodeNotEqualTo(String value) {
            addCriterion("SALE_CODE <>", value, "saleCode");
            return (Criteria) this;
        }

        public Criteria andSaleCodeGreaterThan(String value) {
            addCriterion("SALE_CODE >", value, "saleCode");
            return (Criteria) this;
        }

        public Criteria andSaleCodeGreaterThanOrEqualTo(String value) {
            addCriterion("SALE_CODE >=", value, "saleCode");
            return (Criteria) this;
        }

        public Criteria andSaleCodeLessThan(String value) {
            addCriterion("SALE_CODE <", value, "saleCode");
            return (Criteria) this;
        }

        public Criteria andSaleCodeLessThanOrEqualTo(String value) {
            addCriterion("SALE_CODE <=", value, "saleCode");
            return (Criteria) this;
        }

        public Criteria andSaleCodeLike(String value) {
            addCriterion("SALE_CODE like", value, "saleCode");
            return (Criteria) this;
        }

        public Criteria andSaleCodeNotLike(String value) {
            addCriterion("SALE_CODE not like", value, "saleCode");
            return (Criteria) this;
        }

        public Criteria andSaleCodeIn(List<String> values) {
            addCriterion("SALE_CODE in", values, "saleCode");
            return (Criteria) this;
        }

        public Criteria andSaleCodeNotIn(List<String> values) {
            addCriterion("SALE_CODE not in", values, "saleCode");
            return (Criteria) this;
        }

        public Criteria andSaleCodeBetween(String value1, String value2) {
            addCriterion("SALE_CODE between", value1, value2, "saleCode");
            return (Criteria) this;
        }

        public Criteria andSaleCodeNotBetween(String value1, String value2) {
            addCriterion("SALE_CODE not between", value1, value2, "saleCode");
            return (Criteria) this;
        }

        public Criteria andSaleNameIsNull() {
            addCriterion("SALE_NAME is null");
            return (Criteria) this;
        }

        public Criteria andSaleNameIsNotNull() {
            addCriterion("SALE_NAME is not null");
            return (Criteria) this;
        }

        public Criteria andSaleNameEqualTo(String value) {
            addCriterion("SALE_NAME =", value, "saleName");
            return (Criteria) this;
        }

        public Criteria andSaleNameNotEqualTo(String value) {
            addCriterion("SALE_NAME <>", value, "saleName");
            return (Criteria) this;
        }

        public Criteria andSaleNameGreaterThan(String value) {
            addCriterion("SALE_NAME >", value, "saleName");
            return (Criteria) this;
        }

        public Criteria andSaleNameGreaterThanOrEqualTo(String value) {
            addCriterion("SALE_NAME >=", value, "saleName");
            return (Criteria) this;
        }

        public Criteria andSaleNameLessThan(String value) {
            addCriterion("SALE_NAME <", value, "saleName");
            return (Criteria) this;
        }

        public Criteria andSaleNameLessThanOrEqualTo(String value) {
            addCriterion("SALE_NAME <=", value, "saleName");
            return (Criteria) this;
        }

        public Criteria andSaleNameLike(String value) {
            addCriterion("SALE_NAME like", value, "saleName");
            return (Criteria) this;
        }

        public Criteria andSaleNameNotLike(String value) {
            addCriterion("SALE_NAME not like", value, "saleName");
            return (Criteria) this;
        }

        public Criteria andSaleNameIn(List<String> values) {
            addCriterion("SALE_NAME in", values, "saleName");
            return (Criteria) this;
        }

        public Criteria andSaleNameNotIn(List<String> values) {
            addCriterion("SALE_NAME not in", values, "saleName");
            return (Criteria) this;
        }

        public Criteria andSaleNameBetween(String value1, String value2) {
            addCriterion("SALE_NAME between", value1, value2, "saleName");
            return (Criteria) this;
        }

        public Criteria andSaleNameNotBetween(String value1, String value2) {
            addCriterion("SALE_NAME not between", value1, value2, "saleName");
            return (Criteria) this;
        }

        public Criteria andChannelCodeIsNull() {
            addCriterion("CHANNEL_CODE is null");
            return (Criteria) this;
        }

        public Criteria andChannelCodeIsNotNull() {
            addCriterion("CHANNEL_CODE is not null");
            return (Criteria) this;
        }

        public Criteria andChannelCodeEqualTo(String value) {
            addCriterion("CHANNEL_CODE =", value, "channelCode");
            return (Criteria) this;
        }

        public Criteria andChannelCodeNotEqualTo(String value) {
            addCriterion("CHANNEL_CODE <>", value, "channelCode");
            return (Criteria) this;
        }

        public Criteria andChannelCodeGreaterThan(String value) {
            addCriterion("CHANNEL_CODE >", value, "channelCode");
            return (Criteria) this;
        }

        public Criteria andChannelCodeGreaterThanOrEqualTo(String value) {
            addCriterion("CHANNEL_CODE >=", value, "channelCode");
            return (Criteria) this;
        }

        public Criteria andChannelCodeLessThan(String value) {
            addCriterion("CHANNEL_CODE <", value, "channelCode");
            return (Criteria) this;
        }

        public Criteria andChannelCodeLessThanOrEqualTo(String value) {
            addCriterion("CHANNEL_CODE <=", value, "channelCode");
            return (Criteria) this;
        }

        public Criteria andChannelCodeLike(String value) {
            addCriterion("CHANNEL_CODE like", value, "channelCode");
            return (Criteria) this;
        }

        public Criteria andChannelCodeNotLike(String value) {
            addCriterion("CHANNEL_CODE not like", value, "channelCode");
            return (Criteria) this;
        }

        public Criteria andChannelCodeIn(List<String> values) {
            addCriterion("CHANNEL_CODE in", values, "channelCode");
            return (Criteria) this;
        }

        public Criteria andChannelCodeNotIn(List<String> values) {
            addCriterion("CHANNEL_CODE not in", values, "channelCode");
            return (Criteria) this;
        }

        public Criteria andChannelCodeBetween(String value1, String value2) {
            addCriterion("CHANNEL_CODE between", value1, value2, "channelCode");
            return (Criteria) this;
        }

        public Criteria andChannelCodeNotBetween(String value1, String value2) {
            addCriterion("CHANNEL_CODE not between", value1, value2, "channelCode");
            return (Criteria) this;
        }

        public Criteria andChannelNameIsNull() {
            addCriterion("CHANNEL_NAME is null");
            return (Criteria) this;
        }

        public Criteria andChannelNameIsNotNull() {
            addCriterion("CHANNEL_NAME is not null");
            return (Criteria) this;
        }

        public Criteria andChannelNameEqualTo(String value) {
            addCriterion("CHANNEL_NAME =", value, "channelName");
            return (Criteria) this;
        }

        public Criteria andChannelNameNotEqualTo(String value) {
            addCriterion("CHANNEL_NAME <>", value, "channelName");
            return (Criteria) this;
        }

        public Criteria andChannelNameGreaterThan(String value) {
            addCriterion("CHANNEL_NAME >", value, "channelName");
            return (Criteria) this;
        }

        public Criteria andChannelNameGreaterThanOrEqualTo(String value) {
            addCriterion("CHANNEL_NAME >=", value, "channelName");
            return (Criteria) this;
        }

        public Criteria andChannelNameLessThan(String value) {
            addCriterion("CHANNEL_NAME <", value, "channelName");
            return (Criteria) this;
        }

        public Criteria andChannelNameLessThanOrEqualTo(String value) {
            addCriterion("CHANNEL_NAME <=", value, "channelName");
            return (Criteria) this;
        }

        public Criteria andChannelNameLike(String value) {
            addCriterion("CHANNEL_NAME like", value, "channelName");
            return (Criteria) this;
        }

        public Criteria andChannelNameNotLike(String value) {
            addCriterion("CHANNEL_NAME not like", value, "channelName");
            return (Criteria) this;
        }

        public Criteria andChannelNameIn(List<String> values) {
            addCriterion("CHANNEL_NAME in", values, "channelName");
            return (Criteria) this;
        }

        public Criteria andChannelNameNotIn(List<String> values) {
            addCriterion("CHANNEL_NAME not in", values, "channelName");
            return (Criteria) this;
        }

        public Criteria andChannelNameBetween(String value1, String value2) {
            addCriterion("CHANNEL_NAME between", value1, value2, "channelName");
            return (Criteria) this;
        }

        public Criteria andChannelNameNotBetween(String value1, String value2) {
            addCriterion("CHANNEL_NAME not between", value1, value2, "channelName");
            return (Criteria) this;
        }

        public Criteria andAreaCodeIsNull() {
            addCriterion("AREA_CODE is null");
            return (Criteria) this;
        }

        public Criteria andAreaCodeIsNotNull() {
            addCriterion("AREA_CODE is not null");
            return (Criteria) this;
        }

        public Criteria andAreaCodeEqualTo(String value) {
            addCriterion("AREA_CODE =", value, "areaCode");
            return (Criteria) this;
        }

        public Criteria andAreaCodeNotEqualTo(String value) {
            addCriterion("AREA_CODE <>", value, "areaCode");
            return (Criteria) this;
        }

        public Criteria andAreaCodeGreaterThan(String value) {
            addCriterion("AREA_CODE >", value, "areaCode");
            return (Criteria) this;
        }

        public Criteria andAreaCodeGreaterThanOrEqualTo(String value) {
            addCriterion("AREA_CODE >=", value, "areaCode");
            return (Criteria) this;
        }

        public Criteria andAreaCodeLessThan(String value) {
            addCriterion("AREA_CODE <", value, "areaCode");
            return (Criteria) this;
        }

        public Criteria andAreaCodeLessThanOrEqualTo(String value) {
            addCriterion("AREA_CODE <=", value, "areaCode");
            return (Criteria) this;
        }

        public Criteria andAreaCodeLike(String value) {
            addCriterion("AREA_CODE like", value, "areaCode");
            return (Criteria) this;
        }

        public Criteria andAreaCodeNotLike(String value) {
            addCriterion("AREA_CODE not like", value, "areaCode");
            return (Criteria) this;
        }

        public Criteria andAreaCodeIn(List<String> values) {
            addCriterion("AREA_CODE in", values, "areaCode");
            return (Criteria) this;
        }

        public Criteria andAreaCodeNotIn(List<String> values) {
            addCriterion("AREA_CODE not in", values, "areaCode");
            return (Criteria) this;
        }

        public Criteria andAreaCodeBetween(String value1, String value2) {
            addCriterion("AREA_CODE between", value1, value2, "areaCode");
            return (Criteria) this;
        }

        public Criteria andAreaCodeNotBetween(String value1, String value2) {
            addCriterion("AREA_CODE not between", value1, value2, "areaCode");
            return (Criteria) this;
        }

        public Criteria andAreaNameIsNull() {
            addCriterion("AREA_NAME is null");
            return (Criteria) this;
        }

        public Criteria andAreaNameIsNotNull() {
            addCriterion("AREA_NAME is not null");
            return (Criteria) this;
        }

        public Criteria andAreaNameEqualTo(String value) {
            addCriterion("AREA_NAME =", value, "areaName");
            return (Criteria) this;
        }

        public Criteria andAreaNameNotEqualTo(String value) {
            addCriterion("AREA_NAME <>", value, "areaName");
            return (Criteria) this;
        }

        public Criteria andAreaNameGreaterThan(String value) {
            addCriterion("AREA_NAME >", value, "areaName");
            return (Criteria) this;
        }

        public Criteria andAreaNameGreaterThanOrEqualTo(String value) {
            addCriterion("AREA_NAME >=", value, "areaName");
            return (Criteria) this;
        }

        public Criteria andAreaNameLessThan(String value) {
            addCriterion("AREA_NAME <", value, "areaName");
            return (Criteria) this;
        }

        public Criteria andAreaNameLessThanOrEqualTo(String value) {
            addCriterion("AREA_NAME <=", value, "areaName");
            return (Criteria) this;
        }

        public Criteria andAreaNameLike(String value) {
            addCriterion("AREA_NAME like", value, "areaName");
            return (Criteria) this;
        }

        public Criteria andAreaNameNotLike(String value) {
            addCriterion("AREA_NAME not like", value, "areaName");
            return (Criteria) this;
        }

        public Criteria andAreaNameIn(List<String> values) {
            addCriterion("AREA_NAME in", values, "areaName");
            return (Criteria) this;
        }

        public Criteria andAreaNameNotIn(List<String> values) {
            addCriterion("AREA_NAME not in", values, "areaName");
            return (Criteria) this;
        }

        public Criteria andAreaNameBetween(String value1, String value2) {
            addCriterion("AREA_NAME between", value1, value2, "areaName");
            return (Criteria) this;
        }

        public Criteria andAreaNameNotBetween(String value1, String value2) {
            addCriterion("AREA_NAME not between", value1, value2, "areaName");
            return (Criteria) this;
        }

        public Criteria andIntroduceIdIsNull() {
            addCriterion("INTRODUCE_ID is null");
            return (Criteria) this;
        }

        public Criteria andIntroduceIdIsNotNull() {
            addCriterion("INTRODUCE_ID is not null");
            return (Criteria) this;
        }

        public Criteria andIntroduceIdEqualTo(String value) {
            addCriterion("INTRODUCE_ID =", value, "introduceId");
            return (Criteria) this;
        }

        public Criteria andIntroduceIdNotEqualTo(String value) {
            addCriterion("INTRODUCE_ID <>", value, "introduceId");
            return (Criteria) this;
        }

        public Criteria andIntroduceIdGreaterThan(String value) {
            addCriterion("INTRODUCE_ID >", value, "introduceId");
            return (Criteria) this;
        }

        public Criteria andIntroduceIdGreaterThanOrEqualTo(String value) {
            addCriterion("INTRODUCE_ID >=", value, "introduceId");
            return (Criteria) this;
        }

        public Criteria andIntroduceIdLessThan(String value) {
            addCriterion("INTRODUCE_ID <", value, "introduceId");
            return (Criteria) this;
        }

        public Criteria andIntroduceIdLessThanOrEqualTo(String value) {
            addCriterion("INTRODUCE_ID <=", value, "introduceId");
            return (Criteria) this;
        }

        public Criteria andIntroduceIdLike(String value) {
            addCriterion("INTRODUCE_ID like", value, "introduceId");
            return (Criteria) this;
        }

        public Criteria andIntroduceIdNotLike(String value) {
            addCriterion("INTRODUCE_ID not like", value, "introduceId");
            return (Criteria) this;
        }

        public Criteria andIntroduceIdIn(List<String> values) {
            addCriterion("INTRODUCE_ID in", values, "introduceId");
            return (Criteria) this;
        }

        public Criteria andIntroduceIdNotIn(List<String> values) {
            addCriterion("INTRODUCE_ID not in", values, "introduceId");
            return (Criteria) this;
        }

        public Criteria andIntroduceIdBetween(String value1, String value2) {
            addCriterion("INTRODUCE_ID between", value1, value2, "introduceId");
            return (Criteria) this;
        }

        public Criteria andIntroduceIdNotBetween(String value1, String value2) {
            addCriterion("INTRODUCE_ID not between", value1, value2, "introduceId");
            return (Criteria) this;
        }

        public Criteria andHeadUrlIsNull() {
            addCriterion("HEAD_URL is null");
            return (Criteria) this;
        }

        public Criteria andHeadUrlIsNotNull() {
            addCriterion("HEAD_URL is not null");
            return (Criteria) this;
        }

        public Criteria andHeadUrlEqualTo(String value) {
            addCriterion("HEAD_URL =", value, "headUrl");
            return (Criteria) this;
        }

        public Criteria andHeadUrlNotEqualTo(String value) {
            addCriterion("HEAD_URL <>", value, "headUrl");
            return (Criteria) this;
        }

        public Criteria andHeadUrlGreaterThan(String value) {
            addCriterion("HEAD_URL >", value, "headUrl");
            return (Criteria) this;
        }

        public Criteria andHeadUrlGreaterThanOrEqualTo(String value) {
            addCriterion("HEAD_URL >=", value, "headUrl");
            return (Criteria) this;
        }

        public Criteria andHeadUrlLessThan(String value) {
            addCriterion("HEAD_URL <", value, "headUrl");
            return (Criteria) this;
        }

        public Criteria andHeadUrlLessThanOrEqualTo(String value) {
            addCriterion("HEAD_URL <=", value, "headUrl");
            return (Criteria) this;
        }

        public Criteria andHeadUrlLike(String value) {
            addCriterion("HEAD_URL like", value, "headUrl");
            return (Criteria) this;
        }

        public Criteria andHeadUrlNotLike(String value) {
            addCriterion("HEAD_URL not like", value, "headUrl");
            return (Criteria) this;
        }

        public Criteria andHeadUrlIn(List<String> values) {
            addCriterion("HEAD_URL in", values, "headUrl");
            return (Criteria) this;
        }

        public Criteria andHeadUrlNotIn(List<String> values) {
            addCriterion("HEAD_URL not in", values, "headUrl");
            return (Criteria) this;
        }

        public Criteria andHeadUrlBetween(String value1, String value2) {
            addCriterion("HEAD_URL between", value1, value2, "headUrl");
            return (Criteria) this;
        }

        public Criteria andHeadUrlNotBetween(String value1, String value2) {
            addCriterion("HEAD_URL not between", value1, value2, "headUrl");
            return (Criteria) this;
        }

        public Criteria andWxSubscribeIsNull() {
            addCriterion("WX_SUBSCRIBE is null");
            return (Criteria) this;
        }

        public Criteria andWxSubscribeIsNotNull() {
            addCriterion("WX_SUBSCRIBE is not null");
            return (Criteria) this;
        }

        public Criteria andWxSubscribeEqualTo(String value) {
            addCriterion("WX_SUBSCRIBE =", value, "wxSubscribe");
            return (Criteria) this;
        }

        public Criteria andWxSubscribeNotEqualTo(String value) {
            addCriterion("WX_SUBSCRIBE <>", value, "wxSubscribe");
            return (Criteria) this;
        }

        public Criteria andWxSubscribeGreaterThan(String value) {
            addCriterion("WX_SUBSCRIBE >", value, "wxSubscribe");
            return (Criteria) this;
        }

        public Criteria andWxSubscribeGreaterThanOrEqualTo(String value) {
            addCriterion("WX_SUBSCRIBE >=", value, "wxSubscribe");
            return (Criteria) this;
        }

        public Criteria andWxSubscribeLessThan(String value) {
            addCriterion("WX_SUBSCRIBE <", value, "wxSubscribe");
            return (Criteria) this;
        }

        public Criteria andWxSubscribeLessThanOrEqualTo(String value) {
            addCriterion("WX_SUBSCRIBE <=", value, "wxSubscribe");
            return (Criteria) this;
        }

        public Criteria andWxSubscribeLike(String value) {
            addCriterion("WX_SUBSCRIBE like", value, "wxSubscribe");
            return (Criteria) this;
        }

        public Criteria andWxSubscribeNotLike(String value) {
            addCriterion("WX_SUBSCRIBE not like", value, "wxSubscribe");
            return (Criteria) this;
        }

        public Criteria andWxSubscribeIn(List<String> values) {
            addCriterion("WX_SUBSCRIBE in", values, "wxSubscribe");
            return (Criteria) this;
        }

        public Criteria andWxSubscribeNotIn(List<String> values) {
            addCriterion("WX_SUBSCRIBE not in", values, "wxSubscribe");
            return (Criteria) this;
        }

        public Criteria andWxSubscribeBetween(String value1, String value2) {
            addCriterion("WX_SUBSCRIBE between", value1, value2, "wxSubscribe");
            return (Criteria) this;
        }

        public Criteria andWxSubscribeNotBetween(String value1, String value2) {
            addCriterion("WX_SUBSCRIBE not between", value1, value2, "wxSubscribe");
            return (Criteria) this;
        }

        public Criteria andWxSexIsNull() {
            addCriterion("WX_SEX is null");
            return (Criteria) this;
        }

        public Criteria andWxSexIsNotNull() {
            addCriterion("WX_SEX is not null");
            return (Criteria) this;
        }

        public Criteria andWxSexEqualTo(String value) {
            addCriterion("WX_SEX =", value, "wxSex");
            return (Criteria) this;
        }

        public Criteria andWxSexNotEqualTo(String value) {
            addCriterion("WX_SEX <>", value, "wxSex");
            return (Criteria) this;
        }

        public Criteria andWxSexGreaterThan(String value) {
            addCriterion("WX_SEX >", value, "wxSex");
            return (Criteria) this;
        }

        public Criteria andWxSexGreaterThanOrEqualTo(String value) {
            addCriterion("WX_SEX >=", value, "wxSex");
            return (Criteria) this;
        }

        public Criteria andWxSexLessThan(String value) {
            addCriterion("WX_SEX <", value, "wxSex");
            return (Criteria) this;
        }

        public Criteria andWxSexLessThanOrEqualTo(String value) {
            addCriterion("WX_SEX <=", value, "wxSex");
            return (Criteria) this;
        }

        public Criteria andWxSexLike(String value) {
            addCriterion("WX_SEX like", value, "wxSex");
            return (Criteria) this;
        }

        public Criteria andWxSexNotLike(String value) {
            addCriterion("WX_SEX not like", value, "wxSex");
            return (Criteria) this;
        }

        public Criteria andWxSexIn(List<String> values) {
            addCriterion("WX_SEX in", values, "wxSex");
            return (Criteria) this;
        }

        public Criteria andWxSexNotIn(List<String> values) {
            addCriterion("WX_SEX not in", values, "wxSex");
            return (Criteria) this;
        }

        public Criteria andWxSexBetween(String value1, String value2) {
            addCriterion("WX_SEX between", value1, value2, "wxSex");
            return (Criteria) this;
        }

        public Criteria andWxSexNotBetween(String value1, String value2) {
            addCriterion("WX_SEX not between", value1, value2, "wxSex");
            return (Criteria) this;
        }

        public Criteria andWxCityIsNull() {
            addCriterion("WX_CITY is null");
            return (Criteria) this;
        }

        public Criteria andWxCityIsNotNull() {
            addCriterion("WX_CITY is not null");
            return (Criteria) this;
        }

        public Criteria andWxCityEqualTo(String value) {
            addCriterion("WX_CITY =", value, "wxCity");
            return (Criteria) this;
        }

        public Criteria andWxCityNotEqualTo(String value) {
            addCriterion("WX_CITY <>", value, "wxCity");
            return (Criteria) this;
        }

        public Criteria andWxCityGreaterThan(String value) {
            addCriterion("WX_CITY >", value, "wxCity");
            return (Criteria) this;
        }

        public Criteria andWxCityGreaterThanOrEqualTo(String value) {
            addCriterion("WX_CITY >=", value, "wxCity");
            return (Criteria) this;
        }

        public Criteria andWxCityLessThan(String value) {
            addCriterion("WX_CITY <", value, "wxCity");
            return (Criteria) this;
        }

        public Criteria andWxCityLessThanOrEqualTo(String value) {
            addCriterion("WX_CITY <=", value, "wxCity");
            return (Criteria) this;
        }

        public Criteria andWxCityLike(String value) {
            addCriterion("WX_CITY like", value, "wxCity");
            return (Criteria) this;
        }

        public Criteria andWxCityNotLike(String value) {
            addCriterion("WX_CITY not like", value, "wxCity");
            return (Criteria) this;
        }

        public Criteria andWxCityIn(List<String> values) {
            addCriterion("WX_CITY in", values, "wxCity");
            return (Criteria) this;
        }

        public Criteria andWxCityNotIn(List<String> values) {
            addCriterion("WX_CITY not in", values, "wxCity");
            return (Criteria) this;
        }

        public Criteria andWxCityBetween(String value1, String value2) {
            addCriterion("WX_CITY between", value1, value2, "wxCity");
            return (Criteria) this;
        }

        public Criteria andWxCityNotBetween(String value1, String value2) {
            addCriterion("WX_CITY not between", value1, value2, "wxCity");
            return (Criteria) this;
        }

        public Criteria andWxCountryIsNull() {
            addCriterion("WX_COUNTRY is null");
            return (Criteria) this;
        }

        public Criteria andWxCountryIsNotNull() {
            addCriterion("WX_COUNTRY is not null");
            return (Criteria) this;
        }

        public Criteria andWxCountryEqualTo(String value) {
            addCriterion("WX_COUNTRY =", value, "wxCountry");
            return (Criteria) this;
        }

        public Criteria andWxCountryNotEqualTo(String value) {
            addCriterion("WX_COUNTRY <>", value, "wxCountry");
            return (Criteria) this;
        }

        public Criteria andWxCountryGreaterThan(String value) {
            addCriterion("WX_COUNTRY >", value, "wxCountry");
            return (Criteria) this;
        }

        public Criteria andWxCountryGreaterThanOrEqualTo(String value) {
            addCriterion("WX_COUNTRY >=", value, "wxCountry");
            return (Criteria) this;
        }

        public Criteria andWxCountryLessThan(String value) {
            addCriterion("WX_COUNTRY <", value, "wxCountry");
            return (Criteria) this;
        }

        public Criteria andWxCountryLessThanOrEqualTo(String value) {
            addCriterion("WX_COUNTRY <=", value, "wxCountry");
            return (Criteria) this;
        }

        public Criteria andWxCountryLike(String value) {
            addCriterion("WX_COUNTRY like", value, "wxCountry");
            return (Criteria) this;
        }

        public Criteria andWxCountryNotLike(String value) {
            addCriterion("WX_COUNTRY not like", value, "wxCountry");
            return (Criteria) this;
        }

        public Criteria andWxCountryIn(List<String> values) {
            addCriterion("WX_COUNTRY in", values, "wxCountry");
            return (Criteria) this;
        }

        public Criteria andWxCountryNotIn(List<String> values) {
            addCriterion("WX_COUNTRY not in", values, "wxCountry");
            return (Criteria) this;
        }

        public Criteria andWxCountryBetween(String value1, String value2) {
            addCriterion("WX_COUNTRY between", value1, value2, "wxCountry");
            return (Criteria) this;
        }

        public Criteria andWxCountryNotBetween(String value1, String value2) {
            addCriterion("WX_COUNTRY not between", value1, value2, "wxCountry");
            return (Criteria) this;
        }

        public Criteria andWxProvinceIsNull() {
            addCriterion("WX_PROVINCE is null");
            return (Criteria) this;
        }

        public Criteria andWxProvinceIsNotNull() {
            addCriterion("WX_PROVINCE is not null");
            return (Criteria) this;
        }

        public Criteria andWxProvinceEqualTo(String value) {
            addCriterion("WX_PROVINCE =", value, "wxProvince");
            return (Criteria) this;
        }

        public Criteria andWxProvinceNotEqualTo(String value) {
            addCriterion("WX_PROVINCE <>", value, "wxProvince");
            return (Criteria) this;
        }

        public Criteria andWxProvinceGreaterThan(String value) {
            addCriterion("WX_PROVINCE >", value, "wxProvince");
            return (Criteria) this;
        }

        public Criteria andWxProvinceGreaterThanOrEqualTo(String value) {
            addCriterion("WX_PROVINCE >=", value, "wxProvince");
            return (Criteria) this;
        }

        public Criteria andWxProvinceLessThan(String value) {
            addCriterion("WX_PROVINCE <", value, "wxProvince");
            return (Criteria) this;
        }

        public Criteria andWxProvinceLessThanOrEqualTo(String value) {
            addCriterion("WX_PROVINCE <=", value, "wxProvince");
            return (Criteria) this;
        }

        public Criteria andWxProvinceLike(String value) {
            addCriterion("WX_PROVINCE like", value, "wxProvince");
            return (Criteria) this;
        }

        public Criteria andWxProvinceNotLike(String value) {
            addCriterion("WX_PROVINCE not like", value, "wxProvince");
            return (Criteria) this;
        }

        public Criteria andWxProvinceIn(List<String> values) {
            addCriterion("WX_PROVINCE in", values, "wxProvince");
            return (Criteria) this;
        }

        public Criteria andWxProvinceNotIn(List<String> values) {
            addCriterion("WX_PROVINCE not in", values, "wxProvince");
            return (Criteria) this;
        }

        public Criteria andWxProvinceBetween(String value1, String value2) {
            addCriterion("WX_PROVINCE between", value1, value2, "wxProvince");
            return (Criteria) this;
        }

        public Criteria andWxProvinceNotBetween(String value1, String value2) {
            addCriterion("WX_PROVINCE not between", value1, value2, "wxProvince");
            return (Criteria) this;
        }

        public Criteria andWxLanguageIsNull() {
            addCriterion("WX_LANGUAGE is null");
            return (Criteria) this;
        }

        public Criteria andWxLanguageIsNotNull() {
            addCriterion("WX_LANGUAGE is not null");
            return (Criteria) this;
        }

        public Criteria andWxLanguageEqualTo(String value) {
            addCriterion("WX_LANGUAGE =", value, "wxLanguage");
            return (Criteria) this;
        }

        public Criteria andWxLanguageNotEqualTo(String value) {
            addCriterion("WX_LANGUAGE <>", value, "wxLanguage");
            return (Criteria) this;
        }

        public Criteria andWxLanguageGreaterThan(String value) {
            addCriterion("WX_LANGUAGE >", value, "wxLanguage");
            return (Criteria) this;
        }

        public Criteria andWxLanguageGreaterThanOrEqualTo(String value) {
            addCriterion("WX_LANGUAGE >=", value, "wxLanguage");
            return (Criteria) this;
        }

        public Criteria andWxLanguageLessThan(String value) {
            addCriterion("WX_LANGUAGE <", value, "wxLanguage");
            return (Criteria) this;
        }

        public Criteria andWxLanguageLessThanOrEqualTo(String value) {
            addCriterion("WX_LANGUAGE <=", value, "wxLanguage");
            return (Criteria) this;
        }

        public Criteria andWxLanguageLike(String value) {
            addCriterion("WX_LANGUAGE like", value, "wxLanguage");
            return (Criteria) this;
        }

        public Criteria andWxLanguageNotLike(String value) {
            addCriterion("WX_LANGUAGE not like", value, "wxLanguage");
            return (Criteria) this;
        }

        public Criteria andWxLanguageIn(List<String> values) {
            addCriterion("WX_LANGUAGE in", values, "wxLanguage");
            return (Criteria) this;
        }

        public Criteria andWxLanguageNotIn(List<String> values) {
            addCriterion("WX_LANGUAGE not in", values, "wxLanguage");
            return (Criteria) this;
        }

        public Criteria andWxLanguageBetween(String value1, String value2) {
            addCriterion("WX_LANGUAGE between", value1, value2, "wxLanguage");
            return (Criteria) this;
        }

        public Criteria andWxLanguageNotBetween(String value1, String value2) {
            addCriterion("WX_LANGUAGE not between", value1, value2, "wxLanguage");
            return (Criteria) this;
        }

        public Criteria andWxSubscribeTimeIsNull() {
            addCriterion("WX_SUBSCRIBE_TIME is null");
            return (Criteria) this;
        }

        public Criteria andWxSubscribeTimeIsNotNull() {
            addCriterion("WX_SUBSCRIBE_TIME is not null");
            return (Criteria) this;
        }

        public Criteria andWxSubscribeTimeEqualTo(Date value) {
            addCriterion("WX_SUBSCRIBE_TIME =", value, "wxSubscribeTime");
            return (Criteria) this;
        }

        public Criteria andWxSubscribeTimeNotEqualTo(Date value) {
            addCriterion("WX_SUBSCRIBE_TIME <>", value, "wxSubscribeTime");
            return (Criteria) this;
        }

        public Criteria andWxSubscribeTimeGreaterThan(Date value) {
            addCriterion("WX_SUBSCRIBE_TIME >", value, "wxSubscribeTime");
            return (Criteria) this;
        }

        public Criteria andWxSubscribeTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("WX_SUBSCRIBE_TIME >=", value, "wxSubscribeTime");
            return (Criteria) this;
        }

        public Criteria andWxSubscribeTimeLessThan(Date value) {
            addCriterion("WX_SUBSCRIBE_TIME <", value, "wxSubscribeTime");
            return (Criteria) this;
        }

        public Criteria andWxSubscribeTimeLessThanOrEqualTo(Date value) {
            addCriterion("WX_SUBSCRIBE_TIME <=", value, "wxSubscribeTime");
            return (Criteria) this;
        }

        public Criteria andWxSubscribeTimeIn(List<Date> values) {
            addCriterion("WX_SUBSCRIBE_TIME in", values, "wxSubscribeTime");
            return (Criteria) this;
        }

        public Criteria andWxSubscribeTimeNotIn(List<Date> values) {
            addCriterion("WX_SUBSCRIBE_TIME not in", values, "wxSubscribeTime");
            return (Criteria) this;
        }

        public Criteria andWxSubscribeTimeBetween(Date value1, Date value2) {
            addCriterion("WX_SUBSCRIBE_TIME between", value1, value2, "wxSubscribeTime");
            return (Criteria) this;
        }

        public Criteria andWxSubscribeTimeNotBetween(Date value1, Date value2) {
            addCriterion("WX_SUBSCRIBE_TIME not between", value1, value2, "wxSubscribeTime");
            return (Criteria) this;
        }

        public Criteria andWxUnionidIsNull() {
            addCriterion("WX_UNIONID is null");
            return (Criteria) this;
        }

        public Criteria andWxUnionidIsNotNull() {
            addCriterion("WX_UNIONID is not null");
            return (Criteria) this;
        }

        public Criteria andWxUnionidEqualTo(String value) {
            addCriterion("WX_UNIONID =", value, "wxUnionid");
            return (Criteria) this;
        }

        public Criteria andWxUnionidNotEqualTo(String value) {
            addCriterion("WX_UNIONID <>", value, "wxUnionid");
            return (Criteria) this;
        }

        public Criteria andWxUnionidGreaterThan(String value) {
            addCriterion("WX_UNIONID >", value, "wxUnionid");
            return (Criteria) this;
        }

        public Criteria andWxUnionidGreaterThanOrEqualTo(String value) {
            addCriterion("WX_UNIONID >=", value, "wxUnionid");
            return (Criteria) this;
        }

        public Criteria andWxUnionidLessThan(String value) {
            addCriterion("WX_UNIONID <", value, "wxUnionid");
            return (Criteria) this;
        }

        public Criteria andWxUnionidLessThanOrEqualTo(String value) {
            addCriterion("WX_UNIONID <=", value, "wxUnionid");
            return (Criteria) this;
        }

        public Criteria andWxUnionidLike(String value) {
            addCriterion("WX_UNIONID like", value, "wxUnionid");
            return (Criteria) this;
        }

        public Criteria andWxUnionidNotLike(String value) {
            addCriterion("WX_UNIONID not like", value, "wxUnionid");
            return (Criteria) this;
        }

        public Criteria andWxUnionidIn(List<String> values) {
            addCriterion("WX_UNIONID in", values, "wxUnionid");
            return (Criteria) this;
        }

        public Criteria andWxUnionidNotIn(List<String> values) {
            addCriterion("WX_UNIONID not in", values, "wxUnionid");
            return (Criteria) this;
        }

        public Criteria andWxUnionidBetween(String value1, String value2) {
            addCriterion("WX_UNIONID between", value1, value2, "wxUnionid");
            return (Criteria) this;
        }

        public Criteria andWxUnionidNotBetween(String value1, String value2) {
            addCriterion("WX_UNIONID not between", value1, value2, "wxUnionid");
            return (Criteria) this;
        }

        public Criteria andWxRemarkIsNull() {
            addCriterion("WX_REMARK is null");
            return (Criteria) this;
        }

        public Criteria andWxRemarkIsNotNull() {
            addCriterion("WX_REMARK is not null");
            return (Criteria) this;
        }

        public Criteria andWxRemarkEqualTo(String value) {
            addCriterion("WX_REMARK =", value, "wxRemark");
            return (Criteria) this;
        }

        public Criteria andWxRemarkNotEqualTo(String value) {
            addCriterion("WX_REMARK <>", value, "wxRemark");
            return (Criteria) this;
        }

        public Criteria andWxRemarkGreaterThan(String value) {
            addCriterion("WX_REMARK >", value, "wxRemark");
            return (Criteria) this;
        }

        public Criteria andWxRemarkGreaterThanOrEqualTo(String value) {
            addCriterion("WX_REMARK >=", value, "wxRemark");
            return (Criteria) this;
        }

        public Criteria andWxRemarkLessThan(String value) {
            addCriterion("WX_REMARK <", value, "wxRemark");
            return (Criteria) this;
        }

        public Criteria andWxRemarkLessThanOrEqualTo(String value) {
            addCriterion("WX_REMARK <=", value, "wxRemark");
            return (Criteria) this;
        }

        public Criteria andWxRemarkLike(String value) {
            addCriterion("WX_REMARK like", value, "wxRemark");
            return (Criteria) this;
        }

        public Criteria andWxRemarkNotLike(String value) {
            addCriterion("WX_REMARK not like", value, "wxRemark");
            return (Criteria) this;
        }

        public Criteria andWxRemarkIn(List<String> values) {
            addCriterion("WX_REMARK in", values, "wxRemark");
            return (Criteria) this;
        }

        public Criteria andWxRemarkNotIn(List<String> values) {
            addCriterion("WX_REMARK not in", values, "wxRemark");
            return (Criteria) this;
        }

        public Criteria andWxRemarkBetween(String value1, String value2) {
            addCriterion("WX_REMARK between", value1, value2, "wxRemark");
            return (Criteria) this;
        }

        public Criteria andWxRemarkNotBetween(String value1, String value2) {
            addCriterion("WX_REMARK not between", value1, value2, "wxRemark");
            return (Criteria) this;
        }

        public Criteria andWxGroupidIsNull() {
            addCriterion("WX_GROUPID is null");
            return (Criteria) this;
        }

        public Criteria andWxGroupidIsNotNull() {
            addCriterion("WX_GROUPID is not null");
            return (Criteria) this;
        }

        public Criteria andWxGroupidEqualTo(String value) {
            addCriterion("WX_GROUPID =", value, "wxGroupid");
            return (Criteria) this;
        }

        public Criteria andWxGroupidNotEqualTo(String value) {
            addCriterion("WX_GROUPID <>", value, "wxGroupid");
            return (Criteria) this;
        }

        public Criteria andWxGroupidGreaterThan(String value) {
            addCriterion("WX_GROUPID >", value, "wxGroupid");
            return (Criteria) this;
        }

        public Criteria andWxGroupidGreaterThanOrEqualTo(String value) {
            addCriterion("WX_GROUPID >=", value, "wxGroupid");
            return (Criteria) this;
        }

        public Criteria andWxGroupidLessThan(String value) {
            addCriterion("WX_GROUPID <", value, "wxGroupid");
            return (Criteria) this;
        }

        public Criteria andWxGroupidLessThanOrEqualTo(String value) {
            addCriterion("WX_GROUPID <=", value, "wxGroupid");
            return (Criteria) this;
        }

        public Criteria andWxGroupidLike(String value) {
            addCriterion("WX_GROUPID like", value, "wxGroupid");
            return (Criteria) this;
        }

        public Criteria andWxGroupidNotLike(String value) {
            addCriterion("WX_GROUPID not like", value, "wxGroupid");
            return (Criteria) this;
        }

        public Criteria andWxGroupidIn(List<String> values) {
            addCriterion("WX_GROUPID in", values, "wxGroupid");
            return (Criteria) this;
        }

        public Criteria andWxGroupidNotIn(List<String> values) {
            addCriterion("WX_GROUPID not in", values, "wxGroupid");
            return (Criteria) this;
        }

        public Criteria andWxGroupidBetween(String value1, String value2) {
            addCriterion("WX_GROUPID between", value1, value2, "wxGroupid");
            return (Criteria) this;
        }

        public Criteria andWxGroupidNotBetween(String value1, String value2) {
            addCriterion("WX_GROUPID not between", value1, value2, "wxGroupid");
            return (Criteria) this;
        }

        public Criteria andWxTagidListIsNull() {
            addCriterion("WX_TAGID_LIST is null");
            return (Criteria) this;
        }

        public Criteria andWxTagidListIsNotNull() {
            addCriterion("WX_TAGID_LIST is not null");
            return (Criteria) this;
        }

        public Criteria andWxTagidListEqualTo(String value) {
            addCriterion("WX_TAGID_LIST =", value, "wxTagidList");
            return (Criteria) this;
        }

        public Criteria andWxTagidListNotEqualTo(String value) {
            addCriterion("WX_TAGID_LIST <>", value, "wxTagidList");
            return (Criteria) this;
        }

        public Criteria andWxTagidListGreaterThan(String value) {
            addCriterion("WX_TAGID_LIST >", value, "wxTagidList");
            return (Criteria) this;
        }

        public Criteria andWxTagidListGreaterThanOrEqualTo(String value) {
            addCriterion("WX_TAGID_LIST >=", value, "wxTagidList");
            return (Criteria) this;
        }

        public Criteria andWxTagidListLessThan(String value) {
            addCriterion("WX_TAGID_LIST <", value, "wxTagidList");
            return (Criteria) this;
        }

        public Criteria andWxTagidListLessThanOrEqualTo(String value) {
            addCriterion("WX_TAGID_LIST <=", value, "wxTagidList");
            return (Criteria) this;
        }

        public Criteria andWxTagidListLike(String value) {
            addCriterion("WX_TAGID_LIST like", value, "wxTagidList");
            return (Criteria) this;
        }

        public Criteria andWxTagidListNotLike(String value) {
            addCriterion("WX_TAGID_LIST not like", value, "wxTagidList");
            return (Criteria) this;
        }

        public Criteria andWxTagidListIn(List<String> values) {
            addCriterion("WX_TAGID_LIST in", values, "wxTagidList");
            return (Criteria) this;
        }

        public Criteria andWxTagidListNotIn(List<String> values) {
            addCriterion("WX_TAGID_LIST not in", values, "wxTagidList");
            return (Criteria) this;
        }

        public Criteria andWxTagidListBetween(String value1, String value2) {
            addCriterion("WX_TAGID_LIST between", value1, value2, "wxTagidList");
            return (Criteria) this;
        }

        public Criteria andWxTagidListNotBetween(String value1, String value2) {
            addCriterion("WX_TAGID_LIST not between", value1, value2, "wxTagidList");
            return (Criteria) this;
        }

        public Criteria andPointsIsNull() {
            addCriterion("POINTS is null");
            return (Criteria) this;
        }

        public Criteria andPointsIsNotNull() {
            addCriterion("POINTS is not null");
            return (Criteria) this;
        }

        public Criteria andPointsEqualTo(Integer value) {
            addCriterion("POINTS =", value, "points");
            return (Criteria) this;
        }

        public Criteria andPointsNotEqualTo(Integer value) {
            addCriterion("POINTS <>", value, "points");
            return (Criteria) this;
        }

        public Criteria andPointsGreaterThan(Integer value) {
            addCriterion("POINTS >", value, "points");
            return (Criteria) this;
        }

        public Criteria andPointsGreaterThanOrEqualTo(Integer value) {
            addCriterion("POINTS >=", value, "points");
            return (Criteria) this;
        }

        public Criteria andPointsLessThan(Integer value) {
            addCriterion("POINTS <", value, "points");
            return (Criteria) this;
        }

        public Criteria andPointsLessThanOrEqualTo(Integer value) {
            addCriterion("POINTS <=", value, "points");
            return (Criteria) this;
        }

        public Criteria andPointsIn(List<Integer> values) {
            addCriterion("POINTS in", values, "points");
            return (Criteria) this;
        }

        public Criteria andPointsNotIn(List<Integer> values) {
            addCriterion("POINTS not in", values, "points");
            return (Criteria) this;
        }

        public Criteria andPointsBetween(Integer value1, Integer value2) {
            addCriterion("POINTS between", value1, value2, "points");
            return (Criteria) this;
        }

        public Criteria andPointsNotBetween(Integer value1, Integer value2) {
            addCriterion("POINTS not between", value1, value2, "points");
            return (Criteria) this;
        }

        public Criteria andRegisteDateIsNull() {
            addCriterion("REGISTE_DATE is null");
            return (Criteria) this;
        }

        public Criteria andRegisteDateIsNotNull() {
            addCriterion("REGISTE_DATE is not null");
            return (Criteria) this;
        }

        public Criteria andRegisteDateEqualTo(Date value) {
            addCriterion("REGISTE_DATE =", value, "registeDate");
            return (Criteria) this;
        }

        public Criteria andRegisteDateNotEqualTo(Date value) {
            addCriterion("REGISTE_DATE <>", value, "registeDate");
            return (Criteria) this;
        }

        public Criteria andRegisteDateGreaterThan(Date value) {
            addCriterion("REGISTE_DATE >", value, "registeDate");
            return (Criteria) this;
        }

        public Criteria andRegisteDateGreaterThanOrEqualTo(Date value) {
            addCriterion("REGISTE_DATE >=", value, "registeDate");
            return (Criteria) this;
        }

        public Criteria andRegisteDateLessThan(Date value) {
            addCriterion("REGISTE_DATE <", value, "registeDate");
            return (Criteria) this;
        }

        public Criteria andRegisteDateLessThanOrEqualTo(Date value) {
            addCriterion("REGISTE_DATE <=", value, "registeDate");
            return (Criteria) this;
        }

        public Criteria andRegisteDateIn(List<Date> values) {
            addCriterion("REGISTE_DATE in", values, "registeDate");
            return (Criteria) this;
        }

        public Criteria andRegisteDateNotIn(List<Date> values) {
            addCriterion("REGISTE_DATE not in", values, "registeDate");
            return (Criteria) this;
        }

        public Criteria andRegisteDateBetween(Date value1, Date value2) {
            addCriterion("REGISTE_DATE between", value1, value2, "registeDate");
            return (Criteria) this;
        }

        public Criteria andRegisteDateNotBetween(Date value1, Date value2) {
            addCriterion("REGISTE_DATE not between", value1, value2, "registeDate");
            return (Criteria) this;
        }

        public Criteria andLastloginDateIsNull() {
            addCriterion("LASTLOGIN_DATE is null");
            return (Criteria) this;
        }

        public Criteria andLastloginDateIsNotNull() {
            addCriterion("LASTLOGIN_DATE is not null");
            return (Criteria) this;
        }

        public Criteria andLastloginDateEqualTo(Date value) {
            addCriterion("LASTLOGIN_DATE =", value, "lastloginDate");
            return (Criteria) this;
        }

        public Criteria andLastloginDateNotEqualTo(Date value) {
            addCriterion("LASTLOGIN_DATE <>", value, "lastloginDate");
            return (Criteria) this;
        }

        public Criteria andLastloginDateGreaterThan(Date value) {
            addCriterion("LASTLOGIN_DATE >", value, "lastloginDate");
            return (Criteria) this;
        }

        public Criteria andLastloginDateGreaterThanOrEqualTo(Date value) {
            addCriterion("LASTLOGIN_DATE >=", value, "lastloginDate");
            return (Criteria) this;
        }

        public Criteria andLastloginDateLessThan(Date value) {
            addCriterion("LASTLOGIN_DATE <", value, "lastloginDate");
            return (Criteria) this;
        }

        public Criteria andLastloginDateLessThanOrEqualTo(Date value) {
            addCriterion("LASTLOGIN_DATE <=", value, "lastloginDate");
            return (Criteria) this;
        }

        public Criteria andLastloginDateIn(List<Date> values) {
            addCriterion("LASTLOGIN_DATE in", values, "lastloginDate");
            return (Criteria) this;
        }

        public Criteria andLastloginDateNotIn(List<Date> values) {
            addCriterion("LASTLOGIN_DATE not in", values, "lastloginDate");
            return (Criteria) this;
        }

        public Criteria andLastloginDateBetween(Date value1, Date value2) {
            addCriterion("LASTLOGIN_DATE between", value1, value2, "lastloginDate");
            return (Criteria) this;
        }

        public Criteria andLastloginDateNotBetween(Date value1, Date value2) {
            addCriterion("LASTLOGIN_DATE not between", value1, value2, "lastloginDate");
            return (Criteria) this;
        }

        public Criteria andLoginTimesIsNull() {
            addCriterion("LOGIN_TIMES is null");
            return (Criteria) this;
        }

        public Criteria andLoginTimesIsNotNull() {
            addCriterion("LOGIN_TIMES is not null");
            return (Criteria) this;
        }

        public Criteria andLoginTimesEqualTo(Integer value) {
            addCriterion("LOGIN_TIMES =", value, "loginTimes");
            return (Criteria) this;
        }

        public Criteria andLoginTimesNotEqualTo(Integer value) {
            addCriterion("LOGIN_TIMES <>", value, "loginTimes");
            return (Criteria) this;
        }

        public Criteria andLoginTimesGreaterThan(Integer value) {
            addCriterion("LOGIN_TIMES >", value, "loginTimes");
            return (Criteria) this;
        }

        public Criteria andLoginTimesGreaterThanOrEqualTo(Integer value) {
            addCriterion("LOGIN_TIMES >=", value, "loginTimes");
            return (Criteria) this;
        }

        public Criteria andLoginTimesLessThan(Integer value) {
            addCriterion("LOGIN_TIMES <", value, "loginTimes");
            return (Criteria) this;
        }

        public Criteria andLoginTimesLessThanOrEqualTo(Integer value) {
            addCriterion("LOGIN_TIMES <=", value, "loginTimes");
            return (Criteria) this;
        }

        public Criteria andLoginTimesIn(List<Integer> values) {
            addCriterion("LOGIN_TIMES in", values, "loginTimes");
            return (Criteria) this;
        }

        public Criteria andLoginTimesNotIn(List<Integer> values) {
            addCriterion("LOGIN_TIMES not in", values, "loginTimes");
            return (Criteria) this;
        }

        public Criteria andLoginTimesBetween(Integer value1, Integer value2) {
            addCriterion("LOGIN_TIMES between", value1, value2, "loginTimes");
            return (Criteria) this;
        }

        public Criteria andLoginTimesNotBetween(Integer value1, Integer value2) {
            addCriterion("LOGIN_TIMES not between", value1, value2, "loginTimes");
            return (Criteria) this;
        }

        public Criteria andLoginTimesContinuityIsNull() {
            addCriterion("LOGIN_TIMES_CONTINUITY is null");
            return (Criteria) this;
        }

        public Criteria andLoginTimesContinuityIsNotNull() {
            addCriterion("LOGIN_TIMES_CONTINUITY is not null");
            return (Criteria) this;
        }

        public Criteria andLoginTimesContinuityEqualTo(Integer value) {
            addCriterion("LOGIN_TIMES_CONTINUITY =", value, "loginTimesContinuity");
            return (Criteria) this;
        }

        public Criteria andLoginTimesContinuityNotEqualTo(Integer value) {
            addCriterion("LOGIN_TIMES_CONTINUITY <>", value, "loginTimesContinuity");
            return (Criteria) this;
        }

        public Criteria andLoginTimesContinuityGreaterThan(Integer value) {
            addCriterion("LOGIN_TIMES_CONTINUITY >", value, "loginTimesContinuity");
            return (Criteria) this;
        }

        public Criteria andLoginTimesContinuityGreaterThanOrEqualTo(Integer value) {
            addCriterion("LOGIN_TIMES_CONTINUITY >=", value, "loginTimesContinuity");
            return (Criteria) this;
        }

        public Criteria andLoginTimesContinuityLessThan(Integer value) {
            addCriterion("LOGIN_TIMES_CONTINUITY <", value, "loginTimesContinuity");
            return (Criteria) this;
        }

        public Criteria andLoginTimesContinuityLessThanOrEqualTo(Integer value) {
            addCriterion("LOGIN_TIMES_CONTINUITY <=", value, "loginTimesContinuity");
            return (Criteria) this;
        }

        public Criteria andLoginTimesContinuityIn(List<Integer> values) {
            addCriterion("LOGIN_TIMES_CONTINUITY in", values, "loginTimesContinuity");
            return (Criteria) this;
        }

        public Criteria andLoginTimesContinuityNotIn(List<Integer> values) {
            addCriterion("LOGIN_TIMES_CONTINUITY not in", values, "loginTimesContinuity");
            return (Criteria) this;
        }

        public Criteria andLoginTimesContinuityBetween(Integer value1, Integer value2) {
            addCriterion("LOGIN_TIMES_CONTINUITY between", value1, value2, "loginTimesContinuity");
            return (Criteria) this;
        }

        public Criteria andLoginTimesContinuityNotBetween(Integer value1, Integer value2) {
            addCriterion("LOGIN_TIMES_CONTINUITY not between", value1, value2, "loginTimesContinuity");
            return (Criteria) this;
        }

        public Criteria andShippingAddressIsNull() {
            addCriterion("SHIPPING_ADDRESS is null");
            return (Criteria) this;
        }

        public Criteria andShippingAddressIsNotNull() {
            addCriterion("SHIPPING_ADDRESS is not null");
            return (Criteria) this;
        }

        public Criteria andShippingAddressEqualTo(String value) {
            addCriterion("SHIPPING_ADDRESS =", value, "shippingAddress");
            return (Criteria) this;
        }

        public Criteria andShippingAddressNotEqualTo(String value) {
            addCriterion("SHIPPING_ADDRESS <>", value, "shippingAddress");
            return (Criteria) this;
        }

        public Criteria andShippingAddressGreaterThan(String value) {
            addCriterion("SHIPPING_ADDRESS >", value, "shippingAddress");
            return (Criteria) this;
        }

        public Criteria andShippingAddressGreaterThanOrEqualTo(String value) {
            addCriterion("SHIPPING_ADDRESS >=", value, "shippingAddress");
            return (Criteria) this;
        }

        public Criteria andShippingAddressLessThan(String value) {
            addCriterion("SHIPPING_ADDRESS <", value, "shippingAddress");
            return (Criteria) this;
        }

        public Criteria andShippingAddressLessThanOrEqualTo(String value) {
            addCriterion("SHIPPING_ADDRESS <=", value, "shippingAddress");
            return (Criteria) this;
        }

        public Criteria andShippingAddressLike(String value) {
            addCriterion("SHIPPING_ADDRESS like", value, "shippingAddress");
            return (Criteria) this;
        }

        public Criteria andShippingAddressNotLike(String value) {
            addCriterion("SHIPPING_ADDRESS not like", value, "shippingAddress");
            return (Criteria) this;
        }

        public Criteria andShippingAddressIn(List<String> values) {
            addCriterion("SHIPPING_ADDRESS in", values, "shippingAddress");
            return (Criteria) this;
        }

        public Criteria andShippingAddressNotIn(List<String> values) {
            addCriterion("SHIPPING_ADDRESS not in", values, "shippingAddress");
            return (Criteria) this;
        }

        public Criteria andShippingAddressBetween(String value1, String value2) {
            addCriterion("SHIPPING_ADDRESS between", value1, value2, "shippingAddress");
            return (Criteria) this;
        }

        public Criteria andShippingAddressNotBetween(String value1, String value2) {
            addCriterion("SHIPPING_ADDRESS not between", value1, value2, "shippingAddress");
            return (Criteria) this;
        }

        public Criteria andLevelIsNull() {
            addCriterion("LEVEL is null");
            return (Criteria) this;
        }

        public Criteria andLevelIsNotNull() {
            addCriterion("LEVEL is not null");
            return (Criteria) this;
        }

        public Criteria andLevelEqualTo(String value) {
            addCriterion("LEVEL =", value, "level");
            return (Criteria) this;
        }

        public Criteria andLevelNotEqualTo(String value) {
            addCriterion("LEVEL <>", value, "level");
            return (Criteria) this;
        }

        public Criteria andLevelGreaterThan(String value) {
            addCriterion("LEVEL >", value, "level");
            return (Criteria) this;
        }

        public Criteria andLevelGreaterThanOrEqualTo(String value) {
            addCriterion("LEVEL >=", value, "level");
            return (Criteria) this;
        }

        public Criteria andLevelLessThan(String value) {
            addCriterion("LEVEL <", value, "level");
            return (Criteria) this;
        }

        public Criteria andLevelLessThanOrEqualTo(String value) {
            addCriterion("LEVEL <=", value, "level");
            return (Criteria) this;
        }

        public Criteria andLevelLike(String value) {
            addCriterion("LEVEL like", value, "level");
            return (Criteria) this;
        }

        public Criteria andLevelNotLike(String value) {
            addCriterion("LEVEL not like", value, "level");
            return (Criteria) this;
        }

        public Criteria andLevelIn(List<String> values) {
            addCriterion("LEVEL in", values, "level");
            return (Criteria) this;
        }

        public Criteria andLevelNotIn(List<String> values) {
            addCriterion("LEVEL not in", values, "level");
            return (Criteria) this;
        }

        public Criteria andLevelBetween(String value1, String value2) {
            addCriterion("LEVEL between", value1, value2, "level");
            return (Criteria) this;
        }

        public Criteria andLevelNotBetween(String value1, String value2) {
            addCriterion("LEVEL not between", value1, value2, "level");
            return (Criteria) this;
        }

        public Criteria andStoreCreateTimeIsNull() {
            addCriterion("STORE_CREATE_TIME is null");
            return (Criteria) this;
        }

        public Criteria andStoreCreateTimeIsNotNull() {
            addCriterion("STORE_CREATE_TIME is not null");
            return (Criteria) this;
        }

        public Criteria andStoreCreateTimeEqualTo(Date value) {
            addCriterion("STORE_CREATE_TIME =", value, "storeCreateTime");
            return (Criteria) this;
        }

        public Criteria andStoreCreateTimeNotEqualTo(Date value) {
            addCriterion("STORE_CREATE_TIME <>", value, "storeCreateTime");
            return (Criteria) this;
        }

        public Criteria andStoreCreateTimeGreaterThan(Date value) {
            addCriterion("STORE_CREATE_TIME >", value, "storeCreateTime");
            return (Criteria) this;
        }

        public Criteria andStoreCreateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("STORE_CREATE_TIME >=", value, "storeCreateTime");
            return (Criteria) this;
        }

        public Criteria andStoreCreateTimeLessThan(Date value) {
            addCriterion("STORE_CREATE_TIME <", value, "storeCreateTime");
            return (Criteria) this;
        }

        public Criteria andStoreCreateTimeLessThanOrEqualTo(Date value) {
            addCriterion("STORE_CREATE_TIME <=", value, "storeCreateTime");
            return (Criteria) this;
        }

        public Criteria andStoreCreateTimeIn(List<Date> values) {
            addCriterion("STORE_CREATE_TIME in", values, "storeCreateTime");
            return (Criteria) this;
        }

        public Criteria andStoreCreateTimeNotIn(List<Date> values) {
            addCriterion("STORE_CREATE_TIME not in", values, "storeCreateTime");
            return (Criteria) this;
        }

        public Criteria andStoreCreateTimeBetween(Date value1, Date value2) {
            addCriterion("STORE_CREATE_TIME between", value1, value2, "storeCreateTime");
            return (Criteria) this;
        }

        public Criteria andStoreCreateTimeNotBetween(Date value1, Date value2) {
            addCriterion("STORE_CREATE_TIME not between", value1, value2, "storeCreateTime");
            return (Criteria) this;
        }

        public Criteria andStoreFlagIsNull() {
            addCriterion("STORE_FLAG is null");
            return (Criteria) this;
        }

        public Criteria andStoreFlagIsNotNull() {
            addCriterion("STORE_FLAG is not null");
            return (Criteria) this;
        }

        public Criteria andStoreFlagEqualTo(String value) {
            addCriterion("STORE_FLAG =", value, "storeFlag");
            return (Criteria) this;
        }

        public Criteria andStoreFlagNotEqualTo(String value) {
            addCriterion("STORE_FLAG <>", value, "storeFlag");
            return (Criteria) this;
        }

        public Criteria andStoreFlagGreaterThan(String value) {
            addCriterion("STORE_FLAG >", value, "storeFlag");
            return (Criteria) this;
        }

        public Criteria andStoreFlagGreaterThanOrEqualTo(String value) {
            addCriterion("STORE_FLAG >=", value, "storeFlag");
            return (Criteria) this;
        }

        public Criteria andStoreFlagLessThan(String value) {
            addCriterion("STORE_FLAG <", value, "storeFlag");
            return (Criteria) this;
        }

        public Criteria andStoreFlagLessThanOrEqualTo(String value) {
            addCriterion("STORE_FLAG <=", value, "storeFlag");
            return (Criteria) this;
        }

        public Criteria andStoreFlagLike(String value) {
            addCriterion("STORE_FLAG like", value, "storeFlag");
            return (Criteria) this;
        }

        public Criteria andStoreFlagNotLike(String value) {
            addCriterion("STORE_FLAG not like", value, "storeFlag");
            return (Criteria) this;
        }

        public Criteria andStoreFlagIn(List<String> values) {
            addCriterion("STORE_FLAG in", values, "storeFlag");
            return (Criteria) this;
        }

        public Criteria andStoreFlagNotIn(List<String> values) {
            addCriterion("STORE_FLAG not in", values, "storeFlag");
            return (Criteria) this;
        }

        public Criteria andStoreFlagBetween(String value1, String value2) {
            addCriterion("STORE_FLAG between", value1, value2, "storeFlag");
            return (Criteria) this;
        }

        public Criteria andStoreFlagNotBetween(String value1, String value2) {
            addCriterion("STORE_FLAG not between", value1, value2, "storeFlag");
            return (Criteria) this;
        }

        public Criteria andLoginStateIsNull() {
            addCriterion("LOGIN_STATE is null");
            return (Criteria) this;
        }

        public Criteria andLoginStateIsNotNull() {
            addCriterion("LOGIN_STATE is not null");
            return (Criteria) this;
        }

        public Criteria andLoginStateEqualTo(String value) {
            addCriterion("LOGIN_STATE =", value, "loginState");
            return (Criteria) this;
        }

        public Criteria andLoginStateNotEqualTo(String value) {
            addCriterion("LOGIN_STATE <>", value, "loginState");
            return (Criteria) this;
        }

        public Criteria andLoginStateGreaterThan(String value) {
            addCriterion("LOGIN_STATE >", value, "loginState");
            return (Criteria) this;
        }

        public Criteria andLoginStateGreaterThanOrEqualTo(String value) {
            addCriterion("LOGIN_STATE >=", value, "loginState");
            return (Criteria) this;
        }

        public Criteria andLoginStateLessThan(String value) {
            addCriterion("LOGIN_STATE <", value, "loginState");
            return (Criteria) this;
        }

        public Criteria andLoginStateLessThanOrEqualTo(String value) {
            addCriterion("LOGIN_STATE <=", value, "loginState");
            return (Criteria) this;
        }

        public Criteria andLoginStateLike(String value) {
            addCriterion("LOGIN_STATE like", value, "loginState");
            return (Criteria) this;
        }

        public Criteria andLoginStateNotLike(String value) {
            addCriterion("LOGIN_STATE not like", value, "loginState");
            return (Criteria) this;
        }

        public Criteria andLoginStateIn(List<String> values) {
            addCriterion("LOGIN_STATE in", values, "loginState");
            return (Criteria) this;
        }

        public Criteria andLoginStateNotIn(List<String> values) {
            addCriterion("LOGIN_STATE not in", values, "loginState");
            return (Criteria) this;
        }

        public Criteria andLoginStateBetween(String value1, String value2) {
            addCriterion("LOGIN_STATE between", value1, value2, "loginState");
            return (Criteria) this;
        }

        public Criteria andLoginStateNotBetween(String value1, String value2) {
            addCriterion("LOGIN_STATE not between", value1, value2, "loginState");
            return (Criteria) this;
        }

        public Criteria andStateIsNull() {
            addCriterion("STATE is null");
            return (Criteria) this;
        }

        public Criteria andStateIsNotNull() {
            addCriterion("STATE is not null");
            return (Criteria) this;
        }

        public Criteria andStateEqualTo(String value) {
            addCriterion("STATE =", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateNotEqualTo(String value) {
            addCriterion("STATE <>", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateGreaterThan(String value) {
            addCriterion("STATE >", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateGreaterThanOrEqualTo(String value) {
            addCriterion("STATE >=", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateLessThan(String value) {
            addCriterion("STATE <", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateLessThanOrEqualTo(String value) {
            addCriterion("STATE <=", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateLike(String value) {
            addCriterion("STATE like", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateNotLike(String value) {
            addCriterion("STATE not like", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateIn(List<String> values) {
            addCriterion("STATE in", values, "state");
            return (Criteria) this;
        }

        public Criteria andStateNotIn(List<String> values) {
            addCriterion("STATE not in", values, "state");
            return (Criteria) this;
        }

        public Criteria andStateBetween(String value1, String value2) {
            addCriterion("STATE between", value1, value2, "state");
            return (Criteria) this;
        }

        public Criteria andStateNotBetween(String value1, String value2) {
            addCriterion("STATE not between", value1, value2, "state");
            return (Criteria) this;
        }

        public Criteria andRefereeIdIsNull() {
            addCriterion("REFEREE_ID is null");
            return (Criteria) this;
        }

        public Criteria andRefereeIdIsNotNull() {
            addCriterion("REFEREE_ID is not null");
            return (Criteria) this;
        }

        public Criteria andRefereeIdEqualTo(String value) {
            addCriterion("REFEREE_ID =", value, "refereeId");
            return (Criteria) this;
        }

        public Criteria andRefereeIdNotEqualTo(String value) {
            addCriterion("REFEREE_ID <>", value, "refereeId");
            return (Criteria) this;
        }

        public Criteria andRefereeIdGreaterThan(String value) {
            addCriterion("REFEREE_ID >", value, "refereeId");
            return (Criteria) this;
        }

        public Criteria andRefereeIdGreaterThanOrEqualTo(String value) {
            addCriterion("REFEREE_ID >=", value, "refereeId");
            return (Criteria) this;
        }

        public Criteria andRefereeIdLessThan(String value) {
            addCriterion("REFEREE_ID <", value, "refereeId");
            return (Criteria) this;
        }

        public Criteria andRefereeIdLessThanOrEqualTo(String value) {
            addCriterion("REFEREE_ID <=", value, "refereeId");
            return (Criteria) this;
        }

        public Criteria andRefereeIdLike(String value) {
            addCriterion("REFEREE_ID like", value, "refereeId");
            return (Criteria) this;
        }

        public Criteria andRefereeIdNotLike(String value) {
            addCriterion("REFEREE_ID not like", value, "refereeId");
            return (Criteria) this;
        }

        public Criteria andRefereeIdIn(List<String> values) {
            addCriterion("REFEREE_ID in", values, "refereeId");
            return (Criteria) this;
        }

        public Criteria andRefereeIdNotIn(List<String> values) {
            addCriterion("REFEREE_ID not in", values, "refereeId");
            return (Criteria) this;
        }

        public Criteria andRefereeIdBetween(String value1, String value2) {
            addCriterion("REFEREE_ID between", value1, value2, "refereeId");
            return (Criteria) this;
        }

        public Criteria andRefereeIdNotBetween(String value1, String value2) {
            addCriterion("REFEREE_ID not between", value1, value2, "refereeId");
            return (Criteria) this;
        }

        public Criteria andTypeStateIsNull() {
            addCriterion("TYPE_STATE is null");
            return (Criteria) this;
        }

        public Criteria andTypeStateIsNotNull() {
            addCriterion("TYPE_STATE is not null");
            return (Criteria) this;
        }

        public Criteria andTypeStateEqualTo(String value) {
            addCriterion("TYPE_STATE =", value, "typeState");
            return (Criteria) this;
        }

        public Criteria andTypeStateNotEqualTo(String value) {
            addCriterion("TYPE_STATE <>", value, "typeState");
            return (Criteria) this;
        }

        public Criteria andTypeStateGreaterThan(String value) {
            addCriterion("TYPE_STATE >", value, "typeState");
            return (Criteria) this;
        }

        public Criteria andTypeStateGreaterThanOrEqualTo(String value) {
            addCriterion("TYPE_STATE >=", value, "typeState");
            return (Criteria) this;
        }

        public Criteria andTypeStateLessThan(String value) {
            addCriterion("TYPE_STATE <", value, "typeState");
            return (Criteria) this;
        }

        public Criteria andTypeStateLessThanOrEqualTo(String value) {
            addCriterion("TYPE_STATE <=", value, "typeState");
            return (Criteria) this;
        }

        public Criteria andTypeStateLike(String value) {
            addCriterion("TYPE_STATE like", value, "typeState");
            return (Criteria) this;
        }

        public Criteria andTypeStateNotLike(String value) {
            addCriterion("TYPE_STATE not like", value, "typeState");
            return (Criteria) this;
        }

        public Criteria andTypeStateIn(List<String> values) {
            addCriterion("TYPE_STATE in", values, "typeState");
            return (Criteria) this;
        }

        public Criteria andTypeStateNotIn(List<String> values) {
            addCriterion("TYPE_STATE not in", values, "typeState");
            return (Criteria) this;
        }

        public Criteria andTypeStateBetween(String value1, String value2) {
            addCriterion("TYPE_STATE between", value1, value2, "typeState");
            return (Criteria) this;
        }

        public Criteria andTypeStateNotBetween(String value1, String value2) {
            addCriterion("TYPE_STATE not between", value1, value2, "typeState");
            return (Criteria) this;
        }

        public Criteria andPopularityIsNull() {
            addCriterion("POPULARITY is null");
            return (Criteria) this;
        }

        public Criteria andPopularityIsNotNull() {
            addCriterion("POPULARITY is not null");
            return (Criteria) this;
        }

        public Criteria andPopularityEqualTo(Integer value) {
            addCriterion("POPULARITY =", value, "popularity");
            return (Criteria) this;
        }

        public Criteria andPopularityNotEqualTo(Integer value) {
            addCriterion("POPULARITY <>", value, "popularity");
            return (Criteria) this;
        }

        public Criteria andPopularityGreaterThan(Integer value) {
            addCriterion("POPULARITY >", value, "popularity");
            return (Criteria) this;
        }

        public Criteria andPopularityGreaterThanOrEqualTo(Integer value) {
            addCriterion("POPULARITY >=", value, "popularity");
            return (Criteria) this;
        }

        public Criteria andPopularityLessThan(Integer value) {
            addCriterion("POPULARITY <", value, "popularity");
            return (Criteria) this;
        }

        public Criteria andPopularityLessThanOrEqualTo(Integer value) {
            addCriterion("POPULARITY <=", value, "popularity");
            return (Criteria) this;
        }

        public Criteria andPopularityIn(List<Integer> values) {
            addCriterion("POPULARITY in", values, "popularity");
            return (Criteria) this;
        }

        public Criteria andPopularityNotIn(List<Integer> values) {
            addCriterion("POPULARITY not in", values, "popularity");
            return (Criteria) this;
        }

        public Criteria andPopularityBetween(Integer value1, Integer value2) {
            addCriterion("POPULARITY between", value1, value2, "popularity");
            return (Criteria) this;
        }

        public Criteria andPopularityNotBetween(Integer value1, Integer value2) {
            addCriterion("POPULARITY not between", value1, value2, "popularity");
            return (Criteria) this;
        }

        public Criteria andRefereeProIsNull() {
            addCriterion("REFEREE_PRO is null");
            return (Criteria) this;
        }

        public Criteria andRefereeProIsNotNull() {
            addCriterion("REFEREE_PRO is not null");
            return (Criteria) this;
        }

        public Criteria andRefereeProEqualTo(String value) {
            addCriterion("REFEREE_PRO =", value, "refereePro");
            return (Criteria) this;
        }

        public Criteria andRefereeProNotEqualTo(String value) {
            addCriterion("REFEREE_PRO <>", value, "refereePro");
            return (Criteria) this;
        }

        public Criteria andRefereeProGreaterThan(String value) {
            addCriterion("REFEREE_PRO >", value, "refereePro");
            return (Criteria) this;
        }

        public Criteria andRefereeProGreaterThanOrEqualTo(String value) {
            addCriterion("REFEREE_PRO >=", value, "refereePro");
            return (Criteria) this;
        }

        public Criteria andRefereeProLessThan(String value) {
            addCriterion("REFEREE_PRO <", value, "refereePro");
            return (Criteria) this;
        }

        public Criteria andRefereeProLessThanOrEqualTo(String value) {
            addCriterion("REFEREE_PRO <=", value, "refereePro");
            return (Criteria) this;
        }

        public Criteria andRefereeProLike(String value) {
            addCriterion("REFEREE_PRO like", value, "refereePro");
            return (Criteria) this;
        }

        public Criteria andRefereeProNotLike(String value) {
            addCriterion("REFEREE_PRO not like", value, "refereePro");
            return (Criteria) this;
        }

        public Criteria andRefereeProIn(List<String> values) {
            addCriterion("REFEREE_PRO in", values, "refereePro");
            return (Criteria) this;
        }

        public Criteria andRefereeProNotIn(List<String> values) {
            addCriterion("REFEREE_PRO not in", values, "refereePro");
            return (Criteria) this;
        }

        public Criteria andRefereeProBetween(String value1, String value2) {
            addCriterion("REFEREE_PRO between", value1, value2, "refereePro");
            return (Criteria) this;
        }

        public Criteria andRefereeProNotBetween(String value1, String value2) {
            addCriterion("REFEREE_PRO not between", value1, value2, "refereePro");
            return (Criteria) this;
        }

        public Criteria andShopIntroducIsNull() {
            addCriterion("SHOP_INTRODUC is null");
            return (Criteria) this;
        }

        public Criteria andShopIntroducIsNotNull() {
            addCriterion("SHOP_INTRODUC is not null");
            return (Criteria) this;
        }

        public Criteria andShopIntroducEqualTo(String value) {
            addCriterion("SHOP_INTRODUC =", value, "shopIntroduc");
            return (Criteria) this;
        }

        public Criteria andShopIntroducNotEqualTo(String value) {
            addCriterion("SHOP_INTRODUC <>", value, "shopIntroduc");
            return (Criteria) this;
        }

        public Criteria andShopIntroducGreaterThan(String value) {
            addCriterion("SHOP_INTRODUC >", value, "shopIntroduc");
            return (Criteria) this;
        }

        public Criteria andShopIntroducGreaterThanOrEqualTo(String value) {
            addCriterion("SHOP_INTRODUC >=", value, "shopIntroduc");
            return (Criteria) this;
        }

        public Criteria andShopIntroducLessThan(String value) {
            addCriterion("SHOP_INTRODUC <", value, "shopIntroduc");
            return (Criteria) this;
        }

        public Criteria andShopIntroducLessThanOrEqualTo(String value) {
            addCriterion("SHOP_INTRODUC <=", value, "shopIntroduc");
            return (Criteria) this;
        }

        public Criteria andShopIntroducLike(String value) {
            addCriterion("SHOP_INTRODUC like", value, "shopIntroduc");
            return (Criteria) this;
        }

        public Criteria andShopIntroducNotLike(String value) {
            addCriterion("SHOP_INTRODUC not like", value, "shopIntroduc");
            return (Criteria) this;
        }

        public Criteria andShopIntroducIn(List<String> values) {
            addCriterion("SHOP_INTRODUC in", values, "shopIntroduc");
            return (Criteria) this;
        }

        public Criteria andShopIntroducNotIn(List<String> values) {
            addCriterion("SHOP_INTRODUC not in", values, "shopIntroduc");
            return (Criteria) this;
        }

        public Criteria andShopIntroducBetween(String value1, String value2) {
            addCriterion("SHOP_INTRODUC between", value1, value2, "shopIntroduc");
            return (Criteria) this;
        }

        public Criteria andShopIntroducNotBetween(String value1, String value2) {
            addCriterion("SHOP_INTRODUC not between", value1, value2, "shopIntroduc");
            return (Criteria) this;
        }

        public Criteria andShopNameIsNull() {
            addCriterion("SHOP_NAME is null");
            return (Criteria) this;
        }

        public Criteria andShopNameIsNotNull() {
            addCriterion("SHOP_NAME is not null");
            return (Criteria) this;
        }

        public Criteria andShopNameEqualTo(String value) {
            addCriterion("SHOP_NAME =", value, "shopName");
            return (Criteria) this;
        }

        public Criteria andShopNameNotEqualTo(String value) {
            addCriterion("SHOP_NAME <>", value, "shopName");
            return (Criteria) this;
        }

        public Criteria andShopNameGreaterThan(String value) {
            addCriterion("SHOP_NAME >", value, "shopName");
            return (Criteria) this;
        }

        public Criteria andShopNameGreaterThanOrEqualTo(String value) {
            addCriterion("SHOP_NAME >=", value, "shopName");
            return (Criteria) this;
        }

        public Criteria andShopNameLessThan(String value) {
            addCriterion("SHOP_NAME <", value, "shopName");
            return (Criteria) this;
        }

        public Criteria andShopNameLessThanOrEqualTo(String value) {
            addCriterion("SHOP_NAME <=", value, "shopName");
            return (Criteria) this;
        }

        public Criteria andShopNameLike(String value) {
            addCriterion("SHOP_NAME like", value, "shopName");
            return (Criteria) this;
        }

        public Criteria andShopNameNotLike(String value) {
            addCriterion("SHOP_NAME not like", value, "shopName");
            return (Criteria) this;
        }

        public Criteria andShopNameIn(List<String> values) {
            addCriterion("SHOP_NAME in", values, "shopName");
            return (Criteria) this;
        }

        public Criteria andShopNameNotIn(List<String> values) {
            addCriterion("SHOP_NAME not in", values, "shopName");
            return (Criteria) this;
        }

        public Criteria andShopNameBetween(String value1, String value2) {
            addCriterion("SHOP_NAME between", value1, value2, "shopName");
            return (Criteria) this;
        }

        public Criteria andShopNameNotBetween(String value1, String value2) {
            addCriterion("SHOP_NAME not between", value1, value2, "shopName");
            return (Criteria) this;
        }

        public Criteria andShopBgImgIsNull() {
            addCriterion("SHOP_BG_IMG is null");
            return (Criteria) this;
        }

        public Criteria andShopBgImgIsNotNull() {
            addCriterion("SHOP_BG_IMG is not null");
            return (Criteria) this;
        }

        public Criteria andShopBgImgEqualTo(String value) {
            addCriterion("SHOP_BG_IMG =", value, "shopBgImg");
            return (Criteria) this;
        }

        public Criteria andShopBgImgNotEqualTo(String value) {
            addCriterion("SHOP_BG_IMG <>", value, "shopBgImg");
            return (Criteria) this;
        }

        public Criteria andShopBgImgGreaterThan(String value) {
            addCriterion("SHOP_BG_IMG >", value, "shopBgImg");
            return (Criteria) this;
        }

        public Criteria andShopBgImgGreaterThanOrEqualTo(String value) {
            addCriterion("SHOP_BG_IMG >=", value, "shopBgImg");
            return (Criteria) this;
        }

        public Criteria andShopBgImgLessThan(String value) {
            addCriterion("SHOP_BG_IMG <", value, "shopBgImg");
            return (Criteria) this;
        }

        public Criteria andShopBgImgLessThanOrEqualTo(String value) {
            addCriterion("SHOP_BG_IMG <=", value, "shopBgImg");
            return (Criteria) this;
        }

        public Criteria andShopBgImgLike(String value) {
            addCriterion("SHOP_BG_IMG like", value, "shopBgImg");
            return (Criteria) this;
        }

        public Criteria andShopBgImgNotLike(String value) {
            addCriterion("SHOP_BG_IMG not like", value, "shopBgImg");
            return (Criteria) this;
        }

        public Criteria andShopBgImgIn(List<String> values) {
            addCriterion("SHOP_BG_IMG in", values, "shopBgImg");
            return (Criteria) this;
        }

        public Criteria andShopBgImgNotIn(List<String> values) {
            addCriterion("SHOP_BG_IMG not in", values, "shopBgImg");
            return (Criteria) this;
        }

        public Criteria andShopBgImgBetween(String value1, String value2) {
            addCriterion("SHOP_BG_IMG between", value1, value2, "shopBgImg");
            return (Criteria) this;
        }

        public Criteria andShopBgImgNotBetween(String value1, String value2) {
            addCriterion("SHOP_BG_IMG not between", value1, value2, "shopBgImg");
            return (Criteria) this;
        }

        public Criteria andConfigSerialnoIsNull() {
            addCriterion("CONFIG_SERIALNO is null");
            return (Criteria) this;
        }

        public Criteria andConfigSerialnoIsNotNull() {
            addCriterion("CONFIG_SERIALNO is not null");
            return (Criteria) this;
        }

        public Criteria andConfigSerialnoEqualTo(Integer value) {
            addCriterion("CONFIG_SERIALNO =", value, "configSerialno");
            return (Criteria) this;
        }

        public Criteria andConfigSerialnoNotEqualTo(Integer value) {
            addCriterion("CONFIG_SERIALNO <>", value, "configSerialno");
            return (Criteria) this;
        }

        public Criteria andConfigSerialnoGreaterThan(Integer value) {
            addCriterion("CONFIG_SERIALNO >", value, "configSerialno");
            return (Criteria) this;
        }

        public Criteria andConfigSerialnoGreaterThanOrEqualTo(Integer value) {
            addCriterion("CONFIG_SERIALNO >=", value, "configSerialno");
            return (Criteria) this;
        }

        public Criteria andConfigSerialnoLessThan(Integer value) {
            addCriterion("CONFIG_SERIALNO <", value, "configSerialno");
            return (Criteria) this;
        }

        public Criteria andConfigSerialnoLessThanOrEqualTo(Integer value) {
            addCriterion("CONFIG_SERIALNO <=", value, "configSerialno");
            return (Criteria) this;
        }

        public Criteria andConfigSerialnoIn(List<Integer> values) {
            addCriterion("CONFIG_SERIALNO in", values, "configSerialno");
            return (Criteria) this;
        }

        public Criteria andConfigSerialnoNotIn(List<Integer> values) {
            addCriterion("CONFIG_SERIALNO not in", values, "configSerialno");
            return (Criteria) this;
        }

        public Criteria andConfigSerialnoBetween(Integer value1, Integer value2) {
            addCriterion("CONFIG_SERIALNO between", value1, value2, "configSerialno");
            return (Criteria) this;
        }

        public Criteria andConfigSerialnoNotBetween(Integer value1, Integer value2) {
            addCriterion("CONFIG_SERIALNO not between", value1, value2, "configSerialno");
            return (Criteria) this;
        }

        public Criteria andIdTypeIsNull() {
            addCriterion("ID_TYPE is null");
            return (Criteria) this;
        }

        public Criteria andIdTypeIsNotNull() {
            addCriterion("ID_TYPE is not null");
            return (Criteria) this;
        }

        public Criteria andIdTypeEqualTo(String value) {
            addCriterion("ID_TYPE =", value, "idType");
            return (Criteria) this;
        }

        public Criteria andIdTypeNotEqualTo(String value) {
            addCriterion("ID_TYPE <>", value, "idType");
            return (Criteria) this;
        }

        public Criteria andIdTypeGreaterThan(String value) {
            addCriterion("ID_TYPE >", value, "idType");
            return (Criteria) this;
        }

        public Criteria andIdTypeGreaterThanOrEqualTo(String value) {
            addCriterion("ID_TYPE >=", value, "idType");
            return (Criteria) this;
        }

        public Criteria andIdTypeLessThan(String value) {
            addCriterion("ID_TYPE <", value, "idType");
            return (Criteria) this;
        }

        public Criteria andIdTypeLessThanOrEqualTo(String value) {
            addCriterion("ID_TYPE <=", value, "idType");
            return (Criteria) this;
        }

        public Criteria andIdTypeLike(String value) {
            addCriterion("ID_TYPE like", value, "idType");
            return (Criteria) this;
        }

        public Criteria andIdTypeNotLike(String value) {
            addCriterion("ID_TYPE not like", value, "idType");
            return (Criteria) this;
        }

        public Criteria andIdTypeIn(List<String> values) {
            addCriterion("ID_TYPE in", values, "idType");
            return (Criteria) this;
        }

        public Criteria andIdTypeNotIn(List<String> values) {
            addCriterion("ID_TYPE not in", values, "idType");
            return (Criteria) this;
        }

        public Criteria andIdTypeBetween(String value1, String value2) {
            addCriterion("ID_TYPE between", value1, value2, "idType");
            return (Criteria) this;
        }

        public Criteria andIdTypeNotBetween(String value1, String value2) {
            addCriterion("ID_TYPE not between", value1, value2, "idType");
            return (Criteria) this;
        }

        public Criteria andBaseRateIsNull() {
            addCriterion("BASE_RATE is null");
            return (Criteria) this;
        }

        public Criteria andBaseRateIsNotNull() {
            addCriterion("BASE_RATE is not null");
            return (Criteria) this;
        }

        public Criteria andBaseRateEqualTo(Double value) {
            addCriterion("BASE_RATE =", value, "baseRate");
            return (Criteria) this;
        }

        public Criteria andBaseRateNotEqualTo(Double value) {
            addCriterion("BASE_RATE <>", value, "baseRate");
            return (Criteria) this;
        }

        public Criteria andBaseRateGreaterThan(Double value) {
            addCriterion("BASE_RATE >", value, "baseRate");
            return (Criteria) this;
        }

        public Criteria andBaseRateGreaterThanOrEqualTo(Double value) {
            addCriterion("BASE_RATE >=", value, "baseRate");
            return (Criteria) this;
        }

        public Criteria andBaseRateLessThan(Double value) {
            addCriterion("BASE_RATE <", value, "baseRate");
            return (Criteria) this;
        }

        public Criteria andBaseRateLessThanOrEqualTo(Double value) {
            addCriterion("BASE_RATE <=", value, "baseRate");
            return (Criteria) this;
        }

        public Criteria andBaseRateIn(List<Double> values) {
            addCriterion("BASE_RATE in", values, "baseRate");
            return (Criteria) this;
        }

        public Criteria andBaseRateNotIn(List<Double> values) {
            addCriterion("BASE_RATE not in", values, "baseRate");
            return (Criteria) this;
        }

        public Criteria andBaseRateBetween(Double value1, Double value2) {
            addCriterion("BASE_RATE between", value1, value2, "baseRate");
            return (Criteria) this;
        }

        public Criteria andBaseRateNotBetween(Double value1, Double value2) {
            addCriterion("BASE_RATE not between", value1, value2, "baseRate");
            return (Criteria) this;
        }

        public Criteria andRegisterTypeIsNull() {
            addCriterion("REGISTER_TYPE is null");
            return (Criteria) this;
        }

        public Criteria andRegisterTypeIsNotNull() {
            addCriterion("REGISTER_TYPE is not null");
            return (Criteria) this;
        }

        public Criteria andRegisterTypeEqualTo(String value) {
            addCriterion("REGISTER_TYPE =", value, "registerType");
            return (Criteria) this;
        }

        public Criteria andRegisterTypeNotEqualTo(String value) {
            addCriterion("REGISTER_TYPE <>", value, "registerType");
            return (Criteria) this;
        }

        public Criteria andRegisterTypeGreaterThan(String value) {
            addCriterion("REGISTER_TYPE >", value, "registerType");
            return (Criteria) this;
        }

        public Criteria andRegisterTypeGreaterThanOrEqualTo(String value) {
            addCriterion("REGISTER_TYPE >=", value, "registerType");
            return (Criteria) this;
        }

        public Criteria andRegisterTypeLessThan(String value) {
            addCriterion("REGISTER_TYPE <", value, "registerType");
            return (Criteria) this;
        }

        public Criteria andRegisterTypeLessThanOrEqualTo(String value) {
            addCriterion("REGISTER_TYPE <=", value, "registerType");
            return (Criteria) this;
        }

        public Criteria andRegisterTypeLike(String value) {
            addCriterion("REGISTER_TYPE like", value, "registerType");
            return (Criteria) this;
        }

        public Criteria andRegisterTypeNotLike(String value) {
            addCriterion("REGISTER_TYPE not like", value, "registerType");
            return (Criteria) this;
        }

        public Criteria andRegisterTypeIn(List<String> values) {
            addCriterion("REGISTER_TYPE in", values, "registerType");
            return (Criteria) this;
        }

        public Criteria andRegisterTypeNotIn(List<String> values) {
            addCriterion("REGISTER_TYPE not in", values, "registerType");
            return (Criteria) this;
        }

        public Criteria andRegisterTypeBetween(String value1, String value2) {
            addCriterion("REGISTER_TYPE between", value1, value2, "registerType");
            return (Criteria) this;
        }

        public Criteria andRegisterTypeNotBetween(String value1, String value2) {
            addCriterion("REGISTER_TYPE not between", value1, value2, "registerType");
            return (Criteria) this;
        }

        public Criteria andCanModifyIsNull() {
            addCriterion("CAN_MODIFY is null");
            return (Criteria) this;
        }

        public Criteria andCanModifyIsNotNull() {
            addCriterion("CAN_MODIFY is not null");
            return (Criteria) this;
        }

        public Criteria andCanModifyEqualTo(String value) {
            addCriterion("CAN_MODIFY =", value, "canModify");
            return (Criteria) this;
        }

        public Criteria andCanModifyNotEqualTo(String value) {
            addCriterion("CAN_MODIFY <>", value, "canModify");
            return (Criteria) this;
        }

        public Criteria andCanModifyGreaterThan(String value) {
            addCriterion("CAN_MODIFY >", value, "canModify");
            return (Criteria) this;
        }

        public Criteria andCanModifyGreaterThanOrEqualTo(String value) {
            addCriterion("CAN_MODIFY >=", value, "canModify");
            return (Criteria) this;
        }

        public Criteria andCanModifyLessThan(String value) {
            addCriterion("CAN_MODIFY <", value, "canModify");
            return (Criteria) this;
        }

        public Criteria andCanModifyLessThanOrEqualTo(String value) {
            addCriterion("CAN_MODIFY <=", value, "canModify");
            return (Criteria) this;
        }

        public Criteria andCanModifyLike(String value) {
            addCriterion("CAN_MODIFY like", value, "canModify");
            return (Criteria) this;
        }

        public Criteria andCanModifyNotLike(String value) {
            addCriterion("CAN_MODIFY not like", value, "canModify");
            return (Criteria) this;
        }

        public Criteria andCanModifyIn(List<String> values) {
            addCriterion("CAN_MODIFY in", values, "canModify");
            return (Criteria) this;
        }

        public Criteria andCanModifyNotIn(List<String> values) {
            addCriterion("CAN_MODIFY not in", values, "canModify");
            return (Criteria) this;
        }

        public Criteria andCanModifyBetween(String value1, String value2) {
            addCriterion("CAN_MODIFY between", value1, value2, "canModify");
            return (Criteria) this;
        }

        public Criteria andCanModifyNotBetween(String value1, String value2) {
            addCriterion("CAN_MODIFY not between", value1, value2, "canModify");
            return (Criteria) this;
        }

        public Criteria andCanScoreIsNull() {
            addCriterion("CAN_SCORE is null");
            return (Criteria) this;
        }

        public Criteria andCanScoreIsNotNull() {
            addCriterion("CAN_SCORE is not null");
            return (Criteria) this;
        }

        public Criteria andCanScoreEqualTo(String value) {
            addCriterion("CAN_SCORE =", value, "canScore");
            return (Criteria) this;
        }

        public Criteria andCanScoreNotEqualTo(String value) {
            addCriterion("CAN_SCORE <>", value, "canScore");
            return (Criteria) this;
        }

        public Criteria andCanScoreGreaterThan(String value) {
            addCriterion("CAN_SCORE >", value, "canScore");
            return (Criteria) this;
        }

        public Criteria andCanScoreGreaterThanOrEqualTo(String value) {
            addCriterion("CAN_SCORE >=", value, "canScore");
            return (Criteria) this;
        }

        public Criteria andCanScoreLessThan(String value) {
            addCriterion("CAN_SCORE <", value, "canScore");
            return (Criteria) this;
        }

        public Criteria andCanScoreLessThanOrEqualTo(String value) {
            addCriterion("CAN_SCORE <=", value, "canScore");
            return (Criteria) this;
        }

        public Criteria andCanScoreLike(String value) {
            addCriterion("CAN_SCORE like", value, "canScore");
            return (Criteria) this;
        }

        public Criteria andCanScoreNotLike(String value) {
            addCriterion("CAN_SCORE not like", value, "canScore");
            return (Criteria) this;
        }

        public Criteria andCanScoreIn(List<String> values) {
            addCriterion("CAN_SCORE in", values, "canScore");
            return (Criteria) this;
        }

        public Criteria andCanScoreNotIn(List<String> values) {
            addCriterion("CAN_SCORE not in", values, "canScore");
            return (Criteria) this;
        }

        public Criteria andCanScoreBetween(String value1, String value2) {
            addCriterion("CAN_SCORE between", value1, value2, "canScore");
            return (Criteria) this;
        }

        public Criteria andCanScoreNotBetween(String value1, String value2) {
            addCriterion("CAN_SCORE not between", value1, value2, "canScore");
            return (Criteria) this;
        }

        public Criteria andUpUsercodeIsNull() {
            addCriterion("UP_USERCODE is null");
            return (Criteria) this;
        }

        public Criteria andUpUsercodeIsNotNull() {
            addCriterion("UP_USERCODE is not null");
            return (Criteria) this;
        }

        public Criteria andUpUsercodeEqualTo(String value) {
            addCriterion("UP_USERCODE =", value, "upUsercode");
            return (Criteria) this;
        }

        public Criteria andUpUsercodeNotEqualTo(String value) {
            addCriterion("UP_USERCODE <>", value, "upUsercode");
            return (Criteria) this;
        }

        public Criteria andUpUsercodeGreaterThan(String value) {
            addCriterion("UP_USERCODE >", value, "upUsercode");
            return (Criteria) this;
        }

        public Criteria andUpUsercodeGreaterThanOrEqualTo(String value) {
            addCriterion("UP_USERCODE >=", value, "upUsercode");
            return (Criteria) this;
        }

        public Criteria andUpUsercodeLessThan(String value) {
            addCriterion("UP_USERCODE <", value, "upUsercode");
            return (Criteria) this;
        }

        public Criteria andUpUsercodeLessThanOrEqualTo(String value) {
            addCriterion("UP_USERCODE <=", value, "upUsercode");
            return (Criteria) this;
        }

        public Criteria andUpUsercodeLike(String value) {
            addCriterion("UP_USERCODE like", value, "upUsercode");
            return (Criteria) this;
        }

        public Criteria andUpUsercodeNotLike(String value) {
            addCriterion("UP_USERCODE not like", value, "upUsercode");
            return (Criteria) this;
        }

        public Criteria andUpUsercodeIn(List<String> values) {
            addCriterion("UP_USERCODE in", values, "upUsercode");
            return (Criteria) this;
        }

        public Criteria andUpUsercodeNotIn(List<String> values) {
            addCriterion("UP_USERCODE not in", values, "upUsercode");
            return (Criteria) this;
        }

        public Criteria andUpUsercodeBetween(String value1, String value2) {
            addCriterion("UP_USERCODE between", value1, value2, "upUsercode");
            return (Criteria) this;
        }

        public Criteria andUpUsercodeNotBetween(String value1, String value2) {
            addCriterion("UP_USERCODE not between", value1, value2, "upUsercode");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}