package cn.com.libertymutual.sp.action;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.libertymutual.core.util.Current;
import cn.com.libertymutual.core.web.ServiceResult;
import cn.com.libertymutual.sp.service.api.ActivityService;

@RestController
@RequestMapping(value = "/nol/activity")
public class ActivityController {

	@Autowired
	private ActivityService activityService;

	/**
	 * 获得当前用户抽奖次数
	 * @param userCode
	 * @return
	 */
	@PostMapping(value = "/queryDrawTimes")
	public ServiceResult advertList(HttpServletRequest request, String userCode) {
		return activityService.queryDrawTimes(Current.userCode.get());
	}

	/**
	 * 抽奖
	 * @param userCode
	 * @return
	 */
	@PostMapping(value = "/draw")
	public ServiceResult draw(HttpServletRequest request, String userCode) {

		return activityService.draw(Current.userCode.get());
	}

	@RequestMapping(value = "/getActivityGreatBenefit")
	public ServiceResult getActivityGreatBenefit(HttpServletRequest request) {
		return activityService.getActivityGreatBenefit();
	}

}
