package cn.com.libertymutual.saleplat.controller;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

import java.io.DataOutputStream;
import java.io.IOException;
import java.util.UUID;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriUtils;

import cn.com.libertymutual.core.web.ServiceResult;

@RestController
@CrossOrigin
@RefreshScope
//@RequestMapping(value = "/im")
public class ApiController {
	
	@Value("${soaservice.config.partnerAccountCode}")
	private String appName;
	
	private Logger log = LoggerFactory.getLogger(getClass());
	
	///http://10.132.28.243:18008/refresh

    @ApiOperation(value = "连通性测试", notes = "测试")
    @ApiImplicitParams(value = {
            @ApiImplicitParam(name = "userId", value = "用户Id", required = true, paramType = "query", dataType = "String"),
            @ApiImplicitParam(name = "name", value = "姓名", required = true, paramType = "query", dataType = "String"),
            @ApiImplicitParam(name = "portraitUri", value = "用户头像地址", required = true, paramType = "query", dataType = "String")
    })
    @RequestMapping(value = {"/user/token"}, method = RequestMethod.GET)
    public String getToken(@RequestParam String userId,@RequestParam String name,@RequestParam String portraitUri) throws Exception {
        
        return UUID.randomUUID().toString()+"==="+appName;
        
    }
    
    @ApiOperation(value = "test")
    @RequestMapping(value="/nol/bob", method=RequestMethod.GET)
    public ServiceResult getInfo( String name) {
    	ServiceResult s = new ServiceResult();
    	
    	s.setResult( name );
    	
    	return s;
    }

    @RequestMapping(value="/nol/token/login", method=RequestMethod.GET)
    public ServiceResult getInfo2( String name) {
    	ServiceResult s = new ServiceResult();
    	
    	s.setResult( "login bob" );
    	
    	log.info(name);
    	
    	return s;
    }
    private StringBuilder result;
    
    @PostConstruct
    public void init() {
    	this.result = new StringBuilder();

    }

    @RequestMapping(value="/testSms")
    public void testSms(  @RequestBody String data, HttpServletRequest request, HttpServletResponse response ) throws IOException {

    	System.out.println( request.getContentType() );
    	System.out.println( request.getCharacterEncoding());
    	//System.out.println( name );
    	System.out.println(data.substring( data.indexOf("xml")));
    	
    	String data2 = UriUtils.decode(data, "UTF-8");
    	
    	System.out.println( data2.substring( data2.indexOf("<")) );
    	
    	//s.setResult( name );
    	/*DataOutputStream out = new DataOutputStream(response.getOutputStream());
    	out.writeUTF("success\r\n");
    	out.close();*/
    	this.result.delete(0, this.result.length());
    	this.result.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
    	this.result.append("<PACKET type=\"RESPONSE\" version=\"1.0\">");
        this.result.append("<HEAD>");
        this.result.append("<REQUEST_TYPE>SMS</REQUEST_TYPE>");
        this.result.append("<RESPONSE_CODE>0</RESPONSE_CODE>");
        this.result.append("<ERROR_MESSAGE> Success </ERROR_MESSAGE>");
        this.result.append("</HEAD>");
        this.result.append("</PACKET>");
        
    	/*DataOutputStream os = null;
        try {
          os = new DataOutputStream(response.getOutputStream());
          os.writeUTF(this.result.toString());
        } catch (IOException e) {
          e.printStackTrace();
        } finally {
          if (os != null)
            try {
              os.close();
            } catch (Exception e) {
              e.printStackTrace();
            }
        }*/

        DataOutputStream out = new DataOutputStream(response.getOutputStream());
    	out.writeUTF(this.result.toString());
    	out.close();
        
    	return;//new ResponseEntity<String>("success", HttpStatus.OK);
    }
}
