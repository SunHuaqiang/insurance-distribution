package cn.com.libertymutual.production.dao.nomorcldatasource;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import cn.com.libertymutual.production.model.nomorcldatasource.Prpdshortratelibrary;
import cn.com.libertymutual.production.model.nomorcldatasource.PrpdshortratelibraryExample;
@Mapper
public interface PrpdshortratelibraryMapper {
    int countByExample(PrpdshortratelibraryExample example);

    int deleteByExample(PrpdshortratelibraryExample example);

    int deleteByPrimaryKey(String shortratetype);

    int insert(Prpdshortratelibrary record);

    int insertSelective(Prpdshortratelibrary record);

    List<Prpdshortratelibrary> selectByExample(PrpdshortratelibraryExample example);

    Prpdshortratelibrary selectByPrimaryKey(String shortratetype);

    int updateByExampleSelective(@Param("record") Prpdshortratelibrary record, @Param("example") PrpdshortratelibraryExample example);

    int updateByExample(@Param("record") Prpdshortratelibrary record, @Param("example") PrpdshortratelibraryExample example);

    int updateByPrimaryKeySelective(Prpdshortratelibrary record);

    int updateByPrimaryKey(Prpdshortratelibrary record);
}