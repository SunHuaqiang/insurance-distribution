package cn.com.libertymutual.sp.dto;

public class PlanDto {
	private String gpaPlanCode;
	private String carType;
	private String carNature;
	private String seat;
	private String day;
	private String age;
	private String region;
	private String tonnage;
	
	
	public synchronized String getTonnage() {
		return tonnage;
	}
	public synchronized void setTonnage(String tonnage) {
		this.tonnage = tonnage;
	}
	public String getGpaPlanCode() {
		return gpaPlanCode;
	}
	public void setGpaPlanCode(String gpaPlanCode) {
		this.gpaPlanCode = gpaPlanCode;
	}
	public String getCarType() {
		return carType;
	}
	public void setCarType(String carType) {
		this.carType = carType;
	}
	public String getCarNature() {
		return carNature;
	}
	public void setCarNature(String carNature) {
		this.carNature = carNature;
	}
	public String getSeat() {
		return seat;
	}
	public void setSeat(String seat) {
		this.seat = seat;
	}
	public String getDay() {
		return day;
	}
	public void setDay(String day) {
		this.day = day;
	}
	public String getAge() {
		return age;
	}
	public void setAge(String age) {
		this.age = age;
	}
	public String getRegion() {
		return region;
	}
	public void setRegion(String region) {
		this.region = region;
	}
	@Override
	public String toString() {
		return "PlanDto [gpaPlanCode=" + gpaPlanCode + ", carType=" + carType + ", carNature=" + carNature + ", seat="
				+ seat + ", day=" + day + ", age=" + age + ", region=" + region + ", tonnage=" + tonnage + "]";
	}
	
	
}
