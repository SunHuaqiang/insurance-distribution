package cn.com.libertymutual.production.service.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.com.libertymutual.production.dao.nomorcldatasource.PrpsagentMapper;
import cn.com.libertymutual.production.model.nomorcldatasource.Prpsagent;
import cn.com.libertymutual.production.model.nomorcldatasource.PrpsagentExample;
import cn.com.libertymutual.production.model.nomorcldatasource.PrpsagentWithBLOBs;
import cn.com.libertymutual.production.pojo.request.Request;
import cn.com.libertymutual.production.service.api.PrpsAgentService;

/** 
 * @author GuoYue
 * @date 2017年9月5日
 *  
 */
@Service
public class PrpsAgentServiceImpl implements PrpsAgentService {
	
	@Autowired
	private PrpsagentMapper prpsagentMapper;

	@Override
	public List<PrpsagentWithBLOBs> findPrpsAgentByComCode(List<String> comCodes, Request request) {
		List<PrpsagentWithBLOBs> rs = new ArrayList<PrpsagentWithBLOBs>();
		Set<PrpsagentWithBLOBs> uniqueAgents = new HashSet<PrpsagentWithBLOBs>();
		if(comCodes.isEmpty()) {
			return null;
		} else if (comCodes.contains("ALL")){
			PrpsagentExample example = new PrpsagentExample();
			example.createCriteria().andValidstatusEqualTo("1");
			rs = prpsagentMapper.selectByExampleWithBLOBs(example );
		} else {
			for(String comcode : comCodes) {
				List<PrpsagentWithBLOBs> agents = prpsagentMapper.selectWithComcode(comcode);
				uniqueAgents.addAll(agents);
			}
			rs.addAll(uniqueAgents);
		}
		return rs;
	}

}
