package cn.com.libertymutual.sp.dto.axtx;



public class UnderwriteRequestDto extends RequestBaseDto{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6485601469619286437L;
	/**
	 * 
	 */
	/**
	 * 查询用业务关系代码
	 */
	
	//投保主信息
	private PolicyDataDto policyDataDto;
	//投保人信息
	private AppliInsuredDto   appliInsuredDto;
	//车辆信息
	private ItemCarDto   itemCarDto;
	 
	public PolicyDataDto getPolicyDataDto() {
		return policyDataDto;
	}
	public void setPolicyDataDto(PolicyDataDto policyDataDto) {
		this.policyDataDto = policyDataDto;
	}
	public AppliInsuredDto getAppliInsuredDto() {
		return appliInsuredDto;
	}
	public void setAppliInsuredDto(AppliInsuredDto appliInsuredDto) {
		this.appliInsuredDto = appliInsuredDto;
	}
	public ItemCarDto getItemCarDto() {
		return itemCarDto;
	}
	public void setItemCarDto(ItemCarDto itemCarDto) {
		this.itemCarDto = itemCarDto;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		 
		result = prime * result
				+ ((appliInsuredDto == null) ? 0 : appliInsuredDto.hashCode());
		result = prime * result
				+ ((itemCarDto == null) ? 0 : itemCarDto.hashCode());
		result = prime * result
				+ ((policyDataDto == null) ? 0 : policyDataDto.hashCode());
		 
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		UnderwriteRequestDto other = (UnderwriteRequestDto) obj;
		 
		if (appliInsuredDto == null) {
			if (other.appliInsuredDto != null)
				return false;
		} else if (!appliInsuredDto.equals(other.appliInsuredDto))
			return false;
		if (itemCarDto == null) {
			if (other.itemCarDto != null)
				return false;
		} else if (!itemCarDto.equals(other.itemCarDto))
			return false;
		if (policyDataDto == null) {
			if (other.policyDataDto != null)
				return false;
		} else if (!policyDataDto.equals(other.policyDataDto))
			return false;
	 
		return true;
	}
	@Override
	public String toString() {
		return "UnderwriteRequestDto [  policyDataDto=" + policyDataDto + ", appliInsuredDto="
				+ appliInsuredDto + ", itemCarDto=" + itemCarDto + "]";
	}
	 
	
}
