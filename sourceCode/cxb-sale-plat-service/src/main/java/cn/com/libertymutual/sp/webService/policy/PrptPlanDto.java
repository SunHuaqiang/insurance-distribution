
package cn.com.libertymutual.sp.webService.policy;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for prptPlanDto complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="prptPlanDto">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="payNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="payRefFee" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="planDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="planFee" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="planFeePercent" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="prpPlanCurrency" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "prptPlanDto", propOrder = {
    "payNo",
    "payRefFee",
    "planDate",
    "planFee",
    "planFeePercent",
    "prpPlanCurrency"
})
public class PrptPlanDto implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 7693475325398682783L;
	protected String payNo;
    protected String payRefFee;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar planDate;
    protected String planFee;
    protected String planFeePercent;
    protected String prpPlanCurrency;

    /**
     * Gets the value of the payNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPayNo() {
        return payNo;
    }

    /**
     * Sets the value of the payNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPayNo(String value) {
        this.payNo = value;
    }

    /**
     * Gets the value of the payRefFee property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPayRefFee() {
        return payRefFee;
    }

    /**
     * Sets the value of the payRefFee property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPayRefFee(String value) {
        this.payRefFee = value;
    }

    /**
     * Gets the value of the planDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getPlanDate() {
        return planDate;
    }

    /**
     * Sets the value of the planDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setPlanDate(XMLGregorianCalendar value) {
        this.planDate = value;
    }

    /**
     * Gets the value of the planFee property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPlanFee() {
        return planFee;
    }

    /**
     * Sets the value of the planFee property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPlanFee(String value) {
        this.planFee = value;
    }

    /**
     * Gets the value of the planFeePercent property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPlanFeePercent() {
        return planFeePercent;
    }

    /**
     * Sets the value of the planFeePercent property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPlanFeePercent(String value) {
        this.planFeePercent = value;
    }

    /**
     * Gets the value of the prpPlanCurrency property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrpPlanCurrency() {
        return prpPlanCurrency;
    }

    /**
     * Sets the value of the prpPlanCurrency property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrpPlanCurrency(String value) {
        this.prpPlanCurrency = value;
    }

}
