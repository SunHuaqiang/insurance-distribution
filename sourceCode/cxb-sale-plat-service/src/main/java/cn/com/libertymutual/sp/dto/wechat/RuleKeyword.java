package cn.com.libertymutual.sp.dto.wechat;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Entity - 规则关键词
 * 
 * @author yeyc
 *
 */
@Entity
@Table(name = "t_rule_keyword")
@SequenceGenerator(name = "sequenceGenerator", sequenceName = "t_sequence")
public class RuleKeyword implements Serializable {

	private static final long serialVersionUID = 5940930214223776336L;
	private long id; 
	/** 名称 */
	private String name;
	
	/** 是否全匹配（0-否， 1-是） */
	private Integer isMatchAll;
	
	/** 所属规则 */
	@JsonIgnore
	private Rule rule;

	@NotNull
	@Column(nullable = false, length = 120)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	@Id
	 @Column(name = "ID")
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
	@NotNull
	@Column(nullable = false)
	public Integer getIsMatchAll() {
		return isMatchAll;
	}

	public void setIsMatchAll(Integer isMatchAll) {
		this.isMatchAll = isMatchAll;
	}

	@NotEmpty
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="rule")
	public Rule getRule() {
		return rule;
	}

	public void setRule(Rule rule) {
		this.rule = rule;
	}
	
}

