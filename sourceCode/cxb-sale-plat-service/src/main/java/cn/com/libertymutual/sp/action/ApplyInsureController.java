package cn.com.libertymutual.sp.action;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.libertymutual.core.redis.util.RedisUtils;
import cn.com.libertymutual.core.util.Constants;
import cn.com.libertymutual.core.web.ServiceResult;
import cn.com.libertymutual.sp.service.api.ApplyInsureService;
import cn.com.libertymutual.sp.service.api.CaptchaService;
import cn.com.libertymutual.sp.service.api.SmsService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(value = "/nol/apply")
public class ApplyInsureController {

	private Logger log = LoggerFactory.getLogger(getClass());

	@Resource
	private RedisUtils redis;
	@Resource
	private SmsService smsService;// 短信
	@Resource
	private CaptchaService captchaService;// 图形验证码

	@Resource
	private ApplyInsureService applyInsureService;

	@ApiOperation(value = "智通快速投保申请合作", notes = "测试")
	@ApiImplicitParams(value = { @ApiImplicitParam(name = "imgCode", value = "图形码", required = true, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "mobileCode", value = "短信验证码", required = true, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "nickName", value = "昵称", required = true, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "email", value = "昵称", required = true, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "mobile", value = "手机号", required = true, paramType = "query", dataType = "String") })
	@RequestMapping(value = "/platformInsure_img") // 暂时关闭
	public ServiceResult platformInsure_img(HttpServletRequest request, HttpServletResponse response, String imgCode, String mobileCode,
			String nickName, String email, String mobile) {

		ServiceResult sr = new ServiceResult(Constants.SYS_ERROR_MSG, ServiceResult.STATE_APP_EXCEPTION);
		try {
			// 校验-图形码
			if (!captchaService.validationCaptcha(request, sr, redis, imgCode)) {
				return sr;
			}
			// 校验-短信验证码
			if (!smsService.verifyCodeBool(request, sr, redis, mobileCode, mobile)) {
				return sr;
			}
			if (sr.getState() == ServiceResult.STATE_SUCCESS) {
				sr = applyInsureService.platformInsure(request, response, sr, imgCode, mobileCode, nickName, email, mobile);
			}
		} catch (Exception e) {
			log.warn("申请合作异常:" + e.toString());
		}
		return sr;
	}
}
