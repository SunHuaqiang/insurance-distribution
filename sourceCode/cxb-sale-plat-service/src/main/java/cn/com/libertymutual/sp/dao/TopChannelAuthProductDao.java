package cn.com.libertymutual.sp.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import cn.com.libertymutual.sp.bean.TbSpTopChannelAuthProduct;
@Repository
public interface TopChannelAuthProductDao extends PagingAndSortingRepository<TbSpTopChannelAuthProduct, Integer>, JpaSpecificationExecutor<TbSpTopChannelAuthProduct> {

	@Query("from TbSpTopChannelAuthProduct where userCode =?1 and productId=?2")
	TbSpTopChannelAuthProduct findByUCodeAndPId(String userCode, Integer productId);


	@Query("from TbSpTopChannelAuthProduct where userCode =?1 ")
	List<TbSpTopChannelAuthProduct> findByUserCode(String userCode);
	
	@Transactional
	@Modifying
	@Query(value="delete from tb_sp_topchannel_auth_product where USER_CODE =?1 ",nativeQuery=true)
	int deleteByUserCode(String userCode);
}
