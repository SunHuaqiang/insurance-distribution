package cn.com.libertymutual.production.dao.nomorcldatasource;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import cn.com.libertymutual.production.model.nomorcldatasource.Prpdlogoperation;
import cn.com.libertymutual.production.model.nomorcldatasource.PrpdlogoperationExample;
@Mapper
public interface PrpdlogoperationMapper {
    int countByExample(PrpdlogoperationExample example);

    int deleteByExample(PrpdlogoperationExample example);

    int insert(Prpdlogoperation record);

    int insertSelective(Prpdlogoperation record);

    List<Prpdlogoperation> selectByExampleWithBLOBs(PrpdlogoperationExample example);

    List<Prpdlogoperation> selectByExample(PrpdlogoperationExample example);

    int updateByExampleSelective(@Param("record") Prpdlogoperation record, @Param("example") PrpdlogoperationExample example);

    int updateByExampleWithBLOBs(@Param("record") Prpdlogoperation record, @Param("example") PrpdlogoperationExample example);

    int updateByExample(@Param("record") Prpdlogoperation record, @Param("example") PrpdlogoperationExample example);
}