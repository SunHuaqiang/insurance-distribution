package cn.com.libertymutual.saleplat.bootstrap;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;

import javax.imageio.ImageIO;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.rendering.PDFRenderer;


public class TestMail {

	public static void main(String[] args) {
		//SendMail send = new SendMail();
		
		//send.Send("UserIDManager", "UserID123", "test", "UserIDManager@libertymutual.com.cn", "bob.kuang@libertymutual.com.cn", "mailbody");
		String rate = "33.300000000000004";
		BigDecimal rateDec = new BigDecimal( rate );
		rate = rateDec.setScale(6, RoundingMode.HALF_DOWN).toEngineeringString();
		
		
		System.out.println( String.format("%s%08d", "20171115", 1000 ) );
		System.out.println(  "20171112".compareTo("20171115") );
		System.out.println(  "20171115".compareTo("20171115") );
		System.out.println(  "20171112".compareTo("20171111") );
		
		if( true ) return;
		
		long s = System.currentTimeMillis();
		System.out.println( s );
		
		File file = new File("E://test.pdf");
		File dstFile = new File("E://dst_test.png"); 
		
		PDDocument pdDocument; 
		try { 
			pdDocument = PDDocument.load(file); 
			PDFRenderer renderer = new PDFRenderer(pdDocument); 
			// 0表示第一页，300表示转换dpi，越大转换后越清晰，相对转换速度越慢
			BufferedImage image = renderer.renderImageWithDPI(0,220); 
			
			ImageIO.write(image, "png", dstFile); 
		} catch (IOException e) { // TODO Auto-generated catch block e.printStackTrace(); }
			e.printStackTrace();
		}
		
		System.out.println( System.currentTimeMillis() - s );
	}
}
