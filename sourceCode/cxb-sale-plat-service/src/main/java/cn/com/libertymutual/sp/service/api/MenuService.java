package cn.com.libertymutual.sp.service.api;

import cn.com.libertymutual.core.web.ServiceResult;

public interface MenuService {

	ServiceResult addMenu(Integer menuId, String type,String menuname,String resName,String menunameen,String menuurl,String icon);

	ServiceResult removeMenu(Integer menuId, String type);

	ServiceResult updateMenuName(Integer menuId,String menuname,String menunameen,String menuurl,String icon);

}
