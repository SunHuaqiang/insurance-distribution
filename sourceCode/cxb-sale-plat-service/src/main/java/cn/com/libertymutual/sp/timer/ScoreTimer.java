package cn.com.libertymutual.sp.timer;

import java.util.Date;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import cn.com.libertymutual.core.redis.util.RedisUtils;
import cn.com.libertymutual.core.util.Constants;
import cn.com.libertymutual.core.util.DateUtil;
import cn.com.libertymutual.sp.service.api.ActivityService;
import cn.com.libertymutual.sp.service.api.HomePageService;
import cn.com.libertymutual.sp.service.api.QueryPrdCodeService;

/////@Component
public class ScoreTimer {
	@Autowired private ActivityService  activityService;
	@Resource
	private RedisUtils redisUtils;
	private Logger log = LoggerFactory.getLogger(getClass());
	
	private int expireTime=12*60*60;//超时时间  12h ,单位s
	@Autowired private QueryPrdCodeService queryPrdCodeService;
	 //每3秒执行一次
   /* @Scheduled(fixedRate = 3000)
    public void timerRate() {
        System.out.println("我是定时任务,每3s执行一次");
    }*/
	//每天1点30分0秒时执行@Scheduled(cron = "0 30 1 * * ?")
    //////@Scheduled(cron = "0 0 1 * * ?")不要了，使用Quartz框架
    public void timerCron() {
    	long timerFlag=redisUtils.incr(Constants.TIMER_FLAG);
		if(timerFlag==1){
			redisUtils.setWithExpireTime(Constants.TIMER_FLAG, 2l,expireTime);
	        log.info("开始执行定时任务："+DateUtil.dateFromat(new Date(),DateUtil.DATE_TIME_PATTERN2));
	        activityService.task();
	        queryPrdCodeService.queryCheckCode();
	        log.info("结束执行定时任务："+DateUtil.dateFromat(new Date(),DateUtil.DATE_TIME_PATTERN2));
			redisUtils.set(Constants.TIMER_FLAG, 0l);
		}
    }

}
