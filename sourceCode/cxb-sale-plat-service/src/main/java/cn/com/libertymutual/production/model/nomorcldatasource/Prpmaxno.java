package cn.com.libertymutual.production.model.nomorcldatasource;

public class Prpmaxno extends PrpmaxnoKey {
    private String flag;

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag == null ? null : flag.trim();
    }
}