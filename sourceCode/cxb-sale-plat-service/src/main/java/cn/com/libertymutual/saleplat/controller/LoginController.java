package cn.com.libertymutual.saleplat.controller;

import java.io.UnsupportedEncodingException;
import java.util.Map;
import java.util.UUID;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import cn.com.libertymutual.aop.LogAspect;
import cn.com.libertymutual.core.base.dto.UserInfo;
import cn.com.libertymutual.core.util.Constants;
import cn.com.libertymutual.core.util.Current;
import cn.com.libertymutual.core.web.ServiceResult;
import cn.com.libertymutual.core.web.util.RequestUtils;
import cn.com.libertymutual.sys.service.api.ISysUserService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

@RestController
@CrossOrigin
@RefreshScope
@RequestMapping(value = "/sale/syslogin")
public class LoginController {
	private static Logger log = LoggerFactory.getLogger(LogAspect.class);
	@Value("${soaservice.config.partnerAccountCode}")
	private String appName;
	@Resource private ISysUserService sysUserService;
	///http://10.132.28.243:18008/refresh
	
    @ApiOperation(value = "连通性测试", notes = "测试")
    @ApiImplicitParams(value = {
            @ApiImplicitParam(name = "userId", value = "用户Id", required = true, paramType = "query", dataType = "String"),
            @ApiImplicitParam(name = "name", value = "姓名", required = true, paramType = "query", dataType = "String"),
            @ApiImplicitParam(name = "portraitUri", value = "用户头像地址", required = true, paramType = "query", dataType = "String")
    })
    @RequestMapping(value = {"/user/token"}, method = RequestMethod.GET)
    public String getToken(@RequestParam String userId,@RequestParam String name,@RequestParam String portraitUri) throws Exception {
        
        return UUID.randomUUID().toString()+"==="+appName;
        
    }
    
    @RequestMapping(value = {"/login"}, method = RequestMethod.POST)
    public ServiceResult login(HttpServletRequest request,@RequestParam String username,
			  HttpSession session, HttpServletResponse response,@RequestParam String password) throws UnsupportedEncodingException  {
		ServiceResult rs=new ServiceResult();
		UserInfo userInfo;
		try {
		Map<String, Object> map = RequestUtils.getQueryParams(request);
		System.out.println(map.toString()); 
		
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			userInfo = sysUserService.initUser(username);
			request.getSession(true).setAttribute(Constants.USER_INFO_KEY, userInfo);
			Current.userInfo.set(userInfo); 
		//	response.addHeader("userInfo", userInfo.toString());
			rs.setSuccess();
			rs.setResult(userInfo);
		} catch (Exception e) {
		//	response.addHeader("errorMsg", URLEncoder.encode( e.getMessage(),"UTF-8"));
			rs.setState(ServiceResult.STATE_NO_SESSION);
		}
		
		return rs; 
    }
    /*@ApiOperation(value = "test")
    @RequestMapping(value="/test", method=RequestMethod.GET)
    public ServiceResult getInfo( String name) {
    	ServiceResult s = new ServiceResult();
    	
    	s.setResult( name );
    	
    	return s;
    }*/
    
}
