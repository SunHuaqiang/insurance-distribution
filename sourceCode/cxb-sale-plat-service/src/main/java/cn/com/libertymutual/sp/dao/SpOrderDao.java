package cn.com.libertymutual.sp.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import cn.com.libertymutual.sp.bean.TbSpOrder;

@Repository("spOrderDao")
public interface SpOrderDao extends PagingAndSortingRepository<TbSpOrder, Integer>, JpaSpecificationExecutor<TbSpOrder> {

	List<TbSpOrder> findByOrderNo(String orderNo);

	@Query(value = "select order_No from tb_sp_order where id= (select max(id) from tb_sp_order where date_format( now(), '%Y-%m-%d' ) = date_format( create_time, '%Y-%m-%d' ) )", nativeQuery = true)
	public String findMaxIdAndNoOfToday();

}
