package cn.com.libertymutual.sp.dao.car;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import cn.com.libertymutual.sp.bean.car.PrpKind;

public interface PrpKindDao extends PagingAndSortingRepository<PrpKind, Integer>, JpaSpecificationExecutor<PrpKind> {
	@Query("from PrpKind where validStatus = 1 and riskCode=?1")
	List<PrpKind> findAllKind(String riskCode);
}
