package cn.com.libertymutual.production.utils;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.xiaoleilu.hutool.date.DateUtil;
import com.xiaoleilu.hutool.util.CollectionUtil;

/** 
 * @Description: 对象工具类 
 * @author Steven.Li
 * @date 2017年7月5日
 *  
 */
public class ObjectUtil {

	/**
	 * @Description: 获取对象所有字段值
	 * @param obj
	 * @return String  对象所有字段值
	 * @throws
	 */
	public static String info(Object obj) {
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Field[] objFields = obj.getClass().getDeclaredFields();
		Field[] supperFields = obj.getClass().getSuperclass().getDeclaredFields();
		List<Field> fields = new ArrayList<Field>();
		fields.addAll(CollectionUtil.newArrayList(objFields));
		fields.addAll(CollectionUtil.newArrayList(supperFields));
		
		String rowData = "";
		
		for (Field field : fields) {
			
			String fieldNm = field.getName();
			LogFiled annotation = field.getAnnotation(LogFiled.class);
					
			try {
				Method getMethod = obj.getClass().getMethod("get" + fieldNm.substring(0, 1).toUpperCase() + fieldNm.substring(1));
				Object val = getMethod.invoke(obj);
				
				if (annotation != null) {
					String fieldTypeName = field.getType().toString(); //获取目标对象字段类型
					fieldTypeName = fieldTypeName.substring(fieldTypeName.lastIndexOf('.') + 1);
					if ("Date".equals(fieldTypeName) && val != null) {
						val = DateUtil.format((Date) val, sdf);
					}
					rowData += annotation.chineseName() + "=\"" + (val == null ? "" : val.toString()) + "\"  |  ";
				}
				
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			
		}
		
		rowData = "{" + rowData.substring(0, rowData.length() - 2) + "}";
		
		return rowData;
	}
	
	/**
	 * 比较两个2个对象中所有字段值，返回不相同的字段的值
	 * @param src
	 * @param target
	 * @return 返回src中与target不相同的字段的值
	 */
	public static String infoAfterCompared(Object src, Object target) {
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Field[] objFields = src.getClass().getDeclaredFields();
		Field[] supperFields = src.getClass().getSuperclass().getDeclaredFields();
		List<Field> fields = new ArrayList<Field>();
		fields.addAll(CollectionUtil.newArrayList(objFields));
		fields.addAll(CollectionUtil.newArrayList(supperFields));
		
		String rowData = "";
		
		for (Field field : fields) {
			
			String fieldNm = field.getName();
			LogFiled annotation = field.getAnnotation(LogFiled.class);
			
			try {
				Method srcGetMethod = src.getClass().getMethod("get" + fieldNm.substring(0, 1).toUpperCase() + fieldNm.substring(1));
				Object srcVal = srcGetMethod.invoke(src);
				Method targetGetMethod = target.getClass().getMethod("get" + fieldNm.substring(0, 1).toUpperCase() + fieldNm.substring(1));
				Object targetVal = targetGetMethod.invoke(target);
				
				if ((srcVal != null && targetVal != null && !srcVal.equals(targetVal)) ||
					(srcVal == null && targetVal != null && !"".equals(targetVal)) ||
					(targetVal == null && srcVal != null && !"".equals(srcVal))) {
					
					if (annotation != null) {
						String fieldTypeName = field.getType().toString(); //获取目标对象字段类型
						fieldTypeName = fieldTypeName.substring(fieldTypeName.lastIndexOf('.') + 1);
						if ("Date".equals(fieldTypeName) && srcVal != null) {
							srcVal = DateUtil.format((Date) srcVal, sdf);
						}
						rowData += annotation.chineseName() + "=\"" + (srcVal == null ? "" : srcVal.toString()) + "\"  |  ";
					}
					
				}
				
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			
		}
		if (rowData != null && !"".equals(rowData)) {
			rowData = "{" + rowData.substring(0, rowData.length() - 2) + "}";
		}
		
		return rowData;
	}
}
