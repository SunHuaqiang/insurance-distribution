package cn.com.libertymutual.sp.bean;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name = "tb_sp_checkCode")
public class TbSpCheckCode implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5332124078795546664L;

	private Integer id;
	private String code;// 验车码
	private String checkDate;
	private String status;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date createTime;
	
	private String remark;
	

	public TbSpCheckCode() {
		super();
		// TODO Auto-generated constructor stub
	}

	public TbSpCheckCode(String code, String checkDate, String status,
			Date createTime) {
		super();
		this.code = code;
		this.checkDate = checkDate;
		this.status = status;
		this.createTime = createTime;
	}

	public TbSpCheckCode(String code, String checkDate, String status,
			Date createTime, String remark) {
		super();
		this.code = code;
		this.checkDate = checkDate;
		this.status = status;
		this.createTime = createTime;
		this.remark = remark;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "ID", unique = true, nullable = false)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	@Column(name = "REMARK")
	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.status = remark;
	}
	

	@Column(name = "STATUS")
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Column(name = "CREATE_TIME")
	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
	
	@Column(name = "CHECK_DATE")
	public String getCheckDate() {
		return checkDate;
	}

	public void setCheckDate(String checkDate) {
		this.checkDate = checkDate;
	}

	

}
