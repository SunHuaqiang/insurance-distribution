package cn.com.libertymutual.sp.action;

import java.io.IOException;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.libertymutual.core.annotation.SystemValidate;
import cn.com.libertymutual.core.web.ServiceResult;
import cn.com.libertymutual.sp.biz.SendCodeBiz;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(value = "/nol/code")
public class SendCodeController {

	@Resource
	private SendCodeBiz sendCodeBiz;

	@RequestMapping(value = "/getImgCode")
	@ApiOperation(value = "输出图形码")
	@SystemValidate(validate = false, description = "无需校验接口")
	public void getValidateCode(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		sendCodeBiz.getValidateCode(request, response);
	}

	@ApiOperation(value = "短信验证码", notes = "测试")
	@ApiImplicitParams(value = { @ApiImplicitParam(name = "identCode", value = "请求标识码", required = true, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "mobile", value = "手机号", required = true, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "typeName", value = "操作类型名", required = true, paramType = "query", dataType = "String") })
	@RequestMapping(value = "/smsCode")
	public ServiceResult smsCode(HttpServletRequest request, HttpServletResponse response, String identCode, String mobile, String typeName)
			throws IOException {
		return sendCodeBiz.smsCode(request, response, identCode, mobile, typeName);
	}

	@ApiOperation(value = "短信验证码 & 图形码", notes = "测试")
	@ApiImplicitParams(value = { @ApiImplicitParam(name = "identCode", value = "请求标识码", required = true, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "mobile", value = "手机号", required = true, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "imgCode", value = "图形码", required = true, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "typeName", value = "操作类型名", required = true, paramType = "query", dataType = "String") })
	@RequestMapping(value = "/smsCode_img")
	public ServiceResult smsCodeAndImgCode(HttpServletRequest request, HttpServletResponse response, String identCode, String mobile, String imgCode,
			String typeName) throws IOException {
		return sendCodeBiz.smsCodeAndImgCode(request, response, identCode, mobile, imgCode, typeName);
	}

}
