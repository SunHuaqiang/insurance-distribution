package cn.com.libertymutual.sp.dto;

public class TReportClaimRequestDto {
private  String autoPolNo;//保单号
private  String incidentTime;//事故时间 yyyy-MM-dd HH:mm
private  String claimType;//理赔类型
private  String province;//省份
private  String city;//城市
private  String incidentStreetAddress;//地址
private  String incidentNotes;//备注
private  String userName;//联系人
private  String phoneNo;//联系电话
private  String reportMethod="02";//02
private  String langId="07";//
private  String otherDriversHomePhone="07";//07
private  String OtherDriversHomePhone2="07";//联系人
private  String witnessesPhoneNo="07";//联系人
private  String witnessesPhoneNo2="07";
private  String policePhoneNo2="07";
private  String policeReportNo="07";
public String getAutoPolNo() {
	return autoPolNo;
}
public void setAutoPolNo(String autoPolNo) {
	this.autoPolNo = autoPolNo;
}
public String getIncidentTime() {
	return incidentTime;
}
public void setIncidentTime(String incidentTime) {
	this.incidentTime = incidentTime;
}
public String getClaimType() {
	return claimType;
}
public void setClaimType(String claimType) {
	this.claimType = claimType;
}
public String getProvince() {
	return province;
}
public void setProvince(String province) {
	this.province = province;
}
public String getCity() {
	return city;
}
public void setCity(String city) {
	this.city = city;
}
public String getIncidentStreetAddress() {
	return incidentStreetAddress;
}
public void setIncidentStreetAddress(String incidentStreetAddress) {
	this.incidentStreetAddress = incidentStreetAddress;
}
public String getIncidentNotes() {
	return incidentNotes;
}
public void setIncidentNotes(String incidentNotes) {
	this.incidentNotes = incidentNotes;
}
public String getUserName() {
	return userName;
}
public void setUserName(String userName) {
	this.userName = userName;
}
public String getPhoneNo() {
	return phoneNo;
}
public void setPhoneNo(String phoneNo) {
	this.phoneNo = phoneNo;
}
public String getReportMethod() {
	return reportMethod;
}
public void setReportMethod(String reportMethod) {
	this.reportMethod = reportMethod;
}
public String getLangId() {
	return langId;
}
public void setLangId(String langId) {
	this.langId = langId;
}
public String getOtherDriversHomePhone() {
	return otherDriversHomePhone;
}
public void setOtherDriversHomePhone(String otherDriversHomePhone) {
	this.otherDriversHomePhone = otherDriversHomePhone;
}
public String getOtherDriversHomePhone2() {
	return OtherDriversHomePhone2;
}
public void setOtherDriversHomePhone2(String otherDriversHomePhone2) {
	OtherDriversHomePhone2 = otherDriversHomePhone2;
}
public String getWitnessesPhoneNo() {
	return witnessesPhoneNo;
}
public void setWitnessesPhoneNo(String witnessesPhoneNo) {
	this.witnessesPhoneNo = witnessesPhoneNo;
}
public String getWitnessesPhoneNo2() {
	return witnessesPhoneNo2;
}
public void setWitnessesPhoneNo2(String witnessesPhoneNo2) {
	this.witnessesPhoneNo2 = witnessesPhoneNo2;
}
public String getPolicePhoneNo2() {
	return policePhoneNo2;
}
public void setPolicePhoneNo2(String policePhoneNo2) {
	this.policePhoneNo2 = policePhoneNo2;
}
public String getPoliceReportNo() {
	return policeReportNo;
}
public void setPoliceReportNo(String policeReportNo) {
	this.policeReportNo = policeReportNo;
}


}
