package cn.com.libertymutual.production.model.nomorcldatasource;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class PrpdkindExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public PrpdkindExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andKindcodeIsNull() {
            addCriterion("KINDCODE is null");
            return (Criteria) this;
        }

        public Criteria andKindcodeIsNotNull() {
            addCriterion("KINDCODE is not null");
            return (Criteria) this;
        }

        public Criteria andKindcodeEqualTo(String value) {
            addCriterion("KINDCODE =", value, "kindcode");
            return (Criteria) this;
        }

        public Criteria andKindcodeNotEqualTo(String value) {
            addCriterion("KINDCODE <>", value, "kindcode");
            return (Criteria) this;
        }

        public Criteria andKindcodeGreaterThan(String value) {
            addCriterion("KINDCODE >", value, "kindcode");
            return (Criteria) this;
        }

        public Criteria andKindcodeGreaterThanOrEqualTo(String value) {
            addCriterion("KINDCODE >=", value, "kindcode");
            return (Criteria) this;
        }

        public Criteria andKindcodeLessThan(String value) {
            addCriterion("KINDCODE <", value, "kindcode");
            return (Criteria) this;
        }

        public Criteria andKindcodeLessThanOrEqualTo(String value) {
            addCriterion("KINDCODE <=", value, "kindcode");
            return (Criteria) this;
        }

        public Criteria andKindcodeLike(String value) {
            addCriterion("KINDCODE like", value, "kindcode");
            return (Criteria) this;
        }

        public Criteria andKindcodeNotLike(String value) {
            addCriterion("KINDCODE not like", value, "kindcode");
            return (Criteria) this;
        }

        public Criteria andKindcodeIn(List<String> values) {
            addCriterion("KINDCODE in", values, "kindcode");
            return (Criteria) this;
        }

        public Criteria andKindcodeNotIn(List<String> values) {
            addCriterion("KINDCODE not in", values, "kindcode");
            return (Criteria) this;
        }

        public Criteria andKindcodeBetween(String value1, String value2) {
            addCriterion("KINDCODE between", value1, value2, "kindcode");
            return (Criteria) this;
        }

        public Criteria andKindcodeNotBetween(String value1, String value2) {
            addCriterion("KINDCODE not between", value1, value2, "kindcode");
            return (Criteria) this;
        }

        public Criteria andKindversionIsNull() {
            addCriterion("KINDVERSION is null");
            return (Criteria) this;
        }

        public Criteria andKindversionIsNotNull() {
            addCriterion("KINDVERSION is not null");
            return (Criteria) this;
        }

        public Criteria andKindversionEqualTo(String value) {
            addCriterion("KINDVERSION =", value, "kindversion");
            return (Criteria) this;
        }

        public Criteria andKindversionNotEqualTo(String value) {
            addCriterion("KINDVERSION <>", value, "kindversion");
            return (Criteria) this;
        }

        public Criteria andKindversionGreaterThan(String value) {
            addCriterion("KINDVERSION >", value, "kindversion");
            return (Criteria) this;
        }

        public Criteria andKindversionGreaterThanOrEqualTo(String value) {
            addCriterion("KINDVERSION >=", value, "kindversion");
            return (Criteria) this;
        }

        public Criteria andKindversionLessThan(String value) {
            addCriterion("KINDVERSION <", value, "kindversion");
            return (Criteria) this;
        }

        public Criteria andKindversionLessThanOrEqualTo(String value) {
            addCriterion("KINDVERSION <=", value, "kindversion");
            return (Criteria) this;
        }

        public Criteria andKindversionLike(String value) {
            addCriterion("KINDVERSION like", value, "kindversion");
            return (Criteria) this;
        }

        public Criteria andKindversionNotLike(String value) {
            addCriterion("KINDVERSION not like", value, "kindversion");
            return (Criteria) this;
        }

        public Criteria andKindversionIn(List<String> values) {
            addCriterion("KINDVERSION in", values, "kindversion");
            return (Criteria) this;
        }

        public Criteria andKindversionNotIn(List<String> values) {
            addCriterion("KINDVERSION not in", values, "kindversion");
            return (Criteria) this;
        }

        public Criteria andKindversionBetween(String value1, String value2) {
            addCriterion("KINDVERSION between", value1, value2, "kindversion");
            return (Criteria) this;
        }

        public Criteria andKindversionNotBetween(String value1, String value2) {
            addCriterion("KINDVERSION not between", value1, value2, "kindversion");
            return (Criteria) this;
        }

        public Criteria andRiskcodeIsNull() {
            addCriterion("RISKCODE is null");
            return (Criteria) this;
        }

        public Criteria andRiskcodeIsNotNull() {
            addCriterion("RISKCODE is not null");
            return (Criteria) this;
        }

        public Criteria andRiskcodeEqualTo(String value) {
            addCriterion("RISKCODE =", value, "riskcode");
            return (Criteria) this;
        }

        public Criteria andRiskcodeNotEqualTo(String value) {
            addCriterion("RISKCODE <>", value, "riskcode");
            return (Criteria) this;
        }

        public Criteria andRiskcodeGreaterThan(String value) {
            addCriterion("RISKCODE >", value, "riskcode");
            return (Criteria) this;
        }

        public Criteria andRiskcodeGreaterThanOrEqualTo(String value) {
            addCriterion("RISKCODE >=", value, "riskcode");
            return (Criteria) this;
        }

        public Criteria andRiskcodeLessThan(String value) {
            addCriterion("RISKCODE <", value, "riskcode");
            return (Criteria) this;
        }

        public Criteria andRiskcodeLessThanOrEqualTo(String value) {
            addCriterion("RISKCODE <=", value, "riskcode");
            return (Criteria) this;
        }

        public Criteria andRiskcodeLike(String value) {
            addCriterion("RISKCODE like", value, "riskcode");
            return (Criteria) this;
        }

        public Criteria andRiskcodeNotLike(String value) {
            addCriterion("RISKCODE not like", value, "riskcode");
            return (Criteria) this;
        }

        public Criteria andRiskcodeIn(List<String> values) {
            addCriterion("RISKCODE in", values, "riskcode");
            return (Criteria) this;
        }

        public Criteria andRiskcodeNotIn(List<String> values) {
            addCriterion("RISKCODE not in", values, "riskcode");
            return (Criteria) this;
        }

        public Criteria andRiskcodeBetween(String value1, String value2) {
            addCriterion("RISKCODE between", value1, value2, "riskcode");
            return (Criteria) this;
        }

        public Criteria andRiskcodeNotBetween(String value1, String value2) {
            addCriterion("RISKCODE not between", value1, value2, "riskcode");
            return (Criteria) this;
        }

        public Criteria andRiskversionIsNull() {
            addCriterion("RISKVERSION is null");
            return (Criteria) this;
        }

        public Criteria andRiskversionIsNotNull() {
            addCriterion("RISKVERSION is not null");
            return (Criteria) this;
        }

        public Criteria andRiskversionEqualTo(String value) {
            addCriterion("RISKVERSION =", value, "riskversion");
            return (Criteria) this;
        }

        public Criteria andRiskversionNotEqualTo(String value) {
            addCriterion("RISKVERSION <>", value, "riskversion");
            return (Criteria) this;
        }

        public Criteria andRiskversionGreaterThan(String value) {
            addCriterion("RISKVERSION >", value, "riskversion");
            return (Criteria) this;
        }

        public Criteria andRiskversionGreaterThanOrEqualTo(String value) {
            addCriterion("RISKVERSION >=", value, "riskversion");
            return (Criteria) this;
        }

        public Criteria andRiskversionLessThan(String value) {
            addCriterion("RISKVERSION <", value, "riskversion");
            return (Criteria) this;
        }

        public Criteria andRiskversionLessThanOrEqualTo(String value) {
            addCriterion("RISKVERSION <=", value, "riskversion");
            return (Criteria) this;
        }

        public Criteria andRiskversionLike(String value) {
            addCriterion("RISKVERSION like", value, "riskversion");
            return (Criteria) this;
        }

        public Criteria andRiskversionNotLike(String value) {
            addCriterion("RISKVERSION not like", value, "riskversion");
            return (Criteria) this;
        }

        public Criteria andRiskversionIn(List<String> values) {
            addCriterion("RISKVERSION in", values, "riskversion");
            return (Criteria) this;
        }

        public Criteria andRiskversionNotIn(List<String> values) {
            addCriterion("RISKVERSION not in", values, "riskversion");
            return (Criteria) this;
        }

        public Criteria andRiskversionBetween(String value1, String value2) {
            addCriterion("RISKVERSION between", value1, value2, "riskversion");
            return (Criteria) this;
        }

        public Criteria andRiskversionNotBetween(String value1, String value2) {
            addCriterion("RISKVERSION not between", value1, value2, "riskversion");
            return (Criteria) this;
        }

        public Criteria andPlancodeIsNull() {
            addCriterion("PLANCODE is null");
            return (Criteria) this;
        }

        public Criteria andPlancodeIsNotNull() {
            addCriterion("PLANCODE is not null");
            return (Criteria) this;
        }

        public Criteria andPlancodeEqualTo(String value) {
            addCriterion("PLANCODE =", value, "plancode");
            return (Criteria) this;
        }

        public Criteria andPlancodeNotEqualTo(String value) {
            addCriterion("PLANCODE <>", value, "plancode");
            return (Criteria) this;
        }

        public Criteria andPlancodeGreaterThan(String value) {
            addCriterion("PLANCODE >", value, "plancode");
            return (Criteria) this;
        }

        public Criteria andPlancodeGreaterThanOrEqualTo(String value) {
            addCriterion("PLANCODE >=", value, "plancode");
            return (Criteria) this;
        }

        public Criteria andPlancodeLessThan(String value) {
            addCriterion("PLANCODE <", value, "plancode");
            return (Criteria) this;
        }

        public Criteria andPlancodeLessThanOrEqualTo(String value) {
            addCriterion("PLANCODE <=", value, "plancode");
            return (Criteria) this;
        }

        public Criteria andPlancodeLike(String value) {
            addCriterion("PLANCODE like", value, "plancode");
            return (Criteria) this;
        }

        public Criteria andPlancodeNotLike(String value) {
            addCriterion("PLANCODE not like", value, "plancode");
            return (Criteria) this;
        }

        public Criteria andPlancodeIn(List<String> values) {
            addCriterion("PLANCODE in", values, "plancode");
            return (Criteria) this;
        }

        public Criteria andPlancodeNotIn(List<String> values) {
            addCriterion("PLANCODE not in", values, "plancode");
            return (Criteria) this;
        }

        public Criteria andPlancodeBetween(String value1, String value2) {
            addCriterion("PLANCODE between", value1, value2, "plancode");
            return (Criteria) this;
        }

        public Criteria andPlancodeNotBetween(String value1, String value2) {
            addCriterion("PLANCODE not between", value1, value2, "plancode");
            return (Criteria) this;
        }

        public Criteria andKindcnameIsNull() {
            addCriterion("KINDCNAME is null");
            return (Criteria) this;
        }

        public Criteria andKindcnameIsNotNull() {
            addCriterion("KINDCNAME is not null");
            return (Criteria) this;
        }

        public Criteria andKindcnameEqualTo(String value) {
            addCriterion("KINDCNAME =", value, "kindcname");
            return (Criteria) this;
        }

        public Criteria andKindcnameNotEqualTo(String value) {
            addCriterion("KINDCNAME <>", value, "kindcname");
            return (Criteria) this;
        }

        public Criteria andKindcnameGreaterThan(String value) {
            addCriterion("KINDCNAME >", value, "kindcname");
            return (Criteria) this;
        }

        public Criteria andKindcnameGreaterThanOrEqualTo(String value) {
            addCriterion("KINDCNAME >=", value, "kindcname");
            return (Criteria) this;
        }

        public Criteria andKindcnameLessThan(String value) {
            addCriterion("KINDCNAME <", value, "kindcname");
            return (Criteria) this;
        }

        public Criteria andKindcnameLessThanOrEqualTo(String value) {
            addCriterion("KINDCNAME <=", value, "kindcname");
            return (Criteria) this;
        }

        public Criteria andKindcnameLike(String value) {
            addCriterion("KINDCNAME like", value, "kindcname");
            return (Criteria) this;
        }

        public Criteria andKindcnameNotLike(String value) {
            addCriterion("KINDCNAME not like", value, "kindcname");
            return (Criteria) this;
        }

        public Criteria andKindcnameIn(List<String> values) {
            addCriterion("KINDCNAME in", values, "kindcname");
            return (Criteria) this;
        }

        public Criteria andKindcnameNotIn(List<String> values) {
            addCriterion("KINDCNAME not in", values, "kindcname");
            return (Criteria) this;
        }

        public Criteria andKindcnameBetween(String value1, String value2) {
            addCriterion("KINDCNAME between", value1, value2, "kindcname");
            return (Criteria) this;
        }

        public Criteria andKindcnameNotBetween(String value1, String value2) {
            addCriterion("KINDCNAME not between", value1, value2, "kindcname");
            return (Criteria) this;
        }

        public Criteria andKindenameIsNull() {
            addCriterion("KINDENAME is null");
            return (Criteria) this;
        }

        public Criteria andKindenameIsNotNull() {
            addCriterion("KINDENAME is not null");
            return (Criteria) this;
        }

        public Criteria andKindenameEqualTo(String value) {
            addCriterion("KINDENAME =", value, "kindename");
            return (Criteria) this;
        }

        public Criteria andKindenameNotEqualTo(String value) {
            addCriterion("KINDENAME <>", value, "kindename");
            return (Criteria) this;
        }

        public Criteria andKindenameGreaterThan(String value) {
            addCriterion("KINDENAME >", value, "kindename");
            return (Criteria) this;
        }

        public Criteria andKindenameGreaterThanOrEqualTo(String value) {
            addCriterion("KINDENAME >=", value, "kindename");
            return (Criteria) this;
        }

        public Criteria andKindenameLessThan(String value) {
            addCriterion("KINDENAME <", value, "kindename");
            return (Criteria) this;
        }

        public Criteria andKindenameLessThanOrEqualTo(String value) {
            addCriterion("KINDENAME <=", value, "kindename");
            return (Criteria) this;
        }

        public Criteria andKindenameLike(String value) {
            addCriterion("KINDENAME like", value, "kindename");
            return (Criteria) this;
        }

        public Criteria andKindenameNotLike(String value) {
            addCriterion("KINDENAME not like", value, "kindename");
            return (Criteria) this;
        }

        public Criteria andKindenameIn(List<String> values) {
            addCriterion("KINDENAME in", values, "kindename");
            return (Criteria) this;
        }

        public Criteria andKindenameNotIn(List<String> values) {
            addCriterion("KINDENAME not in", values, "kindename");
            return (Criteria) this;
        }

        public Criteria andKindenameBetween(String value1, String value2) {
            addCriterion("KINDENAME between", value1, value2, "kindename");
            return (Criteria) this;
        }

        public Criteria andKindenameNotBetween(String value1, String value2) {
            addCriterion("KINDENAME not between", value1, value2, "kindename");
            return (Criteria) this;
        }

        public Criteria andStartdateIsNull() {
            addCriterion("STARTDATE is null");
            return (Criteria) this;
        }

        public Criteria andStartdateIsNotNull() {
            addCriterion("STARTDATE is not null");
            return (Criteria) this;
        }

        public Criteria andStartdateEqualTo(Date value) {
            addCriterion("STARTDATE =", value, "startdate");
            return (Criteria) this;
        }

        public Criteria andStartdateNotEqualTo(Date value) {
            addCriterion("STARTDATE <>", value, "startdate");
            return (Criteria) this;
        }

        public Criteria andStartdateGreaterThan(Date value) {
            addCriterion("STARTDATE >", value, "startdate");
            return (Criteria) this;
        }

        public Criteria andStartdateGreaterThanOrEqualTo(Date value) {
            addCriterion("STARTDATE >=", value, "startdate");
            return (Criteria) this;
        }

        public Criteria andStartdateLessThan(Date value) {
            addCriterion("STARTDATE <", value, "startdate");
            return (Criteria) this;
        }

        public Criteria andStartdateLessThanOrEqualTo(Date value) {
            addCriterion("STARTDATE <=", value, "startdate");
            return (Criteria) this;
        }

        public Criteria andStartdateIn(List<Date> values) {
            addCriterion("STARTDATE in", values, "startdate");
            return (Criteria) this;
        }

        public Criteria andStartdateNotIn(List<Date> values) {
            addCriterion("STARTDATE not in", values, "startdate");
            return (Criteria) this;
        }

        public Criteria andStartdateBetween(Date value1, Date value2) {
            addCriterion("STARTDATE between", value1, value2, "startdate");
            return (Criteria) this;
        }

        public Criteria andStartdateNotBetween(Date value1, Date value2) {
            addCriterion("STARTDATE not between", value1, value2, "startdate");
            return (Criteria) this;
        }

        public Criteria andEnddateIsNull() {
            addCriterion("ENDDATE is null");
            return (Criteria) this;
        }

        public Criteria andEnddateIsNotNull() {
            addCriterion("ENDDATE is not null");
            return (Criteria) this;
        }

        public Criteria andEnddateEqualTo(Date value) {
            addCriterion("ENDDATE =", value, "enddate");
            return (Criteria) this;
        }

        public Criteria andEnddateNotEqualTo(Date value) {
            addCriterion("ENDDATE <>", value, "enddate");
            return (Criteria) this;
        }

        public Criteria andEnddateGreaterThan(Date value) {
            addCriterion("ENDDATE >", value, "enddate");
            return (Criteria) this;
        }

        public Criteria andEnddateGreaterThanOrEqualTo(Date value) {
            addCriterion("ENDDATE >=", value, "enddate");
            return (Criteria) this;
        }

        public Criteria andEnddateLessThan(Date value) {
            addCriterion("ENDDATE <", value, "enddate");
            return (Criteria) this;
        }

        public Criteria andEnddateLessThanOrEqualTo(Date value) {
            addCriterion("ENDDATE <=", value, "enddate");
            return (Criteria) this;
        }

        public Criteria andEnddateIn(List<Date> values) {
            addCriterion("ENDDATE in", values, "enddate");
            return (Criteria) this;
        }

        public Criteria andEnddateNotIn(List<Date> values) {
            addCriterion("ENDDATE not in", values, "enddate");
            return (Criteria) this;
        }

        public Criteria andEnddateBetween(Date value1, Date value2) {
            addCriterion("ENDDATE between", value1, value2, "enddate");
            return (Criteria) this;
        }

        public Criteria andEnddateNotBetween(Date value1, Date value2) {
            addCriterion("ENDDATE not between", value1, value2, "enddate");
            return (Criteria) this;
        }

        public Criteria andClausecodeIsNull() {
            addCriterion("CLAUSECODE is null");
            return (Criteria) this;
        }

        public Criteria andClausecodeIsNotNull() {
            addCriterion("CLAUSECODE is not null");
            return (Criteria) this;
        }

        public Criteria andClausecodeEqualTo(String value) {
            addCriterion("CLAUSECODE =", value, "clausecode");
            return (Criteria) this;
        }

        public Criteria andClausecodeNotEqualTo(String value) {
            addCriterion("CLAUSECODE <>", value, "clausecode");
            return (Criteria) this;
        }

        public Criteria andClausecodeGreaterThan(String value) {
            addCriterion("CLAUSECODE >", value, "clausecode");
            return (Criteria) this;
        }

        public Criteria andClausecodeGreaterThanOrEqualTo(String value) {
            addCriterion("CLAUSECODE >=", value, "clausecode");
            return (Criteria) this;
        }

        public Criteria andClausecodeLessThan(String value) {
            addCriterion("CLAUSECODE <", value, "clausecode");
            return (Criteria) this;
        }

        public Criteria andClausecodeLessThanOrEqualTo(String value) {
            addCriterion("CLAUSECODE <=", value, "clausecode");
            return (Criteria) this;
        }

        public Criteria andClausecodeLike(String value) {
            addCriterion("CLAUSECODE like", value, "clausecode");
            return (Criteria) this;
        }

        public Criteria andClausecodeNotLike(String value) {
            addCriterion("CLAUSECODE not like", value, "clausecode");
            return (Criteria) this;
        }

        public Criteria andClausecodeIn(List<String> values) {
            addCriterion("CLAUSECODE in", values, "clausecode");
            return (Criteria) this;
        }

        public Criteria andClausecodeNotIn(List<String> values) {
            addCriterion("CLAUSECODE not in", values, "clausecode");
            return (Criteria) this;
        }

        public Criteria andClausecodeBetween(String value1, String value2) {
            addCriterion("CLAUSECODE between", value1, value2, "clausecode");
            return (Criteria) this;
        }

        public Criteria andClausecodeNotBetween(String value1, String value2) {
            addCriterion("CLAUSECODE not between", value1, value2, "clausecode");
            return (Criteria) this;
        }

        public Criteria andRelyonkindcodeIsNull() {
            addCriterion("RELYONKINDCODE is null");
            return (Criteria) this;
        }

        public Criteria andRelyonkindcodeIsNotNull() {
            addCriterion("RELYONKINDCODE is not null");
            return (Criteria) this;
        }

        public Criteria andRelyonkindcodeEqualTo(String value) {
            addCriterion("RELYONKINDCODE =", value, "relyonkindcode");
            return (Criteria) this;
        }

        public Criteria andRelyonkindcodeNotEqualTo(String value) {
            addCriterion("RELYONKINDCODE <>", value, "relyonkindcode");
            return (Criteria) this;
        }

        public Criteria andRelyonkindcodeGreaterThan(String value) {
            addCriterion("RELYONKINDCODE >", value, "relyonkindcode");
            return (Criteria) this;
        }

        public Criteria andRelyonkindcodeGreaterThanOrEqualTo(String value) {
            addCriterion("RELYONKINDCODE >=", value, "relyonkindcode");
            return (Criteria) this;
        }

        public Criteria andRelyonkindcodeLessThan(String value) {
            addCriterion("RELYONKINDCODE <", value, "relyonkindcode");
            return (Criteria) this;
        }

        public Criteria andRelyonkindcodeLessThanOrEqualTo(String value) {
            addCriterion("RELYONKINDCODE <=", value, "relyonkindcode");
            return (Criteria) this;
        }

        public Criteria andRelyonkindcodeLike(String value) {
            addCriterion("RELYONKINDCODE like", value, "relyonkindcode");
            return (Criteria) this;
        }

        public Criteria andRelyonkindcodeNotLike(String value) {
            addCriterion("RELYONKINDCODE not like", value, "relyonkindcode");
            return (Criteria) this;
        }

        public Criteria andRelyonkindcodeIn(List<String> values) {
            addCriterion("RELYONKINDCODE in", values, "relyonkindcode");
            return (Criteria) this;
        }

        public Criteria andRelyonkindcodeNotIn(List<String> values) {
            addCriterion("RELYONKINDCODE not in", values, "relyonkindcode");
            return (Criteria) this;
        }

        public Criteria andRelyonkindcodeBetween(String value1, String value2) {
            addCriterion("RELYONKINDCODE between", value1, value2, "relyonkindcode");
            return (Criteria) this;
        }

        public Criteria andRelyonkindcodeNotBetween(String value1, String value2) {
            addCriterion("RELYONKINDCODE not between", value1, value2, "relyonkindcode");
            return (Criteria) this;
        }

        public Criteria andRelyonstartdateIsNull() {
            addCriterion("RELYONSTARTDATE is null");
            return (Criteria) this;
        }

        public Criteria andRelyonstartdateIsNotNull() {
            addCriterion("RELYONSTARTDATE is not null");
            return (Criteria) this;
        }

        public Criteria andRelyonstartdateEqualTo(Date value) {
            addCriterion("RELYONSTARTDATE =", value, "relyonstartdate");
            return (Criteria) this;
        }

        public Criteria andRelyonstartdateNotEqualTo(Date value) {
            addCriterion("RELYONSTARTDATE <>", value, "relyonstartdate");
            return (Criteria) this;
        }

        public Criteria andRelyonstartdateGreaterThan(Date value) {
            addCriterion("RELYONSTARTDATE >", value, "relyonstartdate");
            return (Criteria) this;
        }

        public Criteria andRelyonstartdateGreaterThanOrEqualTo(Date value) {
            addCriterion("RELYONSTARTDATE >=", value, "relyonstartdate");
            return (Criteria) this;
        }

        public Criteria andRelyonstartdateLessThan(Date value) {
            addCriterion("RELYONSTARTDATE <", value, "relyonstartdate");
            return (Criteria) this;
        }

        public Criteria andRelyonstartdateLessThanOrEqualTo(Date value) {
            addCriterion("RELYONSTARTDATE <=", value, "relyonstartdate");
            return (Criteria) this;
        }

        public Criteria andRelyonstartdateIn(List<Date> values) {
            addCriterion("RELYONSTARTDATE in", values, "relyonstartdate");
            return (Criteria) this;
        }

        public Criteria andRelyonstartdateNotIn(List<Date> values) {
            addCriterion("RELYONSTARTDATE not in", values, "relyonstartdate");
            return (Criteria) this;
        }

        public Criteria andRelyonstartdateBetween(Date value1, Date value2) {
            addCriterion("RELYONSTARTDATE between", value1, value2, "relyonstartdate");
            return (Criteria) this;
        }

        public Criteria andRelyonstartdateNotBetween(Date value1, Date value2) {
            addCriterion("RELYONSTARTDATE not between", value1, value2, "relyonstartdate");
            return (Criteria) this;
        }

        public Criteria andRelyonenddateIsNull() {
            addCriterion("RELYONENDDATE is null");
            return (Criteria) this;
        }

        public Criteria andRelyonenddateIsNotNull() {
            addCriterion("RELYONENDDATE is not null");
            return (Criteria) this;
        }

        public Criteria andRelyonenddateEqualTo(Date value) {
            addCriterion("RELYONENDDATE =", value, "relyonenddate");
            return (Criteria) this;
        }

        public Criteria andRelyonenddateNotEqualTo(Date value) {
            addCriterion("RELYONENDDATE <>", value, "relyonenddate");
            return (Criteria) this;
        }

        public Criteria andRelyonenddateGreaterThan(Date value) {
            addCriterion("RELYONENDDATE >", value, "relyonenddate");
            return (Criteria) this;
        }

        public Criteria andRelyonenddateGreaterThanOrEqualTo(Date value) {
            addCriterion("RELYONENDDATE >=", value, "relyonenddate");
            return (Criteria) this;
        }

        public Criteria andRelyonenddateLessThan(Date value) {
            addCriterion("RELYONENDDATE <", value, "relyonenddate");
            return (Criteria) this;
        }

        public Criteria andRelyonenddateLessThanOrEqualTo(Date value) {
            addCriterion("RELYONENDDATE <=", value, "relyonenddate");
            return (Criteria) this;
        }

        public Criteria andRelyonenddateIn(List<Date> values) {
            addCriterion("RELYONENDDATE in", values, "relyonenddate");
            return (Criteria) this;
        }

        public Criteria andRelyonenddateNotIn(List<Date> values) {
            addCriterion("RELYONENDDATE not in", values, "relyonenddate");
            return (Criteria) this;
        }

        public Criteria andRelyonenddateBetween(Date value1, Date value2) {
            addCriterion("RELYONENDDATE between", value1, value2, "relyonenddate");
            return (Criteria) this;
        }

        public Criteria andRelyonenddateNotBetween(Date value1, Date value2) {
            addCriterion("RELYONENDDATE not between", value1, value2, "relyonenddate");
            return (Criteria) this;
        }

        public Criteria andCalculateflagIsNull() {
            addCriterion("CALCULATEFLAG is null");
            return (Criteria) this;
        }

        public Criteria andCalculateflagIsNotNull() {
            addCriterion("CALCULATEFLAG is not null");
            return (Criteria) this;
        }

        public Criteria andCalculateflagEqualTo(String value) {
            addCriterion("CALCULATEFLAG =", value, "calculateflag");
            return (Criteria) this;
        }

        public Criteria andCalculateflagNotEqualTo(String value) {
            addCriterion("CALCULATEFLAG <>", value, "calculateflag");
            return (Criteria) this;
        }

        public Criteria andCalculateflagGreaterThan(String value) {
            addCriterion("CALCULATEFLAG >", value, "calculateflag");
            return (Criteria) this;
        }

        public Criteria andCalculateflagGreaterThanOrEqualTo(String value) {
            addCriterion("CALCULATEFLAG >=", value, "calculateflag");
            return (Criteria) this;
        }

        public Criteria andCalculateflagLessThan(String value) {
            addCriterion("CALCULATEFLAG <", value, "calculateflag");
            return (Criteria) this;
        }

        public Criteria andCalculateflagLessThanOrEqualTo(String value) {
            addCriterion("CALCULATEFLAG <=", value, "calculateflag");
            return (Criteria) this;
        }

        public Criteria andCalculateflagLike(String value) {
            addCriterion("CALCULATEFLAG like", value, "calculateflag");
            return (Criteria) this;
        }

        public Criteria andCalculateflagNotLike(String value) {
            addCriterion("CALCULATEFLAG not like", value, "calculateflag");
            return (Criteria) this;
        }

        public Criteria andCalculateflagIn(List<String> values) {
            addCriterion("CALCULATEFLAG in", values, "calculateflag");
            return (Criteria) this;
        }

        public Criteria andCalculateflagNotIn(List<String> values) {
            addCriterion("CALCULATEFLAG not in", values, "calculateflag");
            return (Criteria) this;
        }

        public Criteria andCalculateflagBetween(String value1, String value2) {
            addCriterion("CALCULATEFLAG between", value1, value2, "calculateflag");
            return (Criteria) this;
        }

        public Criteria andCalculateflagNotBetween(String value1, String value2) {
            addCriterion("CALCULATEFLAG not between", value1, value2, "calculateflag");
            return (Criteria) this;
        }

        public Criteria andNewkindcodeIsNull() {
            addCriterion("NEWKINDCODE is null");
            return (Criteria) this;
        }

        public Criteria andNewkindcodeIsNotNull() {
            addCriterion("NEWKINDCODE is not null");
            return (Criteria) this;
        }

        public Criteria andNewkindcodeEqualTo(String value) {
            addCriterion("NEWKINDCODE =", value, "newkindcode");
            return (Criteria) this;
        }

        public Criteria andNewkindcodeNotEqualTo(String value) {
            addCriterion("NEWKINDCODE <>", value, "newkindcode");
            return (Criteria) this;
        }

        public Criteria andNewkindcodeGreaterThan(String value) {
            addCriterion("NEWKINDCODE >", value, "newkindcode");
            return (Criteria) this;
        }

        public Criteria andNewkindcodeGreaterThanOrEqualTo(String value) {
            addCriterion("NEWKINDCODE >=", value, "newkindcode");
            return (Criteria) this;
        }

        public Criteria andNewkindcodeLessThan(String value) {
            addCriterion("NEWKINDCODE <", value, "newkindcode");
            return (Criteria) this;
        }

        public Criteria andNewkindcodeLessThanOrEqualTo(String value) {
            addCriterion("NEWKINDCODE <=", value, "newkindcode");
            return (Criteria) this;
        }

        public Criteria andNewkindcodeLike(String value) {
            addCriterion("NEWKINDCODE like", value, "newkindcode");
            return (Criteria) this;
        }

        public Criteria andNewkindcodeNotLike(String value) {
            addCriterion("NEWKINDCODE not like", value, "newkindcode");
            return (Criteria) this;
        }

        public Criteria andNewkindcodeIn(List<String> values) {
            addCriterion("NEWKINDCODE in", values, "newkindcode");
            return (Criteria) this;
        }

        public Criteria andNewkindcodeNotIn(List<String> values) {
            addCriterion("NEWKINDCODE not in", values, "newkindcode");
            return (Criteria) this;
        }

        public Criteria andNewkindcodeBetween(String value1, String value2) {
            addCriterion("NEWKINDCODE between", value1, value2, "newkindcode");
            return (Criteria) this;
        }

        public Criteria andNewkindcodeNotBetween(String value1, String value2) {
            addCriterion("NEWKINDCODE not between", value1, value2, "newkindcode");
            return (Criteria) this;
        }

        public Criteria andValidstatusIsNull() {
            addCriterion("VALIDSTATUS is null");
            return (Criteria) this;
        }

        public Criteria andValidstatusIsNotNull() {
            addCriterion("VALIDSTATUS is not null");
            return (Criteria) this;
        }

        public Criteria andValidstatusEqualTo(String value) {
            addCriterion("VALIDSTATUS =", value, "validstatus");
            return (Criteria) this;
        }

        public Criteria andValidstatusNotEqualTo(String value) {
            addCriterion("VALIDSTATUS <>", value, "validstatus");
            return (Criteria) this;
        }

        public Criteria andValidstatusGreaterThan(String value) {
            addCriterion("VALIDSTATUS >", value, "validstatus");
            return (Criteria) this;
        }

        public Criteria andValidstatusGreaterThanOrEqualTo(String value) {
            addCriterion("VALIDSTATUS >=", value, "validstatus");
            return (Criteria) this;
        }

        public Criteria andValidstatusLessThan(String value) {
            addCriterion("VALIDSTATUS <", value, "validstatus");
            return (Criteria) this;
        }

        public Criteria andValidstatusLessThanOrEqualTo(String value) {
            addCriterion("VALIDSTATUS <=", value, "validstatus");
            return (Criteria) this;
        }

        public Criteria andValidstatusLike(String value) {
            addCriterion("VALIDSTATUS like", value, "validstatus");
            return (Criteria) this;
        }

        public Criteria andValidstatusNotLike(String value) {
            addCriterion("VALIDSTATUS not like", value, "validstatus");
            return (Criteria) this;
        }

        public Criteria andValidstatusIn(List<String> values) {
            addCriterion("VALIDSTATUS in", values, "validstatus");
            return (Criteria) this;
        }

        public Criteria andValidstatusNotIn(List<String> values) {
            addCriterion("VALIDSTATUS not in", values, "validstatus");
            return (Criteria) this;
        }

        public Criteria andValidstatusBetween(String value1, String value2) {
            addCriterion("VALIDSTATUS between", value1, value2, "validstatus");
            return (Criteria) this;
        }

        public Criteria andValidstatusNotBetween(String value1, String value2) {
            addCriterion("VALIDSTATUS not between", value1, value2, "validstatus");
            return (Criteria) this;
        }

        public Criteria andFlagIsNull() {
            addCriterion("FLAG is null");
            return (Criteria) this;
        }

        public Criteria andFlagIsNotNull() {
            addCriterion("FLAG is not null");
            return (Criteria) this;
        }

        public Criteria andFlagEqualTo(String value) {
            addCriterion("FLAG =", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagNotEqualTo(String value) {
            addCriterion("FLAG <>", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagGreaterThan(String value) {
            addCriterion("FLAG >", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagGreaterThanOrEqualTo(String value) {
            addCriterion("FLAG >=", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagLessThan(String value) {
            addCriterion("FLAG <", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagLessThanOrEqualTo(String value) {
            addCriterion("FLAG <=", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagLike(String value) {
            addCriterion("FLAG like", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagNotLike(String value) {
            addCriterion("FLAG not like", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagIn(List<String> values) {
            addCriterion("FLAG in", values, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagNotIn(List<String> values) {
            addCriterion("FLAG not in", values, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagBetween(String value1, String value2) {
            addCriterion("FLAG between", value1, value2, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagNotBetween(String value1, String value2) {
            addCriterion("FLAG not between", value1, value2, "flag");
            return (Criteria) this;
        }

        public Criteria andOwnerriskcodeIsNull() {
            addCriterion("OWNERRISKCODE is null");
            return (Criteria) this;
        }

        public Criteria andOwnerriskcodeIsNotNull() {
            addCriterion("OWNERRISKCODE is not null");
            return (Criteria) this;
        }

        public Criteria andOwnerriskcodeEqualTo(String value) {
            addCriterion("OWNERRISKCODE =", value, "ownerriskcode");
            return (Criteria) this;
        }

        public Criteria andOwnerriskcodeNotEqualTo(String value) {
            addCriterion("OWNERRISKCODE <>", value, "ownerriskcode");
            return (Criteria) this;
        }

        public Criteria andOwnerriskcodeGreaterThan(String value) {
            addCriterion("OWNERRISKCODE >", value, "ownerriskcode");
            return (Criteria) this;
        }

        public Criteria andOwnerriskcodeGreaterThanOrEqualTo(String value) {
            addCriterion("OWNERRISKCODE >=", value, "ownerriskcode");
            return (Criteria) this;
        }

        public Criteria andOwnerriskcodeLessThan(String value) {
            addCriterion("OWNERRISKCODE <", value, "ownerriskcode");
            return (Criteria) this;
        }

        public Criteria andOwnerriskcodeLessThanOrEqualTo(String value) {
            addCriterion("OWNERRISKCODE <=", value, "ownerriskcode");
            return (Criteria) this;
        }

        public Criteria andOwnerriskcodeLike(String value) {
            addCriterion("OWNERRISKCODE like", value, "ownerriskcode");
            return (Criteria) this;
        }

        public Criteria andOwnerriskcodeNotLike(String value) {
            addCriterion("OWNERRISKCODE not like", value, "ownerriskcode");
            return (Criteria) this;
        }

        public Criteria andOwnerriskcodeIn(List<String> values) {
            addCriterion("OWNERRISKCODE in", values, "ownerriskcode");
            return (Criteria) this;
        }

        public Criteria andOwnerriskcodeNotIn(List<String> values) {
            addCriterion("OWNERRISKCODE not in", values, "ownerriskcode");
            return (Criteria) this;
        }

        public Criteria andOwnerriskcodeBetween(String value1, String value2) {
            addCriterion("OWNERRISKCODE between", value1, value2, "ownerriskcode");
            return (Criteria) this;
        }

        public Criteria andOwnerriskcodeNotBetween(String value1, String value2) {
            addCriterion("OWNERRISKCODE not between", value1, value2, "ownerriskcode");
            return (Criteria) this;
        }

        public Criteria andKindwidthIsNull() {
            addCriterion("KINDWIDTH is null");
            return (Criteria) this;
        }

        public Criteria andKindwidthIsNotNull() {
            addCriterion("KINDWIDTH is not null");
            return (Criteria) this;
        }

        public Criteria andKindwidthEqualTo(String value) {
            addCriterion("KINDWIDTH =", value, "kindwidth");
            return (Criteria) this;
        }

        public Criteria andKindwidthNotEqualTo(String value) {
            addCriterion("KINDWIDTH <>", value, "kindwidth");
            return (Criteria) this;
        }

        public Criteria andKindwidthGreaterThan(String value) {
            addCriterion("KINDWIDTH >", value, "kindwidth");
            return (Criteria) this;
        }

        public Criteria andKindwidthGreaterThanOrEqualTo(String value) {
            addCriterion("KINDWIDTH >=", value, "kindwidth");
            return (Criteria) this;
        }

        public Criteria andKindwidthLessThan(String value) {
            addCriterion("KINDWIDTH <", value, "kindwidth");
            return (Criteria) this;
        }

        public Criteria andKindwidthLessThanOrEqualTo(String value) {
            addCriterion("KINDWIDTH <=", value, "kindwidth");
            return (Criteria) this;
        }

        public Criteria andKindwidthLike(String value) {
            addCriterion("KINDWIDTH like", value, "kindwidth");
            return (Criteria) this;
        }

        public Criteria andKindwidthNotLike(String value) {
            addCriterion("KINDWIDTH not like", value, "kindwidth");
            return (Criteria) this;
        }

        public Criteria andKindwidthIn(List<String> values) {
            addCriterion("KINDWIDTH in", values, "kindwidth");
            return (Criteria) this;
        }

        public Criteria andKindwidthNotIn(List<String> values) {
            addCriterion("KINDWIDTH not in", values, "kindwidth");
            return (Criteria) this;
        }

        public Criteria andKindwidthBetween(String value1, String value2) {
            addCriterion("KINDWIDTH between", value1, value2, "kindwidth");
            return (Criteria) this;
        }

        public Criteria andKindwidthNotBetween(String value1, String value2) {
            addCriterion("KINDWIDTH not between", value1, value2, "kindwidth");
            return (Criteria) this;
        }

        public Criteria andKindheightIsNull() {
            addCriterion("KINDHEIGHT is null");
            return (Criteria) this;
        }

        public Criteria andKindheightIsNotNull() {
            addCriterion("KINDHEIGHT is not null");
            return (Criteria) this;
        }

        public Criteria andKindheightEqualTo(String value) {
            addCriterion("KINDHEIGHT =", value, "kindheight");
            return (Criteria) this;
        }

        public Criteria andKindheightNotEqualTo(String value) {
            addCriterion("KINDHEIGHT <>", value, "kindheight");
            return (Criteria) this;
        }

        public Criteria andKindheightGreaterThan(String value) {
            addCriterion("KINDHEIGHT >", value, "kindheight");
            return (Criteria) this;
        }

        public Criteria andKindheightGreaterThanOrEqualTo(String value) {
            addCriterion("KINDHEIGHT >=", value, "kindheight");
            return (Criteria) this;
        }

        public Criteria andKindheightLessThan(String value) {
            addCriterion("KINDHEIGHT <", value, "kindheight");
            return (Criteria) this;
        }

        public Criteria andKindheightLessThanOrEqualTo(String value) {
            addCriterion("KINDHEIGHT <=", value, "kindheight");
            return (Criteria) this;
        }

        public Criteria andKindheightLike(String value) {
            addCriterion("KINDHEIGHT like", value, "kindheight");
            return (Criteria) this;
        }

        public Criteria andKindheightNotLike(String value) {
            addCriterion("KINDHEIGHT not like", value, "kindheight");
            return (Criteria) this;
        }

        public Criteria andKindheightIn(List<String> values) {
            addCriterion("KINDHEIGHT in", values, "kindheight");
            return (Criteria) this;
        }

        public Criteria andKindheightNotIn(List<String> values) {
            addCriterion("KINDHEIGHT not in", values, "kindheight");
            return (Criteria) this;
        }

        public Criteria andKindheightBetween(String value1, String value2) {
            addCriterion("KINDHEIGHT between", value1, value2, "kindheight");
            return (Criteria) this;
        }

        public Criteria andKindheightNotBetween(String value1, String value2) {
            addCriterion("KINDHEIGHT not between", value1, value2, "kindheight");
            return (Criteria) this;
        }

        public Criteria andShortratetypeIsNull() {
            addCriterion("SHORTRATETYPE is null");
            return (Criteria) this;
        }

        public Criteria andShortratetypeIsNotNull() {
            addCriterion("SHORTRATETYPE is not null");
            return (Criteria) this;
        }

        public Criteria andShortratetypeEqualTo(String value) {
            addCriterion("SHORTRATETYPE =", value, "shortratetype");
            return (Criteria) this;
        }

        public Criteria andShortratetypeNotEqualTo(String value) {
            addCriterion("SHORTRATETYPE <>", value, "shortratetype");
            return (Criteria) this;
        }

        public Criteria andShortratetypeGreaterThan(String value) {
            addCriterion("SHORTRATETYPE >", value, "shortratetype");
            return (Criteria) this;
        }

        public Criteria andShortratetypeGreaterThanOrEqualTo(String value) {
            addCriterion("SHORTRATETYPE >=", value, "shortratetype");
            return (Criteria) this;
        }

        public Criteria andShortratetypeLessThan(String value) {
            addCriterion("SHORTRATETYPE <", value, "shortratetype");
            return (Criteria) this;
        }

        public Criteria andShortratetypeLessThanOrEqualTo(String value) {
            addCriterion("SHORTRATETYPE <=", value, "shortratetype");
            return (Criteria) this;
        }

        public Criteria andShortratetypeLike(String value) {
            addCriterion("SHORTRATETYPE like", value, "shortratetype");
            return (Criteria) this;
        }

        public Criteria andShortratetypeNotLike(String value) {
            addCriterion("SHORTRATETYPE not like", value, "shortratetype");
            return (Criteria) this;
        }

        public Criteria andShortratetypeIn(List<String> values) {
            addCriterion("SHORTRATETYPE in", values, "shortratetype");
            return (Criteria) this;
        }

        public Criteria andShortratetypeNotIn(List<String> values) {
            addCriterion("SHORTRATETYPE not in", values, "shortratetype");
            return (Criteria) this;
        }

        public Criteria andShortratetypeBetween(String value1, String value2) {
            addCriterion("SHORTRATETYPE between", value1, value2, "shortratetype");
            return (Criteria) this;
        }

        public Criteria andShortratetypeNotBetween(String value1, String value2) {
            addCriterion("SHORTRATETYPE not between", value1, value2, "shortratetype");
            return (Criteria) this;
        }

        public Criteria andExt1IsNull() {
            addCriterion("EXT1 is null");
            return (Criteria) this;
        }

        public Criteria andExt1IsNotNull() {
            addCriterion("EXT1 is not null");
            return (Criteria) this;
        }

        public Criteria andExt1EqualTo(String value) {
            addCriterion("EXT1 =", value, "ext1");
            return (Criteria) this;
        }

        public Criteria andExt1NotEqualTo(String value) {
            addCriterion("EXT1 <>", value, "ext1");
            return (Criteria) this;
        }

        public Criteria andExt1GreaterThan(String value) {
            addCriterion("EXT1 >", value, "ext1");
            return (Criteria) this;
        }

        public Criteria andExt1GreaterThanOrEqualTo(String value) {
            addCriterion("EXT1 >=", value, "ext1");
            return (Criteria) this;
        }

        public Criteria andExt1LessThan(String value) {
            addCriterion("EXT1 <", value, "ext1");
            return (Criteria) this;
        }

        public Criteria andExt1LessThanOrEqualTo(String value) {
            addCriterion("EXT1 <=", value, "ext1");
            return (Criteria) this;
        }

        public Criteria andExt1Like(String value) {
            addCriterion("EXT1 like", value, "ext1");
            return (Criteria) this;
        }

        public Criteria andExt1NotLike(String value) {
            addCriterion("EXT1 not like", value, "ext1");
            return (Criteria) this;
        }

        public Criteria andExt1In(List<String> values) {
            addCriterion("EXT1 in", values, "ext1");
            return (Criteria) this;
        }

        public Criteria andExt1NotIn(List<String> values) {
            addCriterion("EXT1 not in", values, "ext1");
            return (Criteria) this;
        }

        public Criteria andExt1Between(String value1, String value2) {
            addCriterion("EXT1 between", value1, value2, "ext1");
            return (Criteria) this;
        }

        public Criteria andExt1NotBetween(String value1, String value2) {
            addCriterion("EXT1 not between", value1, value2, "ext1");
            return (Criteria) this;
        }

        public Criteria andExt2IsNull() {
            addCriterion("EXT2 is null");
            return (Criteria) this;
        }

        public Criteria andExt2IsNotNull() {
            addCriterion("EXT2 is not null");
            return (Criteria) this;
        }

        public Criteria andExt2EqualTo(String value) {
            addCriterion("EXT2 =", value, "ext2");
            return (Criteria) this;
        }

        public Criteria andExt2NotEqualTo(String value) {
            addCriterion("EXT2 <>", value, "ext2");
            return (Criteria) this;
        }

        public Criteria andExt2GreaterThan(String value) {
            addCriterion("EXT2 >", value, "ext2");
            return (Criteria) this;
        }

        public Criteria andExt2GreaterThanOrEqualTo(String value) {
            addCriterion("EXT2 >=", value, "ext2");
            return (Criteria) this;
        }

        public Criteria andExt2LessThan(String value) {
            addCriterion("EXT2 <", value, "ext2");
            return (Criteria) this;
        }

        public Criteria andExt2LessThanOrEqualTo(String value) {
            addCriterion("EXT2 <=", value, "ext2");
            return (Criteria) this;
        }

        public Criteria andExt2Like(String value) {
            addCriterion("EXT2 like", value, "ext2");
            return (Criteria) this;
        }

        public Criteria andExt2NotLike(String value) {
            addCriterion("EXT2 not like", value, "ext2");
            return (Criteria) this;
        }

        public Criteria andExt2In(List<String> values) {
            addCriterion("EXT2 in", values, "ext2");
            return (Criteria) this;
        }

        public Criteria andExt2NotIn(List<String> values) {
            addCriterion("EXT2 not in", values, "ext2");
            return (Criteria) this;
        }

        public Criteria andExt2Between(String value1, String value2) {
            addCriterion("EXT2 between", value1, value2, "ext2");
            return (Criteria) this;
        }

        public Criteria andExt2NotBetween(String value1, String value2) {
            addCriterion("EXT2 not between", value1, value2, "ext2");
            return (Criteria) this;
        }

        public Criteria andExt3IsNull() {
            addCriterion("EXT3 is null");
            return (Criteria) this;
        }

        public Criteria andExt3IsNotNull() {
            addCriterion("EXT3 is not null");
            return (Criteria) this;
        }

        public Criteria andExt3EqualTo(String value) {
            addCriterion("EXT3 =", value, "ext3");
            return (Criteria) this;
        }

        public Criteria andExt3NotEqualTo(String value) {
            addCriterion("EXT3 <>", value, "ext3");
            return (Criteria) this;
        }

        public Criteria andExt3GreaterThan(String value) {
            addCriterion("EXT3 >", value, "ext3");
            return (Criteria) this;
        }

        public Criteria andExt3GreaterThanOrEqualTo(String value) {
            addCriterion("EXT3 >=", value, "ext3");
            return (Criteria) this;
        }

        public Criteria andExt3LessThan(String value) {
            addCriterion("EXT3 <", value, "ext3");
            return (Criteria) this;
        }

        public Criteria andExt3LessThanOrEqualTo(String value) {
            addCriterion("EXT3 <=", value, "ext3");
            return (Criteria) this;
        }

        public Criteria andExt3Like(String value) {
            addCriterion("EXT3 like", value, "ext3");
            return (Criteria) this;
        }

        public Criteria andExt3NotLike(String value) {
            addCriterion("EXT3 not like", value, "ext3");
            return (Criteria) this;
        }

        public Criteria andExt3In(List<String> values) {
            addCriterion("EXT3 in", values, "ext3");
            return (Criteria) this;
        }

        public Criteria andExt3NotIn(List<String> values) {
            addCriterion("EXT3 not in", values, "ext3");
            return (Criteria) this;
        }

        public Criteria andExt3Between(String value1, String value2) {
            addCriterion("EXT3 between", value1, value2, "ext3");
            return (Criteria) this;
        }

        public Criteria andExt3NotBetween(String value1, String value2) {
            addCriterion("EXT3 not between", value1, value2, "ext3");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}