package cn.com.libertymutual.production.model.nomorcldatasource;

import java.util.ArrayList;
import java.util.List;

public class PrpdpropsExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public PrpdpropsExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andPropscodeIsNull() {
            addCriterion("PROPSCODE is null");
            return (Criteria) this;
        }

        public Criteria andPropscodeIsNotNull() {
            addCriterion("PROPSCODE is not null");
            return (Criteria) this;
        }

        public Criteria andPropscodeEqualTo(String value) {
            addCriterion("PROPSCODE =", value, "propscode");
            return (Criteria) this;
        }

        public Criteria andPropscodeNotEqualTo(String value) {
            addCriterion("PROPSCODE <>", value, "propscode");
            return (Criteria) this;
        }

        public Criteria andPropscodeGreaterThan(String value) {
            addCriterion("PROPSCODE >", value, "propscode");
            return (Criteria) this;
        }

        public Criteria andPropscodeGreaterThanOrEqualTo(String value) {
            addCriterion("PROPSCODE >=", value, "propscode");
            return (Criteria) this;
        }

        public Criteria andPropscodeLessThan(String value) {
            addCriterion("PROPSCODE <", value, "propscode");
            return (Criteria) this;
        }

        public Criteria andPropscodeLessThanOrEqualTo(String value) {
            addCriterion("PROPSCODE <=", value, "propscode");
            return (Criteria) this;
        }

        public Criteria andPropscodeLike(String value) {
            addCriterion("PROPSCODE like", value, "propscode");
            return (Criteria) this;
        }

        public Criteria andPropscodeNotLike(String value) {
            addCriterion("PROPSCODE not like", value, "propscode");
            return (Criteria) this;
        }

        public Criteria andPropscodeIn(List<String> values) {
            addCriterion("PROPSCODE in", values, "propscode");
            return (Criteria) this;
        }

        public Criteria andPropscodeNotIn(List<String> values) {
            addCriterion("PROPSCODE not in", values, "propscode");
            return (Criteria) this;
        }

        public Criteria andPropscodeBetween(String value1, String value2) {
            addCriterion("PROPSCODE between", value1, value2, "propscode");
            return (Criteria) this;
        }

        public Criteria andPropscodeNotBetween(String value1, String value2) {
            addCriterion("PROPSCODE not between", value1, value2, "propscode");
            return (Criteria) this;
        }

        public Criteria andPropscnameIsNull() {
            addCriterion("PROPSCNAME is null");
            return (Criteria) this;
        }

        public Criteria andPropscnameIsNotNull() {
            addCriterion("PROPSCNAME is not null");
            return (Criteria) this;
        }

        public Criteria andPropscnameEqualTo(String value) {
            addCriterion("PROPSCNAME =", value, "propscname");
            return (Criteria) this;
        }

        public Criteria andPropscnameNotEqualTo(String value) {
            addCriterion("PROPSCNAME <>", value, "propscname");
            return (Criteria) this;
        }

        public Criteria andPropscnameGreaterThan(String value) {
            addCriterion("PROPSCNAME >", value, "propscname");
            return (Criteria) this;
        }

        public Criteria andPropscnameGreaterThanOrEqualTo(String value) {
            addCriterion("PROPSCNAME >=", value, "propscname");
            return (Criteria) this;
        }

        public Criteria andPropscnameLessThan(String value) {
            addCriterion("PROPSCNAME <", value, "propscname");
            return (Criteria) this;
        }

        public Criteria andPropscnameLessThanOrEqualTo(String value) {
            addCriterion("PROPSCNAME <=", value, "propscname");
            return (Criteria) this;
        }

        public Criteria andPropscnameLike(String value) {
            addCriterion("PROPSCNAME like", value, "propscname");
            return (Criteria) this;
        }

        public Criteria andPropscnameNotLike(String value) {
            addCriterion("PROPSCNAME not like", value, "propscname");
            return (Criteria) this;
        }

        public Criteria andPropscnameIn(List<String> values) {
            addCriterion("PROPSCNAME in", values, "propscname");
            return (Criteria) this;
        }

        public Criteria andPropscnameNotIn(List<String> values) {
            addCriterion("PROPSCNAME not in", values, "propscname");
            return (Criteria) this;
        }

        public Criteria andPropscnameBetween(String value1, String value2) {
            addCriterion("PROPSCNAME between", value1, value2, "propscname");
            return (Criteria) this;
        }

        public Criteria andPropscnameNotBetween(String value1, String value2) {
            addCriterion("PROPSCNAME not between", value1, value2, "propscname");
            return (Criteria) this;
        }

        public Criteria andPropsenameIsNull() {
            addCriterion("PROPSENAME is null");
            return (Criteria) this;
        }

        public Criteria andPropsenameIsNotNull() {
            addCriterion("PROPSENAME is not null");
            return (Criteria) this;
        }

        public Criteria andPropsenameEqualTo(String value) {
            addCriterion("PROPSENAME =", value, "propsename");
            return (Criteria) this;
        }

        public Criteria andPropsenameNotEqualTo(String value) {
            addCriterion("PROPSENAME <>", value, "propsename");
            return (Criteria) this;
        }

        public Criteria andPropsenameGreaterThan(String value) {
            addCriterion("PROPSENAME >", value, "propsename");
            return (Criteria) this;
        }

        public Criteria andPropsenameGreaterThanOrEqualTo(String value) {
            addCriterion("PROPSENAME >=", value, "propsename");
            return (Criteria) this;
        }

        public Criteria andPropsenameLessThan(String value) {
            addCriterion("PROPSENAME <", value, "propsename");
            return (Criteria) this;
        }

        public Criteria andPropsenameLessThanOrEqualTo(String value) {
            addCriterion("PROPSENAME <=", value, "propsename");
            return (Criteria) this;
        }

        public Criteria andPropsenameLike(String value) {
            addCriterion("PROPSENAME like", value, "propsename");
            return (Criteria) this;
        }

        public Criteria andPropsenameNotLike(String value) {
            addCriterion("PROPSENAME not like", value, "propsename");
            return (Criteria) this;
        }

        public Criteria andPropsenameIn(List<String> values) {
            addCriterion("PROPSENAME in", values, "propsename");
            return (Criteria) this;
        }

        public Criteria andPropsenameNotIn(List<String> values) {
            addCriterion("PROPSENAME not in", values, "propsename");
            return (Criteria) this;
        }

        public Criteria andPropsenameBetween(String value1, String value2) {
            addCriterion("PROPSENAME between", value1, value2, "propsename");
            return (Criteria) this;
        }

        public Criteria andPropsenameNotBetween(String value1, String value2) {
            addCriterion("PROPSENAME not between", value1, value2, "propsename");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}