package cn.com.libertymutual.sp.action.production;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.libertymutual.production.pojo.request.PrpdItemLibraryRequest;
import cn.com.libertymutual.production.pojo.response.Response;
import cn.com.libertymutual.production.service.api.business.ItemBusinessService;

/** 
 * @author Steven.Li
 * @date 2017年7月28日
 *  
 */
@RestController
@RequestMapping("item")
public class ItemController {
	
	@Autowired
	protected ItemBusinessService itemBusinessService;
	
	/**
	 * 责任管理页面查询接口
	 * @param request
	 * @return
	 */
	@RequestMapping("queryItem")
	public Response queryItem(PrpdItemLibraryRequest request) {
		Response result = itemBusinessService.selectItem(request);
		return result;
	}
	
	/**
	 * 查询第一层级责任
	 * @param request
	 * @return
	 */
	@RequestMapping("queryParentItems")
	public Response queryParentItems() {
		Response result = itemBusinessService.selectTopItems();
		return result;
	}
	
	/**
	 * 修改责任信息
	 * @param request
	 * @return
	 */
	@RequestMapping("updateItem")
	public Response updateItem(@RequestBody PrpdItemLibraryRequest request) {
		Response result = new Response();
		try {
			result = itemBusinessService.update(request);
			result.setSuccess();
		} catch (Exception e) {
			result.setAppFail();
			result.setResult(e.getMessage());
		}
		return result;
	}
	
}
