package cn.com.libertymutual.production.model.nomorcldatasource;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class FhxlayerExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public FhxlayerExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andTreatynoIsNull() {
            addCriterion("TREATYNO is null");
            return (Criteria) this;
        }

        public Criteria andTreatynoIsNotNull() {
            addCriterion("TREATYNO is not null");
            return (Criteria) this;
        }

        public Criteria andTreatynoEqualTo(String value) {
            addCriterion("TREATYNO =", value, "treatyno");
            return (Criteria) this;
        }

        public Criteria andTreatynoNotEqualTo(String value) {
            addCriterion("TREATYNO <>", value, "treatyno");
            return (Criteria) this;
        }

        public Criteria andTreatynoGreaterThan(String value) {
            addCriterion("TREATYNO >", value, "treatyno");
            return (Criteria) this;
        }

        public Criteria andTreatynoGreaterThanOrEqualTo(String value) {
            addCriterion("TREATYNO >=", value, "treatyno");
            return (Criteria) this;
        }

        public Criteria andTreatynoLessThan(String value) {
            addCriterion("TREATYNO <", value, "treatyno");
            return (Criteria) this;
        }

        public Criteria andTreatynoLessThanOrEqualTo(String value) {
            addCriterion("TREATYNO <=", value, "treatyno");
            return (Criteria) this;
        }

        public Criteria andTreatynoLike(String value) {
            addCriterion("TREATYNO like", value, "treatyno");
            return (Criteria) this;
        }

        public Criteria andTreatynoNotLike(String value) {
            addCriterion("TREATYNO not like", value, "treatyno");
            return (Criteria) this;
        }

        public Criteria andTreatynoIn(List<String> values) {
            addCriterion("TREATYNO in", values, "treatyno");
            return (Criteria) this;
        }

        public Criteria andTreatynoNotIn(List<String> values) {
            addCriterion("TREATYNO not in", values, "treatyno");
            return (Criteria) this;
        }

        public Criteria andTreatynoBetween(String value1, String value2) {
            addCriterion("TREATYNO between", value1, value2, "treatyno");
            return (Criteria) this;
        }

        public Criteria andTreatynoNotBetween(String value1, String value2) {
            addCriterion("TREATYNO not between", value1, value2, "treatyno");
            return (Criteria) this;
        }

        public Criteria andLayernoIsNull() {
            addCriterion("LAYERNO is null");
            return (Criteria) this;
        }

        public Criteria andLayernoIsNotNull() {
            addCriterion("LAYERNO is not null");
            return (Criteria) this;
        }

        public Criteria andLayernoEqualTo(BigDecimal value) {
            addCriterion("LAYERNO =", value, "layerno");
            return (Criteria) this;
        }

        public Criteria andLayernoNotEqualTo(BigDecimal value) {
            addCriterion("LAYERNO <>", value, "layerno");
            return (Criteria) this;
        }

        public Criteria andLayernoGreaterThan(BigDecimal value) {
            addCriterion("LAYERNO >", value, "layerno");
            return (Criteria) this;
        }

        public Criteria andLayernoGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("LAYERNO >=", value, "layerno");
            return (Criteria) this;
        }

        public Criteria andLayernoLessThan(BigDecimal value) {
            addCriterion("LAYERNO <", value, "layerno");
            return (Criteria) this;
        }

        public Criteria andLayernoLessThanOrEqualTo(BigDecimal value) {
            addCriterion("LAYERNO <=", value, "layerno");
            return (Criteria) this;
        }

        public Criteria andLayernoIn(List<BigDecimal> values) {
            addCriterion("LAYERNO in", values, "layerno");
            return (Criteria) this;
        }

        public Criteria andLayernoNotIn(List<BigDecimal> values) {
            addCriterion("LAYERNO not in", values, "layerno");
            return (Criteria) this;
        }

        public Criteria andLayernoBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("LAYERNO between", value1, value2, "layerno");
            return (Criteria) this;
        }

        public Criteria andLayernoNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("LAYERNO not between", value1, value2, "layerno");
            return (Criteria) this;
        }

        public Criteria andLayertypeIsNull() {
            addCriterion("LAYERTYPE is null");
            return (Criteria) this;
        }

        public Criteria andLayertypeIsNotNull() {
            addCriterion("LAYERTYPE is not null");
            return (Criteria) this;
        }

        public Criteria andLayertypeEqualTo(String value) {
            addCriterion("LAYERTYPE =", value, "layertype");
            return (Criteria) this;
        }

        public Criteria andLayertypeNotEqualTo(String value) {
            addCriterion("LAYERTYPE <>", value, "layertype");
            return (Criteria) this;
        }

        public Criteria andLayertypeGreaterThan(String value) {
            addCriterion("LAYERTYPE >", value, "layertype");
            return (Criteria) this;
        }

        public Criteria andLayertypeGreaterThanOrEqualTo(String value) {
            addCriterion("LAYERTYPE >=", value, "layertype");
            return (Criteria) this;
        }

        public Criteria andLayertypeLessThan(String value) {
            addCriterion("LAYERTYPE <", value, "layertype");
            return (Criteria) this;
        }

        public Criteria andLayertypeLessThanOrEqualTo(String value) {
            addCriterion("LAYERTYPE <=", value, "layertype");
            return (Criteria) this;
        }

        public Criteria andLayertypeLike(String value) {
            addCriterion("LAYERTYPE like", value, "layertype");
            return (Criteria) this;
        }

        public Criteria andLayertypeNotLike(String value) {
            addCriterion("LAYERTYPE not like", value, "layertype");
            return (Criteria) this;
        }

        public Criteria andLayertypeIn(List<String> values) {
            addCriterion("LAYERTYPE in", values, "layertype");
            return (Criteria) this;
        }

        public Criteria andLayertypeNotIn(List<String> values) {
            addCriterion("LAYERTYPE not in", values, "layertype");
            return (Criteria) this;
        }

        public Criteria andLayertypeBetween(String value1, String value2) {
            addCriterion("LAYERTYPE between", value1, value2, "layertype");
            return (Criteria) this;
        }

        public Criteria andLayertypeNotBetween(String value1, String value2) {
            addCriterion("LAYERTYPE not between", value1, value2, "layertype");
            return (Criteria) this;
        }

        public Criteria andLayercdescIsNull() {
            addCriterion("LAYERCDESC is null");
            return (Criteria) this;
        }

        public Criteria andLayercdescIsNotNull() {
            addCriterion("LAYERCDESC is not null");
            return (Criteria) this;
        }

        public Criteria andLayercdescEqualTo(String value) {
            addCriterion("LAYERCDESC =", value, "layercdesc");
            return (Criteria) this;
        }

        public Criteria andLayercdescNotEqualTo(String value) {
            addCriterion("LAYERCDESC <>", value, "layercdesc");
            return (Criteria) this;
        }

        public Criteria andLayercdescGreaterThan(String value) {
            addCriterion("LAYERCDESC >", value, "layercdesc");
            return (Criteria) this;
        }

        public Criteria andLayercdescGreaterThanOrEqualTo(String value) {
            addCriterion("LAYERCDESC >=", value, "layercdesc");
            return (Criteria) this;
        }

        public Criteria andLayercdescLessThan(String value) {
            addCriterion("LAYERCDESC <", value, "layercdesc");
            return (Criteria) this;
        }

        public Criteria andLayercdescLessThanOrEqualTo(String value) {
            addCriterion("LAYERCDESC <=", value, "layercdesc");
            return (Criteria) this;
        }

        public Criteria andLayercdescLike(String value) {
            addCriterion("LAYERCDESC like", value, "layercdesc");
            return (Criteria) this;
        }

        public Criteria andLayercdescNotLike(String value) {
            addCriterion("LAYERCDESC not like", value, "layercdesc");
            return (Criteria) this;
        }

        public Criteria andLayercdescIn(List<String> values) {
            addCriterion("LAYERCDESC in", values, "layercdesc");
            return (Criteria) this;
        }

        public Criteria andLayercdescNotIn(List<String> values) {
            addCriterion("LAYERCDESC not in", values, "layercdesc");
            return (Criteria) this;
        }

        public Criteria andLayercdescBetween(String value1, String value2) {
            addCriterion("LAYERCDESC between", value1, value2, "layercdesc");
            return (Criteria) this;
        }

        public Criteria andLayercdescNotBetween(String value1, String value2) {
            addCriterion("LAYERCDESC not between", value1, value2, "layercdesc");
            return (Criteria) this;
        }

        public Criteria andLayeredescIsNull() {
            addCriterion("LAYEREDESC is null");
            return (Criteria) this;
        }

        public Criteria andLayeredescIsNotNull() {
            addCriterion("LAYEREDESC is not null");
            return (Criteria) this;
        }

        public Criteria andLayeredescEqualTo(String value) {
            addCriterion("LAYEREDESC =", value, "layeredesc");
            return (Criteria) this;
        }

        public Criteria andLayeredescNotEqualTo(String value) {
            addCriterion("LAYEREDESC <>", value, "layeredesc");
            return (Criteria) this;
        }

        public Criteria andLayeredescGreaterThan(String value) {
            addCriterion("LAYEREDESC >", value, "layeredesc");
            return (Criteria) this;
        }

        public Criteria andLayeredescGreaterThanOrEqualTo(String value) {
            addCriterion("LAYEREDESC >=", value, "layeredesc");
            return (Criteria) this;
        }

        public Criteria andLayeredescLessThan(String value) {
            addCriterion("LAYEREDESC <", value, "layeredesc");
            return (Criteria) this;
        }

        public Criteria andLayeredescLessThanOrEqualTo(String value) {
            addCriterion("LAYEREDESC <=", value, "layeredesc");
            return (Criteria) this;
        }

        public Criteria andLayeredescLike(String value) {
            addCriterion("LAYEREDESC like", value, "layeredesc");
            return (Criteria) this;
        }

        public Criteria andLayeredescNotLike(String value) {
            addCriterion("LAYEREDESC not like", value, "layeredesc");
            return (Criteria) this;
        }

        public Criteria andLayeredescIn(List<String> values) {
            addCriterion("LAYEREDESC in", values, "layeredesc");
            return (Criteria) this;
        }

        public Criteria andLayeredescNotIn(List<String> values) {
            addCriterion("LAYEREDESC not in", values, "layeredesc");
            return (Criteria) this;
        }

        public Criteria andLayeredescBetween(String value1, String value2) {
            addCriterion("LAYEREDESC between", value1, value2, "layeredesc");
            return (Criteria) this;
        }

        public Criteria andLayeredescNotBetween(String value1, String value2) {
            addCriterion("LAYEREDESC not between", value1, value2, "layeredesc");
            return (Criteria) this;
        }

        public Criteria andCurrencyIsNull() {
            addCriterion("CURRENCY is null");
            return (Criteria) this;
        }

        public Criteria andCurrencyIsNotNull() {
            addCriterion("CURRENCY is not null");
            return (Criteria) this;
        }

        public Criteria andCurrencyEqualTo(String value) {
            addCriterion("CURRENCY =", value, "currency");
            return (Criteria) this;
        }

        public Criteria andCurrencyNotEqualTo(String value) {
            addCriterion("CURRENCY <>", value, "currency");
            return (Criteria) this;
        }

        public Criteria andCurrencyGreaterThan(String value) {
            addCriterion("CURRENCY >", value, "currency");
            return (Criteria) this;
        }

        public Criteria andCurrencyGreaterThanOrEqualTo(String value) {
            addCriterion("CURRENCY >=", value, "currency");
            return (Criteria) this;
        }

        public Criteria andCurrencyLessThan(String value) {
            addCriterion("CURRENCY <", value, "currency");
            return (Criteria) this;
        }

        public Criteria andCurrencyLessThanOrEqualTo(String value) {
            addCriterion("CURRENCY <=", value, "currency");
            return (Criteria) this;
        }

        public Criteria andCurrencyLike(String value) {
            addCriterion("CURRENCY like", value, "currency");
            return (Criteria) this;
        }

        public Criteria andCurrencyNotLike(String value) {
            addCriterion("CURRENCY not like", value, "currency");
            return (Criteria) this;
        }

        public Criteria andCurrencyIn(List<String> values) {
            addCriterion("CURRENCY in", values, "currency");
            return (Criteria) this;
        }

        public Criteria andCurrencyNotIn(List<String> values) {
            addCriterion("CURRENCY not in", values, "currency");
            return (Criteria) this;
        }

        public Criteria andCurrencyBetween(String value1, String value2) {
            addCriterion("CURRENCY between", value1, value2, "currency");
            return (Criteria) this;
        }

        public Criteria andCurrencyNotBetween(String value1, String value2) {
            addCriterion("CURRENCY not between", value1, value2, "currency");
            return (Criteria) this;
        }

        public Criteria andExcesslossIsNull() {
            addCriterion("EXCESSLOSS is null");
            return (Criteria) this;
        }

        public Criteria andExcesslossIsNotNull() {
            addCriterion("EXCESSLOSS is not null");
            return (Criteria) this;
        }

        public Criteria andExcesslossEqualTo(BigDecimal value) {
            addCriterion("EXCESSLOSS =", value, "excessloss");
            return (Criteria) this;
        }

        public Criteria andExcesslossNotEqualTo(BigDecimal value) {
            addCriterion("EXCESSLOSS <>", value, "excessloss");
            return (Criteria) this;
        }

        public Criteria andExcesslossGreaterThan(BigDecimal value) {
            addCriterion("EXCESSLOSS >", value, "excessloss");
            return (Criteria) this;
        }

        public Criteria andExcesslossGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("EXCESSLOSS >=", value, "excessloss");
            return (Criteria) this;
        }

        public Criteria andExcesslossLessThan(BigDecimal value) {
            addCriterion("EXCESSLOSS <", value, "excessloss");
            return (Criteria) this;
        }

        public Criteria andExcesslossLessThanOrEqualTo(BigDecimal value) {
            addCriterion("EXCESSLOSS <=", value, "excessloss");
            return (Criteria) this;
        }

        public Criteria andExcesslossIn(List<BigDecimal> values) {
            addCriterion("EXCESSLOSS in", values, "excessloss");
            return (Criteria) this;
        }

        public Criteria andExcesslossNotIn(List<BigDecimal> values) {
            addCriterion("EXCESSLOSS not in", values, "excessloss");
            return (Criteria) this;
        }

        public Criteria andExcesslossBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("EXCESSLOSS between", value1, value2, "excessloss");
            return (Criteria) this;
        }

        public Criteria andExcesslossNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("EXCESSLOSS not between", value1, value2, "excessloss");
            return (Criteria) this;
        }

        public Criteria andLayerquotaIsNull() {
            addCriterion("LAYERQUOTA is null");
            return (Criteria) this;
        }

        public Criteria andLayerquotaIsNotNull() {
            addCriterion("LAYERQUOTA is not null");
            return (Criteria) this;
        }

        public Criteria andLayerquotaEqualTo(BigDecimal value) {
            addCriterion("LAYERQUOTA =", value, "layerquota");
            return (Criteria) this;
        }

        public Criteria andLayerquotaNotEqualTo(BigDecimal value) {
            addCriterion("LAYERQUOTA <>", value, "layerquota");
            return (Criteria) this;
        }

        public Criteria andLayerquotaGreaterThan(BigDecimal value) {
            addCriterion("LAYERQUOTA >", value, "layerquota");
            return (Criteria) this;
        }

        public Criteria andLayerquotaGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("LAYERQUOTA >=", value, "layerquota");
            return (Criteria) this;
        }

        public Criteria andLayerquotaLessThan(BigDecimal value) {
            addCriterion("LAYERQUOTA <", value, "layerquota");
            return (Criteria) this;
        }

        public Criteria andLayerquotaLessThanOrEqualTo(BigDecimal value) {
            addCriterion("LAYERQUOTA <=", value, "layerquota");
            return (Criteria) this;
        }

        public Criteria andLayerquotaIn(List<BigDecimal> values) {
            addCriterion("LAYERQUOTA in", values, "layerquota");
            return (Criteria) this;
        }

        public Criteria andLayerquotaNotIn(List<BigDecimal> values) {
            addCriterion("LAYERQUOTA not in", values, "layerquota");
            return (Criteria) this;
        }

        public Criteria andLayerquotaBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("LAYERQUOTA between", value1, value2, "layerquota");
            return (Criteria) this;
        }

        public Criteria andLayerquotaNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("LAYERQUOTA not between", value1, value2, "layerquota");
            return (Criteria) this;
        }

        public Criteria andTotalquotaIsNull() {
            addCriterion("TOTALQUOTA is null");
            return (Criteria) this;
        }

        public Criteria andTotalquotaIsNotNull() {
            addCriterion("TOTALQUOTA is not null");
            return (Criteria) this;
        }

        public Criteria andTotalquotaEqualTo(BigDecimal value) {
            addCriterion("TOTALQUOTA =", value, "totalquota");
            return (Criteria) this;
        }

        public Criteria andTotalquotaNotEqualTo(BigDecimal value) {
            addCriterion("TOTALQUOTA <>", value, "totalquota");
            return (Criteria) this;
        }

        public Criteria andTotalquotaGreaterThan(BigDecimal value) {
            addCriterion("TOTALQUOTA >", value, "totalquota");
            return (Criteria) this;
        }

        public Criteria andTotalquotaGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("TOTALQUOTA >=", value, "totalquota");
            return (Criteria) this;
        }

        public Criteria andTotalquotaLessThan(BigDecimal value) {
            addCriterion("TOTALQUOTA <", value, "totalquota");
            return (Criteria) this;
        }

        public Criteria andTotalquotaLessThanOrEqualTo(BigDecimal value) {
            addCriterion("TOTALQUOTA <=", value, "totalquota");
            return (Criteria) this;
        }

        public Criteria andTotalquotaIn(List<BigDecimal> values) {
            addCriterion("TOTALQUOTA in", values, "totalquota");
            return (Criteria) this;
        }

        public Criteria andTotalquotaNotIn(List<BigDecimal> values) {
            addCriterion("TOTALQUOTA not in", values, "totalquota");
            return (Criteria) this;
        }

        public Criteria andTotalquotaBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("TOTALQUOTA between", value1, value2, "totalquota");
            return (Criteria) this;
        }

        public Criteria andTotalquotaNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("TOTALQUOTA not between", value1, value2, "totalquota");
            return (Criteria) this;
        }

        public Criteria andEgnpiIsNull() {
            addCriterion("EGNPI is null");
            return (Criteria) this;
        }

        public Criteria andEgnpiIsNotNull() {
            addCriterion("EGNPI is not null");
            return (Criteria) this;
        }

        public Criteria andEgnpiEqualTo(BigDecimal value) {
            addCriterion("EGNPI =", value, "egnpi");
            return (Criteria) this;
        }

        public Criteria andEgnpiNotEqualTo(BigDecimal value) {
            addCriterion("EGNPI <>", value, "egnpi");
            return (Criteria) this;
        }

        public Criteria andEgnpiGreaterThan(BigDecimal value) {
            addCriterion("EGNPI >", value, "egnpi");
            return (Criteria) this;
        }

        public Criteria andEgnpiGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("EGNPI >=", value, "egnpi");
            return (Criteria) this;
        }

        public Criteria andEgnpiLessThan(BigDecimal value) {
            addCriterion("EGNPI <", value, "egnpi");
            return (Criteria) this;
        }

        public Criteria andEgnpiLessThanOrEqualTo(BigDecimal value) {
            addCriterion("EGNPI <=", value, "egnpi");
            return (Criteria) this;
        }

        public Criteria andEgnpiIn(List<BigDecimal> values) {
            addCriterion("EGNPI in", values, "egnpi");
            return (Criteria) this;
        }

        public Criteria andEgnpiNotIn(List<BigDecimal> values) {
            addCriterion("EGNPI not in", values, "egnpi");
            return (Criteria) this;
        }

        public Criteria andEgnpiBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("EGNPI between", value1, value2, "egnpi");
            return (Criteria) this;
        }

        public Criteria andEgnpiNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("EGNPI not between", value1, value2, "egnpi");
            return (Criteria) this;
        }

        public Criteria andGnpiIsNull() {
            addCriterion("GNPI is null");
            return (Criteria) this;
        }

        public Criteria andGnpiIsNotNull() {
            addCriterion("GNPI is not null");
            return (Criteria) this;
        }

        public Criteria andGnpiEqualTo(BigDecimal value) {
            addCriterion("GNPI =", value, "gnpi");
            return (Criteria) this;
        }

        public Criteria andGnpiNotEqualTo(BigDecimal value) {
            addCriterion("GNPI <>", value, "gnpi");
            return (Criteria) this;
        }

        public Criteria andGnpiGreaterThan(BigDecimal value) {
            addCriterion("GNPI >", value, "gnpi");
            return (Criteria) this;
        }

        public Criteria andGnpiGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("GNPI >=", value, "gnpi");
            return (Criteria) this;
        }

        public Criteria andGnpiLessThan(BigDecimal value) {
            addCriterion("GNPI <", value, "gnpi");
            return (Criteria) this;
        }

        public Criteria andGnpiLessThanOrEqualTo(BigDecimal value) {
            addCriterion("GNPI <=", value, "gnpi");
            return (Criteria) this;
        }

        public Criteria andGnpiIn(List<BigDecimal> values) {
            addCriterion("GNPI in", values, "gnpi");
            return (Criteria) this;
        }

        public Criteria andGnpiNotIn(List<BigDecimal> values) {
            addCriterion("GNPI not in", values, "gnpi");
            return (Criteria) this;
        }

        public Criteria andGnpiBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("GNPI between", value1, value2, "gnpi");
            return (Criteria) this;
        }

        public Criteria andGnpiNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("GNPI not between", value1, value2, "gnpi");
            return (Criteria) this;
        }

        public Criteria andRateIsNull() {
            addCriterion("RATE is null");
            return (Criteria) this;
        }

        public Criteria andRateIsNotNull() {
            addCriterion("RATE is not null");
            return (Criteria) this;
        }

        public Criteria andRateEqualTo(BigDecimal value) {
            addCriterion("RATE =", value, "rate");
            return (Criteria) this;
        }

        public Criteria andRateNotEqualTo(BigDecimal value) {
            addCriterion("RATE <>", value, "rate");
            return (Criteria) this;
        }

        public Criteria andRateGreaterThan(BigDecimal value) {
            addCriterion("RATE >", value, "rate");
            return (Criteria) this;
        }

        public Criteria andRateGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("RATE >=", value, "rate");
            return (Criteria) this;
        }

        public Criteria andRateLessThan(BigDecimal value) {
            addCriterion("RATE <", value, "rate");
            return (Criteria) this;
        }

        public Criteria andRateLessThanOrEqualTo(BigDecimal value) {
            addCriterion("RATE <=", value, "rate");
            return (Criteria) this;
        }

        public Criteria andRateIn(List<BigDecimal> values) {
            addCriterion("RATE in", values, "rate");
            return (Criteria) this;
        }

        public Criteria andRateNotIn(List<BigDecimal> values) {
            addCriterion("RATE not in", values, "rate");
            return (Criteria) this;
        }

        public Criteria andRateBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("RATE between", value1, value2, "rate");
            return (Criteria) this;
        }

        public Criteria andRateNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("RATE not between", value1, value2, "rate");
            return (Criteria) this;
        }

        public Criteria andRolIsNull() {
            addCriterion("ROL is null");
            return (Criteria) this;
        }

        public Criteria andRolIsNotNull() {
            addCriterion("ROL is not null");
            return (Criteria) this;
        }

        public Criteria andRolEqualTo(BigDecimal value) {
            addCriterion("ROL =", value, "rol");
            return (Criteria) this;
        }

        public Criteria andRolNotEqualTo(BigDecimal value) {
            addCriterion("ROL <>", value, "rol");
            return (Criteria) this;
        }

        public Criteria andRolGreaterThan(BigDecimal value) {
            addCriterion("ROL >", value, "rol");
            return (Criteria) this;
        }

        public Criteria andRolGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("ROL >=", value, "rol");
            return (Criteria) this;
        }

        public Criteria andRolLessThan(BigDecimal value) {
            addCriterion("ROL <", value, "rol");
            return (Criteria) this;
        }

        public Criteria andRolLessThanOrEqualTo(BigDecimal value) {
            addCriterion("ROL <=", value, "rol");
            return (Criteria) this;
        }

        public Criteria andRolIn(List<BigDecimal> values) {
            addCriterion("ROL in", values, "rol");
            return (Criteria) this;
        }

        public Criteria andRolNotIn(List<BigDecimal> values) {
            addCriterion("ROL not in", values, "rol");
            return (Criteria) this;
        }

        public Criteria andRolBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("ROL between", value1, value2, "rol");
            return (Criteria) this;
        }

        public Criteria andRolNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("ROL not between", value1, value2, "rol");
            return (Criteria) this;
        }

        public Criteria andMdprateIsNull() {
            addCriterion("MDPRATE is null");
            return (Criteria) this;
        }

        public Criteria andMdprateIsNotNull() {
            addCriterion("MDPRATE is not null");
            return (Criteria) this;
        }

        public Criteria andMdprateEqualTo(BigDecimal value) {
            addCriterion("MDPRATE =", value, "mdprate");
            return (Criteria) this;
        }

        public Criteria andMdprateNotEqualTo(BigDecimal value) {
            addCriterion("MDPRATE <>", value, "mdprate");
            return (Criteria) this;
        }

        public Criteria andMdprateGreaterThan(BigDecimal value) {
            addCriterion("MDPRATE >", value, "mdprate");
            return (Criteria) this;
        }

        public Criteria andMdprateGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("MDPRATE >=", value, "mdprate");
            return (Criteria) this;
        }

        public Criteria andMdprateLessThan(BigDecimal value) {
            addCriterion("MDPRATE <", value, "mdprate");
            return (Criteria) this;
        }

        public Criteria andMdprateLessThanOrEqualTo(BigDecimal value) {
            addCriterion("MDPRATE <=", value, "mdprate");
            return (Criteria) this;
        }

        public Criteria andMdprateIn(List<BigDecimal> values) {
            addCriterion("MDPRATE in", values, "mdprate");
            return (Criteria) this;
        }

        public Criteria andMdprateNotIn(List<BigDecimal> values) {
            addCriterion("MDPRATE not in", values, "mdprate");
            return (Criteria) this;
        }

        public Criteria andMdprateBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("MDPRATE between", value1, value2, "mdprate");
            return (Criteria) this;
        }

        public Criteria andMdprateNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("MDPRATE not between", value1, value2, "mdprate");
            return (Criteria) this;
        }

        public Criteria andMdpIsNull() {
            addCriterion("MDP is null");
            return (Criteria) this;
        }

        public Criteria andMdpIsNotNull() {
            addCriterion("MDP is not null");
            return (Criteria) this;
        }

        public Criteria andMdpEqualTo(BigDecimal value) {
            addCriterion("MDP =", value, "mdp");
            return (Criteria) this;
        }

        public Criteria andMdpNotEqualTo(BigDecimal value) {
            addCriterion("MDP <>", value, "mdp");
            return (Criteria) this;
        }

        public Criteria andMdpGreaterThan(BigDecimal value) {
            addCriterion("MDP >", value, "mdp");
            return (Criteria) this;
        }

        public Criteria andMdpGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("MDP >=", value, "mdp");
            return (Criteria) this;
        }

        public Criteria andMdpLessThan(BigDecimal value) {
            addCriterion("MDP <", value, "mdp");
            return (Criteria) this;
        }

        public Criteria andMdpLessThanOrEqualTo(BigDecimal value) {
            addCriterion("MDP <=", value, "mdp");
            return (Criteria) this;
        }

        public Criteria andMdpIn(List<BigDecimal> values) {
            addCriterion("MDP in", values, "mdp");
            return (Criteria) this;
        }

        public Criteria andMdpNotIn(List<BigDecimal> values) {
            addCriterion("MDP not in", values, "mdp");
            return (Criteria) this;
        }

        public Criteria andMdpBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("MDP between", value1, value2, "mdp");
            return (Criteria) this;
        }

        public Criteria andMdpNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("MDP not between", value1, value2, "mdp");
            return (Criteria) this;
        }

        public Criteria andLayerpremiumIsNull() {
            addCriterion("LAYERPREMIUM is null");
            return (Criteria) this;
        }

        public Criteria andLayerpremiumIsNotNull() {
            addCriterion("LAYERPREMIUM is not null");
            return (Criteria) this;
        }

        public Criteria andLayerpremiumEqualTo(BigDecimal value) {
            addCriterion("LAYERPREMIUM =", value, "layerpremium");
            return (Criteria) this;
        }

        public Criteria andLayerpremiumNotEqualTo(BigDecimal value) {
            addCriterion("LAYERPREMIUM <>", value, "layerpremium");
            return (Criteria) this;
        }

        public Criteria andLayerpremiumGreaterThan(BigDecimal value) {
            addCriterion("LAYERPREMIUM >", value, "layerpremium");
            return (Criteria) this;
        }

        public Criteria andLayerpremiumGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("LAYERPREMIUM >=", value, "layerpremium");
            return (Criteria) this;
        }

        public Criteria andLayerpremiumLessThan(BigDecimal value) {
            addCriterion("LAYERPREMIUM <", value, "layerpremium");
            return (Criteria) this;
        }

        public Criteria andLayerpremiumLessThanOrEqualTo(BigDecimal value) {
            addCriterion("LAYERPREMIUM <=", value, "layerpremium");
            return (Criteria) this;
        }

        public Criteria andLayerpremiumIn(List<BigDecimal> values) {
            addCriterion("LAYERPREMIUM in", values, "layerpremium");
            return (Criteria) this;
        }

        public Criteria andLayerpremiumNotIn(List<BigDecimal> values) {
            addCriterion("LAYERPREMIUM not in", values, "layerpremium");
            return (Criteria) this;
        }

        public Criteria andLayerpremiumBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("LAYERPREMIUM between", value1, value2, "layerpremium");
            return (Criteria) this;
        }

        public Criteria andLayerpremiumNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("LAYERPREMIUM not between", value1, value2, "layerpremium");
            return (Criteria) this;
        }

        public Criteria andSharerateIsNull() {
            addCriterion("SHARERATE is null");
            return (Criteria) this;
        }

        public Criteria andSharerateIsNotNull() {
            addCriterion("SHARERATE is not null");
            return (Criteria) this;
        }

        public Criteria andSharerateEqualTo(BigDecimal value) {
            addCriterion("SHARERATE =", value, "sharerate");
            return (Criteria) this;
        }

        public Criteria andSharerateNotEqualTo(BigDecimal value) {
            addCriterion("SHARERATE <>", value, "sharerate");
            return (Criteria) this;
        }

        public Criteria andSharerateGreaterThan(BigDecimal value) {
            addCriterion("SHARERATE >", value, "sharerate");
            return (Criteria) this;
        }

        public Criteria andSharerateGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("SHARERATE >=", value, "sharerate");
            return (Criteria) this;
        }

        public Criteria andSharerateLessThan(BigDecimal value) {
            addCriterion("SHARERATE <", value, "sharerate");
            return (Criteria) this;
        }

        public Criteria andSharerateLessThanOrEqualTo(BigDecimal value) {
            addCriterion("SHARERATE <=", value, "sharerate");
            return (Criteria) this;
        }

        public Criteria andSharerateIn(List<BigDecimal> values) {
            addCriterion("SHARERATE in", values, "sharerate");
            return (Criteria) this;
        }

        public Criteria andSharerateNotIn(List<BigDecimal> values) {
            addCriterion("SHARERATE not in", values, "sharerate");
            return (Criteria) this;
        }

        public Criteria andSharerateBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("SHARERATE between", value1, value2, "sharerate");
            return (Criteria) this;
        }

        public Criteria andSharerateNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("SHARERATE not between", value1, value2, "sharerate");
            return (Criteria) this;
        }

        public Criteria andReinsttimesIsNull() {
            addCriterion("REINSTTIMES is null");
            return (Criteria) this;
        }

        public Criteria andReinsttimesIsNotNull() {
            addCriterion("REINSTTIMES is not null");
            return (Criteria) this;
        }

        public Criteria andReinsttimesEqualTo(BigDecimal value) {
            addCriterion("REINSTTIMES =", value, "reinsttimes");
            return (Criteria) this;
        }

        public Criteria andReinsttimesNotEqualTo(BigDecimal value) {
            addCriterion("REINSTTIMES <>", value, "reinsttimes");
            return (Criteria) this;
        }

        public Criteria andReinsttimesGreaterThan(BigDecimal value) {
            addCriterion("REINSTTIMES >", value, "reinsttimes");
            return (Criteria) this;
        }

        public Criteria andReinsttimesGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("REINSTTIMES >=", value, "reinsttimes");
            return (Criteria) this;
        }

        public Criteria andReinsttimesLessThan(BigDecimal value) {
            addCriterion("REINSTTIMES <", value, "reinsttimes");
            return (Criteria) this;
        }

        public Criteria andReinsttimesLessThanOrEqualTo(BigDecimal value) {
            addCriterion("REINSTTIMES <=", value, "reinsttimes");
            return (Criteria) this;
        }

        public Criteria andReinsttimesIn(List<BigDecimal> values) {
            addCriterion("REINSTTIMES in", values, "reinsttimes");
            return (Criteria) this;
        }

        public Criteria andReinsttimesNotIn(List<BigDecimal> values) {
            addCriterion("REINSTTIMES not in", values, "reinsttimes");
            return (Criteria) this;
        }

        public Criteria andReinsttimesBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("REINSTTIMES between", value1, value2, "reinsttimes");
            return (Criteria) this;
        }

        public Criteria andReinsttimesNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("REINSTTIMES not between", value1, value2, "reinsttimes");
            return (Criteria) this;
        }

        public Criteria andReinstrateIsNull() {
            addCriterion("REINSTRATE is null");
            return (Criteria) this;
        }

        public Criteria andReinstrateIsNotNull() {
            addCriterion("REINSTRATE is not null");
            return (Criteria) this;
        }

        public Criteria andReinstrateEqualTo(BigDecimal value) {
            addCriterion("REINSTRATE =", value, "reinstrate");
            return (Criteria) this;
        }

        public Criteria andReinstrateNotEqualTo(BigDecimal value) {
            addCriterion("REINSTRATE <>", value, "reinstrate");
            return (Criteria) this;
        }

        public Criteria andReinstrateGreaterThan(BigDecimal value) {
            addCriterion("REINSTRATE >", value, "reinstrate");
            return (Criteria) this;
        }

        public Criteria andReinstrateGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("REINSTRATE >=", value, "reinstrate");
            return (Criteria) this;
        }

        public Criteria andReinstrateLessThan(BigDecimal value) {
            addCriterion("REINSTRATE <", value, "reinstrate");
            return (Criteria) this;
        }

        public Criteria andReinstrateLessThanOrEqualTo(BigDecimal value) {
            addCriterion("REINSTRATE <=", value, "reinstrate");
            return (Criteria) this;
        }

        public Criteria andReinstrateIn(List<BigDecimal> values) {
            addCriterion("REINSTRATE in", values, "reinstrate");
            return (Criteria) this;
        }

        public Criteria andReinstrateNotIn(List<BigDecimal> values) {
            addCriterion("REINSTRATE not in", values, "reinstrate");
            return (Criteria) this;
        }

        public Criteria andReinstrateBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("REINSTRATE between", value1, value2, "reinstrate");
            return (Criteria) this;
        }

        public Criteria andReinstrateNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("REINSTRATE not between", value1, value2, "reinstrate");
            return (Criteria) this;
        }

        public Criteria andResidualreinstsumIsNull() {
            addCriterion("RESIDUALREINSTSUM is null");
            return (Criteria) this;
        }

        public Criteria andResidualreinstsumIsNotNull() {
            addCriterion("RESIDUALREINSTSUM is not null");
            return (Criteria) this;
        }

        public Criteria andResidualreinstsumEqualTo(BigDecimal value) {
            addCriterion("RESIDUALREINSTSUM =", value, "residualreinstsum");
            return (Criteria) this;
        }

        public Criteria andResidualreinstsumNotEqualTo(BigDecimal value) {
            addCriterion("RESIDUALREINSTSUM <>", value, "residualreinstsum");
            return (Criteria) this;
        }

        public Criteria andResidualreinstsumGreaterThan(BigDecimal value) {
            addCriterion("RESIDUALREINSTSUM >", value, "residualreinstsum");
            return (Criteria) this;
        }

        public Criteria andResidualreinstsumGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("RESIDUALREINSTSUM >=", value, "residualreinstsum");
            return (Criteria) this;
        }

        public Criteria andResidualreinstsumLessThan(BigDecimal value) {
            addCriterion("RESIDUALREINSTSUM <", value, "residualreinstsum");
            return (Criteria) this;
        }

        public Criteria andResidualreinstsumLessThanOrEqualTo(BigDecimal value) {
            addCriterion("RESIDUALREINSTSUM <=", value, "residualreinstsum");
            return (Criteria) this;
        }

        public Criteria andResidualreinstsumIn(List<BigDecimal> values) {
            addCriterion("RESIDUALREINSTSUM in", values, "residualreinstsum");
            return (Criteria) this;
        }

        public Criteria andResidualreinstsumNotIn(List<BigDecimal> values) {
            addCriterion("RESIDUALREINSTSUM not in", values, "residualreinstsum");
            return (Criteria) this;
        }

        public Criteria andResidualreinstsumBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("RESIDUALREINSTSUM between", value1, value2, "residualreinstsum");
            return (Criteria) this;
        }

        public Criteria andResidualreinstsumNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("RESIDUALREINSTSUM not between", value1, value2, "residualreinstsum");
            return (Criteria) this;
        }

        public Criteria andReinsttypeIsNull() {
            addCriterion("REINSTTYPE is null");
            return (Criteria) this;
        }

        public Criteria andReinsttypeIsNotNull() {
            addCriterion("REINSTTYPE is not null");
            return (Criteria) this;
        }

        public Criteria andReinsttypeEqualTo(String value) {
            addCriterion("REINSTTYPE =", value, "reinsttype");
            return (Criteria) this;
        }

        public Criteria andReinsttypeNotEqualTo(String value) {
            addCriterion("REINSTTYPE <>", value, "reinsttype");
            return (Criteria) this;
        }

        public Criteria andReinsttypeGreaterThan(String value) {
            addCriterion("REINSTTYPE >", value, "reinsttype");
            return (Criteria) this;
        }

        public Criteria andReinsttypeGreaterThanOrEqualTo(String value) {
            addCriterion("REINSTTYPE >=", value, "reinsttype");
            return (Criteria) this;
        }

        public Criteria andReinsttypeLessThan(String value) {
            addCriterion("REINSTTYPE <", value, "reinsttype");
            return (Criteria) this;
        }

        public Criteria andReinsttypeLessThanOrEqualTo(String value) {
            addCriterion("REINSTTYPE <=", value, "reinsttype");
            return (Criteria) this;
        }

        public Criteria andReinsttypeLike(String value) {
            addCriterion("REINSTTYPE like", value, "reinsttype");
            return (Criteria) this;
        }

        public Criteria andReinsttypeNotLike(String value) {
            addCriterion("REINSTTYPE not like", value, "reinsttype");
            return (Criteria) this;
        }

        public Criteria andReinsttypeIn(List<String> values) {
            addCriterion("REINSTTYPE in", values, "reinsttype");
            return (Criteria) this;
        }

        public Criteria andReinsttypeNotIn(List<String> values) {
            addCriterion("REINSTTYPE not in", values, "reinsttype");
            return (Criteria) this;
        }

        public Criteria andReinsttypeBetween(String value1, String value2) {
            addCriterion("REINSTTYPE between", value1, value2, "reinsttype");
            return (Criteria) this;
        }

        public Criteria andReinsttypeNotBetween(String value1, String value2) {
            addCriterion("REINSTTYPE not between", value1, value2, "reinsttype");
            return (Criteria) this;
        }

        public Criteria andRemarksIsNull() {
            addCriterion("REMARKS is null");
            return (Criteria) this;
        }

        public Criteria andRemarksIsNotNull() {
            addCriterion("REMARKS is not null");
            return (Criteria) this;
        }

        public Criteria andRemarksEqualTo(String value) {
            addCriterion("REMARKS =", value, "remarks");
            return (Criteria) this;
        }

        public Criteria andRemarksNotEqualTo(String value) {
            addCriterion("REMARKS <>", value, "remarks");
            return (Criteria) this;
        }

        public Criteria andRemarksGreaterThan(String value) {
            addCriterion("REMARKS >", value, "remarks");
            return (Criteria) this;
        }

        public Criteria andRemarksGreaterThanOrEqualTo(String value) {
            addCriterion("REMARKS >=", value, "remarks");
            return (Criteria) this;
        }

        public Criteria andRemarksLessThan(String value) {
            addCriterion("REMARKS <", value, "remarks");
            return (Criteria) this;
        }

        public Criteria andRemarksLessThanOrEqualTo(String value) {
            addCriterion("REMARKS <=", value, "remarks");
            return (Criteria) this;
        }

        public Criteria andRemarksLike(String value) {
            addCriterion("REMARKS like", value, "remarks");
            return (Criteria) this;
        }

        public Criteria andRemarksNotLike(String value) {
            addCriterion("REMARKS not like", value, "remarks");
            return (Criteria) this;
        }

        public Criteria andRemarksIn(List<String> values) {
            addCriterion("REMARKS in", values, "remarks");
            return (Criteria) this;
        }

        public Criteria andRemarksNotIn(List<String> values) {
            addCriterion("REMARKS not in", values, "remarks");
            return (Criteria) this;
        }

        public Criteria andRemarksBetween(String value1, String value2) {
            addCriterion("REMARKS between", value1, value2, "remarks");
            return (Criteria) this;
        }

        public Criteria andRemarksNotBetween(String value1, String value2) {
            addCriterion("REMARKS not between", value1, value2, "remarks");
            return (Criteria) this;
        }

        public Criteria andUpdatercodeIsNull() {
            addCriterion("UPDATERCODE is null");
            return (Criteria) this;
        }

        public Criteria andUpdatercodeIsNotNull() {
            addCriterion("UPDATERCODE is not null");
            return (Criteria) this;
        }

        public Criteria andUpdatercodeEqualTo(String value) {
            addCriterion("UPDATERCODE =", value, "updatercode");
            return (Criteria) this;
        }

        public Criteria andUpdatercodeNotEqualTo(String value) {
            addCriterion("UPDATERCODE <>", value, "updatercode");
            return (Criteria) this;
        }

        public Criteria andUpdatercodeGreaterThan(String value) {
            addCriterion("UPDATERCODE >", value, "updatercode");
            return (Criteria) this;
        }

        public Criteria andUpdatercodeGreaterThanOrEqualTo(String value) {
            addCriterion("UPDATERCODE >=", value, "updatercode");
            return (Criteria) this;
        }

        public Criteria andUpdatercodeLessThan(String value) {
            addCriterion("UPDATERCODE <", value, "updatercode");
            return (Criteria) this;
        }

        public Criteria andUpdatercodeLessThanOrEqualTo(String value) {
            addCriterion("UPDATERCODE <=", value, "updatercode");
            return (Criteria) this;
        }

        public Criteria andUpdatercodeLike(String value) {
            addCriterion("UPDATERCODE like", value, "updatercode");
            return (Criteria) this;
        }

        public Criteria andUpdatercodeNotLike(String value) {
            addCriterion("UPDATERCODE not like", value, "updatercode");
            return (Criteria) this;
        }

        public Criteria andUpdatercodeIn(List<String> values) {
            addCriterion("UPDATERCODE in", values, "updatercode");
            return (Criteria) this;
        }

        public Criteria andUpdatercodeNotIn(List<String> values) {
            addCriterion("UPDATERCODE not in", values, "updatercode");
            return (Criteria) this;
        }

        public Criteria andUpdatercodeBetween(String value1, String value2) {
            addCriterion("UPDATERCODE between", value1, value2, "updatercode");
            return (Criteria) this;
        }

        public Criteria andUpdatercodeNotBetween(String value1, String value2) {
            addCriterion("UPDATERCODE not between", value1, value2, "updatercode");
            return (Criteria) this;
        }

        public Criteria andUpdaterdateIsNull() {
            addCriterion("UPDATERDATE is null");
            return (Criteria) this;
        }

        public Criteria andUpdaterdateIsNotNull() {
            addCriterion("UPDATERDATE is not null");
            return (Criteria) this;
        }

        public Criteria andUpdaterdateEqualTo(Date value) {
            addCriterion("UPDATERDATE =", value, "updaterdate");
            return (Criteria) this;
        }

        public Criteria andUpdaterdateNotEqualTo(Date value) {
            addCriterion("UPDATERDATE <>", value, "updaterdate");
            return (Criteria) this;
        }

        public Criteria andUpdaterdateGreaterThan(Date value) {
            addCriterion("UPDATERDATE >", value, "updaterdate");
            return (Criteria) this;
        }

        public Criteria andUpdaterdateGreaterThanOrEqualTo(Date value) {
            addCriterion("UPDATERDATE >=", value, "updaterdate");
            return (Criteria) this;
        }

        public Criteria andUpdaterdateLessThan(Date value) {
            addCriterion("UPDATERDATE <", value, "updaterdate");
            return (Criteria) this;
        }

        public Criteria andUpdaterdateLessThanOrEqualTo(Date value) {
            addCriterion("UPDATERDATE <=", value, "updaterdate");
            return (Criteria) this;
        }

        public Criteria andUpdaterdateIn(List<Date> values) {
            addCriterion("UPDATERDATE in", values, "updaterdate");
            return (Criteria) this;
        }

        public Criteria andUpdaterdateNotIn(List<Date> values) {
            addCriterion("UPDATERDATE not in", values, "updaterdate");
            return (Criteria) this;
        }

        public Criteria andUpdaterdateBetween(Date value1, Date value2) {
            addCriterion("UPDATERDATE between", value1, value2, "updaterdate");
            return (Criteria) this;
        }

        public Criteria andUpdaterdateNotBetween(Date value1, Date value2) {
            addCriterion("UPDATERDATE not between", value1, value2, "updaterdate");
            return (Criteria) this;
        }

        public Criteria andFlagIsNull() {
            addCriterion("FLAG is null");
            return (Criteria) this;
        }

        public Criteria andFlagIsNotNull() {
            addCriterion("FLAG is not null");
            return (Criteria) this;
        }

        public Criteria andFlagEqualTo(String value) {
            addCriterion("FLAG =", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagNotEqualTo(String value) {
            addCriterion("FLAG <>", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagGreaterThan(String value) {
            addCriterion("FLAG >", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagGreaterThanOrEqualTo(String value) {
            addCriterion("FLAG >=", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagLessThan(String value) {
            addCriterion("FLAG <", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagLessThanOrEqualTo(String value) {
            addCriterion("FLAG <=", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagLike(String value) {
            addCriterion("FLAG like", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagNotLike(String value) {
            addCriterion("FLAG not like", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagIn(List<String> values) {
            addCriterion("FLAG in", values, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagNotIn(List<String> values) {
            addCriterion("FLAG not in", values, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagBetween(String value1, String value2) {
            addCriterion("FLAG between", value1, value2, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagNotBetween(String value1, String value2) {
            addCriterion("FLAG not between", value1, value2, "flag");
            return (Criteria) this;
        }

        public Criteria andRwpIsNull() {
            addCriterion("RWP is null");
            return (Criteria) this;
        }

        public Criteria andRwpIsNotNull() {
            addCriterion("RWP is not null");
            return (Criteria) this;
        }

        public Criteria andRwpEqualTo(BigDecimal value) {
            addCriterion("RWP =", value, "rwp");
            return (Criteria) this;
        }

        public Criteria andRwpNotEqualTo(BigDecimal value) {
            addCriterion("RWP <>", value, "rwp");
            return (Criteria) this;
        }

        public Criteria andRwpGreaterThan(BigDecimal value) {
            addCriterion("RWP >", value, "rwp");
            return (Criteria) this;
        }

        public Criteria andRwpGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("RWP >=", value, "rwp");
            return (Criteria) this;
        }

        public Criteria andRwpLessThan(BigDecimal value) {
            addCriterion("RWP <", value, "rwp");
            return (Criteria) this;
        }

        public Criteria andRwpLessThanOrEqualTo(BigDecimal value) {
            addCriterion("RWP <=", value, "rwp");
            return (Criteria) this;
        }

        public Criteria andRwpIn(List<BigDecimal> values) {
            addCriterion("RWP in", values, "rwp");
            return (Criteria) this;
        }

        public Criteria andRwpNotIn(List<BigDecimal> values) {
            addCriterion("RWP not in", values, "rwp");
            return (Criteria) this;
        }

        public Criteria andRwpBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("RWP between", value1, value2, "rwp");
            return (Criteria) this;
        }

        public Criteria andRwpNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("RWP not between", value1, value2, "rwp");
            return (Criteria) this;
        }

        public Criteria andExpirelossIsNull() {
            addCriterion("EXPIRELOSS is null");
            return (Criteria) this;
        }

        public Criteria andExpirelossIsNotNull() {
            addCriterion("EXPIRELOSS is not null");
            return (Criteria) this;
        }

        public Criteria andExpirelossEqualTo(BigDecimal value) {
            addCriterion("EXPIRELOSS =", value, "expireloss");
            return (Criteria) this;
        }

        public Criteria andExpirelossNotEqualTo(BigDecimal value) {
            addCriterion("EXPIRELOSS <>", value, "expireloss");
            return (Criteria) this;
        }

        public Criteria andExpirelossGreaterThan(BigDecimal value) {
            addCriterion("EXPIRELOSS >", value, "expireloss");
            return (Criteria) this;
        }

        public Criteria andExpirelossGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("EXPIRELOSS >=", value, "expireloss");
            return (Criteria) this;
        }

        public Criteria andExpirelossLessThan(BigDecimal value) {
            addCriterion("EXPIRELOSS <", value, "expireloss");
            return (Criteria) this;
        }

        public Criteria andExpirelossLessThanOrEqualTo(BigDecimal value) {
            addCriterion("EXPIRELOSS <=", value, "expireloss");
            return (Criteria) this;
        }

        public Criteria andExpirelossIn(List<BigDecimal> values) {
            addCriterion("EXPIRELOSS in", values, "expireloss");
            return (Criteria) this;
        }

        public Criteria andExpirelossNotIn(List<BigDecimal> values) {
            addCriterion("EXPIRELOSS not in", values, "expireloss");
            return (Criteria) this;
        }

        public Criteria andExpirelossBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("EXPIRELOSS between", value1, value2, "expireloss");
            return (Criteria) this;
        }

        public Criteria andExpirelossNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("EXPIRELOSS not between", value1, value2, "expireloss");
            return (Criteria) this;
        }

        public Criteria andRemarks1IsNull() {
            addCriterion("REMARKS1 is null");
            return (Criteria) this;
        }

        public Criteria andRemarks1IsNotNull() {
            addCriterion("REMARKS1 is not null");
            return (Criteria) this;
        }

        public Criteria andRemarks1EqualTo(String value) {
            addCriterion("REMARKS1 =", value, "remarks1");
            return (Criteria) this;
        }

        public Criteria andRemarks1NotEqualTo(String value) {
            addCriterion("REMARKS1 <>", value, "remarks1");
            return (Criteria) this;
        }

        public Criteria andRemarks1GreaterThan(String value) {
            addCriterion("REMARKS1 >", value, "remarks1");
            return (Criteria) this;
        }

        public Criteria andRemarks1GreaterThanOrEqualTo(String value) {
            addCriterion("REMARKS1 >=", value, "remarks1");
            return (Criteria) this;
        }

        public Criteria andRemarks1LessThan(String value) {
            addCriterion("REMARKS1 <", value, "remarks1");
            return (Criteria) this;
        }

        public Criteria andRemarks1LessThanOrEqualTo(String value) {
            addCriterion("REMARKS1 <=", value, "remarks1");
            return (Criteria) this;
        }

        public Criteria andRemarks1Like(String value) {
            addCriterion("REMARKS1 like", value, "remarks1");
            return (Criteria) this;
        }

        public Criteria andRemarks1NotLike(String value) {
            addCriterion("REMARKS1 not like", value, "remarks1");
            return (Criteria) this;
        }

        public Criteria andRemarks1In(List<String> values) {
            addCriterion("REMARKS1 in", values, "remarks1");
            return (Criteria) this;
        }

        public Criteria andRemarks1NotIn(List<String> values) {
            addCriterion("REMARKS1 not in", values, "remarks1");
            return (Criteria) this;
        }

        public Criteria andRemarks1Between(String value1, String value2) {
            addCriterion("REMARKS1 between", value1, value2, "remarks1");
            return (Criteria) this;
        }

        public Criteria andRemarks1NotBetween(String value1, String value2) {
            addCriterion("REMARKS1 not between", value1, value2, "remarks1");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}