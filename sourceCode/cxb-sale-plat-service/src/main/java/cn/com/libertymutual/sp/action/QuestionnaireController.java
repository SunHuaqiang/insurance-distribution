package cn.com.libertymutual.sp.action;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.libertymutual.core.web.ServiceResult;
import cn.com.libertymutual.sp.service.api.QuestionnaireService;

@RestController
@RequestMapping(value = "/nol/questionn")
public class QuestionnaireController {
	@Autowired
	private QuestionnaireService questionnaireService;

	@PostMapping(value = "/findByQueId")
	public ServiceResult findByQueId(String id) {
		return questionnaireService.findQueId(id);
	}
}
