package cn.com.libertymutual.production.pojo.request;

import java.util.List;

import cn.com.libertymutual.production.model.nomorcldatasource.Prpditemlibrary;
import cn.com.libertymutual.production.model.nomorcldatasource.PrpditemlibraryWithBLOBs;
import cn.com.libertymutual.production.model.nomorcldatasource.Prpdrisk;


public class PrpdItemRequest extends Request {
	private String itemcode;
	
	private String itemcname;

	private String itemename;

	private String itemflag;
	
	private String plancode;
	
	private String riskcode;
    
    private String riskversion;
    
    private String validstatus;
    
    private Prpdrisk risk;
    
    private List<PrpditemlibraryWithBLOBs> selectedRows;
    
	public List<PrpditemlibraryWithBLOBs> getSelectedRows() {
		return selectedRows;
	}

	public void setSelectedRows(List<PrpditemlibraryWithBLOBs> selectedRows) {
		this.selectedRows = selectedRows;
	}

	public Prpdrisk getRisk() {
		return risk;
	}

	public void setRisk(Prpdrisk risk) {
		this.risk = risk;
	}

	public String getItemcode() {
		return itemcode;
	}

	public void setItemcode(String itemcode) {
		this.itemcode = itemcode;
	}

	public String getItemcname() {
		return itemcname;
	}

	public void setItemcname(String itemcname) {
		this.itemcname = itemcname;
	}

	public String getItemename() {
		return itemename;
	}

	public void setItemename(String itemename) {
		this.itemename = itemename;
	}

	public String getItemflag() {
		return itemflag;
	}

	public void setItemflag(String itemflag) {
		this.itemflag = itemflag;
	}

	public String getPlancode() {
		return plancode;
	}

	public void setPlancode(String plancode) {
		this.plancode = plancode;
	}

	public String getRiskcode() {
		return riskcode;
	}

	public void setRiskcode(String riskcode) {
		this.riskcode = riskcode;
	}

	public String getRiskversion() {
		return riskversion;
	}

	public void setRiskversion(String riskversion) {
		this.riskversion = riskversion;
	}

	public String getValidstatus() {
		return validstatus;
	}

	public void setValidstatus(String validstatus) {
		this.validstatus = validstatus;
	}
    
}
