package cn.com.libertymutual.sp.dto.callback;

import java.util.Date;

public class Insured {
	private String insuredName; //被保险人姓名
	private String insuredCertificationCode; //被保险人证件号
	private String insuredCertificationType; //被保人证件类型
	private String sex; // 被保险人性别
	private Date birthday ; //被保险人出生年月日
	private String address; // 被保险人地址
	private String insuredMobile; //被保人电话
	private String insuredEmail; // 被保人邮箱
	private String relation;
	
	
	public String getRelation() {
		return relation;
	}
	public void setRelation(String relation) {
		this.relation = relation;
	}
	public String getInsuredName() {
		return insuredName;
	}
	public void setInsuredName(String insuredName) {
		this.insuredName = insuredName;
	}
	public String getInsuredCertificationCode() {
		return insuredCertificationCode;
	}
	public void setInsuredCertificationCode(String insuredCertificationCode) {
		this.insuredCertificationCode = insuredCertificationCode;
	}
	public String getInsuredCertificationType() {
		return insuredCertificationType;
	}
	public void setInsuredCertificationType(String insuredCertificationType) {
		this.insuredCertificationType = insuredCertificationType;
	}
	public String getSex() {
		return sex;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}
	public Date getBirthday() {
		return birthday;
	}
	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getInsuredMobile() {
		return insuredMobile;
	}
	public void setInsuredMobile(String insuredMobile) {
		this.insuredMobile = insuredMobile;
	}
	public String getInsuredEmail() {
		return insuredEmail;
	}
	public void setInsuredEmail(String insuredEmail) {
		this.insuredEmail = insuredEmail;
	}
	@Override
	public String toString() {
		return "Insured [insuredName=" + insuredName + ", insuredCertificationCode=" + insuredCertificationCode
				+ ", insuredCertificationType=" + insuredCertificationType + ", sex=" + sex + ", birthday=" + birthday
				+ ", address=" + address + ", insuredMobile=" + insuredMobile + ", insuredEmail=" + insuredEmail + "]";
	}
	
}
