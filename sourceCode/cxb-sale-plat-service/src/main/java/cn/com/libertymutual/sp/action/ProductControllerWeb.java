package cn.com.libertymutual.sp.action;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import cn.com.libertymutual.core.redis.util.RedisUtils;
import cn.com.libertymutual.core.util.Constants;
import cn.com.libertymutual.core.web.ServiceResult;
import cn.com.libertymutual.sp.bean.TbSpHotproduct;
import cn.com.libertymutual.sp.bean.TbSpProduct;
import cn.com.libertymutual.sp.bean.TbSpProductConfig;
import cn.com.libertymutual.sp.dao.ProductConfigDao;
import cn.com.libertymutual.sp.dao.ProductDao;
import cn.com.libertymutual.sp.service.api.CodeRelateService;
import cn.com.libertymutual.sp.service.api.PosterService;
import cn.com.libertymutual.sp.service.api.ProductService;
import cn.com.libertymutual.sys.bean.SysCodeNode;
import cn.com.libertymutual.sys.bean.SysCodeRelate;
import cn.com.libertymutual.sys.dao.ISysCodeNodeDao;
import cn.com.libertymutual.sys.service.impl.SysCodeNodeService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping(value = "/admin/productWeb")
public class ProductControllerWeb {

	private Logger log = LoggerFactory.getLogger(getClass());
	@Autowired
	private ProductService productService;
	@Resource
	private CodeRelateService codeRelateService;
	@Autowired
	private ProductDao productDao;
	@Autowired
	private ProductConfigDao productConfigDao;
	@Resource
	private RedisUtils redisUtils;
	@Autowired
	private ISysCodeNodeDao sysCodeNodeDao;
	@Resource
	private SysCodeNodeService sysCodeNodeService;

	/*
	 * 后台添加产品
	 */
	@ApiOperation(value = "后台添加产品", notes = "后台添加产品")
	@PostMapping("/addProduct")
	public ServiceResult addProduct(
			@RequestBody @ApiParam(name = "tbSpProduct", value = "tbSpProduct", required = true) TbSpProduct tbSpProduct) {
		log.info(tbSpProduct.getProductCname());
		ServiceResult sr = new ServiceResult();
		sr = productService.addProduct(tbSpProduct);
		return sr;
	}

	/*
	 * 后台设置计划
	 */
	@ApiOperation(value = "后台设置计划", notes = "后台设置计划")
	@PostMapping("/addPlans")
	public ServiceResult addPlans(
			@RequestBody @ApiParam(name = "tbSpProduct", value = "tbSpProduct", required = true) TbSpProduct tbSpProduct) {
		log.info(String.valueOf(tbSpProduct.getId()));
		ServiceResult sr = new ServiceResult();
		sr = productService.addPlans(tbSpProduct);
		return sr;
	}

	/*
	 * 计划列表
	 */
	@ApiOperation(value = "计划列表", notes = "计划列表")
	@ApiImplicitParams(value = {
			@ApiImplicitParam(name = "productId", value = "productId", required = true, paramType = "query", dataType = "Long") })
	@PostMapping("/planList")
	public ServiceResult planList(Integer productId) {
		ServiceResult sr = new ServiceResult();
		sr = productService.planList(productId);
		return sr;
	}

	/*
	 * 后台查询产品列表
	 */
	@ApiOperation(value = "按不同分类查询产品", notes = "后台查询产品列表")
	@ApiImplicitParams(value = {
			@ApiImplicitParam(name = "riskCode", value = "险种编码", required = false, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "status", value = "状态", required = false, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "branchCode", value = "机构编码", required = false, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "productCname", value = "中文名", required = false, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "startDate", value = "开始日期", required = false, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "endDate", value = "结束日期", required = false, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "pageNumber", value = "页码", required = true, paramType = "query", dataType = "Long"),
			@ApiImplicitParam(name = "pageSize", value = "条数", required = true, paramType = "query", dataType = "Long") })
	@PostMapping("/productList")
	public ServiceResult ProductList(HttpServletRequest request, String riskCode, String status, String branchCode,
			String productCname, String startDate, String endDate, Integer pageNumber, Integer pageSize) {
		log.info("---------web--productList---");
		ServiceResult sr = new ServiceResult();
		sr = productService.ProductList(riskCode, status, branchCode, productCname, startDate, endDate, pageNumber,
				pageSize);
		return sr;
	}
	
	
	
	/*
	 * 后台查询渠道产品列表
	 */
	@ApiOperation(value = "按不同分类查询产品", notes = "后台查询产品列表")
	@ApiImplicitParams(value = {
			@ApiImplicitParam(name = "userCode", value = "用户编码", required = false, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "pageNumber", value = "页码", required = true, paramType = "query", dataType = "Long"),
			@ApiImplicitParam(name = "pageSize", value = "条数", required = true, paramType = "query", dataType = "Long") })
	@PostMapping("/channelProductList")
	public ServiceResult channelProductList(String userCode,Integer pageNumber, Integer pageSize) {
		ServiceResult sr = new ServiceResult();
		sr = productService.channelProductList(userCode, pageNumber,pageSize);
		return sr;
	}
	

	@ApiOperation(value = "按不同分类查询热销产品", notes = "后台查询热销产品列表")
	@ApiImplicitParams(value = {
			@ApiImplicitParam(name = "branchCode", value = "机构编码", required = false, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "pageNumber", value = "页码", required = true, paramType = "query", dataType = "Long"),
			@ApiImplicitParam(name = "pageSize", value = "条数", required = true, paramType = "query", dataType = "Long") })
	@PostMapping("/areaHotProductList")
	public ServiceResult hotProductList(String branchCode, Integer pageNumber, Integer pageSize) {
		ServiceResult sr = new ServiceResult();
		sr = productService.hotProductList(branchCode, pageNumber, pageSize);
		return sr;
	}

	@ApiOperation(value = "按不同分类查询热销表", notes = "后台查询热销列表")
	@ApiImplicitParams(value = {
			@ApiImplicitParam(name = "branchCode", value = "机构编码", required = false, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "pageNumber", value = "页码", required = true, paramType = "query", dataType = "Long"),
			@ApiImplicitParam(name = "pageSize", value = "条数", required = true, paramType = "query", dataType = "Long") })
	@PostMapping("/branchHotProductList")
	public ServiceResult branchHotProductList(String branchCode, Integer pageNumber, Integer pageSize) {
		ServiceResult sr = new ServiceResult();
		sr = productService.branchHotProductList(branchCode, pageNumber, pageSize);
		return sr;
	}

	/*
	 * 后台修改产品
	 */
	@ApiOperation(value = "后台修改产品", notes = "后台修改产品")
	@PostMapping("/updateProduct")
	public ServiceResult updateProduct(
			@RequestBody @ApiParam(name = "tbSpProduct", value = "tbSpProduct", required = true) TbSpProduct tbSpProduct) {

		ServiceResult sr = new ServiceResult();
		sr = productService.updateProduct(tbSpProduct);
		return sr;
	}

	/*
	 * 后台修改产品序号
	 */
	@ApiOperation(value = "后台修改产品序号", notes = "后台修改产品序号")
	@ApiImplicitParams(value = {
			@ApiImplicitParam(name = "id", value = "机构编码", required = false, paramType = "query", dataType = "Long"),
			@ApiImplicitParam(name = "serialNo", value = "页码", required = true, paramType = "query", dataType = "Long") })
	@PostMapping("/updateProductSerialNo")
	public ServiceResult updateProductSerialNo(Integer id, Integer serialNo) {

		ServiceResult sr = new ServiceResult();
		sr = productService.updateProductSerialNo(id, serialNo);
		return sr;
	}

	/*
	 * 后台删除产品
	 */
	@ApiOperation(value = "后台删除产品", notes = "后台删除产品")
	@ApiImplicitParam(name = "ids", value = "删除的产品的id", required = true, paramType = "query", dataType = "String")
	@PostMapping("/deleteProduct")
	public ServiceResult deleteProduct(String ids) {
		log.info("ids:" + ids);
		String[] list = ids.split(",");

		//log.info("list:" + list);
		ServiceResult sr = new ServiceResult();
		sr = productService.removeProduct(list);
		return sr;
	}

	/*
	 * 后台恢复产品
	 */
	@ApiOperation(value = "后台恢复产品", notes = "后台恢复产品")
	@ApiImplicitParam(name = "ids", value = "删除的产品的id", required = true, paramType = "query", dataType = "String")
	@PostMapping("/rebackProduct")
	public ServiceResult rebackProduct(String ids) {
		log.info("ids:" + ids);
		String[] list = ids.split(",");

		//log.info("list:" + list);
		ServiceResult sr = new ServiceResult();
		sr = productService.rebackProduct(list);
		return sr;
	}

	@ApiOperation(value = "后台设置热销", notes = "后台设置热销")
	@PostMapping("/setHot")
	public ServiceResult setHot(
			@RequestBody @ApiParam(name = "tbSpHotproduct", value = "tbSpHotproduct", required = true) TbSpHotproduct tbSpHotproduct) {

		ServiceResult sr = new ServiceResult();
		sr = productService.setHot(tbSpHotproduct);
		return sr;
	}

	@ApiOperation(value = "后台批量添加地区热销", notes = "后台批量添加地区热销")
	@ApiImplicitParams(value = {
			@ApiImplicitParam(name = "branchCode", value = "机构编码", required = true, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "ids", value = "添加的产品的id", required = true, paramType = "query", dataType = "String") })
	@PostMapping("/addToAreaHot")
	public ServiceResult addToAreaHot(String branchCode, String ids) {

		ServiceResult sr = new ServiceResult();

		String[] listProductId = ids.split(",");
		//log.info("listProductId:" + "----" + listProductId);
		sr = productService.addToAreaHot(branchCode, listProductId);
		return sr;
	}

	@ApiOperation(value = "getHotProduct", notes = "getHotProduct")
	@ApiImplicitParams(value = {
			@ApiImplicitParam(name = "productId", value = "热销产品的productId", required = true, paramType = "query", dataType = "Long"),
			@ApiImplicitParam(name = "branchCode", value = "热销产品的机构编码", required = true, paramType = "query", dataType = "String") })
	@PostMapping("/getHotProduct")
	public ServiceResult getHotProduct(Integer productId, String branchCode) {

		ServiceResult sr = new ServiceResult();
		sr = productService.getHotProduct(productId, branchCode);
		return sr;
	}

	@ApiOperation(value = "后台取消某地区热销", notes = "后台取消某地区热销")
	@ApiImplicitParams(value = {
			@ApiImplicitParam(name = "branchCode", value = "机构编码", required = true, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "ids", value = "热销产品的id组", required = true, paramType = "query", dataType = "String") })
	@PostMapping("/removeHotProduct")
	public ServiceResult removeHotProduct(String branchCode, String ids) {

		log.info(branchCode + " " + "ids:" + "----" + ids);
		ServiceResult sr = new ServiceResult();
		String[] listProductId = ids.split(",");

		sr = productService.removeHotProduct(branchCode, listProductId);
		return sr;
	}

	@ApiOperation(value = "机构列表", notes = "机构列表")
	@ApiImplicitParam(name = "type", value = "code", required = true, paramType = "query", dataType = "String")
	@PostMapping("/codeNodeList")
	public ServiceResult codeNodeList(String type) {
		log.info("机构--------------列表" + type);
		ServiceResult sr = new ServiceResult();
		if (type.equals("RiskCode")) {
			sr = codeRelateService.riskList();
		} else {
			sr = productService.codeNodeList(type);
		}
		return sr;
	}

	@ApiOperation(value = "添加险种", notes = "添加险种")
	@PostMapping("/addRisk")
	public ServiceResult addRisk(
			@RequestBody @ApiParam(name = "sysCodeRelate", value = "sysCodeRelate", required = true) SysCodeRelate sysCodeRelate) {
		ServiceResult sr = new ServiceResult();
		log.info("=======添加的险种====="+sysCodeRelate.toString());
		sr = codeRelateService.addRisk(sysCodeRelate);
		return sr;
	}

	@ApiOperation(value = "险种列表", notes = "险种列表")
	@ApiImplicitParams(value = {
			@ApiImplicitParam(name = "pageNumber", value = "页码", required = true, paramType = "query", dataType = "Long"),
			@ApiImplicitParam(name = "pageSize", value = "每页条数", required = true, paramType = "query", dataType = "Long") })
	@PostMapping("/riskList")
	public ServiceResult riskList(int pageNumber, int pageSize) {
		ServiceResult sr = new ServiceResult();
		sr = codeRelateService.riskListPage(pageNumber, pageSize);
		return sr;
	}

	@ApiOperation(value = "删除险种", notes = "删除险种")
	@ApiImplicitParam(name = "id", value = "险种id", required = true, paramType = "query", dataType = "Long")
	@PostMapping("/removeRisk")
	public ServiceResult removeRisk(Integer id) {
		log.info("删除--------------险种" + id);
		ServiceResult sr = new ServiceResult();
		sr = codeRelateService.removeRisk(id);
		return sr;
	}
	
	
	/*
	 * 根据险种查询所有产品
	 */
	@ApiOperation(value = "根据险种查询所有产品", notes = "根据险种查询所有产品")
	@ApiImplicitParam(name = "riskCode", value = "险种", required = true, paramType = "query", dataType = "String")
	@PostMapping("/productByRisk")
	public ServiceResult productByRisk(String riskCode) {
		log.info("--------------险种" + riskCode);
		ServiceResult sr = new ServiceResult();
		sr = productService.productByRisk(riskCode);
		return sr;
	}
	
	
	
	
	@ApiOperation(value = "专属", notes = "专属")
	@ApiImplicitParams(value = {
		@ApiImplicitParam(name = "productId", value = "产品id", required = true, paramType = "query", dataType = "Long"),
		@ApiImplicitParam(name = "agrementNo", value = "业务关系代码", required = true, paramType = "query", dataType = "String"),
		@ApiImplicitParam(name = "userCode", value = "用户编码", required = true, paramType = "query", dataType = "String"),
		@ApiImplicitParam(name = "userName", value = "用户真实姓名", required = true, paramType = "query", dataType = "String"),
		@ApiImplicitParam(name = "branchCode", value = "机构编码", required = true, paramType = "query", dataType = "String"),
		@ApiImplicitParam(name = "pageNumber", value = "页码", required = true, paramType = "query", dataType = "Long"), 
		@ApiImplicitParam(name = "pageSize", value = "每页条数", required = true, paramType = "query", dataType = "Long") 
		})
	@PostMapping("/getExclusive")
	public ServiceResult getExclusive(Integer productId,String userCode, String userName, String branchCode,
			String agrementNo,String isIn,int pageNumber ,int pageSize ) {
		ServiceResult sr = new ServiceResult();
		sr = productService.getExclusive(productId,userCode,userName,branchCode,agrementNo,isIn,pageNumber,pageSize);
		return sr;
	}
	
	
	@ApiOperation(value = "设置专属", notes = "设置专属")
	@ApiImplicitParams(value = {
		@ApiImplicitParam(name = "productId", value = "产品id", required = true, paramType = "query", dataType = "Long"),
		@ApiImplicitParam(name = "isExclusive", value = "是否专属", required = true, paramType = "query", dataType = "String"),
		@ApiImplicitParam(name = "codes", value = "userCodes", required = true, paramType = "query", dataType = "String") })
	@PostMapping("/setExclusive")
	public ServiceResult setExclusive(Integer productId,String isExclusive, String codes) {
		ServiceResult sr = new ServiceResult();
		String[] userCodes = codes.split(",");
		sr = productService.setExclusive(productId,isExclusive, userCodes);
		return sr;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	@ApiOperation(value = "addIntegral", notes = "addIntegral")
	@PostMapping(value = "/addIntegral")
	public ServiceResult addIntegral() {
		ServiceResult sr = new ServiceResult();
		HashMap<String, Map<String, SysCodeNode>> codeMap = (HashMap<String, Map<String, SysCodeNode>>) redisUtils
				.get(Constants.MAP_CODE);
		
		Map<String,SysCodeNode> branchMap = codeMap.get("BranchCode");
		List<TbSpProduct> ps = productDao.findAllProduct();
//		List<Integer> ps = new ArrayList<>();
//		ps.add(169);
//		ps.add(170);
//		ps.add(171);
		 for (String branchCode : branchMap.keySet()) {
			 SysCodeNode str = branchMap.get(branchCode);//得到每个key多对用value的值
			 for (TbSpProduct tp:ps) {
				List<TbSpProductConfig> dbtspc =  productConfigDao.findByProductIdAndBranchCode(tp.getId(),str.getCode());
				if (null == dbtspc || dbtspc.size() == 0) {
					TbSpProductConfig tspc = new TbSpProductConfig();
					tspc.setBranchCode(str.getCode());
					tspc.setBranchName(str.getCodeCname());
					tspc.setProductId(tp.getId());
					tspc.setIsExclusive("0");
					tspc.setStatus("0");
					tspc.setIsShow("1");
					tspc.setRate(0.00);
					productConfigDao.save(tspc);
				}
			}
		 }
		sr.setSuccess();
		return sr;
	}
	
	@ApiOperation(value = "查询产品所属保险公司", notes = "查询产品所属保险公司")
	@ApiImplicitParams(value = { 
			@ApiImplicitParam(name = "pageNumber", value = "分页", required = true, paramType = "query", dataType = "Long"),
			@ApiImplicitParam(name = "pageSize", value = "分页", required = true, paramType = "query", dataType = "Long")})
	@PostMapping("/productCompanyList")
	public ServiceResult productCompany(int pageNumber, int pageSize){
		ServiceResult sr = new ServiceResult();
		Map<String, Object> map = new HashMap<String, Object>();
		int count = sysCodeNodeDao.findProductCompanyCount();
		List<SysCodeNode> codeList = sysCodeNodeDao.findProductCompany((pageNumber - 1) * pageSize, pageNumber * pageSize);
		map.put("total", count);
		map.put("list", codeList);
		sr.setResult(map);
		return sr;
	}
	
	@ApiOperation(value = "添加、修改产品所属保险公司", notes = "添加、修改产品所属保险公司")
	@PostMapping("/addProductCompany")
	public ServiceResult addProductCompany(@RequestBody @ApiParam(name = "sysCodeNode", value = "sysCodeNode", required = true) SysCodeNode sysCodeNode){
		ServiceResult sr = new ServiceResult();
		sr = sysCodeNodeService.addProductCompany(sysCodeNode);
		return sr;
	}
	
	@ApiOperation(value = "删除产品所属保险公司", notes = "删除产品所属保险公司")
	@ApiImplicitParam(name = "id", value = "id", required = true, paramType = "query", dataType = "Long")
	@PostMapping("/deleteProductCompany")
	public ServiceResult deleteProductCompany(Integer id){
		ServiceResult sr = new ServiceResult();
		sysCodeNodeDao.deleteById(id);
		sr.setSuccess();
		return sr;
	}
	
}
