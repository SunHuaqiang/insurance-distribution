package cn.com.libertymutual.sp.action;

import java.io.IOException;
import java.text.ParseException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.ResourceAccessException;

import cn.com.libertymutual.core.util.Constants;
import cn.com.libertymutual.core.web.ServiceResult;
import cn.com.libertymutual.sp.dto.UserDto;
import cn.com.libertymutual.sp.req.StoreRateReq;
import cn.com.libertymutual.sp.service.api.LevelService;
import cn.com.libertymutual.sp.service.api.ShopService;
import cn.com.libertymutual.sys.bean.SysBranch;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping(value = "/admin/level")
public class LevelControllerWeb {

	@Autowired
	private LevelService levelService;
	@Autowired
	private ShopService shopService;
	private Logger log = LoggerFactory.getLogger(getClass());

	@ApiOperation(value = "层级列表", notes = "层级列表")
	@PostMapping(value = "/levelList")
	@ApiImplicitParams(value = { @ApiImplicitParam(name = "branchNo", value = "机构编码", required = true, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "branchCname", value = "机构名称", required = true, paramType = "query", dataType = "String"), })
	public ServiceResult levelList(String branchNo, String branchCname) {
		ServiceResult sr = new ServiceResult();

		sr = levelService.levelList(branchNo, branchCname);
		return sr;

	}

	@ApiOperation(value = "校验是否需要合并账户", notes = "校验是否需要合并账户")
	@PostMapping(value = "/isMergeUser")
	@ApiImplicitParams(value = { 
			@ApiImplicitParam(name = "mobile", value = "电话号码", required = true, paramType = "query", dataType = "String"), 
			@ApiImplicitParam(name = "comCode", value = "", required = true, paramType = "query", dataType = "String"), 
	})
	public ServiceResult isMergeUser(String mobile,String comCode) {
		ServiceResult sr = new ServiceResult();

		sr = levelService.isMergeUser(mobile,comCode);
		return sr;

	}
	
	@ApiOperation(value = "校验身份证", notes = "校验身份证")
	@PostMapping(value = "/isHasIdnumber")
	@ApiImplicitParams(value = { @ApiImplicitParam(name = "idNumber", value = "证件号", required = true, paramType = "query", dataType = "String"), })
	public ServiceResult isHasIdnumber(String idNumber) {
		ServiceResult sr = new ServiceResult();

		sr = levelService.isHasIdnumber(idNumber);
		return sr;

	}

	@ApiOperation(value = "创建渠道用户", notes = "创建渠道用户")
	@PostMapping(value = "/createChannelUser")
	public ServiceResult createChannelUser(@RequestBody UserDto userDto) {
		ServiceResult sr = new ServiceResult();

		try {
			sr = levelService.createChannelUser(userDto);
		} catch (Exception e) {
			sr.setFail();
			sr.setResult("创建渠道用户失败！");
			e.printStackTrace();
		}
		return sr;

	}

	@ApiOperation(value = "修改信渠道用户息", notes = "修改渠道用户信息")
	@PostMapping(value = "/updateChannelUser")
	public ServiceResult updateChannelUser(@RequestBody UserDto userDto) {
		ServiceResult sr = new ServiceResult();
		try {
			sr = levelService.updateChannelUser(userDto);
		} catch (Exception e) {
			sr.setFail();
			sr.setResult("修改信渠道用户息失败！");
			e.printStackTrace();
		}
		return sr;

	}

	
	
	@ApiOperation(value = "创建渠道用户", notes = "创建渠道用户")
	@PostMapping(value = "/branchManage")
	public ServiceResult branchManage(@RequestBody SysBranch sysBranch) {
		ServiceResult sr = new ServiceResult();

		try {
			sr = levelService.branchManage(sysBranch);
		} catch (Exception e) {
			sr.setFail();
			sr.setResult("创建渠道用户失败！");
			e.printStackTrace();
		}
		return sr;

	}
	
	
	
	@ApiOperation(value = "查询下级渠道列表", notes = "查询下级渠道列表")
	@PostMapping(value = "/nextChannelList")
	@ApiImplicitParams(value = { 
			@ApiImplicitParam(name = "mobile", value = "手机号码", required = true, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "istopUser", value = "是否顶级", required = true, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "branchNo", value = "机构编码", required = true, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "channelId", value = "渠道ID", required = false, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "shopName", value = "渠道名称", required = false, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "pageSize", value = "条数", required = true, paramType = "query", dataType = "Long"),
			@ApiImplicitParam(name = "pageNumber", value = "页码", required = true, paramType = "query", dataType = "Long"),
	})
	public ServiceResult nextChannelList(String mobile,String istopUser,String branchNo, String channelId, String shopName, Integer pageSize, Integer pageNumber) {
		ServiceResult sr = new ServiceResult();

		sr = levelService.nextChannelList(mobile,istopUser,branchNo, channelId, shopName, pageSize, pageNumber);
		return sr;

	}

	@ApiOperation(value = "树-查询下级渠道", notes = "查询下级渠道")
	@PostMapping(value = "/treeNextChannelList")
	@ApiImplicitParams(value = { 
			@ApiImplicitParam(name = "branchNo", value = "机构编码", required = true, paramType = "query", dataType = "String"), 
			@ApiImplicitParam(name = "type", value = "类型", required = true, paramType = "query", dataType = "String"), 
			})
	public ServiceResult treeNextChannelList(String branchNo,String type) {
		ServiceResult sr = new ServiceResult();

		sr = levelService.treeNextChannelList(branchNo,type);
		return sr;

	}
	
	
	@ApiOperation(value = "树-查询下级机构", notes = "查询下级机构")
	@PostMapping(value = "/nextBranchList")
	@ApiImplicitParams(value = { 
			@ApiImplicitParam(name = "branchNo", value = "机构编码", required = true, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "nextBranchNo", value = "渠道ID", required = false, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "nextBranchName", value = "渠道名称", required = false, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "pageSize", value = "条数", required = true, paramType = "query", dataType = "Long"),
			@ApiImplicitParam(name = "pageNumber", value = "页码", required = true, paramType = "query", dataType = "Long"),
	})
	public ServiceResult nextBranchList(String branchNo,String nextBranchNo,String nextBranchName,int pageNumber,int pageSize) {
		ServiceResult sr = new ServiceResult();

		sr = levelService.nextBranchList(branchNo,nextBranchNo,nextBranchName,pageNumber,pageSize);
		return sr;

	}
	
	
	@ApiOperation(value = "树-查询下级机构", notes = "查询下级机构")
	@PostMapping(value = "/treeNextBranchList")
	@ApiImplicitParams(value = { 
			@ApiImplicitParam(name = "branchNo", value = "机构编码", required = true, paramType = "query", dataType = "String"),
	})
	public ServiceResult treeNextBranchList(String branchNo) {
		ServiceResult sr = new ServiceResult();

		sr = levelService.treeNextBranchList(branchNo);
		return sr;

	}

	@ApiOperation(value = "查询当前渠道", notes = "查询当前渠道")
	@PostMapping(value = "/thisChannel")
	@ApiImplicitParams(value = { @ApiImplicitParam(name = "userCode", value = "用户编码", required = true, paramType = "query", dataType = "String"), })
	public ServiceResult thisChannel(String userCode) {
		ServiceResult sr = new ServiceResult();

		sr = levelService.thisChannel(userCode);
		return sr;

	}

	@ApiOperation(value = "查询业务关系代码", notes = "测试")
	@ApiImplicitParams(value = {
			@ApiImplicitParam(name = "agreementCode", value = "关系代码", required = true, paramType = "query", dataType = "String") })
	@RequestMapping(value = "/findByAgreementNo")
	public ServiceResult findAgreementNo(HttpServletRequest request, HttpServletResponse response, String agreementCode) {
		ServiceResult sr = new ServiceResult(Constants.SYS_ERROR_MSG, ServiceResult.STATE_EXCEPTION);
		try {
			sr = shopService.findByAgreementNo(request, sr, agreementCode);
		} catch (IOException e) {
			sr.setResult("查询业务关系代码超时");
			log.warn("查询业务关系代码IO异常:" + e.toString());
		} catch (ResourceAccessException e) {
			sr.setResult("资源访问异常");
			log.warn("查询业务关系代码资源访问异常:" + e.toString());
		} catch (ParseException e) {
			sr.setResult("数据解析异常");
			log.warn("查询业务关系代码数据解析异常:" + e.toString());
		} catch (Exception e) {
			sr.setResult("查询业务关系代码异常");
			log.warn("查询业务关系代码异常:" + e.toString());
		}
		return sr;
	}

	@ApiOperation(value = "订单查询权限用户列表", notes = "订单查询权限用户列表")
	@ApiImplicitParams(value = { @ApiImplicitParam(name = "userId", value = "用户名", required = true, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "mobile", value = "手机号", required = false, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "type", value = "用户分类", required = false, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "pageSize", value = "条数", required = true, paramType = "query", dataType = "Long"),
			@ApiImplicitParam(name = "pageNumber", value = "页码", required = true, paramType = "query", dataType = "Long"), })
	@RequestMapping(value = "/authUserList")
	public ServiceResult authUserList(String userId, String mobile, String type, int pageNumber, int pageSize) {
		ServiceResult sr = new ServiceResult();
		sr = levelService.authUserList(userId, mobile, type, pageNumber, pageSize);
		return sr;
	}

	@ApiOperation(value = "订单查询权限", notes = "订单查询权限")
	@ApiImplicitParams(value = { 
		@ApiImplicitParam(name = "userId", value = "用户名", required = true, paramType = "query", dataType = "String"),
		@ApiImplicitParam(name = "type", value = "类型", required = true, paramType = "query", dataType = "String"),
	})
	@RequestMapping(value = "/orderAuth")
	public ServiceResult orderAuth(String userId,String type) {
		ServiceResult sr = new ServiceResult();
		sr = levelService.orderAuth(userId,type);
		return sr;
	}

	@ApiOperation(value = "修改订单查询权限", notes = "修改订单查询权限")
	@ApiImplicitParams(value = {
			@ApiImplicitParam(name = "branchNos", value = "机构编码逗号隔开", required = true, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "userCode", value = "用户编码", required = true, paramType = "query", dataType = "String") 
	})
	@RequestMapping(value = "/updateOrderAuth")
	public ServiceResult updateOrderAuth(String branchNos, String userCode) {
		ServiceResult sr = new ServiceResult();
		String[] list = branchNos.split(",");

		sr = levelService.updateOrderAuth(list, userCode);
		return sr;
	}

	
	@ApiOperation(value = "顶级渠道", notes = "顶级渠道")
	@ApiImplicitParams(value = {
			@ApiImplicitParam(name = "userCode", value = "用户编码", required = true, paramType = "query", dataType = "String") })
	@RequestMapping(value = "/topChannelUser")
	public ServiceResult topChannelUser(String userCode) {
		ServiceResult sr = new ServiceResult();
		sr = levelService.topChannelUser(userCode);
		return sr;
	}

	
	
	/*
	 * 授权顶级渠道店铺产品
	 */

	@ApiOperation(value = "配置店铺佣金比例", notes = "配置店铺佣金比例")
	@PostMapping(value = "/setShopPro")
	public ServiceResult setShopPro(@RequestBody @ApiParam(name = "storeRate", value = "storeRate", required = true) StoreRateReq store) {
		ServiceResult sr = new ServiceResult();
		if (StringUtils.isEmpty(store.getUserCodes())) {
			sr.setFail();
			sr.setResult("信息不能为空！");
			return sr;
		}
		try {
			sr = levelService.setShopPro(store);
			return sr;
		} catch (Exception e) {
			log.info(e.getMessage());
			e.printStackTrace();
		}
		return null;
	}
}
