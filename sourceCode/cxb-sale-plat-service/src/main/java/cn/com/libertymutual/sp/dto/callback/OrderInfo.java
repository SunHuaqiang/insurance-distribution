package cn.com.libertymutual.sp.dto.callback;

import java.util.Date;
import java.util.List;

import cn.com.libertymutual.sp.dto.queryplans.CrossSaleKind;

public class OrderInfo {
	private String userId;   //业务员
	private String cityId;   //销售城市
	private String orderAmount;  //订单原价金额
	private String orderDiscount;  //订单优惠总金额
	private String finalAmount;   //订单最终应付金额
	private String commissionRate; //手续费率
	private String fee; //手续费
	private String circPaymentNo; //付费流水号
	private String status;  //订单状态
	private String payNode;  //支付方式 支付方式（支付宝、微信、快钱、银联等）
	private Date payNotifyDate; //支付通知时间
	private String payPerson; //付款人
	private String applyPolicyNo; //投保单号
	private String policyNo;  //保单号 
	private Date applyPolicyDate; //投保日期
	private String numberOfInsured;  //投保份数
	private String protocolNo; //业务关系代码
	private String adultMaxSum; //总保额
	private List<CrossSaleKind> kinds;
	private Product product; //投保产品
	private List<Insured> insuredList;
	private Appnt appntPerson;
	private String accessUrl;
	private String dealuuid;
	private String signature;
	
	
	public String getSignature() {
		return signature;
	}
	public void setSignature(String signature) {
		this.signature = signature;
	}

	public String getDealuuid() {
		return dealuuid;
	}
	public void setDealuuid(String dealuuid) {
		this.dealuuid = dealuuid;
	}
	public String getAccessUrl() {
		return accessUrl;
	}
	public void setAccessUrl(String accessUrl) {
		this.accessUrl = accessUrl;
	}
	public String getAdultMaxSum() {
		return adultMaxSum;
	}
	public void setAdultMaxSum(String adultMaxSum) {
		this.adultMaxSum = adultMaxSum;
	}
	public List<CrossSaleKind> getKinds() {
		return kinds;
	}
	public void setKinds(List<CrossSaleKind> kinds) {
		this.kinds = kinds;
	}
	public String getProtocolNo() {
		return protocolNo;
	}
	public void setProtocolNo(String protocolNo) {
		this.protocolNo = protocolNo;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getCityId() {
		return cityId;
	}
	public void setCityId(String cityId) {
		this.cityId = cityId;
	}
	public String getOrderAmount() {
		return orderAmount;
	}
	public void setOrderAmount(String orderAmount) {
		this.orderAmount = orderAmount;
	}
	public String getOrderDiscount() {
		return orderDiscount;
	}
	public void setOrderDiscount(String orderDiscount) {
		this.orderDiscount = orderDiscount;
	}
	public String getFinalAmount() {
		return finalAmount;
	}
	public void setFinalAmount(String finalAmount) {
		this.finalAmount = finalAmount;
	}
	public String getCommissionRate() {
		return commissionRate;
	}
	public void setCommissionRate(String commissionRate) {
		this.commissionRate = commissionRate;
	}
	public String getFee() {
		return fee;
	}
	public void setFee(String fee) {
		this.fee = fee;
	}
	public String getCircPaymentNo() {
		return circPaymentNo;
	}
	public void setCircPaymentNo(String circPaymentNo) {
		this.circPaymentNo = circPaymentNo;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getPayNode() {
		return payNode;
	}
	public void setPayNode(String payNode) {
		this.payNode = payNode;
	}
	public Date getPayNotifyDate() {
		return payNotifyDate;
	}
	public void setPayNotifyDate(Date payNotifyDate) {
		this.payNotifyDate = payNotifyDate;
	}
	public String getPayPerson() {
		return payPerson;
	}
	public void setPayPerson(String payPerson) {
		this.payPerson = payPerson;
	}
	public String getApplyPolicyNo() {
		return applyPolicyNo;
	}
	public void setApplyPolicyNo(String applyPolicyNo) {
		this.applyPolicyNo = applyPolicyNo;
	}
	public String getPolicyNo() {
		return policyNo;
	}
	public void setPolicyNo(String policyNo) {
		this.policyNo = policyNo;
	}
	public Date getApplyPolicyDate() {
		return applyPolicyDate;
	}
	public void setApplyPolicyDate(Date applyPolicyDate) {
		this.applyPolicyDate = applyPolicyDate;
	}
	public String getNumberOfInsured() {
		return numberOfInsured;
	}
	public void setNumberOfInsured(String numberOfInsured) {
		this.numberOfInsured = numberOfInsured;
	}
	public Product getProduct() {
		return product;
	}
	public void setProduct(Product product) {
		this.product = product;
	}
	public List<Insured> getInsuredList() {
		return insuredList;
	}
	public void setInsuredList(List<Insured> insuredList) {
		this.insuredList = insuredList;
	}
	public Appnt getAppntPerson() {
		return appntPerson;
	}
	public void setAppntPerson(Appnt appntPerson) {
		this.appntPerson = appntPerson;
	}
	@Override
	public String toString() {
		return "OrderInfo [userId=" + userId + ", cityId=" + cityId + ", orderAmount=" + orderAmount
				+ ", orderDiscount=" + orderDiscount + ", finalAmount=" + finalAmount + ", commissionRate="
				+ commissionRate + ", fee=" + fee + ", circPaymentNo=" + circPaymentNo + ", status=" + status
				+ ", payNode=" + payNode + ", payNotifyDate=" + payNotifyDate + ", payPerson=" + payPerson
				+ ", applyPolicyNo=" + applyPolicyNo + ", policyNo=" + policyNo + ", applyPolicyDate=" + applyPolicyDate
				+ ", numberOfInsured=" + numberOfInsured + ", product=" + product + ", insuredList=" + insuredList
				+ ", appntPerson=" + appntPerson + "]";
	}
	
	
	
}
