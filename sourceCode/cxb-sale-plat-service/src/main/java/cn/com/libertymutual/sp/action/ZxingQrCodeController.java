package cn.com.libertymutual.sp.action;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.libertymutual.core.util.Constants;
import cn.com.libertymutual.core.util.Current;
import cn.com.libertymutual.core.web.ServiceResult;
import cn.com.libertymutual.sp.service.api.ZxingQrCodeService;

@RestController
@RequestMapping(value = "/nol/qr_code")
public class ZxingQrCodeController {
	// 注入原子性业务逻辑
	@Resource
	private ZxingQrCodeService zxingQrCodeService;

	/** 
	 * 邀请好友注册二维码<br>
	 * Version：1.0<br>
	 * Author：AoYi<br>
	 * DateTime：2018年3月30日上午9:54:33<br>
	 * @param request
	 * @param response
	 * @param userCode 用户编码
	 * @param shareUrl 分享地址
	 * @param userHeadUrl 头像地址
	 * @param fileType 图片类型(jpg,png等)
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/registerZxingQrCode")
	public ServiceResult registerZxingQrCode(HttpServletRequest request, HttpServletResponse response, String userCode, String shareUrl,
			String userHeadUrl, String fileType) {
		ServiceResult sr = new ServiceResult(Constants.SYS_ERROR_MSG, ServiceResult.STATE_APP_EXCEPTION);
		try {
			sr = zxingQrCodeService.registerZxingQrCode(request, response, Current.userCode.get(), shareUrl, userHeadUrl, fileType);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return sr;
	}

	/** 
	 * 店铺分享产品二维码<br>
	 * Version：1.0<br>
	 * Author：AoYi<br>
	 * DateTime：2018年3月30日上午9:48:42<br>
	 * @param request
	 * @param response
	 * @param userCode 用户编码
	 * @param shareUrl 分享地址
	 * @param userHeadUrl 头像地址
	 * @param productCName 产品名称
	 * @param fontSize 字体大小
	 * @param fileType 图片类型(jpg,png等)
	 * @return
	 */
	@RequestMapping(value = "/shareZxingQrCode")
	public ServiceResult shareZxingQrCode(HttpServletRequest request, HttpServletResponse response, String userCode, String shareUrl,
			String userHeadUrl, String productCName, Integer fontSize, String fileType) {
		ServiceResult sr = new ServiceResult(Constants.SYS_ERROR_MSG, ServiceResult.STATE_APP_EXCEPTION);
		try {
			sr = zxingQrCodeService.shareZxingQrCode(request, response, Current.userCode.get(), shareUrl, userHeadUrl, productCName, fontSize,
					fileType);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return sr;
	}
}
