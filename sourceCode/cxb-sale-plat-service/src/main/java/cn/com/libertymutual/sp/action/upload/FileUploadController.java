package cn.com.libertymutual.sp.action.upload;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import cn.com.libertymutual.core.util.Current;
import cn.com.libertymutual.core.web.ServiceResult;
import cn.com.libertymutual.sp.biz.FileUploadBiz;
import cn.com.libertymutual.sp.dto.ImageBase64Dto;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(value = "/nol/upload")
public class FileUploadController {
	@Autowired
	private FileUploadBiz fileUploadBiz;

	@ApiOperation(value = "头像上传[file]", notes = "测试")
	@ApiImplicitParams(value = { @ApiImplicitParam(name = "identCode", value = "请求标识码", required = true),
			@ApiImplicitParam(name = "userCode", value = "用户编码", required = true),
			@ApiImplicitParam(name = "imgFile", value = "图片file流", required = true) })
	@RequestMapping(value = "/uploadHeadImg", method = RequestMethod.POST)
	public ServiceResult uploadHeadImg(HttpServletRequest request, HttpServletResponse response, String identCode, String userCode,
			MultipartFile file) {
		return fileUploadBiz.uploadHeadImg(request, response, identCode, Current.userCode.get(), file);
	}

	@ApiOperation(value = "多图片上传[file]", notes = "测试")
	@RequestMapping(value = "/uploadImages", method = RequestMethod.POST)
	public ServiceResult uploadImages(HttpServletRequest request, HttpServletResponse response, String identCode, MultipartFile[] imgFiles) {
		return fileUploadBiz.uploadImages(request, response, identCode, imgFiles);
	}

	@ApiOperation(value = "头像上传[64编码]", notes = "测试")
	@ApiImplicitParams(value = { @ApiImplicitParam(name = "identCode", value = "请求标识码", required = true),
			@ApiImplicitParam(name = "userCode", value = "用户编码", required = true),
			@ApiImplicitParam(name = "imgFileCode", value = "文件base64编码数据", required = true) })
	@RequestMapping(value = "/uploadHeadImgByCode", method = RequestMethod.POST)
	public ServiceResult uploadHeadImgByCode(HttpServletRequest request, HttpServletResponse response, String identCode, String imgFileCode,
			String userCode) {
		return fileUploadBiz.uploadHeadImgByCode(request, response, identCode, Current.userCode.get(), imgFileCode);
	}

	@ApiOperation(value = "多图片上传[64编码]", notes = "测试")
	@ApiImplicitParams(value = { @ApiImplicitParam(name = "identCode", value = "请求标识码", required = true),
			@ApiImplicitParam(name = "identCode", value = "标识码", required = true),
			@ApiImplicitParam(name = "imageBase64Dto", value = "文件base64编码数据", required = true) })
	@RequestMapping(value = "/uploadImagesByCode", method = RequestMethod.POST)
	public ServiceResult uploadImagesByCode(HttpServletRequest request, HttpServletResponse response, String identCode,
			ImageBase64Dto imageBase64Dto) {
		return fileUploadBiz.uploadImagesByCode(request, response, identCode, imageBase64Dto);
	}

	@ApiOperation(value = "获取(输出)图片流", notes = "测试")
	@ApiImplicitParams(value = { @ApiImplicitParam(name = "identCode", value = "请求标识码", required = true),
			@ApiImplicitParam(name = "fileName", value = "带后缀的文件全名", required = true) })
	@RequestMapping(value = "/getImgByName", method = RequestMethod.GET)
	public void output(HttpServletRequest request, HttpServletResponse response, String identCode, String fileName) throws IOException {
		fileUploadBiz.outputImgFile(request, response, identCode, fileName);
	}

}
