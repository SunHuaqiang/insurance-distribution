package cn.com.libertymutual.production.model.nomorcldatasource;

public class Prpditem extends PrpditemKey {
    private String itemflag;

    private String newitemcode;

    private String validstatus;

    private String flag;

    public String getItemflag() {
        return itemflag;
    }

    public void setItemflag(String itemflag) {
        this.itemflag = itemflag == null ? null : itemflag.trim();
    }

    public String getNewitemcode() {
        return newitemcode;
    }

    public void setNewitemcode(String newitemcode) {
        this.newitemcode = newitemcode == null ? null : newitemcode.trim();
    }

    public String getValidstatus() {
        return validstatus;
    }

    public void setValidstatus(String validstatus) {
        this.validstatus = validstatus == null ? null : validstatus.trim();
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag == null ? null : flag.trim();
    }
}