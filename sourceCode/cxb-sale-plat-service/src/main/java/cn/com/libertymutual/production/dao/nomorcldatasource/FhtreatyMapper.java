package cn.com.libertymutual.production.dao.nomorcldatasource;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import cn.com.libertymutual.production.model.nomorcldatasource.Fhtreaty;
import cn.com.libertymutual.production.model.nomorcldatasource.FhtreatyExample;
@Mapper
public interface FhtreatyMapper {
    int countByExample(FhtreatyExample example);

    int deleteByExample(FhtreatyExample example);

    int deleteByPrimaryKey(String treatyno);

    int insert(Fhtreaty record);

    int insertSelective(Fhtreaty record);

    List<Fhtreaty> selectByExample(FhtreatyExample example);

    Fhtreaty selectByPrimaryKey(String treatyno);

    int updateByExampleSelective(@Param("record") Fhtreaty record, @Param("example") FhtreatyExample example);

    int updateByExample(@Param("record") Fhtreaty record, @Param("example") FhtreatyExample example);

    int updateByPrimaryKeySelective(Fhtreaty record);

    int updateByPrimaryKey(Fhtreaty record);
    
    @Select(
            "SELECT *  FROM FHTREATY A WHERE A.UWYEAR = #{uwyear} AND EXISTS (SELECT 1 FROM FHSECTION B WHERE B.TREATYNO = A.TREATYNO) "
    )
    List<Fhtreaty> findAllBySectionExisted(String uwyear);
}