package cn.com.libertymutual.sp.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.collect.Maps;

import cn.com.libertymutual.core.redis.util.RedisUtils;
import cn.com.libertymutual.core.util.Constants;
import cn.com.libertymutual.sp.bean.TbSpPlan;
import cn.com.libertymutual.sp.bean.TbSysHotarea;
import cn.com.libertymutual.sp.dao.SpPlanDao;
import cn.com.libertymutual.sp.dao.TbSysHotareaDao;
import cn.com.libertymutual.sp.service.api.InitService;

@Service("initService")
public class InitServiceImpl implements InitService {

	@Resource
	TbSysHotareaDao tbSysHotareaDao;
	@Resource
	RedisUtils redisUtils;


	@Resource
	SpPlanDao spPlanDao;
	@Override
	public void initData() {
		this.initAgreementNoInto();
		this.initPlan();
	}
	
	
	public void initAgreementNoInto(){
		
		List<TbSysHotarea> numList = tbSysHotareaDao.findAllArea();
		if (null != numList && numList.size() != 0) {
			Map<String, List<TbSysHotarea>> branchMap = Maps.newHashMap();
			for (TbSysHotarea tbSysHotarea : numList) {
				List<TbSysHotarea> branchAreaList = tbSysHotareaDao.findByBranchCode(tbSysHotarea.getBranchCode());
				branchMap.put(tbSysHotarea.getBranchCode(), branchAreaList);
			}
			redisUtils.set(Constants.BRANCH_AREA_INFO, branchMap);
		}
	}
	
	
	public void initPlan(){
		Map<String, List<TbSpPlan>> planMap = Maps.newHashMap();
		List<TbSpPlan> spPlans = spPlanDao.findAllPlan();
		for (TbSpPlan plan :spPlans) {
			List<TbSpPlan> PlanOfProduct = spPlanDao.findByProductId(plan.getProductId());
			planMap.put(plan.getProductId(), PlanOfProduct);
		}
		redisUtils.set(Constants.PLAN_INFO, planMap);
	}
}
