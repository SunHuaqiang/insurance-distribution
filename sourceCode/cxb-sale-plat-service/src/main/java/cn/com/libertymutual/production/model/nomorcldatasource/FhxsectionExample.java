package cn.com.libertymutual.production.model.nomorcldatasource;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class FhxsectionExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public FhxsectionExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andTreatynoIsNull() {
            addCriterion("TREATYNO is null");
            return (Criteria) this;
        }

        public Criteria andTreatynoIsNotNull() {
            addCriterion("TREATYNO is not null");
            return (Criteria) this;
        }

        public Criteria andTreatynoEqualTo(String value) {
            addCriterion("TREATYNO =", value, "treatyno");
            return (Criteria) this;
        }

        public Criteria andTreatynoNotEqualTo(String value) {
            addCriterion("TREATYNO <>", value, "treatyno");
            return (Criteria) this;
        }

        public Criteria andTreatynoGreaterThan(String value) {
            addCriterion("TREATYNO >", value, "treatyno");
            return (Criteria) this;
        }

        public Criteria andTreatynoGreaterThanOrEqualTo(String value) {
            addCriterion("TREATYNO >=", value, "treatyno");
            return (Criteria) this;
        }

        public Criteria andTreatynoLessThan(String value) {
            addCriterion("TREATYNO <", value, "treatyno");
            return (Criteria) this;
        }

        public Criteria andTreatynoLessThanOrEqualTo(String value) {
            addCriterion("TREATYNO <=", value, "treatyno");
            return (Criteria) this;
        }

        public Criteria andTreatynoLike(String value) {
            addCriterion("TREATYNO like", value, "treatyno");
            return (Criteria) this;
        }

        public Criteria andTreatynoNotLike(String value) {
            addCriterion("TREATYNO not like", value, "treatyno");
            return (Criteria) this;
        }

        public Criteria andTreatynoIn(List<String> values) {
            addCriterion("TREATYNO in", values, "treatyno");
            return (Criteria) this;
        }

        public Criteria andTreatynoNotIn(List<String> values) {
            addCriterion("TREATYNO not in", values, "treatyno");
            return (Criteria) this;
        }

        public Criteria andTreatynoBetween(String value1, String value2) {
            addCriterion("TREATYNO between", value1, value2, "treatyno");
            return (Criteria) this;
        }

        public Criteria andTreatynoNotBetween(String value1, String value2) {
            addCriterion("TREATYNO not between", value1, value2, "treatyno");
            return (Criteria) this;
        }

        public Criteria andLayernoIsNull() {
            addCriterion("LAYERNO is null");
            return (Criteria) this;
        }

        public Criteria andLayernoIsNotNull() {
            addCriterion("LAYERNO is not null");
            return (Criteria) this;
        }

        public Criteria andLayernoEqualTo(Short value) {
            addCriterion("LAYERNO =", value, "layerno");
            return (Criteria) this;
        }

        public Criteria andLayernoNotEqualTo(Short value) {
            addCriterion("LAYERNO <>", value, "layerno");
            return (Criteria) this;
        }

        public Criteria andLayernoGreaterThan(Short value) {
            addCriterion("LAYERNO >", value, "layerno");
            return (Criteria) this;
        }

        public Criteria andLayernoGreaterThanOrEqualTo(Short value) {
            addCriterion("LAYERNO >=", value, "layerno");
            return (Criteria) this;
        }

        public Criteria andLayernoLessThan(Short value) {
            addCriterion("LAYERNO <", value, "layerno");
            return (Criteria) this;
        }

        public Criteria andLayernoLessThanOrEqualTo(Short value) {
            addCriterion("LAYERNO <=", value, "layerno");
            return (Criteria) this;
        }

        public Criteria andLayernoIn(List<Short> values) {
            addCriterion("LAYERNO in", values, "layerno");
            return (Criteria) this;
        }

        public Criteria andLayernoNotIn(List<Short> values) {
            addCriterion("LAYERNO not in", values, "layerno");
            return (Criteria) this;
        }

        public Criteria andLayernoBetween(Short value1, Short value2) {
            addCriterion("LAYERNO between", value1, value2, "layerno");
            return (Criteria) this;
        }

        public Criteria andLayernoNotBetween(Short value1, Short value2) {
            addCriterion("LAYERNO not between", value1, value2, "layerno");
            return (Criteria) this;
        }

        public Criteria andSectionnoIsNull() {
            addCriterion("SECTIONNO is null");
            return (Criteria) this;
        }

        public Criteria andSectionnoIsNotNull() {
            addCriterion("SECTIONNO is not null");
            return (Criteria) this;
        }

        public Criteria andSectionnoEqualTo(String value) {
            addCriterion("SECTIONNO =", value, "sectionno");
            return (Criteria) this;
        }

        public Criteria andSectionnoNotEqualTo(String value) {
            addCriterion("SECTIONNO <>", value, "sectionno");
            return (Criteria) this;
        }

        public Criteria andSectionnoGreaterThan(String value) {
            addCriterion("SECTIONNO >", value, "sectionno");
            return (Criteria) this;
        }

        public Criteria andSectionnoGreaterThanOrEqualTo(String value) {
            addCriterion("SECTIONNO >=", value, "sectionno");
            return (Criteria) this;
        }

        public Criteria andSectionnoLessThan(String value) {
            addCriterion("SECTIONNO <", value, "sectionno");
            return (Criteria) this;
        }

        public Criteria andSectionnoLessThanOrEqualTo(String value) {
            addCriterion("SECTIONNO <=", value, "sectionno");
            return (Criteria) this;
        }

        public Criteria andSectionnoLike(String value) {
            addCriterion("SECTIONNO like", value, "sectionno");
            return (Criteria) this;
        }

        public Criteria andSectionnoNotLike(String value) {
            addCriterion("SECTIONNO not like", value, "sectionno");
            return (Criteria) this;
        }

        public Criteria andSectionnoIn(List<String> values) {
            addCriterion("SECTIONNO in", values, "sectionno");
            return (Criteria) this;
        }

        public Criteria andSectionnoNotIn(List<String> values) {
            addCriterion("SECTIONNO not in", values, "sectionno");
            return (Criteria) this;
        }

        public Criteria andSectionnoBetween(String value1, String value2) {
            addCriterion("SECTIONNO between", value1, value2, "sectionno");
            return (Criteria) this;
        }

        public Criteria andSectionnoNotBetween(String value1, String value2) {
            addCriterion("SECTIONNO not between", value1, value2, "sectionno");
            return (Criteria) this;
        }

        public Criteria andSectioncdescIsNull() {
            addCriterion("SECTIONCDESC is null");
            return (Criteria) this;
        }

        public Criteria andSectioncdescIsNotNull() {
            addCriterion("SECTIONCDESC is not null");
            return (Criteria) this;
        }

        public Criteria andSectioncdescEqualTo(String value) {
            addCriterion("SECTIONCDESC =", value, "sectioncdesc");
            return (Criteria) this;
        }

        public Criteria andSectioncdescNotEqualTo(String value) {
            addCriterion("SECTIONCDESC <>", value, "sectioncdesc");
            return (Criteria) this;
        }

        public Criteria andSectioncdescGreaterThan(String value) {
            addCriterion("SECTIONCDESC >", value, "sectioncdesc");
            return (Criteria) this;
        }

        public Criteria andSectioncdescGreaterThanOrEqualTo(String value) {
            addCriterion("SECTIONCDESC >=", value, "sectioncdesc");
            return (Criteria) this;
        }

        public Criteria andSectioncdescLessThan(String value) {
            addCriterion("SECTIONCDESC <", value, "sectioncdesc");
            return (Criteria) this;
        }

        public Criteria andSectioncdescLessThanOrEqualTo(String value) {
            addCriterion("SECTIONCDESC <=", value, "sectioncdesc");
            return (Criteria) this;
        }

        public Criteria andSectioncdescLike(String value) {
            addCriterion("SECTIONCDESC like", value, "sectioncdesc");
            return (Criteria) this;
        }

        public Criteria andSectioncdescNotLike(String value) {
            addCriterion("SECTIONCDESC not like", value, "sectioncdesc");
            return (Criteria) this;
        }

        public Criteria andSectioncdescIn(List<String> values) {
            addCriterion("SECTIONCDESC in", values, "sectioncdesc");
            return (Criteria) this;
        }

        public Criteria andSectioncdescNotIn(List<String> values) {
            addCriterion("SECTIONCDESC not in", values, "sectioncdesc");
            return (Criteria) this;
        }

        public Criteria andSectioncdescBetween(String value1, String value2) {
            addCriterion("SECTIONCDESC between", value1, value2, "sectioncdesc");
            return (Criteria) this;
        }

        public Criteria andSectioncdescNotBetween(String value1, String value2) {
            addCriterion("SECTIONCDESC not between", value1, value2, "sectioncdesc");
            return (Criteria) this;
        }

        public Criteria andSectionedescIsNull() {
            addCriterion("SECTIONEDESC is null");
            return (Criteria) this;
        }

        public Criteria andSectionedescIsNotNull() {
            addCriterion("SECTIONEDESC is not null");
            return (Criteria) this;
        }

        public Criteria andSectionedescEqualTo(String value) {
            addCriterion("SECTIONEDESC =", value, "sectionedesc");
            return (Criteria) this;
        }

        public Criteria andSectionedescNotEqualTo(String value) {
            addCriterion("SECTIONEDESC <>", value, "sectionedesc");
            return (Criteria) this;
        }

        public Criteria andSectionedescGreaterThan(String value) {
            addCriterion("SECTIONEDESC >", value, "sectionedesc");
            return (Criteria) this;
        }

        public Criteria andSectionedescGreaterThanOrEqualTo(String value) {
            addCriterion("SECTIONEDESC >=", value, "sectionedesc");
            return (Criteria) this;
        }

        public Criteria andSectionedescLessThan(String value) {
            addCriterion("SECTIONEDESC <", value, "sectionedesc");
            return (Criteria) this;
        }

        public Criteria andSectionedescLessThanOrEqualTo(String value) {
            addCriterion("SECTIONEDESC <=", value, "sectionedesc");
            return (Criteria) this;
        }

        public Criteria andSectionedescLike(String value) {
            addCriterion("SECTIONEDESC like", value, "sectionedesc");
            return (Criteria) this;
        }

        public Criteria andSectionedescNotLike(String value) {
            addCriterion("SECTIONEDESC not like", value, "sectionedesc");
            return (Criteria) this;
        }

        public Criteria andSectionedescIn(List<String> values) {
            addCriterion("SECTIONEDESC in", values, "sectionedesc");
            return (Criteria) this;
        }

        public Criteria andSectionedescNotIn(List<String> values) {
            addCriterion("SECTIONEDESC not in", values, "sectionedesc");
            return (Criteria) this;
        }

        public Criteria andSectionedescBetween(String value1, String value2) {
            addCriterion("SECTIONEDESC between", value1, value2, "sectionedesc");
            return (Criteria) this;
        }

        public Criteria andSectionedescNotBetween(String value1, String value2) {
            addCriterion("SECTIONEDESC not between", value1, value2, "sectionedesc");
            return (Criteria) this;
        }

        public Criteria andGnpiIsNull() {
            addCriterion("GNPI is null");
            return (Criteria) this;
        }

        public Criteria andGnpiIsNotNull() {
            addCriterion("GNPI is not null");
            return (Criteria) this;
        }

        public Criteria andGnpiEqualTo(BigDecimal value) {
            addCriterion("GNPI =", value, "gnpi");
            return (Criteria) this;
        }

        public Criteria andGnpiNotEqualTo(BigDecimal value) {
            addCriterion("GNPI <>", value, "gnpi");
            return (Criteria) this;
        }

        public Criteria andGnpiGreaterThan(BigDecimal value) {
            addCriterion("GNPI >", value, "gnpi");
            return (Criteria) this;
        }

        public Criteria andGnpiGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("GNPI >=", value, "gnpi");
            return (Criteria) this;
        }

        public Criteria andGnpiLessThan(BigDecimal value) {
            addCriterion("GNPI <", value, "gnpi");
            return (Criteria) this;
        }

        public Criteria andGnpiLessThanOrEqualTo(BigDecimal value) {
            addCriterion("GNPI <=", value, "gnpi");
            return (Criteria) this;
        }

        public Criteria andGnpiIn(List<BigDecimal> values) {
            addCriterion("GNPI in", values, "gnpi");
            return (Criteria) this;
        }

        public Criteria andGnpiNotIn(List<BigDecimal> values) {
            addCriterion("GNPI not in", values, "gnpi");
            return (Criteria) this;
        }

        public Criteria andGnpiBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("GNPI between", value1, value2, "gnpi");
            return (Criteria) this;
        }

        public Criteria andGnpiNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("GNPI not between", value1, value2, "gnpi");
            return (Criteria) this;
        }

        public Criteria andExcesslossIsNull() {
            addCriterion("EXCESSLOSS is null");
            return (Criteria) this;
        }

        public Criteria andExcesslossIsNotNull() {
            addCriterion("EXCESSLOSS is not null");
            return (Criteria) this;
        }

        public Criteria andExcesslossEqualTo(BigDecimal value) {
            addCriterion("EXCESSLOSS =", value, "excessloss");
            return (Criteria) this;
        }

        public Criteria andExcesslossNotEqualTo(BigDecimal value) {
            addCriterion("EXCESSLOSS <>", value, "excessloss");
            return (Criteria) this;
        }

        public Criteria andExcesslossGreaterThan(BigDecimal value) {
            addCriterion("EXCESSLOSS >", value, "excessloss");
            return (Criteria) this;
        }

        public Criteria andExcesslossGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("EXCESSLOSS >=", value, "excessloss");
            return (Criteria) this;
        }

        public Criteria andExcesslossLessThan(BigDecimal value) {
            addCriterion("EXCESSLOSS <", value, "excessloss");
            return (Criteria) this;
        }

        public Criteria andExcesslossLessThanOrEqualTo(BigDecimal value) {
            addCriterion("EXCESSLOSS <=", value, "excessloss");
            return (Criteria) this;
        }

        public Criteria andExcesslossIn(List<BigDecimal> values) {
            addCriterion("EXCESSLOSS in", values, "excessloss");
            return (Criteria) this;
        }

        public Criteria andExcesslossNotIn(List<BigDecimal> values) {
            addCriterion("EXCESSLOSS not in", values, "excessloss");
            return (Criteria) this;
        }

        public Criteria andExcesslossBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("EXCESSLOSS between", value1, value2, "excessloss");
            return (Criteria) this;
        }

        public Criteria andExcesslossNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("EXCESSLOSS not between", value1, value2, "excessloss");
            return (Criteria) this;
        }

        public Criteria andSectionquotaIsNull() {
            addCriterion("SECTIONQUOTA is null");
            return (Criteria) this;
        }

        public Criteria andSectionquotaIsNotNull() {
            addCriterion("SECTIONQUOTA is not null");
            return (Criteria) this;
        }

        public Criteria andSectionquotaEqualTo(BigDecimal value) {
            addCriterion("SECTIONQUOTA =", value, "sectionquota");
            return (Criteria) this;
        }

        public Criteria andSectionquotaNotEqualTo(BigDecimal value) {
            addCriterion("SECTIONQUOTA <>", value, "sectionquota");
            return (Criteria) this;
        }

        public Criteria andSectionquotaGreaterThan(BigDecimal value) {
            addCriterion("SECTIONQUOTA >", value, "sectionquota");
            return (Criteria) this;
        }

        public Criteria andSectionquotaGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("SECTIONQUOTA >=", value, "sectionquota");
            return (Criteria) this;
        }

        public Criteria andSectionquotaLessThan(BigDecimal value) {
            addCriterion("SECTIONQUOTA <", value, "sectionquota");
            return (Criteria) this;
        }

        public Criteria andSectionquotaLessThanOrEqualTo(BigDecimal value) {
            addCriterion("SECTIONQUOTA <=", value, "sectionquota");
            return (Criteria) this;
        }

        public Criteria andSectionquotaIn(List<BigDecimal> values) {
            addCriterion("SECTIONQUOTA in", values, "sectionquota");
            return (Criteria) this;
        }

        public Criteria andSectionquotaNotIn(List<BigDecimal> values) {
            addCriterion("SECTIONQUOTA not in", values, "sectionquota");
            return (Criteria) this;
        }

        public Criteria andSectionquotaBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("SECTIONQUOTA between", value1, value2, "sectionquota");
            return (Criteria) this;
        }

        public Criteria andSectionquotaNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("SECTIONQUOTA not between", value1, value2, "sectionquota");
            return (Criteria) this;
        }

        public Criteria andExpirelossIsNull() {
            addCriterion("EXPIRELOSS is null");
            return (Criteria) this;
        }

        public Criteria andExpirelossIsNotNull() {
            addCriterion("EXPIRELOSS is not null");
            return (Criteria) this;
        }

        public Criteria andExpirelossEqualTo(BigDecimal value) {
            addCriterion("EXPIRELOSS =", value, "expireloss");
            return (Criteria) this;
        }

        public Criteria andExpirelossNotEqualTo(BigDecimal value) {
            addCriterion("EXPIRELOSS <>", value, "expireloss");
            return (Criteria) this;
        }

        public Criteria andExpirelossGreaterThan(BigDecimal value) {
            addCriterion("EXPIRELOSS >", value, "expireloss");
            return (Criteria) this;
        }

        public Criteria andExpirelossGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("EXPIRELOSS >=", value, "expireloss");
            return (Criteria) this;
        }

        public Criteria andExpirelossLessThan(BigDecimal value) {
            addCriterion("EXPIRELOSS <", value, "expireloss");
            return (Criteria) this;
        }

        public Criteria andExpirelossLessThanOrEqualTo(BigDecimal value) {
            addCriterion("EXPIRELOSS <=", value, "expireloss");
            return (Criteria) this;
        }

        public Criteria andExpirelossIn(List<BigDecimal> values) {
            addCriterion("EXPIRELOSS in", values, "expireloss");
            return (Criteria) this;
        }

        public Criteria andExpirelossNotIn(List<BigDecimal> values) {
            addCriterion("EXPIRELOSS not in", values, "expireloss");
            return (Criteria) this;
        }

        public Criteria andExpirelossBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("EXPIRELOSS between", value1, value2, "expireloss");
            return (Criteria) this;
        }

        public Criteria andExpirelossNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("EXPIRELOSS not between", value1, value2, "expireloss");
            return (Criteria) this;
        }

        public Criteria andExconamountIsNull() {
            addCriterion("EXCONAMOUNT is null");
            return (Criteria) this;
        }

        public Criteria andExconamountIsNotNull() {
            addCriterion("EXCONAMOUNT is not null");
            return (Criteria) this;
        }

        public Criteria andExconamountEqualTo(BigDecimal value) {
            addCriterion("EXCONAMOUNT =", value, "exconamount");
            return (Criteria) this;
        }

        public Criteria andExconamountNotEqualTo(BigDecimal value) {
            addCriterion("EXCONAMOUNT <>", value, "exconamount");
            return (Criteria) this;
        }

        public Criteria andExconamountGreaterThan(BigDecimal value) {
            addCriterion("EXCONAMOUNT >", value, "exconamount");
            return (Criteria) this;
        }

        public Criteria andExconamountGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("EXCONAMOUNT >=", value, "exconamount");
            return (Criteria) this;
        }

        public Criteria andExconamountLessThan(BigDecimal value) {
            addCriterion("EXCONAMOUNT <", value, "exconamount");
            return (Criteria) this;
        }

        public Criteria andExconamountLessThanOrEqualTo(BigDecimal value) {
            addCriterion("EXCONAMOUNT <=", value, "exconamount");
            return (Criteria) this;
        }

        public Criteria andExconamountIn(List<BigDecimal> values) {
            addCriterion("EXCONAMOUNT in", values, "exconamount");
            return (Criteria) this;
        }

        public Criteria andExconamountNotIn(List<BigDecimal> values) {
            addCriterion("EXCONAMOUNT not in", values, "exconamount");
            return (Criteria) this;
        }

        public Criteria andExconamountBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("EXCONAMOUNT between", value1, value2, "exconamount");
            return (Criteria) this;
        }

        public Criteria andExconamountNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("EXCONAMOUNT not between", value1, value2, "exconamount");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}