package cn.com.libertymutual.production.pojo.request;

import java.util.List;

import cn.com.libertymutual.production.model.nomorcldatasource.Prpdplanprops;
import cn.com.libertymutual.production.model.nomorcldatasource.Prpdriskplan;

public class PrpdPlanPropsRequest extends Request {

	private Prpdriskplan plan;
	private List<Prpdplanprops> props;

	public List<Prpdplanprops> getProps() {
		return props;
	}

	public void setProps(List<Prpdplanprops> props) {
		this.props = props;
	}

	public Prpdriskplan getPlan() {
		return plan;
	}

	public void setPlan(Prpdriskplan plan) {
		this.plan = plan;
	}

}
