package cn.com.libertymutual.sp.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import cn.com.libertymutual.sp.bean.TbSpSaveData;

@Repository
public interface SaveDataDao extends PagingAndSortingRepository<TbSpSaveData, Integer>, JpaSpecificationExecutor<TbSpSaveData> {

	@Query("from TbSpSaveData where userCode =?1")
	List<TbSpSaveData> findUserCode(String userCode);

	@Query("from TbSpSaveData where id =?1")
	TbSpSaveData findSaveDataId(Integer id);

}
