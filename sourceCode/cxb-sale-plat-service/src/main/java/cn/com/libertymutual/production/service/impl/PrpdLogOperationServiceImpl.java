package cn.com.libertymutual.production.service.impl;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.com.libertymutual.production.dao.nomorcldatasource.PrpdlogoperationMapper;
import cn.com.libertymutual.production.model.nomorcldatasource.Prpdlogoperation;
import cn.com.libertymutual.production.model.nomorcldatasource.PrpdlogoperationExample;
import cn.com.libertymutual.production.service.api.PrpdLogOperationService;

@Service
public class PrpdLogOperationServiceImpl implements PrpdLogOperationService {

	@Autowired
	private PrpdlogoperationMapper prpdlogoperationMapper;
	
	@Override
	public void insert(Prpdlogoperation record) {
		prpdlogoperationMapper.insertSelective(record);
	}

	@Override
	public Prpdlogoperation findLogDetail(String logId) {
		PrpdlogoperationExample example = new PrpdlogoperationExample();
		example.createCriteria().andLogidEqualTo(new Long(logId));
		List<Prpdlogoperation> logDetails = prpdlogoperationMapper.selectByExampleWithBLOBs(example );
		if (!logDetails.isEmpty()) {
			return logDetails.get(0);
		}
		return null;
	}

}
