package cn.com.libertymutual.sp.service.api;

import cn.com.libertymutual.core.web.ServiceResult;
import cn.com.libertymutual.sp.bean.TbSpPoster;

public interface PosterService {

	ServiceResult posterList(String productName, String riskCode, String name,String type,String status,int pageNumber, int pageSize);

	ServiceResult addOrUpdatePoster(TbSpPoster tbSpPoster);

	ServiceResult approve(String approveType,Integer id,String type,String remark);

	ServiceResult approveHistory(String type, Integer id,Integer pageNumber,Integer pageSize);

	ServiceResult allPoster(String branchCode, String name, String type, Integer pageNumber, Integer pageSize,String userCode);

	ServiceResult findPoster(Integer id);

	ServiceResult waitAapprove(String approveType, String status, Integer pageNumber, Integer pageSize);

}
