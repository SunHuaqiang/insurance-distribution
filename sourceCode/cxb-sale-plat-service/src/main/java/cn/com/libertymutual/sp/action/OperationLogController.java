package cn.com.libertymutual.sp.action;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import cn.com.libertymutual.core.util.Current;
import cn.com.libertymutual.core.web.ServiceResult;
import cn.com.libertymutual.sp.service.api.OperationLogService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(value = "/admin/log")
public class OperationLogController {

	private Logger log = LoggerFactory.getLogger(getClass());

	@Autowired
	OperationLogService operationLogService;

	@ApiOperation(value = "日志列表", notes = "日志列表")
	@ApiImplicitParams(value = { @ApiImplicitParam(name = "pageNumber", value = "分页", required = true, paramType = "query", dataType = "Long"),
			@ApiImplicitParam(name = "pageSize", value = "分页", required = true, paramType = "query", dataType = "Long"), })
	@RequestMapping(value = "/logList", method = RequestMethod.POST)
	public ServiceResult logList(int pageNumber, int pageSize) throws Exception {
		ServiceResult sr = new ServiceResult();
		sr = operationLogService.logList(pageNumber, pageSize);
		return sr;
	}

	@ApiOperation(value = "分类查询日志", notes = "分类查询")
	@ApiImplicitParams(value = { @ApiImplicitParam(name = "userId", value = "用户名", required = true, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "level", value = "风险等级", required = true, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "startTime", value = "操作时间", required = true, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "endTime", value = "操作时间", required = true, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "pageNumber", value = "分页", required = true, paramType = "query", dataType = "Long"),
			@ApiImplicitParam(name = "pageSize", value = "分页", required = true, paramType = "query", dataType = "Long"), })
	@RequestMapping(value = "/queryLog", method = RequestMethod.POST)
	public ServiceResult queryLog(String userId, String level, String startTime, String endTime, int pageNumber, int pageSize) throws Exception {
		ServiceResult sr = new ServiceResult();
		sr = operationLogService.queryLog(userId, level, startTime, endTime, pageNumber, pageSize);
		return sr;
	}

	/*
	 * 
	 * 查询前端操作日志
	 */

	@ApiOperation(value = "分类查询前端日志", notes = "分类查询")
	@ApiImplicitParams(value = { @ApiImplicitParam(name = "requestData", value = "请求数据", required = true, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "responseData", value = "响应数据", required = true, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "startTime", value = "操作时间", required = true, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "endTime", value = "操作时间", required = true, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "userCode", value = "用户编码", required = true, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "mark", value = "标识", required = true, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "operation", value = "操作", required = true, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "pageNumber", value = "分页", required = true, paramType = "query", dataType = "Long"),
			@ApiImplicitParam(name = "pageSize", value = "分页", required = true, paramType = "query", dataType = "Long"), })
	@RequestMapping(value = "/querySaleLog", method = RequestMethod.POST)
	public ServiceResult querySaleLog(String requestData, String responseData, String startTime, String endTime, String userCode, String mark,
			String operation, int pageNumber, int pageSize) throws Exception {
		ServiceResult sr = new ServiceResult();
		sr = operationLogService.querySaleLog(requestData, responseData, startTime, endTime, userCode, mark, operation, pageNumber,
				pageSize);
		return sr;
	}

	/*
	 * 删除前端操作日志
	 */

	@ApiOperation(value = "删除前端操作日志", notes = "删除前端操作日志")
	@ApiImplicitParams(value = { @ApiImplicitParam(name = "id", value = "id", required = true, paramType = "query", dataType = "Long"), })
	@RequestMapping(value = "/deleteSaleLog", method = RequestMethod.POST)
	public ServiceResult deleteSaleLog(Integer id) throws Exception {
		ServiceResult sr = new ServiceResult();
		sr = operationLogService.deleteSaleLog(id);
		return sr;
	}

}
