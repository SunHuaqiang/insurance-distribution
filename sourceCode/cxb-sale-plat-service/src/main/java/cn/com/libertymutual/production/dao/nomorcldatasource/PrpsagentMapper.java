package cn.com.libertymutual.production.dao.nomorcldatasource;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import cn.com.libertymutual.production.model.nomorcldatasource.Prpsagent;
import cn.com.libertymutual.production.model.nomorcldatasource.PrpsagentExample;
import cn.com.libertymutual.production.model.nomorcldatasource.PrpsagentWithBLOBs;
@Mapper
public interface PrpsagentMapper {
    int countByExample(PrpsagentExample example);

    int deleteByExample(PrpsagentExample example);

    int deleteByPrimaryKey(String agentcode);

    int insert(PrpsagentWithBLOBs record);

    int insertSelective(PrpsagentWithBLOBs record);

    List<PrpsagentWithBLOBs> selectByExampleWithBLOBs(PrpsagentExample example);

    List<Prpsagent> selectByExample(PrpsagentExample example);

    PrpsagentWithBLOBs selectByPrimaryKey(String agentcode);

    int updateByExampleSelective(@Param("record") PrpsagentWithBLOBs record, @Param("example") PrpsagentExample example);

    int updateByExampleWithBLOBs(@Param("record") PrpsagentWithBLOBs record, @Param("example") PrpsagentExample example);

    int updateByExample(@Param("record") Prpsagent record, @Param("example") PrpsagentExample example);

    int updateByPrimaryKeySelective(PrpsagentWithBLOBs record);

    int updateByPrimaryKeyWithBLOBs(PrpsagentWithBLOBs record);

    int updateByPrimaryKey(Prpsagent record);
    
    @Select(
    		" SELECT C.* "
    				+ "  FROM (SELECT A.COMCODE "
    				+ "          FROM PRPDCOMPANY A "
    				+ "         WHERE A.VALIDSTATUS = '1' "
    				+ "         START WITH A.COMCODE = #{comcode} "
    				+ "        CONNECT BY PRIOR A.COMCODE = A.UPPERCOMCODE) B "
    				+ "  LEFT JOIN PRPSAGENT C "
    				+ "    ON C.COMCODE = B.COMCODE "
    				+ " WHERE C.VALIDSTATUS = '1' "
    				+ " ORDER BY C.AGENTCODE "
    		)
    List<PrpsagentWithBLOBs> selectWithComcode(@Param("comcode") String comcode);
}