
package cn.com.libertymutual.sp.webService.allpolicy;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for policyListQuery complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="policyListQuery">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="policyListQueryRequestDto" type="{http://prpall.liberty.com/all/cb/policyListQuery/bean}policyListQueryRequestDto" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "policyListQuery", namespace = "http://prpall.liberty.com/all/cb/policyListQuery/intf", propOrder = {
    "policyListQueryRequestDto"
})
public class PolicyListQuery {

    protected PolicyListQueryRequestDto policyListQueryRequestDto;

    /**
     * Gets the value of the policyListQueryRequestDto property.
     * 
     * @return
     *     possible object is
     *     {@link PolicyListQueryRequestDto }
     *     
     */
    public PolicyListQueryRequestDto getPolicyListQueryRequestDto() {
        return policyListQueryRequestDto;
    }

    /**
     * Sets the value of the policyListQueryRequestDto property.
     * 
     * @param value
     *     allowed object is
     *     {@link PolicyListQueryRequestDto }
     *     
     */
    public void setPolicyListQueryRequestDto(PolicyListQueryRequestDto value) {
        this.policyListQueryRequestDto = value;
    }

}
