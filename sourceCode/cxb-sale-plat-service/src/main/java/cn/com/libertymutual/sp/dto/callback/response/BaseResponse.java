package cn.com.libertymutual.sp.dto.callback.response;

public class BaseResponse {
	private String key;
	private String 	status;
	private ServiceRes mqreqsult;
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public ServiceRes getMqreqsult() {
		return mqreqsult;
	}
	public void setMqreqsult(ServiceRes mqreqsult) {
		this.mqreqsult = mqreqsult;
	}
	
	
	
}
