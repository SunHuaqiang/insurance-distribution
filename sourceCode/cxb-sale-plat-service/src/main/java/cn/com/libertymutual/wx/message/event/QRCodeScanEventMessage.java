package cn.com.libertymutual.wx.message.event;

public class QRCodeScanEventMessage extends BaseEventMessage {
	private String EventKey;
	private String Ticket;

	public String getEventKey() {
		return this.EventKey;
	}

	public void setEventKey(String eventKey) {
		this.EventKey = eventKey;
	}

	public String getTicket() {
		return this.Ticket;
	}

	public void setTicket(String ticket) {
		this.Ticket = ticket;
	}
}
