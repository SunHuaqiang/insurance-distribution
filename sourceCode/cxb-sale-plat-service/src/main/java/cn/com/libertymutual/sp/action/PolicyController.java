package cn.com.libertymutual.sp.action;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import cn.com.libertymutual.core.util.StringUtil;
import cn.com.libertymutual.core.web.ServiceResult;
import cn.com.libertymutual.sp.dto.TQueryPolicyResponseDto;
import cn.com.libertymutual.sp.service.api.PolicyService;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(value = "/nol/policy")
public class PolicyController {
	private Logger log = LoggerFactory.getLogger(getClass());
	
	@Autowired PolicyService policyService;
	
	@ApiOperation(value = "通过保单号查询详情", notes = "通过保单号查询详情")
    @RequestMapping(value = "/queryPolicyDetail", method = RequestMethod.POST)
	public ServiceResult queryPolicyDetail(HttpServletRequest request, HttpServletResponse response,  String policyNo)
			throws Exception {
		ServiceResult rs=new  ServiceResult();
		//policyNo="8805081100170000147000";
		if(StringUtil.isEmpty(policyNo)){
			rs.setFail();
			rs.setResult("保单号不能为空！");
			return rs;
		}
		Map<String, Object> map=new HashMap<String, Object>();
		if("05".equals(policyNo.substring(2, 4))){//车险
			 map=policyService.doCxfQueryCarPolicy(policyNo);
		}else{
			map=policyService.doCxfQueryNonCarPolicy(policyNo);
		}
		rs.setResult(map);
		rs.setSuccess();
        return rs;
    }
	@ApiOperation(value = "查询详情", notes = "通过业务号查询详情")
    @RequestMapping(value = "/queryDetail", method = RequestMethod.POST)
	public TQueryPolicyResponseDto queryDetail(HttpServletRequest request, HttpServletResponse response,  String policyNo)
			throws Exception {
		TQueryPolicyResponseDto rs=new  TQueryPolicyResponseDto();
		//policyNo="8805081100170000147000";
		if(StringUtil.isEmpty(policyNo)){
			rs.setStatus(false);
			rs.setResultMessage("业务号不能为空！");
			return rs;
		}
		 rs=policyService.queryDetail(policyNo);
		rs.setStatus(false);
        return rs;
    }
	
	
	
	
	
}
