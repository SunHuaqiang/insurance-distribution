package cn.com.libertymutual.production.model.nomorcldatasource;

import java.math.BigDecimal;

public class FhxlayerKey {
    private String treatyno;

    private BigDecimal layerno;

    public String getTreatyno() {
        return treatyno;
    }

    public void setTreatyno(String treatyno) {
        this.treatyno = treatyno == null ? null : treatyno.trim();
    }

    public BigDecimal getLayerno() {
        return layerno;
    }

    public void setLayerno(BigDecimal layerno) {
        this.layerno = layerno;
    }
}