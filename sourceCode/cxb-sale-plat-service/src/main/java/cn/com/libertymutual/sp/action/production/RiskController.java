package cn.com.libertymutual.sp.action.production;

import cn.com.libertymutual.production.pojo.request.LinkKindRequest;
import cn.com.libertymutual.production.pojo.request.PrpdRiskRequest;
import cn.com.libertymutual.production.pojo.response.Response;
import cn.com.libertymutual.production.service.api.business.RiskBusinessService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/** 
 * @author Steven.Li
 * @date 2017年7月28日
 *  
 */
@RestController
@RequestMapping("risk")
public class RiskController {
	
	@Autowired
	protected RiskBusinessService riskBusinessService;

	@RequestMapping("checkRiskCodeUsed")
	public Response checkRiskCodeUsed(String riskcode) {
		Response result = new Response();
		result = riskBusinessService.checkRiskCodeUsed(riskcode);
		return result;
	}
	
	/**
	 * 新增险种
	 * @param request
	 * @return
	 */
	@RequestMapping("addPrpdRisk")
	public Response addPrpdRisk(@RequestBody PrpdRiskRequest request) {
		Response result = new Response();
		try {
			result = riskBusinessService.insert(request);
			result.setSuccess();
		} catch (Exception e) {
			result.setAppFail();
			result.setResult(e.getMessage());
		}
		return result;
	}
	
	/**
	 * 修改险种
	 * @param request
	 * @return
	 */
	@RequestMapping("updatePrpdRisk")
	public Response updatePrpdRisk(PrpdRiskRequest request) {
		Response result = new Response();
		try {
			result = riskBusinessService.update(request);
			result.setSuccess();
		} catch (Exception e) {
			result.setAppFail();
			result.setResult(e.getMessage());
		}
		return result;
	}
	
	/**
	 * 险种关联条款
	 * @param request
	 * @return
	 */
	@RequestMapping("linkKind")
	public Response linkKind(@RequestBody LinkKindRequest request) {
		Response result = new Response();
		try {
			result = riskBusinessService.linkKind(request);
			result.setSuccess();
		} catch (Exception e) {
			result.setAppFail();
			result.setResult(e.getMessage());
		}
		return result;
	}

	/**
	 * 查询合约
	 * @return
     */
	@RequestMapping("queryAllFhtreaty")
	public Response queryAllFhtreaty() {
		Response result = riskBusinessService.queryAllFhtreaty();
		return result;
	}

	/**
	 * 查询合约节
	 * @param treatyno
	 * @return
     */
	@RequestMapping("queryFhsections")
	public Response queryFhsections(String treatyno) {
		Response result = riskBusinessService.queryFhsections(treatyno);
		return result;
	}

	/**
	 * 查询合约除外责任
	 * @param treatyno
	 * @param sectionno
     * @return
     */
	@RequestMapping("queryFhexitemkinds")
	public Response queryFhexitemkinds(String treatyno, String sectionno) {
		Response result = riskBusinessService.queryFhexitemkinds(treatyno, sectionno);
		return result;
	}

	/**
	 * 查询超赔合约
	 * @return
     */
	@RequestMapping("queryAllFhxtreaty")
	public Response queryAllFhxtreaty() {
		Response result = riskBusinessService.queryAllFhxtreaty();
		return result;
	}

	/**
	 * 查询超赔合约层
	 * @param treatyno
	 * @return
     */
	@RequestMapping("queryFhxlayers")
	public Response queryFhxlayers(String treatyno) {
		Response result = riskBusinessService.queryFhxlayers(treatyno);
		return result;
	}

	/**
	 * 查询超赔合约节
	 * @param treatyno
	 * @param layerno
     * @return
     */
	@RequestMapping("queryFhxsections")
	public Response queryFhxsections(String treatyno, String layerno) {
		Response result = riskBusinessService.queryFhxsections(treatyno, layerno);
		return result;
	}

	/**
	 * 查询再保方式
	 * @param riskcode
	 * @return
	 */
	@RequestMapping("queryFdriskconfigByRisk")
	public Response queryFdriskconfigByRisk(String riskcode) {
		Response result = riskBusinessService.queryFdriskconfigByRisk(riskcode);
		return result;
	}

	/**
	 * 查询合约配置
	 * @param riskcode
	 * @return
	 */
	@RequestMapping("queryFhriskByRisk")
	public Response queryFhriskByRisk(String riskcode) {
		Response result = riskBusinessService.queryFhriskByRisk(riskcode);
		return result;
	}

	/**
	 * 查询除外合约条件配置
	 * @param riskcode
	 * @return
	 */
	@RequestMapping("queryFhexitemkindByRisk")
	public Response queryFhexitemkindByRisk(String riskcode) {
		Response result = riskBusinessService.queryFhexitemkindByRisk(riskcode);
		return result;
	}

	/**
	 * 查询超赔合约配置
	 * @param riskcode
	 * @return
	 */
	@RequestMapping("queryFhxriskByRisk")
	public Response queryFhxriskByRisk(String riskcode) {
		Response result = riskBusinessService.queryFhxriskByRisk(riskcode);
		return result;
	}

	/**
	 * 查询险种
	 * @return
	 */
	@RequestMapping("queryRiskByCodes")
	public Response queryRiskByCodes(@RequestParam(value = "riskcodes[]") List<String> riskcodes) {
		Response result = riskBusinessService.queryRiskByCodes(riskcodes);
		return result;
	}
}
