package cn.com.libertymutual.saleplat.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration("restTemplateProperties")
@ConfigurationProperties(prefix="spring.http.rest")
public class RestTemplateProperties {

	/*@Value("${spring.http.rest.connectTimeout}")
	private int restConnectTimeout;
	@Value("${spring.http.rest.readTimeout}")
	private int readTimeout;
	
	@Value("${spring.http.rest.pollingConnection.maxTotal}")
	private int pollMaxTotal;
	@Value("${spring.http.rest.pollingConnection.defaultMaxPerRoute}")
	private int pollDefaultMaxPerRoute;
	*/
	
	private int connectTimeout = 30000;
	private int readTimeout = 60000;
	private RestPollingConnection pollingConnection;
	private boolean bufferRequestBody = true;
	

	public int getConnectTimeout() {
		return connectTimeout;
	}


	public void setConnectTimeout(int connectTimeout) {
		this.connectTimeout = connectTimeout;
	}


	public int getReadTimeout() {
		return readTimeout;
	}


	public void setReadTimeout(int readTimeout) {
		this.readTimeout = readTimeout;
	}


	public boolean isBufferRequestBody() {
		return bufferRequestBody;
	}


	public void setBufferRequestBody(boolean bufferRequestBody) {
		this.bufferRequestBody = bufferRequestBody;
	}


	public RestPollingConnection getPollingConnection() {
		if( pollingConnection == null ) {
			pollingConnection = new RestPollingConnection(); 
		}
		return pollingConnection;
	}


	public void setPollingConnection(RestPollingConnection pollingConnection) {
		this.pollingConnection = pollingConnection;
	}
	
}
