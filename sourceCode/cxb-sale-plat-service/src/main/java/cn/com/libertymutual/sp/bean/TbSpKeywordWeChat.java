package cn.com.libertymutual.sp.bean;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tb_sp_keyword_we_chat")
public class TbSpKeywordWeChat implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 505761812102258307L;
	private Integer id;
	private String keyName;// 关键词标识
	private String keyWord;// 关键词
	private String replyText;// 回复内容
	private String type;// 关键词类型，0=公众号默认回复，1=单个关键词回复，2=多个关键词回复
	private String state;// 是否有效，0=无效，1=有效

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "KEY_NAME", length = 32)
	public String getKeyName() {
		return keyName;
	}

	public void setKeyName(String keyName) {
		this.keyName = keyName;
	}

	@Column(name = "KEY_WORD", length = 255)
	public String getKeyWord() {
		return keyWord;
	}

	public void setKeyWord(String keyWord) {
		this.keyWord = keyWord;
	}

	@Column(name = "REPLY_TEXT")
	public String getReplyText() {
		return replyText;
	}

	public void setReplyText(String replyText) {
		this.replyText = replyText;
	}

	@Column(name = "TYPE")
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Column(name = "STATE")
	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

}
