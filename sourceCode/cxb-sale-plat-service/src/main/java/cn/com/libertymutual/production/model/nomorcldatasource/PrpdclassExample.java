package cn.com.libertymutual.production.model.nomorcldatasource;

import java.util.ArrayList;
import java.util.List;

public class PrpdclassExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public PrpdclassExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andClasscodeIsNull() {
            addCriterion("CLASSCODE is null");
            return (Criteria) this;
        }

        public Criteria andClasscodeIsNotNull() {
            addCriterion("CLASSCODE is not null");
            return (Criteria) this;
        }

        public Criteria andClasscodeEqualTo(String value) {
            addCriterion("CLASSCODE =", value, "classcode");
            return (Criteria) this;
        }

        public Criteria andClasscodeNotEqualTo(String value) {
            addCriterion("CLASSCODE <>", value, "classcode");
            return (Criteria) this;
        }

        public Criteria andClasscodeGreaterThan(String value) {
            addCriterion("CLASSCODE >", value, "classcode");
            return (Criteria) this;
        }

        public Criteria andClasscodeGreaterThanOrEqualTo(String value) {
            addCriterion("CLASSCODE >=", value, "classcode");
            return (Criteria) this;
        }

        public Criteria andClasscodeLessThan(String value) {
            addCriterion("CLASSCODE <", value, "classcode");
            return (Criteria) this;
        }

        public Criteria andClasscodeLessThanOrEqualTo(String value) {
            addCriterion("CLASSCODE <=", value, "classcode");
            return (Criteria) this;
        }

        public Criteria andClasscodeLike(String value) {
            addCriterion("CLASSCODE like", value, "classcode");
            return (Criteria) this;
        }

        public Criteria andClasscodeNotLike(String value) {
            addCriterion("CLASSCODE not like", value, "classcode");
            return (Criteria) this;
        }

        public Criteria andClasscodeIn(List<String> values) {
            addCriterion("CLASSCODE in", values, "classcode");
            return (Criteria) this;
        }

        public Criteria andClasscodeNotIn(List<String> values) {
            addCriterion("CLASSCODE not in", values, "classcode");
            return (Criteria) this;
        }

        public Criteria andClasscodeBetween(String value1, String value2) {
            addCriterion("CLASSCODE between", value1, value2, "classcode");
            return (Criteria) this;
        }

        public Criteria andClasscodeNotBetween(String value1, String value2) {
            addCriterion("CLASSCODE not between", value1, value2, "classcode");
            return (Criteria) this;
        }

        public Criteria andClassnameIsNull() {
            addCriterion("CLASSNAME is null");
            return (Criteria) this;
        }

        public Criteria andClassnameIsNotNull() {
            addCriterion("CLASSNAME is not null");
            return (Criteria) this;
        }

        public Criteria andClassnameEqualTo(String value) {
            addCriterion("CLASSNAME =", value, "classname");
            return (Criteria) this;
        }

        public Criteria andClassnameNotEqualTo(String value) {
            addCriterion("CLASSNAME <>", value, "classname");
            return (Criteria) this;
        }

        public Criteria andClassnameGreaterThan(String value) {
            addCriterion("CLASSNAME >", value, "classname");
            return (Criteria) this;
        }

        public Criteria andClassnameGreaterThanOrEqualTo(String value) {
            addCriterion("CLASSNAME >=", value, "classname");
            return (Criteria) this;
        }

        public Criteria andClassnameLessThan(String value) {
            addCriterion("CLASSNAME <", value, "classname");
            return (Criteria) this;
        }

        public Criteria andClassnameLessThanOrEqualTo(String value) {
            addCriterion("CLASSNAME <=", value, "classname");
            return (Criteria) this;
        }

        public Criteria andClassnameLike(String value) {
            addCriterion("CLASSNAME like", value, "classname");
            return (Criteria) this;
        }

        public Criteria andClassnameNotLike(String value) {
            addCriterion("CLASSNAME not like", value, "classname");
            return (Criteria) this;
        }

        public Criteria andClassnameIn(List<String> values) {
            addCriterion("CLASSNAME in", values, "classname");
            return (Criteria) this;
        }

        public Criteria andClassnameNotIn(List<String> values) {
            addCriterion("CLASSNAME not in", values, "classname");
            return (Criteria) this;
        }

        public Criteria andClassnameBetween(String value1, String value2) {
            addCriterion("CLASSNAME between", value1, value2, "classname");
            return (Criteria) this;
        }

        public Criteria andClassnameNotBetween(String value1, String value2) {
            addCriterion("CLASSNAME not between", value1, value2, "classname");
            return (Criteria) this;
        }

        public Criteria andClassenameIsNull() {
            addCriterion("CLASSENAME is null");
            return (Criteria) this;
        }

        public Criteria andClassenameIsNotNull() {
            addCriterion("CLASSENAME is not null");
            return (Criteria) this;
        }

        public Criteria andClassenameEqualTo(String value) {
            addCriterion("CLASSENAME =", value, "classename");
            return (Criteria) this;
        }

        public Criteria andClassenameNotEqualTo(String value) {
            addCriterion("CLASSENAME <>", value, "classename");
            return (Criteria) this;
        }

        public Criteria andClassenameGreaterThan(String value) {
            addCriterion("CLASSENAME >", value, "classename");
            return (Criteria) this;
        }

        public Criteria andClassenameGreaterThanOrEqualTo(String value) {
            addCriterion("CLASSENAME >=", value, "classename");
            return (Criteria) this;
        }

        public Criteria andClassenameLessThan(String value) {
            addCriterion("CLASSENAME <", value, "classename");
            return (Criteria) this;
        }

        public Criteria andClassenameLessThanOrEqualTo(String value) {
            addCriterion("CLASSENAME <=", value, "classename");
            return (Criteria) this;
        }

        public Criteria andClassenameLike(String value) {
            addCriterion("CLASSENAME like", value, "classename");
            return (Criteria) this;
        }

        public Criteria andClassenameNotLike(String value) {
            addCriterion("CLASSENAME not like", value, "classename");
            return (Criteria) this;
        }

        public Criteria andClassenameIn(List<String> values) {
            addCriterion("CLASSENAME in", values, "classename");
            return (Criteria) this;
        }

        public Criteria andClassenameNotIn(List<String> values) {
            addCriterion("CLASSENAME not in", values, "classename");
            return (Criteria) this;
        }

        public Criteria andClassenameBetween(String value1, String value2) {
            addCriterion("CLASSENAME between", value1, value2, "classename");
            return (Criteria) this;
        }

        public Criteria andClassenameNotBetween(String value1, String value2) {
            addCriterion("CLASSENAME not between", value1, value2, "classename");
            return (Criteria) this;
        }

        public Criteria andAcccodeIsNull() {
            addCriterion("ACCCODE is null");
            return (Criteria) this;
        }

        public Criteria andAcccodeIsNotNull() {
            addCriterion("ACCCODE is not null");
            return (Criteria) this;
        }

        public Criteria andAcccodeEqualTo(String value) {
            addCriterion("ACCCODE =", value, "acccode");
            return (Criteria) this;
        }

        public Criteria andAcccodeNotEqualTo(String value) {
            addCriterion("ACCCODE <>", value, "acccode");
            return (Criteria) this;
        }

        public Criteria andAcccodeGreaterThan(String value) {
            addCriterion("ACCCODE >", value, "acccode");
            return (Criteria) this;
        }

        public Criteria andAcccodeGreaterThanOrEqualTo(String value) {
            addCriterion("ACCCODE >=", value, "acccode");
            return (Criteria) this;
        }

        public Criteria andAcccodeLessThan(String value) {
            addCriterion("ACCCODE <", value, "acccode");
            return (Criteria) this;
        }

        public Criteria andAcccodeLessThanOrEqualTo(String value) {
            addCriterion("ACCCODE <=", value, "acccode");
            return (Criteria) this;
        }

        public Criteria andAcccodeLike(String value) {
            addCriterion("ACCCODE like", value, "acccode");
            return (Criteria) this;
        }

        public Criteria andAcccodeNotLike(String value) {
            addCriterion("ACCCODE not like", value, "acccode");
            return (Criteria) this;
        }

        public Criteria andAcccodeIn(List<String> values) {
            addCriterion("ACCCODE in", values, "acccode");
            return (Criteria) this;
        }

        public Criteria andAcccodeNotIn(List<String> values) {
            addCriterion("ACCCODE not in", values, "acccode");
            return (Criteria) this;
        }

        public Criteria andAcccodeBetween(String value1, String value2) {
            addCriterion("ACCCODE between", value1, value2, "acccode");
            return (Criteria) this;
        }

        public Criteria andAcccodeNotBetween(String value1, String value2) {
            addCriterion("ACCCODE not between", value1, value2, "acccode");
            return (Criteria) this;
        }

        public Criteria andNewclasscodeIsNull() {
            addCriterion("NEWCLASSCODE is null");
            return (Criteria) this;
        }

        public Criteria andNewclasscodeIsNotNull() {
            addCriterion("NEWCLASSCODE is not null");
            return (Criteria) this;
        }

        public Criteria andNewclasscodeEqualTo(String value) {
            addCriterion("NEWCLASSCODE =", value, "newclasscode");
            return (Criteria) this;
        }

        public Criteria andNewclasscodeNotEqualTo(String value) {
            addCriterion("NEWCLASSCODE <>", value, "newclasscode");
            return (Criteria) this;
        }

        public Criteria andNewclasscodeGreaterThan(String value) {
            addCriterion("NEWCLASSCODE >", value, "newclasscode");
            return (Criteria) this;
        }

        public Criteria andNewclasscodeGreaterThanOrEqualTo(String value) {
            addCriterion("NEWCLASSCODE >=", value, "newclasscode");
            return (Criteria) this;
        }

        public Criteria andNewclasscodeLessThan(String value) {
            addCriterion("NEWCLASSCODE <", value, "newclasscode");
            return (Criteria) this;
        }

        public Criteria andNewclasscodeLessThanOrEqualTo(String value) {
            addCriterion("NEWCLASSCODE <=", value, "newclasscode");
            return (Criteria) this;
        }

        public Criteria andNewclasscodeLike(String value) {
            addCriterion("NEWCLASSCODE like", value, "newclasscode");
            return (Criteria) this;
        }

        public Criteria andNewclasscodeNotLike(String value) {
            addCriterion("NEWCLASSCODE not like", value, "newclasscode");
            return (Criteria) this;
        }

        public Criteria andNewclasscodeIn(List<String> values) {
            addCriterion("NEWCLASSCODE in", values, "newclasscode");
            return (Criteria) this;
        }

        public Criteria andNewclasscodeNotIn(List<String> values) {
            addCriterion("NEWCLASSCODE not in", values, "newclasscode");
            return (Criteria) this;
        }

        public Criteria andNewclasscodeBetween(String value1, String value2) {
            addCriterion("NEWCLASSCODE between", value1, value2, "newclasscode");
            return (Criteria) this;
        }

        public Criteria andNewclasscodeNotBetween(String value1, String value2) {
            addCriterion("NEWCLASSCODE not between", value1, value2, "newclasscode");
            return (Criteria) this;
        }

        public Criteria andValidstatusIsNull() {
            addCriterion("VALIDSTATUS is null");
            return (Criteria) this;
        }

        public Criteria andValidstatusIsNotNull() {
            addCriterion("VALIDSTATUS is not null");
            return (Criteria) this;
        }

        public Criteria andValidstatusEqualTo(String value) {
            addCriterion("VALIDSTATUS =", value, "validstatus");
            return (Criteria) this;
        }

        public Criteria andValidstatusNotEqualTo(String value) {
            addCriterion("VALIDSTATUS <>", value, "validstatus");
            return (Criteria) this;
        }

        public Criteria andValidstatusGreaterThan(String value) {
            addCriterion("VALIDSTATUS >", value, "validstatus");
            return (Criteria) this;
        }

        public Criteria andValidstatusGreaterThanOrEqualTo(String value) {
            addCriterion("VALIDSTATUS >=", value, "validstatus");
            return (Criteria) this;
        }

        public Criteria andValidstatusLessThan(String value) {
            addCriterion("VALIDSTATUS <", value, "validstatus");
            return (Criteria) this;
        }

        public Criteria andValidstatusLessThanOrEqualTo(String value) {
            addCriterion("VALIDSTATUS <=", value, "validstatus");
            return (Criteria) this;
        }

        public Criteria andValidstatusLike(String value) {
            addCriterion("VALIDSTATUS like", value, "validstatus");
            return (Criteria) this;
        }

        public Criteria andValidstatusNotLike(String value) {
            addCriterion("VALIDSTATUS not like", value, "validstatus");
            return (Criteria) this;
        }

        public Criteria andValidstatusIn(List<String> values) {
            addCriterion("VALIDSTATUS in", values, "validstatus");
            return (Criteria) this;
        }

        public Criteria andValidstatusNotIn(List<String> values) {
            addCriterion("VALIDSTATUS not in", values, "validstatus");
            return (Criteria) this;
        }

        public Criteria andValidstatusBetween(String value1, String value2) {
            addCriterion("VALIDSTATUS between", value1, value2, "validstatus");
            return (Criteria) this;
        }

        public Criteria andValidstatusNotBetween(String value1, String value2) {
            addCriterion("VALIDSTATUS not between", value1, value2, "validstatus");
            return (Criteria) this;
        }

        public Criteria andFlagIsNull() {
            addCriterion("FLAG is null");
            return (Criteria) this;
        }

        public Criteria andFlagIsNotNull() {
            addCriterion("FLAG is not null");
            return (Criteria) this;
        }

        public Criteria andFlagEqualTo(String value) {
            addCriterion("FLAG =", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagNotEqualTo(String value) {
            addCriterion("FLAG <>", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagGreaterThan(String value) {
            addCriterion("FLAG >", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagGreaterThanOrEqualTo(String value) {
            addCriterion("FLAG >=", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagLessThan(String value) {
            addCriterion("FLAG <", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagLessThanOrEqualTo(String value) {
            addCriterion("FLAG <=", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagLike(String value) {
            addCriterion("FLAG like", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagNotLike(String value) {
            addCriterion("FLAG not like", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagIn(List<String> values) {
            addCriterion("FLAG in", values, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagNotIn(List<String> values) {
            addCriterion("FLAG not in", values, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagBetween(String value1, String value2) {
            addCriterion("FLAG between", value1, value2, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagNotBetween(String value1, String value2) {
            addCriterion("FLAG not between", value1, value2, "flag");
            return (Criteria) this;
        }

        public Criteria andRiskcategoryIsNull() {
            addCriterion("RISKCATEGORY is null");
            return (Criteria) this;
        }

        public Criteria andRiskcategoryIsNotNull() {
            addCriterion("RISKCATEGORY is not null");
            return (Criteria) this;
        }

        public Criteria andRiskcategoryEqualTo(String value) {
            addCriterion("RISKCATEGORY =", value, "riskcategory");
            return (Criteria) this;
        }

        public Criteria andRiskcategoryNotEqualTo(String value) {
            addCriterion("RISKCATEGORY <>", value, "riskcategory");
            return (Criteria) this;
        }

        public Criteria andRiskcategoryGreaterThan(String value) {
            addCriterion("RISKCATEGORY >", value, "riskcategory");
            return (Criteria) this;
        }

        public Criteria andRiskcategoryGreaterThanOrEqualTo(String value) {
            addCriterion("RISKCATEGORY >=", value, "riskcategory");
            return (Criteria) this;
        }

        public Criteria andRiskcategoryLessThan(String value) {
            addCriterion("RISKCATEGORY <", value, "riskcategory");
            return (Criteria) this;
        }

        public Criteria andRiskcategoryLessThanOrEqualTo(String value) {
            addCriterion("RISKCATEGORY <=", value, "riskcategory");
            return (Criteria) this;
        }

        public Criteria andRiskcategoryLike(String value) {
            addCriterion("RISKCATEGORY like", value, "riskcategory");
            return (Criteria) this;
        }

        public Criteria andRiskcategoryNotLike(String value) {
            addCriterion("RISKCATEGORY not like", value, "riskcategory");
            return (Criteria) this;
        }

        public Criteria andRiskcategoryIn(List<String> values) {
            addCriterion("RISKCATEGORY in", values, "riskcategory");
            return (Criteria) this;
        }

        public Criteria andRiskcategoryNotIn(List<String> values) {
            addCriterion("RISKCATEGORY not in", values, "riskcategory");
            return (Criteria) this;
        }

        public Criteria andRiskcategoryBetween(String value1, String value2) {
            addCriterion("RISKCATEGORY between", value1, value2, "riskcategory");
            return (Criteria) this;
        }

        public Criteria andRiskcategoryNotBetween(String value1, String value2) {
            addCriterion("RISKCATEGORY not between", value1, value2, "riskcategory");
            return (Criteria) this;
        }

        public Criteria andCompositeflagIsNull() {
            addCriterion("COMPOSITEFLAG is null");
            return (Criteria) this;
        }

        public Criteria andCompositeflagIsNotNull() {
            addCriterion("COMPOSITEFLAG is not null");
            return (Criteria) this;
        }

        public Criteria andCompositeflagEqualTo(String value) {
            addCriterion("COMPOSITEFLAG =", value, "compositeflag");
            return (Criteria) this;
        }

        public Criteria andCompositeflagNotEqualTo(String value) {
            addCriterion("COMPOSITEFLAG <>", value, "compositeflag");
            return (Criteria) this;
        }

        public Criteria andCompositeflagGreaterThan(String value) {
            addCriterion("COMPOSITEFLAG >", value, "compositeflag");
            return (Criteria) this;
        }

        public Criteria andCompositeflagGreaterThanOrEqualTo(String value) {
            addCriterion("COMPOSITEFLAG >=", value, "compositeflag");
            return (Criteria) this;
        }

        public Criteria andCompositeflagLessThan(String value) {
            addCriterion("COMPOSITEFLAG <", value, "compositeflag");
            return (Criteria) this;
        }

        public Criteria andCompositeflagLessThanOrEqualTo(String value) {
            addCriterion("COMPOSITEFLAG <=", value, "compositeflag");
            return (Criteria) this;
        }

        public Criteria andCompositeflagLike(String value) {
            addCriterion("COMPOSITEFLAG like", value, "compositeflag");
            return (Criteria) this;
        }

        public Criteria andCompositeflagNotLike(String value) {
            addCriterion("COMPOSITEFLAG not like", value, "compositeflag");
            return (Criteria) this;
        }

        public Criteria andCompositeflagIn(List<String> values) {
            addCriterion("COMPOSITEFLAG in", values, "compositeflag");
            return (Criteria) this;
        }

        public Criteria andCompositeflagNotIn(List<String> values) {
            addCriterion("COMPOSITEFLAG not in", values, "compositeflag");
            return (Criteria) this;
        }

        public Criteria andCompositeflagBetween(String value1, String value2) {
            addCriterion("COMPOSITEFLAG between", value1, value2, "compositeflag");
            return (Criteria) this;
        }

        public Criteria andCompositeflagNotBetween(String value1, String value2) {
            addCriterion("COMPOSITEFLAG not between", value1, value2, "compositeflag");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}