package cn.com.libertymutual.sp.action;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import cn.com.libertymutual.core.exception.CustomLangException;
import cn.com.libertymutual.core.redis.util.RedisUtils;
import cn.com.libertymutual.core.util.Constants;
import cn.com.libertymutual.core.web.ServiceResult;
import cn.com.libertymutual.sp.service.api.InitService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(value = "/admin/cache")
public class CacheControllerWeb {

	private Logger log = LoggerFactory.getLogger(getClass());
	@Autowired
	private InitService initService;
	@Resource
	private RedisUtils redisUtils;

	@ApiOperation(value = "刷新缓存", notes = "刷新缓存")
	@ApiImplicitParams(value = { @ApiImplicitParam(name = "type", value = "类型", required = true, paramType = "query", dataType = "String") })
	@RequestMapping(value = "/refresh", method = RequestMethod.POST)
	public ServiceResult refreshCache(String type) throws Exception {
		ServiceResult sr = new ServiceResult();
		try {
			// if ("planInfo".equals(type)) {
			initService.initData();
			log.info("---------refresh planInfo----------");
			// }
			// if ("adconfig".equals(type)) {
			redisUtils.deletelike(Constants.AD_CONFIG_INFO);
			log.info("---------refresh adconfig----------");
			// }
			// if ("serviceMenu".equals(type)) {
			redisUtils.deletelike(Constants.SERVICE_MENU_INFO);
			log.info("---------refresh serviceMenu----------");
			// }
			// if ("areaInfo".equals(type)) {
			redisUtils.deletelike(Constants.BRANCH_AREA_CODE_INFO);
			log.info("---------refresh areaInfo----------");
			// }
			// if ("clauseInfo".equals(type)) {
			redisUtils.deletelike(Constants.CLAUSE_INFO);
			log.info("---------refresh clauseInfo----------");

			redisUtils.deletelike(Constants.SCORE_CONFIG_INFO);
			log.info("---------refresh clauseInfo----------");
			// }
			// if ("riskCodeNameInfo".equals(type)) {
			redisUtils.deletelike(Constants.RISK_CODE_NAME_INFO);
			log.info("---------refresh riskCodeNameInfo----------");
			redisUtils.deletelike(Constants.TRAVEL_ADDRESS_INFO);
			log.info("---------refresh travelAddressInfo----------");
			redisUtils.deletelike(Constants.CAR_OFFER_INIT);
			log.info("---------refresh TRAVEL_ADDRESS_INFO_QUERY----------");
			redisUtils.deletelike(Constants.TRAVEL_ADDRESS_INFO_QUERY);

			// }

		} catch (Exception e) {
			CustomLangException ce = new CustomLangException(e);
			log.error(e.getMessage(), e);
			// 返回失败信息给前端
			sr.setAppFail(ce);
			return sr;
		}
		sr.setResult("刷新缓存成功!");
		return sr;
	}

}
