package cn.com.libertymutual.sp.bean;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name = "tb_sp_bank")
public class TbSpBank implements Serializable {

	private static final long serialVersionUID = -3179274832054046642L;
	private Integer id;// ID
	private String userCode;// 所属用户编码
	private String number;// 银行卡号
	private String mobile;// 银行预留手机号
	private String userName;// 持卡人姓名
	private String idNumber;// 持卡人身份证号
	private String fullName;// 银行全称
	private String fullNamePinYin;// 银行全称拼音
	private String typeName;// 银行卡卡种名称
	private String typeNamePinYin;// 银行卡卡种名称拼音
	private String branchName;// 开户支行名称
	private String province;// 银行所属省份
	private String city;// 银行所属市
	private String area;// 银行所属区
	private String detailedAddress;// 开户行详细地址
	private String abbreviatedLetters;// 简写英文字母
	private String code;// 银行编码
	private String backgroundColor;// 背景颜色值
	private Integer isDefault = 1;// 是否默认银行卡,0=非默认,1=默认
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date createTime;// 绑定时间
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date updateTime;// 最后修改时间
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date unbundingTime;// 解绑时间
	private Integer state = 1;// 绑定状态,0=无效,1=有效

	// // CascadeType.ALL=级联关系,all=增删改查四种都是,optional是否允许为null,默认为true，即允许为null
	// @ManyToOne(cascade = CascadeType.ALL, optional = false)
	// @JoinColumn(name = "ID")
	// @Basic(fetch = FetchType.LAZY)
	// private TbSpUser tbSpUser;
	//
	// public TbSpUser getTbSpUser() {
	// return tbSpUser;
	// }
	//
	// public void setTbSpUser(TbSpUser tbSpUser) {
	// this.tbSpUser = tbSpUser;
	// }

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "ID", unique = true, nullable = false)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "USER_CODE")
	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	@Column(name = "NUMBER")
	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	@Column(name = "MOBILE")
	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	@Column(name = "USER_NAME")
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	@Column(name = "ID_NUMBER")
	public String getIdNumber() {
		return idNumber;
	}

	public void setIdNumber(String idNumber) {
		this.idNumber = idNumber;
	}

	@Column(name = "FULL_NAME")
	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	@Column(name = "FULL_NAME_PIN_YIN")
	public String getFullNamePinYin() {
		return fullNamePinYin;
	}

	public void setFullNamePinYin(String fullNamePinYin) {
		this.fullNamePinYin = fullNamePinYin;
	}

	@Column(name = "TYPE_NAME")
	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	@Column(name = "TYPE_NAME_PIN_YIN")
	public String getTypeNamePinYin() {
		return typeNamePinYin;
	}

	public void setTypeNamePinYin(String typeNamePinYin) {
		this.typeNamePinYin = typeNamePinYin;
	}

	@Column(name = "BRANCH_NAME")
	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	@Column(name = "PROVINCE")
	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	@Column(name = "CITY")
	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	@Column(name = "AREA")
	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	@Column(name = "DETAILED_ADDRESS")
	public String getDetailedAddress() {
		return detailedAddress;
	}

	public void setDetailedAddress(String detailedAddress) {
		this.detailedAddress = detailedAddress;
	}

	@Column(name = "ABBREVIATED_LETTERS")
	public String getAbbreviatedLetters() {
		return abbreviatedLetters;
	}

	public void setAbbreviatedLetters(String abbreviatedLetters) {
		this.abbreviatedLetters = abbreviatedLetters;
	}

	@Column(name = "CODE")
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@Column(name = "BACKGROUND_COLOR")
	public String getBackgroundColor() {
		return backgroundColor;
	}

	public void setBackgroundColor(String backgroundColor) {
		this.backgroundColor = backgroundColor;
	}

	@Column(name = "IS_DEFAULT")
	public Integer getIsDefault() {
		return isDefault;
	}

	public void setIsDefault(Integer isDefault) {
		this.isDefault = isDefault;
	}

	@Column(name = "CREATE_TIME")
	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	@Column(name = "UPDATE_TIME")
	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	@Column(name = "UNBUNDING_TIME")
	public Date getUnbundingTime() {
		return unbundingTime;
	}

	public void setUnbundingTime(Date unbundingTime) {
		this.unbundingTime = unbundingTime;
	}

	@Column(name = "STATE")
	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}
}
