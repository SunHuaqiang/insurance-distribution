package cn.com.libertymutual.production.dao.nomorcldatasource;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import cn.com.libertymutual.production.model.nomorcldatasource.Prpdplanprops;
import cn.com.libertymutual.production.model.nomorcldatasource.PrpdplanpropsExample;
import cn.com.libertymutual.production.model.nomorcldatasource.PrpdplanpropsKey;
@Mapper
public interface PrpdplanpropsMapper {
    int countByExample(PrpdplanpropsExample example);

    int deleteByExample(PrpdplanpropsExample example);

    int deleteByPrimaryKey(PrpdplanpropsKey key);

    int insert(Prpdplanprops record);

    int insertSelective(Prpdplanprops record);

    List<Prpdplanprops> selectByExampleWithBLOBs(PrpdplanpropsExample example);

    List<Prpdplanprops> selectByExample(PrpdplanpropsExample example);

    Prpdplanprops selectByPrimaryKey(PrpdplanpropsKey key);

    int updateByExampleSelective(@Param("record") Prpdplanprops record, @Param("example") PrpdplanpropsExample example);

    int updateByExampleWithBLOBs(@Param("record") Prpdplanprops record, @Param("example") PrpdplanpropsExample example);

    int updateByExample(@Param("record") Prpdplanprops record, @Param("example") PrpdplanpropsExample example);

    int updateByPrimaryKeySelective(Prpdplanprops record);

    int updateByPrimaryKeyWithBLOBs(Prpdplanprops record);

    int updateByPrimaryKey(Prpdplanprops record);
}