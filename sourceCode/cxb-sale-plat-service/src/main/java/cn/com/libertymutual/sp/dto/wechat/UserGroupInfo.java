package cn.com.libertymutual.sp.dto.wechat;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.validator.constraints.Length;

/**
 * Entity - 微信用户分组信息
 * 
 * @author ycye
 *
 */
@Entity
@Table(name = "t_user_group_info")
@SequenceGenerator(name = "sequenceGenerator", sequenceName = "t_sequence")
public class UserGroupInfo  implements Serializable{

	private static final long serialVersionUID = -8698812320359725718L;
	private int id; 
	/**
	 * 分组ID，微信分配
	 */
	private String groupId;
	
	/**
	 * 分组名称
	 */
	private String name;
	
	/**
	 * 分组内用户数量
	 */
	private long count;
	
	/**
	 * 备注
	 */
	private String remark;
	
	private Long publicId;
	
	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}
	@Id
	 @Column(name = "ID")
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	@Length(min = 1, max = 30)
	@Column(nullable = false)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getCount() {
		return count;
	}

	public void setCount(long count) {
		this.count = count;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Long getPublicId() {
		return publicId;
	}

	public void setPublicId(Long publicId) {
		this.publicId = publicId;
	}

}
