package cn.com.libertymutual.wx.message.requestdto;

import com.thoughtworks.xstream.annotations.XStreamAlias;

public class LocationMessage extends RequestBaseMessage {
	@XStreamAlias("Location_X")
	private String locationX;
	@XStreamAlias("Location_Y")
	private String locationY;
	@XStreamAlias("Scale")
	private String scale;
	@XStreamAlias("Label")
	private String label;

	public String getLocationX() {
		return this.locationX;
	}

	public void setLocationX(String locationX) {
		this.locationX = locationX;
	}

	public String getLocationY() {
		return this.locationY;
	}

	public void setLocationY(String locationY) {
		this.locationY = locationY;
	}

	public String getScale() {
		return this.scale;
	}

	public void setScale(String scale) {
		this.scale = scale;
	}

	public String getLabel() {
		return this.label;
	}

	public void setLabel(String label) {
		this.label = label;
	}
}
