package cn.com.libertymutual.production.service.api;

import java.util.List;

import cn.com.libertymutual.production.model.nomorcldatasource.Prpdclass;
import cn.com.libertymutual.production.pojo.request.PrpdClassRequest;

import com.github.pagehelper.PageInfo;

public interface PrpdClassService {
	
	/**
	 * 获取所有险别
	 * @return
	 */
	public List<Prpdclass> findAllPrpdClass();
	
	/**
	 * 获取非组合险险类
	 * @return
	 */
	public List<Prpdclass> findClassNotComposite();
	
	/**
	 * 获取组合险险类
	 * @return
	 */
	public List<Prpdclass> findClassIsComposite();
	
	/**
	 * 分页查询险类信息
	 * @param request
	 * @return
	 */
	public PageInfo<Prpdclass> findByPage(PrpdClassRequest request);
	
	/**
	 * 校验险类代码是否使用
	 * @param classcode
	 * @return
	 */
	public boolean checkClassCodeUsed(String classcode);
	
	/**
	 * 新增险类
	 * @param request
	 */
	public void insert(Prpdclass record);
	
	/**
	 * 修改险类
	 * @param record
	 */
	public void update(Prpdclass record);
}
