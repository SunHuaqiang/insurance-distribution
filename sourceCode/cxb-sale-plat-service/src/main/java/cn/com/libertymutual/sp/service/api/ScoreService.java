package cn.com.libertymutual.sp.service.api;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import cn.com.libertymutual.core.web.ServiceResult;
import cn.com.libertymutual.sp.bean.TbSpOrder;
import cn.com.libertymutual.sp.bean.TbSpScoreConfig;
import cn.com.libertymutual.sp.bean.TbSpScoreLogIn;
import cn.com.libertymutual.sp.bean.TbSpScoreLogOut;
import cn.com.libertymutual.sp.bean.TbSpUser;

public interface ScoreService {

	ServiceResult listByhand(String startDate, String endDate, String status, int pageNumber, int pageSize);

	ServiceResult approve(String approveType, String id, String type, String remark);

	ServiceResult inStorageDetail(String comCode,String startDate, String endDate, String changeType, String userCode, String branchCode,String policyNo, String mobile,
			int pageNumber, int pageSize);

	ServiceResult outStorageDetail(String comCode,String startDate, String endDate, String policyNo, String changeType, String userCode, String mobile,
			int pageNumber, int pageSize);

	ServiceResult beforeApprove(Integer id);

	ServiceResult scoreByHand(String path, String remark, MultipartFile file) throws Exception;

	// TbSpScoreLogIn ruleInScore(Integer productId, String policyNo, Double amount,
	// Date startDate);

	TbSpScoreLogIn ruleInScore(TbSpOrder order, TbSpUser user);

	int ruleOutScore(String policyNo, String userCode);

	List<TbSpScoreLogIn> outScore(String type, String userCode, Double amount, String reason, String policyNo);

	ServiceResult myScore(String userCode);

	ServiceResult scoreDetail(String userCode, String type, int pageNumber, int pageSize);

	ServiceResult cash(String userCode, double score, String branchCode, String bankNumber, Integer bankId);

	ServiceResult cashLog(String comCode,String startDate, String endDate, String status, String userCode, String mobile, String businessNo, int pageNumber,
			int pageSize);

	ServiceResult scoreConfig();

	ServiceResult updateScoreConfig(TbSpScoreConfig scoreConfig);

	ServiceResult userScore(String comCode,String startDate, String endDate, String userCode, String mobile, int pageNumber, int pageSize);

	ServiceResult findIntegralDetail(String type, Integer id);

	ServiceResult queryCnp(String businessNo, String branchCode, String startDate, String endDate, int pageNumber, int pageSize);

	ServiceResult drawLogList(String startDate, String endDate, String status, String policyNo, String mobile, String userCode, int pageNumber,
			int pageSize);

	TbSpScoreLogOut outInvalidScore(TbSpScoreLogIn orderin,String remark);

	TbSpUser scoreUser(TbSpOrder order);

	ServiceResult cashPatch(String businessNo, String branchCode, Integer bankId);

	ServiceResult cashToInScoreDetail(String comCode,String startDate, String endDate, String branchCode, String userCode, String mobile, int pageNumber,
			int pageSize);

	ServiceResult cashTest(String userCode, Double score, String policyNo);

	List<TbSpScoreLogIn> personalInScore(TbSpOrder order, TbSpUser user);

	List<TbSpScoreLogIn> channelInScore(TbSpOrder order, TbSpUser user);

	ServiceResult findScoreConfig();

	List<TbSpScoreLogIn> cxbInScore(TbSpOrder rewriteOrder, TbSpUser scoreUser);
}
