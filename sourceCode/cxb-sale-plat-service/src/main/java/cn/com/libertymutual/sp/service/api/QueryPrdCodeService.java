package cn.com.libertymutual.sp.service.api;

import cn.com.libertymutual.core.web.ServiceResult;
import cn.com.libertymutual.wx.message.responsedto.TextMessage;

public interface QueryPrdCodeService {

	/** 验车码生产业务<br>
	 * Version：1.0<br>
	 * Author：AoYi<br>
	 * DateTime：2018年3月20日下午7:00:06<br>
	 * @return
	 */
	public ServiceResult queryCheckCode();

	/** 执行验车码业务并返回微信回复的XML<br>
	 * Version：1.0<br>
	 * Author：AoYi<br>
	 * DateTime：2018年3月20日下午6:59:43<br>
	 * @param returnXML
	 * @param textMessage
	 * @return
	 */
	public String executeQueryCheckCode(String returnXML, TextMessage textMessage);
}
