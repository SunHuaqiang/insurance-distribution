package cn.com.libertymutual.sp.service.api;

import cn.com.libertymutual.core.web.ServiceResult;
import cn.com.libertymutual.sp.bean.TbWebRequestLog;

public interface TbWebRequestLogService {
	ServiceResult saveWebRequestLog(TbWebRequestLog tequestLog);
}
