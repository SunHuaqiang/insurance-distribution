package cn.com.libertymutual.sp.dto;

import java.io.Serializable;

import cn.com.libertymutual.core.base.dto.RequestBaseDto;


public class CommonRequestDto extends RequestBaseDto implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	//如果业务系统需要的为json数据,则传值requestJson
	private String requestJson;//业务类Json格式字符串
	//如果业务系统需要的为xml数据,则传值requestXml
	private String requestXml;//业务类Xml格式字符串

	private String requestType;//请求类型
     private String callBackUrl;//回调地址
	public String getRequestJson() {
		return requestJson;
	}


	public void setRequestJson(String requestJson) {
		this.requestJson = requestJson;
	}


	public String getRequestType() {
		return requestType;
	}


	public void setRequestType(String requestType) {
		this.requestType = requestType;
	}


	public String getRequestXml() {
		return requestXml;
	}


	public void setRequestXml(String requestXml) {
		this.requestXml = requestXml;
	}


	public String getCallBackUrl() {
		return callBackUrl;
	}


	public void setCallBackUrl(String callBackUrl) {
		this.callBackUrl = callBackUrl;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((requestJson == null) ? 0 : requestJson.hashCode());
		result = prime * result
				+ ((requestType == null) ? 0 : requestType.hashCode());
		result = prime * result
				+ ((requestXml == null) ? 0 : requestXml.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof CommonRequestDto))
			return false;
		CommonRequestDto other = (CommonRequestDto) obj;
		if (requestJson == null) {
			if (other.requestJson != null)
				return false;
		} else if (!requestJson.equals(other.requestJson))
			return false;
		if (requestType == null) {
			if (other.requestType != null)
				return false;
		} else if (!requestType.equals(other.requestType))
			return false;
		if (requestXml == null) {
			if (other.requestXml != null)
				return false;
		} else if (!requestXml.equals(other.requestXml))
			return false;
		return true;
	}


	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("CommonRequestDto [requestJson=").append(requestJson)
				.append(", requestXml=").append(requestXml)
				.append(", requestType=").append(requestType).append("]");
		return builder.toString();
	}


	
	
	
}
