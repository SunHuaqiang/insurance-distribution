package cn.com.libertymutual.sp.dto.callback.response;

public class ServiceRes {
	private String sendStatus;
	private String msgId;
	private String queueOffset;
	private String offsetMsgId;
	private String regionId;
	private String topic;
	private String brokerName;
	private String queueId;
	public String getSendStatus() {
		return sendStatus;
	}
	public void setSendStatus(String sendStatus) {
		this.sendStatus = sendStatus;
	}
	public String getMsgId() {
		return msgId;
	}
	public void setMsgId(String msgId) {
		this.msgId = msgId;
	}
	public String getQueueOffset() {
		return queueOffset;
	}
	public void setQueueOffset(String queueOffset) {
		this.queueOffset = queueOffset;
	}
	public String getOffsetMsgId() {
		return offsetMsgId;
	}
	public void setOffsetMsgId(String offsetMsgId) {
		this.offsetMsgId = offsetMsgId;
	}
	public String getRegionId() {
		return regionId;
	}
	public void setRegionId(String regionId) {
		this.regionId = regionId;
	}
	public String getTopic() {
		return topic;
	}
	public void setTopic(String topic) {
		this.topic = topic;
	}
	public String getBrokerName() {
		return brokerName;
	}
	public void setBrokerName(String brokerName) {
		this.brokerName = brokerName;
	}
	public String getQueueId() {
		return queueId;
	}
	public void setQueueId(String queueId) {
		this.queueId = queueId;
	}
	
	
}
