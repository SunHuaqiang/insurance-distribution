package cn.com.libertymutual.sp.action;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.mobile.device.Device;
import org.springframework.mobile.device.DeviceUtils;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import cn.com.libertymutual.core.annotation.SystemValidate;
import cn.com.libertymutual.core.util.Constants;
import cn.com.libertymutual.core.util.Current;
import cn.com.libertymutual.core.web.ServiceResult;
import cn.com.libertymutual.sp.biz.IdentifierMarkBiz;
import cn.com.libertymutual.sp.biz.ProductBiz;
import cn.com.libertymutual.sp.dto.PlanDto;
import cn.com.libertymutual.sp.dto.queryplans.QueryPlansRequestDTO;
import cn.com.libertymutual.sp.dto.savePlan.PropsalSaveRequestDto;
import cn.com.libertymutual.sp.service.api.AdConfigService;
import cn.com.libertymutual.sp.service.api.CodeRelateService;
import cn.com.libertymutual.sp.service.api.ProductService;
import cn.com.libertymutual.sp.service.api.ServiceMenuService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(value = "/nol/product")
public class ProductController {
	@Resource
	private ProductService productService;
	@Resource
	private IdentifierMarkBiz identifierMarkBiz;
	@Resource
	private ProductBiz productBiz;
	@Resource
	private CodeRelateService codeRelateService;
	@Resource
	private ServiceMenuService serviceMenuService;
	@Resource
	private AdConfigService adConfigService;

	@ApiOperation(value = "前端三个接口合并", notes = "前端三个接口合并")
	@ApiImplicitParams(value = { @ApiImplicitParam(name = "region", value = "地区名", required = true, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "type", value = "分类", required = true, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "sortfield", value = "排序字段", required = true, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "sorttype", value = "排序方式", required = true, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "pageNumber", value = "页码", required = true, paramType = "query", dataType = "Long"),
			@ApiImplicitParam(name = "pageSize", value = "条数", required = true, paramType = "query", dataType = "Long") })
	@RequestMapping(value = "/productMenuAd")
	@SystemValidate(validate = false, description = "无需校验接口")
	public ServiceResult productMenuAd(HttpServletRequest request, HttpServletResponse response, String region, String type, String sorttype,
			Integer pageNumber, Integer pageSize, String pointX, String pointY, String userId, String uuid, String userCode, boolean isShop,
			String markUuid) throws Exception {
		return productBiz.productMenuAd(request, response, region, type, sorttype, pageNumber, pageSize, pointX, pointY, userId, uuid,
				Current.userCode.get(), isShop, markUuid);
	}

	@RequestMapping(value = "/queryInitRiskData")
	@SystemValidate(validate = false, description = "无需校验接口")
	public ServiceResult queryInitRiskData() {
		return codeRelateService.getSaleInitData("SaleInitData");
	}

	@RequestMapping(value = "/queryMenuData")
	@SystemValidate(validate = false, description = "无需校验接口")
	public ServiceResult queryMenuData(String sorttype) {
		return serviceMenuService.menuList(sorttype);
	}

	@RequestMapping(value = "/advertInit")
	@SystemValidate(validate = false, description = "无需校验接口")
	public ServiceResult indexInit(String sorttype) {
		return adConfigService.advertList(sorttype);
	}

	@ApiOperation(value = "按不同分类查询产品", notes = "测试")
	@ApiImplicitParams(value = { @ApiImplicitParam(name = "region", value = "地区名", required = true, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "type", value = "分类", required = true, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "sortfield", value = "排序字段", required = true, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "sorttype", value = "排序方式", required = true, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "pageNumber", value = "页码", required = true, paramType = "query", dataType = "Long"),
			@ApiImplicitParam(name = "pageSize", value = "条数", required = true, paramType = "query", dataType = "Long") })
	@RequestMapping(value = "/productList")
	@SystemValidate(validate = false, description = "无需校验接口")
	public ServiceResult productList(HttpServletRequest request, HttpServletResponse response, String identCode, String region, String type,
			String sorttype, Integer pageNumber, Integer pageSize) throws Exception {
		return productBiz.productList(request, response, identCode, region, type, sorttype, pageNumber, pageSize);
	}

	@RequestMapping(value = "/getInitUrl")
	public ServiceResult getInitUrl(HttpServletRequest request, HttpServletResponse response, String identCode) throws Exception {
		return productBiz.getInitUrl(request, response, identCode);

	}

	@ApiOperation(value = "根据产品ID获取套餐计划", notes = "测试")
	@ApiImplicitParams(value = { @ApiImplicitParam(name = "gpaPlanCode", value = "计划planCode", required = true, paramType = "query", dataType = "String[]"),
			@ApiImplicitParam(name = "paraMap", value = "动态因子", required = false, paramType = "query", dataType = "Map")
			 })
	@RequestMapping(value = "/queryPlans", method = RequestMethod.POST)
	public ServiceResult queryPlans(HttpServletRequest request, HttpServletResponse response,String planId, String age,
			String deadLine,String seat) throws Exception {
		// response.setHeader(Constants.JWT_TOKEN_NAME,
		// identifierMarkBiz.getIdentifier(request, device));
		/**
		 *  00005 车辆类型(0-货运车,1-乘用车)   1
	 * 00006 车辆使用性质(0-营业,1-非营业) 1
	 * 00007 车辆吨位数
	 * 00001 座位
	 * 00002 天
	 * 00003 年龄
	 * 00004 地区
	 * */
		return productBiz.queryPlans(planId,age,deadLine,seat);
	}

	@RequestMapping(value = "/queryPlan", method = RequestMethod.POST)
	@SystemValidate(validate = false, description = "无需校验接口")
	public ServiceResult queryPlan(HttpServletRequest request, HttpServletResponse response, String identCode, String planId, Integer age,
			Integer deadLine) throws Exception {
		return productBiz.queryPlan(request, response, identCode, planId, age, deadLine);
	}

	@RequestMapping(value = "/querySalePlan", method = RequestMethod.POST)
	@SystemValidate(validate = false, description = "无需校验接口")
	public ServiceResult querySalePlan(HttpServletRequest request, HttpServletResponse response, String identCode, String productId, String userCode)
			throws Exception {
		return productBiz.querySalePlan(request, response, identCode, productId, Current.userCode.get());
	}

	@RequestMapping(value = "/savePlan", method = RequestMethod.POST)
	@SystemValidate(validate = false, description = "无需校验接口")
	public ServiceResult savePlan(HttpServletRequest request, HttpServletResponse response, String identCode,
			@RequestBody PropsalSaveRequestDto requestDto) throws Exception {
		return productBiz.savePlan(request, response, identCode, requestDto);
	}

	@RequestMapping(value = "/shareFindPlan", method = RequestMethod.POST)
	@SystemValidate(validate = false, description = "无需校验接口")
	public ServiceResult shareFindPlan(HttpServletRequest request, HttpServletResponse response, Integer proId, String refereeId, String userCode)
			throws Exception {
		return productService.findById(proId, refereeId, request, Current.userCode.get());

	}

	@ApiOperation(value = "查询条款", notes = "查询条款")
	@ApiImplicitParams(value = { @ApiImplicitParam(name = "planId", value = "产品id", required = true, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "clauseTitle", value = "被保人年龄", required = false, paramType = "query", dataType = "String"), })
	@RequestMapping(value = "/queryClause", method = RequestMethod.POST)
	@SystemValidate(validate = false, description = "无需校验接口")
	public ServiceResult queryClause(HttpServletRequest request, HttpServletResponse response, String identCode, String planId, String clauseTitle)
			throws Exception {
		return productBiz.queryClause(request, response, identCode, planId, clauseTitle);
	}

	@ApiOperation(value = "查询条款", notes = "查询条款")
	@ApiImplicitParams(value = { @ApiImplicitParam(name = "planId", value = "产品id", required = true, paramType = "query", dataType = "String") })
	@RequestMapping(value = "/queryClauseTitle", method = RequestMethod.POST)
	@SystemValidate(validate = false, description = "无需校验接口")
	public ServiceResult queryClauseTitle(String planId) throws Exception {
		return productBiz.queryClauseTitle(planId);
	}

	@RequestMapping(value = "/valData", method = RequestMethod.POST)
	@SystemValidate(validate = false, description = "无需校验接口")
	public void valData(HttpServletResponse response, HttpServletRequest request,
			@RequestAttribute(DeviceUtils.CURRENT_DEVICE_ATTRIBUTE) Device device) throws Exception {
		response.setHeader(Constants.JWT_TOKEN_NAME, identifierMarkBiz.getIdentifier(request, device));
	}
}
