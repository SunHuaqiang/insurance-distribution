package cn.com.libertymutual.sp.action.production;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.libertymutual.production.pojo.request.PrpdClauseRequest;
import cn.com.libertymutual.production.pojo.response.Response;
import cn.com.libertymutual.production.service.api.business.ClauseBusinessService;

/** 
 * @author Steven.Li
 * @date 2017年7月28日
 *  
 */
@RestController
@RequestMapping("clause")
public class ClauseController {
	
	@Autowired
	protected ClauseBusinessService clauseBusinessService;
	
	/**
	 * 获取特约
	 * @param PrpdClauseLibraryRequest
	 * @return
	 */
	@RequestMapping("queryPrpdClause")
	public Response queryPrpdClause(PrpdClauseRequest prpdClauseRequest) {
		Response result = clauseBusinessService.findPrpdClause(prpdClauseRequest);
		return result;
	}
	
	/**
	 * 获取特约序列号
	 * @return
	 */
	@RequestMapping("queryPrpdClauseCodeNo")
	public Response queryPrpdClauseCodeNo() {
		Response result = new Response();
		result = clauseBusinessService.findPrpdClauseCodeNo();
		return result;
	}
	
	/**
	 * 新增特约
	 * @param
	 * @return
	 */
	@RequestMapping("addClause")
	public Response addClause(PrpdClauseRequest request) {
		Response result = new Response();
		try {
			String clausecode = clauseBusinessService.findPrpdClauseCodeNo().getResult().toString();
			request.setClausecode(clausecode);
			result = clauseBusinessService.insertPrpdClause(request);
			result.setSuccess();
		} catch (Exception e) {
			result.setAppFail();
			result.setResult(e.getMessage());
		}
		return result;
	}
	
	/**
	 * 通过特约查询适用险种
	 * @param clausecode
	 * @return
	 */
	@RequestMapping("queryCodeRiskByClause")
	public Response queryCodeRiskByClause(String clausecode) {
		Response result = new Response();
		result = clauseBusinessService.findCodeRiskByClause(clausecode);
		return result;
	}
	
	/**
	 * 通过特约查询默认带出险种、默认带出分公司、适用计划
	 * @param clausecode
	 * @return
	 */
	@RequestMapping("queryRiskClauseByClause")
	public Response queryRiskClauseByClause(String clausecode) {
		Response result = new Response();
		result = clauseBusinessService.findRiskClauseByClause(clausecode);
		return result;
	}
	
	/**
	 * 修改特约
	 * @param request
	 * @return
	 */
	@RequestMapping("updatePrpdClause")
	public Response updatePrpdClause(PrpdClauseRequest request) {
		Response result = new Response();
		try {
			result = clauseBusinessService.updatePrpdClause(request);
			result.setSuccess();
		} catch (Exception e) {
			result.setAppFail();
			result.setResult(e.getMessage());
		}
		return result;
	}
	
}
