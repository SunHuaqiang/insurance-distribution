package cn.com.libertymutual.production.dao.nomorcldatasource;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import cn.com.libertymutual.production.model.nomorcldatasource.Prpdplansub;
import cn.com.libertymutual.production.model.nomorcldatasource.PrpdplansubExample;
import cn.com.libertymutual.production.model.nomorcldatasource.PrpdplansubKey;
@Mapper
public interface PrpdplansubMapper {
    int countByExample(PrpdplansubExample example);

    int deleteByExample(PrpdplansubExample example);

    int deleteByPrimaryKey(PrpdplansubKey key);

    int insert(Prpdplansub record);

    int insertSelective(Prpdplansub record);

    List<Prpdplansub> selectByExampleWithBLOBs(PrpdplansubExample example);

    List<Prpdplansub> selectByExample(PrpdplansubExample example);

    Prpdplansub selectByPrimaryKey(PrpdplansubKey key);

    int updateByExampleSelective(@Param("record") Prpdplansub record, @Param("example") PrpdplansubExample example);

    int updateByExampleWithBLOBs(@Param("record") Prpdplansub record, @Param("example") PrpdplansubExample example);

    int updateByExample(@Param("record") Prpdplansub record, @Param("example") PrpdplansubExample example);

    int updateByPrimaryKeySelective(Prpdplansub record);

    int updateByPrimaryKeyWithBLOBs(Prpdplansub record);

    int updateByPrimaryKey(Prpdplansub record);
}