/*****************************************************************************************************
 * DESC			:	API函数定义
 * Author		:	Steven.Li
 * Datetime		:	2017-07-06
 * ***************************************************************************************************
 * 函数组			函数名称			函数作用
 *
 * 接口函数
 * 			           queryKindData		           按条件获取条款信息
 *                                             initRiskSelect                            初始化险种下拉框信息
 * ***************************************************************************************************
 */

import Constant from "../constant";
import Validations from "../validations";
import Utils from "../utils";

const EFileAPI = {

            eFileFormValidation(_this) {
                //添加条款，表单非空校验
                let requiredFields = [];
                requiredFields.push( _this.formData.efilecname);
                requiredFields.push( _this.formData.efileename);
                requiredFields.push( _this.formData.matchlevel);
                requiredFields.push( _this.formData.kindcode);
                requiredFields.push( _this.formData.kindcname);
                requiredFields.push( _this.formData.kindversion);
                requiredFields.push( _this.formData.startdate);
                requiredFields.push( _this.formData.enddate);
                if(!Validations.required(requiredFields,_this)) {
                        return false;
                }
                return true;
            },

            openAddDialog(_this) {
                    _this.showAddDialog = true;
            },

            openEditDialog(_this) {
                    if (_this.selectedRows.length > 1) {
                        _this.$message({type: 'error',message: '只能选择一行数据进行编辑'});
                        return;
                    } else if (_this.selectedRows.length < 1) {
                        _this.$message({type: 'error',message: '请选择备案号！'});
                        return;
                    }
                    _this.showEditDialog = true;
            },
            openKindSelectDialog(_this) {
                    _this.showKindSelectDialog = true;
            },

            add(_this) {
                _this.loading = true;
                let api = this;
                if (!api.eFileFormValidation(_this)) {
                    _this.loading = false;
                    return ;
                }
                let  formData = api.convertFormData(_this.formData);
                $.ajax({
                    type: 'POST',
                    url: Constant.urls.insertEFile,
                    dataType: 'json',
                    cache: false,
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(data) {
                        if(data.state === 3) {
                            _this.$router.push('/login');
                            _this.$message({type: 'error',message: data.result});
                            return false;
                        }
                        if(data.state == 0) {
                            _this.$message({type: 'success',message: '新增备案号成功!'});
                            _this.$parent.queryData();
                            _this.off();
                        } else {
                            Utils.printErrorMsg(data.result,_this);
                        }
                    },
                    complete:function(XMLHttpRequest,textStatus){  
                        if(textStatus=='timeout'){
                            _this.$message({type: 'error',message: '服务器连接超时!'});
                        }
                    },  
                    error:function(XMLHttpRequest, textStatus){  
                        console.log(XMLHttpRequest);
                        console.log(textStatus);
                       _this.$message({type: 'error',message: '服务器错误!'});
                    }
                });
                _this.loading = false;
            },

            delete(_this) {
                    if (_this.selectedRows.length < 1) {
                        _this.$message({
                            type: 'error',
                            message: '请选择条款！'
                        });
                        return;
                    }
                    _this.$confirm('此操作将删除所选备案号, 是否继续?', '提示', {
                        confirmButtonText: '确定',
                        cancelButtonText: '取消',
                        type: 'warning'
                    }).then(() => {
                        // TODO 执行删除逻辑
                        _this.$message({
                            type: 'success',
                            message: '删除成功!'
                        });
                        _this.queryData(); //重新加载表格数据
                    }).catch(() => {
                        _this.$message({
                            type: 'info',
                            message: '已取消删除'
                        });
                    });
            },

            edit(_this) {
                let api = this;
                if (!api.eFileFormValidation(_this)) {
                    return ;
                }
                let  formData = api.convertFormData(_this.formData);
                 _this.$confirm('此操作将修改该备案号, 是否继续?', '提示', {
                        confirmButtonText: '确定',
                        cancelButtonText: '取消',
                        type: 'warning'
                    }).then(() => {
                        _this.loading = true;
                        $.ajax({
                                type: 'POST',
                                url: Constant.urls.updateEFile,
                                dataType: 'json',
                                cache: false,
                                data: formData,
                                processData: false,
                                contentType: false,
                                success: function(data) {
                                    if(data.state === 3) {
                                        _this.$router.push('/login');
                                        _this.$message({type: 'error',message: data.result});
                                        return false;
                                    }
                                    if(data.state == 0) {
                                        _this.$message({type: 'success',message: '修改备案号成功!'});
                                        api.query(_this.$parent);
                                        _this.off();
                                    } else {
                                        Utils.printErrorMsg(data.result,_this);
                                    }
                                },
                                complete:function(XMLHttpRequest,textStatus){  
                                    if(textStatus=='timeout'){
                                        _this.$message({type: 'error',message: '服务器连接超时!'});
                                    }
                                },  
                                error:function(XMLHttpRequest, textStatus){  
                                    console.log(XMLHttpRequest);
                                    console.log(textStatus);
                                   _this.$message({type: 'error',message: '服务器错误!'});
                                }
                            });
                        _this.loading = false;
                    }).catch(() => {
                        _this.$message({type: 'info',message: '已取消修改'});
                    });
            },

            query(_this) {
                _this.loading = true;
                $.ajax({
                    type: 'POST',
                    url: Constant.urls.queryPrpdEFile,
                    data: _this.formData,
                    dataType: 'json',
                    success: function(data) {
                        _this.loading = false;
                        if(data.state === 3) {
                            _this.$router.push('/login');
                            _this.$message({type: 'error',message: data.result});
                            return false;
                        }
                        if(data.state == 0) {
                            _this.tableData = data.result;
                            _this.total = data.total;
                        } else {
                           _this.$message({type: 'error',message: '调用接口错误！'});
                        }
                    },
                    complete:function(XMLHttpRequest,textStatus){  
                        if(textStatus=='timeout'){
                            _this.$message({type: 'error',message: '服务器连接超时!'});
                        }
                        _this.loading = false;
                    },  
                    error:function(XMLHttpRequest, textStatus){  
                        console.log(XMLHttpRequest);
                        console.log(textStatus);
                       _this.$message({type: 'error',message: '服务器错误!'});
                       _this.loading = false;
                    }
                });
                 setTimeout(function(){
                    _this.isSortRequest = true;
                },500);
            },

            convertFormData(form) {
                    let formData = new FormData(); 
                    for(var key in form){
                            formData.append(key, form[key]);
                    }
                    return formData;
            },
            openLogDialog(_this) {
                    if (_this.selectedRows.length > 1) {
                            _this.$message({type: 'error',message: '只能选择一行数据进行查看'});
                            return;
                        } else if (_this.selectedRows.length < 1) {
                            _this.$message({type: 'error',message: '请选择备案号！'});
                            return;
                        }
                        _this.logBusinessKey = _this.selectedRows[0].efilecname;
                        _this.logBusinessKeyValue = _this.selectedRows[0].efilecode;
                        _this.showLogDialog = true;
            },
}

export default EFileAPI
