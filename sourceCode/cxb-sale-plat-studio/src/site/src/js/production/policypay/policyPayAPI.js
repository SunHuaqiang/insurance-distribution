/*****************************************************************************************************
 * DESC      :  API函数定义
 * Author    :  Steven.Li
 * Datetime    :  2017-07-06
 * ***************************************************************************************************
 * 函数组      函数名称      函数作用
 *
 * 接口函数
 *                 queryKindData               按条件获取条款信息
 *                                             initRiskSelect                            初始化险种下拉框信息
 * ***************************************************************************************************
 */

import Constant from "../constant";

const PolicyPayAPI = {

  initCompanySelect(_this) {
    $.ajax({
      type: 'POST',
      url: Constant.urls.queryCompanys,
      dataType: 'json',
      success: function (data) {
        if (data.state === 3) {
          _this.$router.push('/login');
          _this.$message({type: 'error', message: data.result});
          return false;
        }
        if (data.state == 0) {
            _this.comcodeOptions = data.result;
        } else {
          _this.$message({type: 'error', message: '调用接口错误！'});
        }
      }
    });
  },

  initRiskSelect(_this) {
    $.ajax({
      type: 'POST',
      url: Constant.urls.queryAllPrpdRisk,
      dataType: 'json',
      success: function(data) {
        if(data.state === 3) {
          _this.$router.push('/login');
          _this.$message({type: 'error',message: data.result});
          return false;
        }
        if(data.state == 0) {
          _this.riskcodeOptions = data.result;
        } else {
          alert(data.result);
        }
      }
    });
  },

  initAgentSelect(_this) {
    $.ajax({
      type: 'POST',
      url: Constant.urls.queryAgentByComCode,
      data: {
        comCodes :  _this.formData.comcode
      },
      dataType: 'json',
      success: function(data) {
        if(data.state === 3) {
          _this.$router.push('/login');
          _this.$message({type: 'error',message: data.result});
          return false;
        }
        if(data.state == 0) {
          _this.agentcodeOptions.push({
            agentcode: "ALL",
            agentname: "全部代理人",
            agentename: "All Agents",
          })
          for(let index in data.result) {
            _this.agentcodeOptions.push(data.result[index]);
          }
        } else {
          alert(data.result);
        }
      }
    });
  },

  add(_this) {
    _this.loading = true;
    let api = this;
    $.ajax({
      type: 'POST',
      url: Constant.urls.insertPrpdPolicyPay,
      data: _this.formData,
      dataType: 'json',
      success: function (data) {
        if (data.state === 3) {
          _this.$router.push('/login');
          _this.$message({type: 'error', message: data.result});
          return false;
        }
        if (data.state == 0) {
          _this.$message({type: 'success', message: '新增见费出单配置成功！'});
          api.query(_this.$parent);
          _this.off();
        } else {
          alert(data.result);
        }
      },
      complete: function (XMLHttpRequest, textStatus) {
        if (textStatus == 'timeout') {
          _this.$message({type: 'error', message: '服务器连接超时!'});
        }
      },
      error: function (XMLHttpRequest, textStatus) {
        console.log(XMLHttpRequest);
        console.log(textStatus);
        _this.$message({type: 'error', message: '服务器错误!'});
      }
    });
    _this.loading = false;
  },

  delete(_this) {
    if (_this.selectedRows.length < 1) {
      _this.$message({
        type: 'error',
        message: '请选择一行数据！'
      });
      _this.loading = false;
      return;
    }
    _this.$confirm('此操作将删除所选数据, 是否继续?', '提示', {
      confirmButtonText: '确定',
      cancelButtonText: '取消',
      type: 'warning'
    }).then(() => {
      _this.loading = true;
      let requestData = {selectedRows: _this.selectedRows};
      $.ajax({
        type: 'POST',
        url: Constant.urls.deletePrpdPolicyPay,
        data: JSON.stringify(requestData),
        dataType: 'json',
        contentType: 'application/json',
        success: function (data) {
          if (data.state === 3) {
            _this.$router.push('/login');
            _this.$message({type: 'error', message: data.result});
            return false;
          }
          if (data.state == 0) {
            _this.$message({type: 'success', message: '删除成功!'});
            _this.queryData(); //重新加载表格数据
          } else {
            if (data.result.length > 40) {
              alert(data.result);
            } else {
              _this.$message({type: 'error', message: data.result});
            }
          }
        },
        complete: function (XMLHttpRequest, textStatus) {
          if (textStatus == 'timeout') {
            _this.$message({type: 'error', message: '服务器连接超时!'});
          }
        },
        error: function (XMLHttpRequest, textStatus) {
          console.log(XMLHttpRequest);
          console.log(textStatus);
          _this.$message({type: 'error', message: '服务器错误!'});
        }
      });
      _this.loading = false;
    }).catch(() => {
      _this.$message({
        type: 'info',
        message: '已取消注销'
      });
    });
  },

  edit(_this) {
    let api = this;
    _this.$confirm('此操作将修改该配置, 是否继续?', '提示', {
      confirmButtonText: '确定',
      cancelButtonText: '取消',
      type: 'warning'
    }).then(() => {
      _this.loading = true;
      $.ajax({
        type: 'POST',
        url: Constant.urls.updatePrpdPolicyPay,
        data: _this.formData,
        dataType: 'json',
        success: function (data) {
          if (data.state === 3) {
            _this.$router.push('/login');
            _this.$message({type: 'error', message: data.result});
            return false;
          }
          if (data.state == 0) {
            _this.$message({type: 'success', message: '修改成功!'});
            api.query(_this.$parent);
            _this.off();
          } else {
            alert(data.result);
          }
        },
        complete: function (XMLHttpRequest, textStatus) {
          if (textStatus == 'timeout') {
            _this.$message({type: 'error', message: '服务器连接超时!'});
          }
        },
        error: function (XMLHttpRequest, textStatus) {
          console.log(XMLHttpRequest);
          console.log(textStatus);
          _this.$message({type: 'error', message: '服务器错误!'});
        }
      });
      _this.loading = false;
    }).catch(() => {
      _this.$message({type: 'info', message: '已取消修改'});
    });
  },

  query(_this) {
    _this.loading = true;
    $.ajax({
      type: 'POST',
      url: Constant.urls.queryPolicyPays,
      data: _this.formData,
      dataType: 'json',
      success: function (data) {
        _this.loading = false;
        if (data.state === 3) {
          _this.$router.push('/login');
          _this.$message({type: 'error', message: data.result});
          return false;
        }
        if (data.state == 0) {
          _this.tableData = data.result;
          _this.total = data.total;
        } else {
          alert(data.result);
        }
      },
      complete: function (XMLHttpRequest, textStatus) {
        if (textStatus == 'timeout') {
          _this.$message({type: 'error', message: '服务器连接超时!'});
        }
        _this.loading = false;
      },
      error: function (XMLHttpRequest, textStatus) {
        console.log(XMLHttpRequest);
        console.log(textStatus);
        _this.$message({type: 'error', message: '服务器错误!'});
        _this.loading = false;
      }
    });
  },

  openAddDialog(_this) {
    _this.showAddDialog = true;
  },

  openEditDialog(_this) {
    if (_this.selectedRows.length > 1) {
      _this.$message({type: 'error', message: '只能选择一行数据进行编辑'});
      return;
    } else if (_this.selectedRows.length < 1) {
      _this.$message({type: 'error', message: '请选择一行数据！'});
      return;
    }
    _this.showEditDialog = true;
  },

}

export default PolicyPayAPI
