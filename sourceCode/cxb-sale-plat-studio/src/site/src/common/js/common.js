// import  codeNodeList  from '../../api/api';



// export function getAreaList(){
//       let para = new FormData();
//       para.append("type","BranchCode");
//       codeNodeList(para).then((res) => {
//           return res;
//       });
// };
import { codeNodeList } from 'src/api/api';
export default {
  //     getscreenwidth() {
  //         return $(window).height() - 140;
  //     },
  //     //获取地区列表
  //     getAreaList(){
  //       let para = new FormData();
  //       para.append('"type","AREACODE"');
  //       codeNodeList(list).then((res) => {
  //           return res;
  //     });
  //     }
  RemoveUrl(value, objFile) {
    console.log(value);
    console.log(objFile);
    var URL = value.split(",");
    for (var i = 0; i < URL.length; i++) {
      if (URL[i].indexOf(objFile.name) >= 0) {
        URL.splice(i, 1);
        i--;
      }
    }
    return URL.join();
  },
  SuccessUrl(value, objFile, bool) {
    var str;
    if (!value) {
      str = objFile.response[0];
    }
    else {
      if (bool) {
        str = value;
        str += ',' + objFile.response[0];
      }
      else {
        str = objFile.response[0];
      }

    }
    return str;
  },
  SuccessPlanUrl(value, objFile) {
    var str;
    for (var i = 0; i < value.length; i++) {
      if (!value) {
        str = objFile.response[0];
      }
      else {
        str = value;
        str += ',' + objFile.response[0];
      }
    }

    return str;
  },
  // 时间格式加上时分秒
  addHMS(t) {
    var date = new Date(parseInt(t)),
      hour = date.getHours(),
      minute = date.getMinutes(),
      second = date.getSeconds();
    if (t) {
      return t = t + ':' + hour + ':' + minute + ':' + second;
    }
  },

  //时间戳转格式时间
  getDaytime(nS, stu, bool) {
    if (nS == "" || nS == null) {
      return '';
    }
    else {
      var date = new Date(parseInt(nS)),
        year = date.getFullYear(),
        month = date.getMonth() + 1,
        day = date.getDate(),
        hour = date.getHours(),
        minute = date.getMinutes(),
        second = date.getSeconds();

      function lessten(e, num) {
        var str;
        if (e < 10) {
          if (num == -1) {
            if (e == 1) {
              str = 12;
            }
            else {
              str = "0" + (e - 1);
            }
          }
          else {
            str = "0" + e;
          }

        } else {
          if (num == -1) {

            str = e - 1;
          }
          else {
            str = e;
          }

        }
        return str;
      };
      var datetime;
      var daytime;
      var daydate;
      var picturedate;
      datetime = year + "-" + lessten(month) + "-" + lessten(day) + " " + lessten(hour) + ":" + lessten(minute) + ":" + lessten(second);
      daytime = year + "-" + lessten(month) + "-" + lessten(day) + " " + lessten(hour) + ":" + lessten(minute);
      daydate = year + "-" + lessten(month) + "-" + lessten(day);
      picturedate = year.toString() + (lessten(month)).toString() + (lessten(day)).toString();
      if (bool) {
        datetime = year + "-" + lessten(month, -1) + "-" + lessten(day) + " " + lessten(hour) + ":" + lessten(minute) + ":" + lessten(second);
      }
      if (stu == 0) {
        return daytime;
      }
      else if (stu == 1) {
        return datetime;
      }
      else if (stu == 2) {
        return daydate;
      }
      else if (stu == 3) {
        return picturedate;
      }
    }


  },
  //截取路径中的文件名
  interceptName(url) {
    var arr;
    arr = url.split('/');
    return arr[arr.length - 1];
  },
  //机构code
  BranchCode() {

  },
  //订单状态
  guarantStaus(value) {
    var str = '';
    if (value == '0') {
      str = '支付失败';
    }
    else if (value == '1') {
      str = '已支付';
    }
    else if (value == '2') {
      str = '未支付';
    }
    else if (value == '3') {
      str = '无效';
    }
    else if (value == '4') {
      str = '新保';
    }
    else if (value == '5') {
      str = '下发修改或拒保';
    }
    else if (value == '6') {
      str = '自动核保';
    }
    else if (value == '7') {
      str = '等待核保';
    }
    else if (value == '8') {
      str = '主动撤回';
    }
    else if (value == '9') {
      str = '查勘岗';
    }
    else if (value == '10') {
      str = '单证审核不通过';
    }
    else if (value == '11') {
      str = '待单证审核';
    }
    else if (value == '12') {
      str = '待绩效审核';
    }
    else if (value == '13') {
      str = '绩效审核不通过';
    }
    else if (value == '14') {
      str = '批退';
    }
    else if (value == '15') {
      str = '批改';
    }
    else if (value == '16') {
      str = '保单遗失';
    }
    else if (value == '17') {
      str = '保单注销';
    }
    else {
      str = '';
    }
    return str;
  },
  //支付方式
  payTypes(value) {
    var str = '';
    if (value == '1' || value == 1) {
      str = '刷卡';
    }
    else if (value == '2' || value == 2) {
      str = '网银转账';
    }
    else if (value == '3' || value == 3) {
      str = '支票';
    }
    else if (value == '4' || value == 4) {
      str = '现金';
    }
    else if (value == '5' || value == 5) {
      str = '内部转账';
    }
    else if (value == '7' || value == 7) {
      str = '在线支付';
    }
    else {
      str = value;
    }
    return str;
  },
  //险种
  CodeName(obj, code, str) {
    if (str == "RiskCode") {
      for (var i in obj.result) {
        if (parseInt(obj.result[i].codeEname1) == parseInt(code)) {
          return obj.result[i].codeType1;
        }
      }
    }
    else if (str == "BranchCode") {
      for (var i in obj.result) {
        if (parseInt(obj.result[i].code) == parseInt(code)) {
          return obj.result[i].codeCname;
        }
      }
    }
    else if (str == "ProductCompany") {
      for (var i in obj) {
        if (parseInt(obj[i].id) == parseInt(code)) {
          return obj[i].codeCname;
        }
      }
    }
    else {
      return null;
    }
  },
  //机构
  Branchs(value) {
    switch (value) {
      case "5000":
        return "重庆分公司";
        break;
      case "4400":
        return "广东分公司";
        break;
      case "3700":
        return "山东分公司";
        break;
      case "3300":
        return "浙江分公司";
        break;
      case "3399":
        return "宁波分公司";
        break;
      case "5100":
        return "四川分公司";
        break;
      case "1100":
        return "北京分公司";
        break;
      case "1301":
        return "石家庄中心支公司";
        break;
      case "0000":
        return "总公司";
        break;
      default:
        return value;
    }
  },
  //订单险种
  riskCodes(value, obj) {
    for (var i in obj) {
      if (value == obj[i].code) {
        return obj[i].codeCname;
      }
    }
    return value;
  },
  //地区
  hotArea(value, obj) {
    for (var i in obj) {
      if (value == obj[i].areaCode) {
        return obj[i].areaName;
      }
    }
    return '';
  },
  //热销产品排序
  hotSort(obj) {
    var array = [];
    for (var i in obj) {
      if (obj[i].hotFlag == true) {
        array.push(obj[i]);
      }
    }
    for (var i in obj) {
      if (obj[i].hotFlag == false) {
        array.push(obj[i]);
      }
    }
    return array;
  },

  //用户状态
  userStaus(value) {
    return value == 1 ? '无效' : '有效';
  },
  //标准时间转YM
  changeDate(time, str) {
    var d = new Date(time);
    if (time) {
      if (str == 'start') {
        return d.getFullYear() + '-' + ((d.getMonth() + 1) < 10 ? '0' + (d.getMonth() + 1) : (d.getMonth() + 1)) + '-' + (d.getDate() < 10 ? '0' + d.getDate() : d.getDate());
      }
      else {
        return d.getFullYear() + '-' + ((d.getMonth() + 1) < 10 ? '0' + (d.getMonth() + 1) : (d.getMonth() + 1)) + '-' + (d.getDate() < 10 ? '0' + d.getDate() : d.getDate());
      }
    }
    else {
      return '';
    }
  },
  //标准时间转YM
  changeTime(time, str) {
    var d = new Date(time);
    if (time) {
      if (str == 'start') {
        return d.getFullYear() + '-' + ((d.getMonth() + 1) < 10 ? '0' + (d.getMonth() + 1) : (d.getMonth() + 1)) + '-' + (d.getDate() < 10 ? '0' + d.getDate() : d.getDate()) + ' ' + "00:00:00";
      }
      else {
        return d.getFullYear() + '-' + ((d.getMonth() + 1) < 10 ? '0' + (d.getMonth() + 1) : (d.getMonth() + 1)) + '-' + (d.getDate() < 10 ? '0' + d.getDate() : d.getDate()) + ' ' + "23:59:59";
      }
    }
    else {
      return '';
    }
  },
  //标准时间断转YM   date.getHours(),minute = date.getMinutes(),second = date.getSeconds();
  changeTimes(time, str, bool) {
    var d;

    if (time[0] && time[1]) {
      if (str == 'start') {
        d = new Date(time[0])
        if (bool) {
          return d.getFullYear() + '-' + ((d.getMonth() + 1) < 10 ? '0' + (d.getMonth() + 1) : (d.getMonth() + 1)) + '-' + (d.getDate() < 10 ? '0' + d.getDate() : d.getDate()) + ' ' + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
        }
        else {
          return d.getFullYear() + '-' + ((d.getMonth() + 1) < 10 ? '0' + (d.getMonth() + 1) : (d.getMonth() + 1)) + '-' + (d.getDate() < 10 ? '0' + d.getDate() : d.getDate()) + ' ' + "00:00:00";
        }

      }
      else {
        d = new Date(time[1])
        if (bool) {
          return d.getFullYear() + '-' + ((d.getMonth() + 1) < 10 ? '0' + (d.getMonth() + 1) : (d.getMonth() + 1)) + '-' + (d.getDate() < 10 ? '0' + d.getDate() : d.getDate()) + ' ' + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
        }
        else {
          return d.getFullYear() + '-' + ((d.getMonth() + 1) < 10 ? '0' + (d.getMonth() + 1) : (d.getMonth() + 1)) + '-' + (d.getDate() < 10 ? '0' + d.getDate() : d.getDate()) + ' ' + "23:59:59";
        }
      }
    }
    else {
      return '';
    }
  },

  changeDates(time, str, bool) {
    var d;

    if (time[0] && time[1]) {
      if (str == 'start') {
        d = new Date(time[0])
        if (bool) {
          return d.getFullYear() + '-' + ((d.getMonth() + 1) < 10 ? '0' + (d.getMonth() + 1) : (d.getMonth() + 1)) + '-' + (d.getDate() < 10 ? '0' + d.getDate() : d.getDate());
        }
        else {
          return d.getFullYear() + '-' + ((d.getMonth() + 1) < 10 ? '0' + (d.getMonth() + 1) : (d.getMonth() + 1)) + '-' + (d.getDate() < 10 ? '0' + d.getDate() : d.getDate());
        }

      }
      else {
        d = new Date(time[1])
        if (bool) {
          return d.getFullYear() + '-' + ((d.getMonth() + 1) < 10 ? '0' + (d.getMonth() + 1) : (d.getMonth() + 1)) + '-' + (d.getDate() < 10 ? '0' + d.getDate() : d.getDate());
        }
        else {
          return d.getFullYear() + '-' + ((d.getMonth() + 1) < 10 ? '0' + (d.getMonth() + 1) : (d.getMonth() + 1)) + '-' + (d.getDate() < 10 ? '0' + d.getDate() : d.getDate());
        }
      }
    }
    else {
      return '';
    }
  },
  //投保，被保
  secureData() {

  },
  //是否有效
  IsValidate(value) {
    return value == "1" ? "是" : "否";
  },
  //是否首页
  IsFirst(value) {
    return value == "1" ? "是" : "否";
  },
  
  //跳转方式
  ToType(value) {
    var str = '';
    switch (value) {
      case '0':
        str = "内部跳转";
        break;
      case '1':
        str = "外部跳转";
        break;
      case '2':
        str = "电话号码";
        break;
      case '3':
        str = "内部渲染";
        break;
      case '4':
        str = "产品跳转";
        break;
      case '5':
        str = "弹出广告";
        break;
      case '6':
        str = "弹出全屏广告";
        break;
      case '7':
        str = "内置活动-红包";
        break;
    }
    return str;
  },
  //被告人证件类别
  applicantCarType(value) {
    if (value == '01') {
      return '身份证';
    }
    else if (value == '03') {
      return '护照';
    }
    else if (value == '04') {
      return '军官证';
    }
    else if (value == '99') {
      return '其他';
    }
    else {
      return '';
    }
  },
  //文章类型
  articleType(value) {
    return value == "1" ? "旅行常识" : (value == "2" ? "突发事件" : (value == "3" ? "保险常识" : "常见风险"));
  },
  //文章状态
  articleStatus(value) {
    // return value =="1"?"上架":(value =="2"?) "下架";
    var str = '';
    switch (value) {
      case '1':
        str = "上架";
        break;
      case '2':
        str = "添加待审批";
        break;
      case '3':
        str = "修改待审批";
        break;
      case '4':
        str = "修改待审批";
        break;
      case '0':
        str = "下架";
        break;
    }
    return str;

  },
  //海报类型
  posterType(value) {
    return value == "1" ? "店铺海报" : (value == "2" ? "产品海报" : "");
  },

  //店主分类
  customType(value) {
    var str = "";
    switch (value) {
      case "1":
        str = "游客";
        break;
      case "2":
        str = "普通客户";
        break;
      case "3":
        str = "专业代理";
        break;
      case "4":
        str = "商户";
        break;
      case "5":
        str = "销售人员";
        break;
    }
    return str;
  },
  //活动状态
  activityStatus(value) {
    var str = '';
    switch (value) {
      case "0":
        str = "已停止";
        break;
      case "1":
        str = "有效";
        break;
      case "2":
        str = "待审批有效";
        break;
      case "3":
        str = "待审批停止";
        break;
      case "4":
        str = "待审批预算";
        break;
      case "5":
        str = "审批未通过";
        break;
      case "6":
        str = "到期结束";
        break;
      case "7":
        str = "未及时审批";
        break;
    }
    return str;
  },
  //消费类型
  consumeStatus(value) {
    var str = '';
    switch (value) {
      case "1":
        str = "活动";
        break;
      case "2":
        str = "规则";
        break;
      case "3":
        str = "手工";
        break;
      case "4":
        str = "兑换";
        break;
      case "5":
        str = "退保";
        break;
      case "6":
        str = "过期";
        break;
      case "7":
        str = "商城退还";
        break;
      case "8":
        str = "退保差额";
        break;
      case "9":
        str = "提现";
        break;
      case "10":
        str = "扣减差额";
        break;
      case "11":
        str = "直接好友出单";
        break;
      case "12":
        str = "间接好友出单";
        break;
      case "13":
        str = "渠道出单";
        break;
      case "14":
        str = "好友签单退保";
        break;
      case "15":
        str = "间接好友签单退保";
        break;
      case "16":
        str = "渠道退保";
        break;
    }
    return str;
  },
  //提现状态
  payeeStatus(value) {
    var str = '';
    switch (value) {
      case "00":
        str = "成功";
        break;
      case "1":
        str = "申请";
        break;
      case "2":
        str = "处理中";
        break;
      case "9":
        str = "失败";
        break;
    }
    return str;
  },
  //绝对值
  abs(value) {
    return Math.abs(value);
  },
  //判断多维数组是否存在某值
  ifArrVal(arr, value) {
    for (var i = 0; i < arr.length; i++) {
      if (arr[i] instanceof Array) {
        // return ifArrVal(arr[i],value);
        for (var k = 0; k < arr[i].length; k++) {
          if (arr[i][k] instanceof Array) {

          }
          else {
            if (arr[i][k] == value) {
              return 1;//存在
            }
          }
        }
      } else {
        if (arr[i] == value) {
          return 1;//存在
        }
      }
    }
    return -1;//不存在
  },
  //遍历权限
  contains: function (arr, obj) {
    try {
      var i = arr.length;
      while (i--) {
        if (arr[i] === obj) {
          return true;
        }
      }
    }
    catch (error) {

    }
    return false;
  },
  //活动类型
  activityType(value) {
    var str = '';
    switch (value) {
      case "1":
        str = "出单活动";
        break;
      case "2":
        str = "使用推广";
        break;
      case "3":
        str = "抽奖活动";
        break;
    }
    return str;
  },
  isEmpty(obj) {
    return obj == 'null' || obj == 'undefined' || obj == undefined || obj == null || obj == '' || (Array.isArray(obj) && obj.length === 0)
      || (Object.prototype.isPrototypeOf(obj) && Object.keys(obj).length === 0);
  },
  GetByID(Data, ID) {
    var Deep, T, F;
    for (F = Data.length; F;) {
      T = Data[--F]
      if (ID === T.id) return T
      if (T.children) {
        Deep = this.GetByID(T.children, ID)
        if (Deep) return Deep
      }
    }
  },
  ApproveUser(approveAuth, userArr) {
    if (this.contains(userArr, '1')) {
      if (approveAuth == 'true') {
        return false;
      } else {
        return true;
      }
    } else {
      return true;
    }
  },
  helptext(value) {
    if (value == null || value == " " && !value == '0' || value.length == 0) {
      return '比例不能为空';
    }
    else if (!(/^-?\d+(\.\d{1,6})?$/.test(value))) {
      return '小数不能超过6位';
    }
    else if (value > 1) {
      return '比例不能大于1';
    }
    else {
      return '';
    }
  },

}