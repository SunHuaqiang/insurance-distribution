package cn.com.libertymutual.core.util;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.HashMap;
import java.util.Random;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.common.collect.Maps;

import cn.com.libertymutual.core.redis.util.RedisUtils;

/**

@ClassName
:CaptchaUtil*@Description
:关于图片验证码的工具类*/
public final class CaptchaUtil {

	private CaptchaUtil() {
	}

	/* 随机字符字典 */
	private static final char[] CHARS = { '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'J', 'K', 'L', 'M', 'N',
			'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z' };

	/* 随机数 */
	private static Random random = new Random();
	private static int width = 80;// 验证码图片 宽
	private static int height = 30;// 高
	private static int randomsize = 4; // 随机数长度

	/* 获取4位随机数 */
	private static String getRandomString() {
		StringBuffer buffer = new StringBuffer();
		for (int i = 0; i < randomsize; i++) {
			buffer.append(CHARS[random.nextInt(CHARS.length)]);
		}
		return buffer.toString();
	}

	/* 获取随机数颜色 */
	private static Color getRandomColor() {
		return new Color(random.nextInt(255), random.nextInt(255), random.nextInt(255));
	}

	/* 返回某颜色的反色 */
	private static Color getReverseColor(Color c) {
		return new Color(255 - c.getRed(), 255 - c.getGreen(), 255 - c.getBlue());
	}

	/**Remarks: 输出图形验证码图像<br>
	* Version：1.0<br>
	* Author：AoYi<br>
	* DateTime：2017年10月8日下午3:04:32<br>
	* Project：liberty_sale_plat<br>
	* @param request
	* @param response
	* @param redis
	* @throws ServletException
	* @throws IOException
	*/
	public static void outputCaptcha(HttpServletRequest request, HttpServletResponse response, RedisUtils redis)
			throws ServletException, IOException {
		response.setContentType("image/jpeg");

		String randomString = getRandomString();
		// 存储到缓存
		HashMap<String, String> captcha = Maps.newHashMap();
		captcha.put("code", randomString);
		captcha.put("time", DateUtil.getStringDate2());// yyyy-mm-dd hh:mm:ss.sss

		// 获取缓存中的图形码
		Object object = redis.get(request.getSession().getId(), Constants.REDIS_IMG_VERIFY_CODE);
		if (object != null) {
			// 状态更新后清除图形码
			redis.deleteWithPrefix(request.getSession().getId(), Constants.REDIS_IMG_VERIFY_CODE);
		}
		redis.setWithExpireTime(request.getSession().getId(), Constants.REDIS_IMG_VERIFY_CODE, captcha, Constants.IMG_VALIDATE_OUTTIME);
		// 获取缓存中的图形码
		object = redis.get(request.getSession().getId(), Constants.REDIS_IMG_VERIFY_CODE);
		System.out.println("图形码=" + object);

		Color color = getRandomColor();
		Color reverse = getReverseColor(color);

		BufferedImage bi = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
		Graphics2D g = bi.createGraphics();
		g.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 16));
		g.setColor(color);
		g.fillRect(0, 0, width, height);
		g.setColor(reverse);
		g.drawString(randomString, 18, 20);
		for (int i = 0, n = random.nextInt(100); i < n; i++) {
			g.drawRect(random.nextInt(width), random.nextInt(height), 1, 1);
		}

		// 转成JPEG格式
		ServletOutputStream out = response.getOutputStream();

		ImageIO.write(bi, "jpg", out);
		out.close();
		bi.flush();

		/*
		 * JPEGImageEncoder encoder = JPEGCodec.createJPEGEncoder(out);
		 * encoder.encode(bi); out.flush();
		 */
	}
}