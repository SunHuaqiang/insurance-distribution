package cn.com.libertymutual.core.security.token;

import cn.com.libertymutual.core.exception.AppException;

public interface IFrontSecurityCheck {

	public void checkToken() throws AppException;

}
