package cn.com.libertymutual.core.exception;

import com.google.common.base.Strings;


/**
 * 	参数类型不正确
 * 
 *  * @author 2016-05-19 2:31 zhaoyu
 *
 */

public class ParameterTypeErrorException extends RuntimeException {

	


	/**
	 * 
	 */
	private static final long serialVersionUID = -7655093853930334973L;
	/**
	 * 
	 */
	
	private int errorCode = 3003;
	private String errorMessage;
	
	//添加枚举类型的属性  author:tracy.liao date:2016-4-27
	private ExceptionEnums exceptionEnums;
	
	public ParameterTypeErrorException() {
		super();
	}
	
	public ParameterTypeErrorException( Throwable e ) {
		super( e );
	}
	

	public ParameterTypeErrorException( String message ) {
		super( message );
		errorMessage = message;
	}
	public ParameterTypeErrorException( String message, Throwable e ) {
		super( message, e );
		errorMessage = message;
	}

	public ParameterTypeErrorException( int errorCode, String message ) {
		super(message);
		this.errorCode = errorCode;
		this.errorMessage = message;
	}
	public ParameterTypeErrorException( int errorCode, String message, Throwable e ) {
		super(message, e);
		this.errorCode = errorCode;
		this.errorMessage = message;
	}
	
	/**
	 * 添加枚举类型构造方法
	 * @author tracy.liao
	 * @date 2016-4-27
	 * @param exceptionEnums
	 * @param message
	 */
	public ParameterTypeErrorException(ExceptionEnums exceptionEnums, String message){
		super(message);
		this.exceptionEnums=exceptionEnums;
		this.errorMessage=message;
	}

	/**
	 * @return the exceptionEnums
	 */
	public ExceptionEnums getExceptionEnums() {
		return exceptionEnums;
	}

	/**
	 * @param exceptionEnums the exceptionEnums to set
	 */
	public void setExceptionEnums(ExceptionEnums exceptionEnums) {
		this.exceptionEnums = exceptionEnums;
	}

	public int getErrorCode() {
		return errorCode;
	}
	public String getErrorMessage() {
		return errorMessage;
	}
	
	/**获取错误码和错误信息*/
	public String getErrorCodeAndMessage(){
		return String.format("%d-%s", errorCode, Strings.isNullOrEmpty(errorMessage) ? "系统异常，如果多次出现，请联系管理员" : errorMessage );
	}
}
