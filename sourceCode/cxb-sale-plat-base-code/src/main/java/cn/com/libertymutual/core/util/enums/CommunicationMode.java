package cn.com.libertymutual.core.util.enums;

public enum CommunicationMode {
	SYNC,
	ASYNC,
	ONEWAY
}
