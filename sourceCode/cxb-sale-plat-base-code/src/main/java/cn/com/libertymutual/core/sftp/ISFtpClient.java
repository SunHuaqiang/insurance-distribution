package cn.com.libertymutual.core.sftp;

import java.io.InputStream;

import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;

public interface ISFtpClient {
	public ChannelSftp connect() throws JSchException, SftpException;
	
	public boolean putFile( String fileName, String dirs ) throws Exception;
	
	public boolean putFile( String fileName ) throws Exception;
	
	public boolean putFile( InputStream src, String fileName, String dirs ) throws Exception;
	
	public boolean putFile( InputStream src, String fileName ) throws Exception;
}
