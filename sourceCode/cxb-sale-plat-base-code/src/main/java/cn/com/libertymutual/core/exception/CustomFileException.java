package cn.com.libertymutual.core.exception;

import java.io.EOFException;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.xml.bind.JAXBException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactoryConfigurationError;

import com.google.common.base.Strings;


/**
 * 	自定义的文件操作的异常
 * 
 *  @author 2016-05-23 12:07 zhaoyu
 *
 *
 *	@version 1.0
 */

public class CustomFileException extends RuntimeException {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7535257129022457899L;
	private int errorCode = 9997;
	private String errorMessage;
	
	//添加枚举类型的属性  author:tracy.liao date:2016-4-27
	private ExceptionEnums exceptionEnums;
	
	public CustomFileException() {
		super();
	}
	
	public CustomFileException( Throwable e ) {
		//如果是他自己
		if(e instanceof CustomFileException){
			this.errorCode = ((CustomFileException) e).getErrorCode();
			this.errorMessage = ((CustomFileException) e).getErrorMessage();
		// 文件已结束异常
		}else if(e instanceof EOFException){
			this.errorCode = LangExceptionEnums.EOFException_KEY.getCode();
			this.errorMessage =  LangExceptionEnums.EOFException_KEY.getMessage();
		//文件未找到异常
		}else if(e instanceof FileNotFoundException){
			this.errorCode = LangExceptionEnums.FileNotFoundException_KEY.getCode();
			this.errorMessage =  LangExceptionEnums.FileNotFoundException_KEY.getMessage();
		//输入输出异常
		}else if(e instanceof IOException){
			this.errorCode = LangExceptionEnums.IOException_KEY.getCode();
				this.errorMessage =  LangExceptionEnums.IOException_KEY.getMessage();
		//解析xml错误
		}else if(e instanceof JAXBException||e instanceof TransformerException){
			this.errorCode = LangExceptionEnums.JAXB_EXCEPTION.getCode();
			this.errorMessage =  LangExceptionEnums.JAXB_EXCEPTION.getMessage();
		//文件上传错误	
		/*}else if(e instanceof FileUploadException){
			this.errorCode = LangExceptionEnums.FILE_UPLOAD_ERROR.getCode();
			this.errorMessage =  LangExceptionEnums.FILE_UPLOAD_ERROR.getMessage();*/
		//xml映射失败
		/*}else if(e instanceof XmlMappingException){
			this.errorCode = LangExceptionEnums.XML_MAPPING_EXCEPTION.getCode();
			this.errorMessage =  LangExceptionEnums.XML_MAPPING_EXCEPTION.getMessage();
		//配置文件发生严重错误
		}else if(e instanceof XmlMappingException||e instanceof TransformerFactoryConfigurationError){
			this.errorCode = LangExceptionEnums.TRANSFORMER_CONFIGURATION_EXCEPTION.getCode();
			this.errorMessage =  LangExceptionEnums.TRANSFORMER_CONFIGURATION_EXCEPTION.getMessage();
			
		//根异常。用以描述应用程序希望捕获的情况
		}else if(e instanceof WebServiceIOException){
			this.errorCode = LangExceptionEnums.WEBSERVICE_IOEXCEPTION.getCode();
			this.errorMessage =  LangExceptionEnums.WEBSERVICE_IOEXCEPTION.getMessage();
		*/	
		//根异常。用以描述应用程序希望捕获的情况
		}else{
			this.errorCode = LangExceptionEnums.Exception_KEY.getCode();
			this.errorMessage =  LangExceptionEnums.Exception_KEY.getMessage();
		}

	}
	

	public CustomFileException( String message ) {
		super( message );
		errorMessage = message;
	}
	public CustomFileException( String message, Throwable e ) {
		super( message, e );
		errorMessage = message;
	}

	public CustomFileException( int errorCode, String message ) {
		super(message);
		this.errorCode = errorCode;
		this.errorMessage = message;
	}
	public CustomFileException( int errorCode, String message, Throwable e ) {
		super(message, e);
		this.errorCode = errorCode;
		this.errorMessage = message;
	}
	
	/**
	 * 添加枚举类型构造方法
	 * @author tracy.liao
	 * @date 2016-4-27
	 * @param exceptionEnums
	 * @param message
	 */
	public CustomFileException(ExceptionEnums exceptionEnums, String message){
		super(message);
		this.exceptionEnums=exceptionEnums;
		this.errorMessage=message;
	}

	/**
	 * @return the exceptionEnums
	 */
	public ExceptionEnums getExceptionEnums() {
		return exceptionEnums;
	}

	/**
	 * @param exceptionEnums the exceptionEnums to set
	 */
	public void setExceptionEnums(ExceptionEnums exceptionEnums) {
		this.exceptionEnums = exceptionEnums;
	}

	public int getErrorCode() {
		return errorCode;
	}
	public String getErrorMessage() {
		return errorMessage;
	}
	
	/**获取错误码和错误信息*/
	public String getErrorCodeAndMessage(){
		
		return String.format("%d-%s", errorCode, Strings.isNullOrEmpty(errorMessage) ? "系统异常，如果多次出现，请联系管理员" : errorMessage );
	}
	
}
