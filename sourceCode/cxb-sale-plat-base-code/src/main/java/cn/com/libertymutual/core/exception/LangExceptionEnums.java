/*
 *	Copyright (c) liberty 2016
 *	cn.com.libertymutual.insure.exception
 *  Create Date 2016年4月27日
 *	@author tracy.liao
 */
package cn.com.libertymutual.core.exception;

/**
 * 通用程序异常类
 * @author tracy.liao
 * @date 2016年4月27日
 */
public enum LangExceptionEnums implements ExceptionEnums {

	SECURITY_TOKEN_EXCEPTION(110, "非法访问，利宝保险保留法律追究的权利"),
	// 空指针异常类：NullPointerException
	NullPointerException_KEY(2001, "系统处理数据异常,无法对空数据进行处理"),
	// 方法的参数错误 IllegalArgumentException
	IllegalArgumentException_KEY(2002, "方法的参数错误"),
	// 数组负下标异常：NegativeArrayException
	NegativeArray_KEY(2003, "数组负下标异常"),
	// 文件已结束异常：EOFException
	EOFException_KEY(2004, "文件已结束异常"),
	// 文件未找到异常：FileNotFoundException
	FileNotFoundException_KEY(2005, "文件未找到异常"),
	// 操作数据库异常：SQLException
	SQLException_KEY(2006, "操作数据库异常"),
	// 输入输出异常：IOException
	IOException_KEY(2007, "输入输出异常"),
	// 抽象方法错误：AbstractMethodError
	AbstractMethodError_KEY(2008, "抽象方法错误"),
	// 断言错,用来指示一个断言失败的情况：AssertionError
	AssertionError_KEY(2009, "断言错"),
	// 类循环依赖错误。在初始化一个类时，若检测到类之间循环依赖则抛出该异常。：java.lang.ClassCircularityError
	ClassCircularityError_KEY(2010, "类循环依赖错误"),
	// 类格式错误。当Java虚拟机试图从一个文件中读取Java类，而检测到该文件的内容不符合类的有效格式时抛出：ClassFormatError
	ClassFormatError_KEY(2011, "类格式错误"),
	// 错误：java.lang.Error
	Error_KEY(2012, "错误"),
	// 初始化程序错误。当执行一个类的静态初始化程序的过程中，发生了异常时抛出。静态初始化程序是指直接包含于类中的static语句段。：java.lang.ExceptionInInitializerError
	ExceptionInInitializerError_KEY(2013, "初始化程序错误"),
	// 违法访问错误。当一个应用试图访问、修改某个类的域（Field）或者调用其方法，但是又违反域或方法的可见性声明，则抛出该异常：IllegalAccessError
	IllegalAccessError_KEY(2014, "违法访问错误"),
	// 不兼容的类变化错误：IncompatibleClassChangeError
	IncompatibleClassChangeError_KEY(2015, "不兼容的类变化错误"),
	// 实例化错误。当一个应用试图通过Java的new操作符构造一个抽象类或者接口时抛出该异常：InstantiationError
	InstantiationError_KEY(2016, "实例化错误"),
	// 内部错误。用于指示Java虚拟机发生了内部错误：InternalError
	InternalError_KEY(2017, "内部错误"),
	// 链接错误。该错误及其所有子类指示某个类依赖于另外一些类，在该类编译之后，被依赖的类改变了其类定义而没有重新编译所有的类，进而引发错误的情况：LinkageError
	LinkageError_KEY(2018, "链接错误"),
	// 未找到类定义错误。当Java虚拟机或者类装载器试图实例化某个类，而找不到该类的定义时抛出该错误：NoClassDefFoundError
	NoClassDefFoundError_KEY(2019, "未找到类定义错误"),
	// 域不存在错误。当应用试图访问或者修改某类的某个域，而该类的定义中没有该域的定义时抛出该错误：NoSuchFieldError
	NoSuchFieldError_KEY(2020, "域不存在错误"),
	// 内存不足错误。当可用内存不足以让Java虚拟机分配给一个对象时抛出该错误：OutOfMemoryError
	OutOfMemoryError_KEY(2021, "内存不足错误"),
	// 堆栈溢出错误。当一个应用递归调用的层次太深而导致堆栈溢出时抛出该错误：StackOverflowError
	StackOverflowError_KEY(2022, "堆栈溢出错误"),
	// 线程结束。当调用Thread类的stop方法时抛出该错误，用于指示线程结束：ThreadDeath
	ThreadDeath_KEY(2023, "线程结束"),
	// 未知错误。用于指示Java虚拟机发生了未知严重错误的情况：UnknownError
	UnknownError_KEY(2024, "未知错误"),
    // 未满足的链接错误。当Java虚拟机未找到某个类的声明为native方法的本机语言定义时抛出：UnsatisfiedLinkError
	UnsatisfiedLinkError_KEY(2025, "未满足的链接错误"),
    // 不支持的类版本错误。当Java虚拟机试图从读取某个类文件，但是发现该文件的主、次版本号不被当前Java虚拟机支持的时候，抛出该错误：UnsupportedClassVersionError
	UnsupportedClassVersionError_KEY(2026, "不支持的类版本错误"),
	// 验证错误。当验证器检测到某个类文件中存在内部不兼容或者安全问题时抛出该错误： VerifyError
	VerifyError_KEY(2027, "验证错误"),
	// 虚拟机错误。用于指示虚拟机被破坏或者继续执行操作所需的资源不足的情况： VirtualMachineError
	VirtualMachineError_KEY(2028, "虚拟机错误"),
	// 算术条件异常。譬如：整数除零等： ArithmeticException
	ArithmeticException_KEY(2029, "算术条件异常，整数除零或商有无限小数等"),
	// 数组索引越界异常。当对数组的索引值为负数或大于等于数组大小时抛出： ArrayIndexOutOfBoundsException
	ArrayIndexOutOfBoundsException_KEY(2030, "数组索引越界异常"),
	// 数组存储异常。当向数组中存放非数组声明类型对象时抛出：ArrayStoreException
	ArrayStoreException_KEY(2031, "数组存储异常"),
	// 类造型异常。假设有类A和B（A不是B的父类或子类），O是A的实例，那么当强制将O构造为类B的实例时抛出该异常。该异常经常被称为强制类型转换异常：
	// ClassCastException
	ClassCastException_KEY(2032, "类造型异常"),
	// 找不到类异常。当应用试图根据字符串形式的类名构造类，而在遍历CLASSPAH之后找不到对应名称的class文件时，抛出该异常：ClassNotFoundException
	ClassNotFoundException_KEY(2033, "找不到类异常"),
	// 不支持克隆异常。当没有实现Cloneable接口或者不支持克隆方法时,调用其clone()方法则抛出： CloneNotSupportedException
	CloneNotSupportedException_KEY(2034, "不支持克隆异常"),
	// 枚举常量不存在异常。当应用试图通过名称和枚举类型访问一个枚举对象，但该枚举对象并不包含常量时，抛出该异常：EnumConstantNotPresentException
	EnumConstantNotPresentException_KEY(2035, "枚举常量不存在异常"),
	// 根异常。用以描述应用程序希望捕获的情况：Exception
	Exception_KEY(2036, "后台错误，如果连续出现,请联系管理员"),
	// 违法的访问异常。当应用试图通过反射方式创建某个类的实例、访问该类属性、调用该类方法，而当时又无法访问类的、属性的、方法的或构造方法的定义时抛出该异常：IllegalAccessException
	IllegalAccessException_KEY(2037, "违法的访问异常"),
	// 违法的监控状态异常。当某个线程试图等待一个自己并不拥有的对象（O）的监控器或者通知其他线程等待该对象（O）的监控器时，抛出该异常：IllegalMonitorStateException
	IllegalMonitorStateException_KEY(2038, "违法的监控状态异常"),
	// 违法的状态异常。当在Java环境和应用尚未处于某个方法的合法调用状态，而调用了该方法时，抛出该异常： IllegalStateException
	IllegalStateException_KEY(2039, "违法的状态异常"),
	// 违法的线程状态异常。当县城尚未处于某个方法的合法调用状态，而调用了该方法时，抛出异：IllegalThreadStateException
	IllegalThreadStateException_KEY(2040, "违法的线程状态异常"),
	// 索引越界异常。当访问某个序列的索引值小于0或大于等于序列大小时，抛出该异常： IndexOutOfBoundsException
	IndexOutOfBoundsException_KEY(2041, "索引越界异常"),
	// 实例化异常。当试图通过newInstance()方法创建某个类的实例，而该类是一个抽象类或接口时，抛出该异常：InstantiationException
	InstantiationException_KEY(2042, "实例化异常"),
	// 被中止异常。当某个线程处于长时间的等待、休眠或其他暂停状态，而此时其他的线程通过Thread的interrupt方法终止该线程时抛出该异常：InterruptedException
	InterruptedException_KEY(2043, "被中止异常"),
	// 数组大小为负值异常。当使用负数大小值创建数组时抛出该异常：NegativeArraySizeException
	NegativeArraySizeException_KEY(2044, "数组大小为负值异常"),
	// 属性不存在异常。当访问某个类的不存在的属性时抛出该异常： NoSuchFieldException
	NoSuchFieldException_KEY(2045, "属性不存在异常"),
//	// 方法不存在异常。当访问某个类的不存在的方法时抛出该异常： NoSuchMethodException
	NoSuchMethodException_KEY(2046, "方法不存在异常"),
	// 数字格式异常。当试图将一个String转换为指定的数字类型，而该字符串确不满足数字类型要求的格式时，抛出该异常：NumberFormatException
	NumberFormatException_KEY(2047, "数字格式异常"),
	// 运行时异常。是所有Java虚拟机正常操作期间可以被抛出的异常的父类：RuntimeException
	RuntimeException_KEY(2048, "运行时异常"),
	// 安全异常。由安全管理器抛出，用于指示违反安全情况的异常： SecurityException
	SecurityException_KEY(2049, "安全异常"),
	// 字符串索引越界异常。当使用索引值访问某个字符串中的字符，而该索引值小于0或大于等于序列大小时，抛出该异常：StringIndexOutOfBoundsException
	StringIndexOutOfBoundsException_KEY(2050, "字符串索引越界异常"),
	// 类型不存在异常： TypeNotPresentException
	TypeNotPresentException_KEY(2051, "类型不存在异常"),
	// 不支持的方法异常。指明请求的方法不被支持情况的异常： UnsupportedOperationException
	UnsupportedOperationException_KEY(2052, "不支持的方法异常"),
	//redis异常
	REDIS_EXCEPTION_KEY(2053,"Redis异常"),
	//xml解析出错JDOMException
	XML_EXCEPTION_KEY(2054,"xml解析出错"),
	//json转换失败JsonGenerationException,JsonMappingException
	JSON_EXCEPTION_KEY(2055,"json转换出错"),
	//编码出错:UnsupportedEncodingException
	UNSUPPORTEDENCODINGEXCEPTION_KEY(2056,"编码出错"),
	//邮件异常
	MESSAGING_EXCEPTION(2057,"邮件异常"),
	//不支持编码异常
	UNSUPPORTED_ENCODING_EXCEPTION(2058,"不支持的编码异常"),
	//jaxbException:解析xml错误
	JAXB_EXCEPTION(2059, "解析XML错误!"),
	//jsonException:json格式化错误
	JSON_EXCEPTION(2060, "JSON格式化错误!"),
	//xmlMappingException:xml映射失败
	XML_MAPPING_EXCEPTION(2061,"XML映射失败!"),
	//transformerConfigurationException:java配置发生严重错误!
	TRANSFORMER_CONFIGURATION_EXCEPTION(2062,"java配置发生严重错误!"),
	//fileUploadError:文档上传错误!
	FILE_UPLOAD_ERROR(2063,"文档上传错误!"),
	WEBSERVICE_IOEXCEPTION(2064,"核心接口网络错误，或是接口升级!"),
	WEBSERVICE_SOAP_IOEXCEPTION(2065,"核心接口错误，请联系管理员处理。"),
	SERVICE_TIMEOUT_EXCEPTION(2066,"Service请求超时!");
	

	public int code;
	public String message;

	LangExceptionEnums(int code, String message) {
		this.code = code;
		this.message = message;
	}

	@Override
	public int getCode() {
		// TODO Auto-generated method stub
		return code;
	}

	@Override
	public String getMessage() {
		// TODO Auto-generated method stub
		return message;
	}
}
