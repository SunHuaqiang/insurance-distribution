/*
 *	Copyright (c) liberty 2016
 *	cn.com.libertymutual.insure.exception
 *  Create Date 2016年4月26日
 *	@author tracy.liao
 */
package cn.com.libertymutual.core.exception;


/**
 * hibernate框架抛出的异常
 * @author zhaoyu
 * @date 2016-05-18 15:26
 */

public enum HibernateExceptionEnums implements ExceptionEnums {
	//实体被锁定:LockingStrategyException
	LOCKING_STRATEGY_EXCEPTION_KEY(5001,"实体被锁定"),
	//关联查询的两个表有相同的列名:NonUniqueDiscoveredSqlAliasException
	NONUNIQUE_DISCOVERED_SQL_ALIAS_EXCEPTION_KEY(5002,"关联查询的两个表有相同的列名"),
	//重复的主键:NonUniqueResultException
	NONUNIQUE_RESULT_EXCEPTION_KEY(5003,"重复的主键"),
	//某个字段自动生成,无法设置值:PersistentObjectException
	PERSISTENT_OBJECT_EXCEPTION_KEY(5004,"某个字段自动生成,无法设置值"),
	//属性和数据关联失败,字段值不合法:PropertyAccessException,PropertyValueException
	PROPERTY_ACCESS_OR_VALUE_EXCEPTION_KEY(5005,"属性和数据关联失败,字段值不合法"),
	//查询语句出错:QueryException
	QUERY_EXCEPTION_KEY(5006,"查询语句出错"),
	//事物提交失败:BatchFailedException
	BATCH_FAILED_EXCEPTION_KEY(5007,"事物提交失败"),
	//回滚事物失败:CallbackException
	CALL_BACK_EXCEPTION_KEY(5008,"回滚事物失败"),
	//jdbc驱动错误或配置异常:ServiceException
	SERVICE_EXCEPTION_KEY(5009,"jdbc驱动错误或配置异常"),
	//session异常:SessionException
	SESSION_EXCEPTION_KEY(5010,"session异常"),
	//开启事物异常:TransactionException
	TRANSACTION_EXCEPTION_KEY(5011,"开启事物异常"),
	//hibernate其他异常:HibernateException
	HIBERNATE_EXCEPTION_KEY(5012,"hibernate异常");
	
	public int code;
	public String message;

	HibernateExceptionEnums(int code, String message) {
		this.code = code;
		this.message = message;
	}

	@Override
	public int getCode() {
		// TODO Auto-generated method stub
		return code;
	}

	@Override
	public String getMessage() {
		// TODO Auto-generated method stub
		return message;
	}

}
